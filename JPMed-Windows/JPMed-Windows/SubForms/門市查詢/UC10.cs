﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows.Forms;
using JPMed_Windows.Utility;
using log4net;

namespace JPMed_Windows.SubForms.門市查詢
{
    public partial class UC10 : UserControl
    {
        string formName = "門市前台作業系統";
        static string storeCode = Properties.Settings.Default.WerksCode;
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC10));
        DataTable defaultDT = null;
        SQLCreator sql;
        DBConnection dbConn;

        public UC10()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            defaultControl(true);
            IntitalControl();
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void defaultControl(bool first)
        {
            laSCode.Text = storeCode;

            if (first)
            {
                laSName.Text = string.Empty;
                tbEAN.Text = string.Empty;
            }
            
            laStockCode.Text = string.Empty;
            laStockName.Text = string.Empty;

            cb0.Enabled = false;
            cb0.Checked = false;

            cb1.Enabled = false;
            cb1.Checked = false;

            cb2.Enabled = false;
            cb2.Checked = false;

            laWarm.Visible = false;

            dgList.DataSource = defaultDT;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC10");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                string sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉[庫存查詢]功能?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls[1].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                laWarm.Visible = false;
                string prdCode = tbEAN.Text.ToUpper();
                if (string.IsNullOrEmpty(prdCode))
                {
                    defaultControl(false);
                    MessageBox.Show("請輸入商品條碼(EAN)");
                }
                else
                {
                    
                    bool loop = true;
                    while (loop)
                    {
                        if (prdCode.StartsWith("0") && prdCode.Length > 0)
                            prdCode = prdCode.Substring(1);
                        else
                        {
                            loop = false;
                            if (prdCode.Length == 0)
                            {
                                defaultControl(false);
                                MessageBox.Show("請輸入有效之商品條碼(EAN)");
                                return;
                            }
                            else tbEAN.Text = prdCode;
                        }
                    }

                    string sqlCmd = sql.GetCommand("GetAssortInformation", new string[] { storeCode, prdCode });
                    DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (tmpDT.Rows.Count == 0)
                    {
                        defaultControl(false);
                        MessageBox.Show("查無相關產品資訊，請重新輸入");
                    }
                    else
                    {
                        //去除前方0
                        string tmpStockCode = tmpDT.Rows[0]["MATNR"].ToString();
                        loop = true;
                        while (loop)
                        {
                            if (tmpStockCode.StartsWith("0") && tmpStockCode.Length > 0)
                                tmpStockCode = tmpStockCode.Substring(1);
                            else
                            {
                                loop = false;
                                laStockCode.Text = tmpStockCode;
                            }
                        }
                        
                        laStockName.Text = tmpDT.Rows[0]["MAKTX"].ToString();

                        cb0.Checked = (tmpDT.Rows[0]["BWSCL"].ToString() == "4" || string.IsNullOrEmpty(tmpDT.Rows[0]["BWSCL"].ToString()) || tmpDT.Rows[0]["BWSCL"].ToString() == " ");
                        cb1.Checked = (tmpDT.Rows[0]["LABOR"].ToString() == "002");
                        cb2.Checked = (tmpDT.Rows[0]["RUECK"].ToString() == "04");
                        laWarm.Visible = (tmpDT.Rows[0]["DELFG"].ToString() == "X");

                        string p_spbup = string.Empty;
                        int term = PublicClass.GetIso8601WeekOfYear(DateTime.Now) - 1;
                        if (term == 0)
                        {
                            p_spbup = new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1).Year.ToString() +
                                      PublicClass.GetIso8601WeekOfYear(new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1)).ToString("00");
                        }
                        else
                        {
                            p_spbup = DateTime.Today.Year.ToString() + term.ToString("00");
                        }

                        sqlCmd = sql.GetCommand("GetPrdSalesRecord", new string[] { prdCode, p_spbup });
                        tmpDT = dbConn.GetDataTable(sqlCmd);
                        if (tmpDT.Rows.Count > 0)
                        {
                            dgList.DataSource = tmpDT;
                            defaultDT = tmpDT.Clone();
                        }
                        else
                            dgList.DataSource = defaultDT;
                    }
                }

                tbEAN.Focus();
                tbEAN.Select(0, tbEAN.Text.Length);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                MessageBox.Show("系統執行發生錯誤: " + ex.Message);
                btnExit_Click(null, null);
            }
        }
    }
}
