﻿namespace JPMed_Windows.SubForms.門市查詢
{
    partial class UC10
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.la1 = new System.Windows.Forms.Label();
            this.la2 = new System.Windows.Forms.Label();
            this.la3 = new System.Windows.Forms.Label();
            this.la4 = new System.Windows.Forms.Label();
            this.la5 = new System.Windows.Forms.Label();
            this.la6 = new System.Windows.Forms.Label();
            this.cb0 = new System.Windows.Forms.CheckBox();
            this.cb1 = new System.Windows.Forms.CheckBox();
            this.cb2 = new System.Windows.Forms.CheckBox();
            this.dgList = new System.Windows.Forms.DataGridView();
            this.門市總倉 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.名稱 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.庫存量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.前週銷量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laSCode = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.tbEAN = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.laStockCode = new System.Windows.Forms.Label();
            this.laStockName = new System.Windows.Forms.Label();
            this.laWarm = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(13, 11);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 0;
            this.la1.Text = "所在門市：";
            // 
            // la2
            // 
            this.la2.AutoSize = true;
            this.la2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la2.Location = new System.Drawing.Point(13, 46);
            this.la2.Name = "la2";
            this.la2.Size = new System.Drawing.Size(88, 16);
            this.la2.TabIndex = 1;
            this.la2.Text = "查詢條件：";
            // 
            // la3
            // 
            this.la3.AutoSize = true;
            this.la3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la3.Location = new System.Drawing.Point(101, 46);
            this.la3.Name = "la3";
            this.la3.Size = new System.Drawing.Size(117, 16);
            this.la3.TabIndex = 2;
            this.la3.Text = "商品條碼 (EAN)";
            // 
            // la4
            // 
            this.la4.AutoSize = true;
            this.la4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la4.Location = new System.Drawing.Point(13, 79);
            this.la4.Name = "la4";
            this.la4.Size = new System.Drawing.Size(88, 16);
            this.la4.TabIndex = 3;
            this.la4.Text = "查詢結果：";
            // 
            // la5
            // 
            this.la5.AutoSize = true;
            this.la5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la5.Location = new System.Drawing.Point(101, 79);
            this.la5.Name = "la5";
            this.la5.Size = new System.Drawing.Size(115, 16);
            this.la5.TabIndex = 4;
            this.la5.Text = "SAP物料編碼：";
            // 
            // la6
            // 
            this.la6.AutoSize = true;
            this.la6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la6.Location = new System.Drawing.Point(101, 105);
            this.la6.Name = "la6";
            this.la6.Size = new System.Drawing.Size(56, 16);
            this.la6.TabIndex = 5;
            this.la6.Text = "品名：";
            // 
            // cb0
            // 
            this.cb0.AutoSize = true;
            this.cb0.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cb0.Location = new System.Drawing.Point(103, 133);
            this.cb0.Name = "cb0";
            this.cb0.Size = new System.Drawing.Size(75, 20);
            this.cb0.TabIndex = 6;
            this.cb0.Text = "進倉貨";
            this.cb0.UseVisualStyleBackColor = true;
            // 
            // cb1
            // 
            this.cb1.AutoSize = true;
            this.cb1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cb1.Location = new System.Drawing.Point(195, 133);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(75, 20);
            this.cb1.TabIndex = 7;
            this.cb1.Text = "廠商貨";
            this.cb1.UseVisualStyleBackColor = true;
            // 
            // cb2
            // 
            this.cb2.AutoSize = true;
            this.cb2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cb2.Location = new System.Drawing.Point(291, 133);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(75, 20);
            this.cb2.TabIndex = 8;
            this.cb2.Text = "買斷貨";
            this.cb2.UseVisualStyleBackColor = true;
            // 
            // dgList
            // 
            this.dgList.AllowUserToAddRows = false;
            this.dgList.AllowUserToDeleteRows = false;
            this.dgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.門市總倉,
            this.名稱,
            this.庫存量,
            this.前週銷量});
            this.dgList.Location = new System.Drawing.Point(16, 172);
            this.dgList.Name = "dgList";
            this.dgList.ReadOnly = true;
            this.dgList.RowTemplate.Height = 24;
            this.dgList.Size = new System.Drawing.Size(737, 341);
            this.dgList.TabIndex = 9;
            // 
            // 門市總倉
            // 
            this.門市總倉.DataPropertyName = "門市/總倉";
            this.門市總倉.HeaderText = "門市/總倉";
            this.門市總倉.Name = "門市總倉";
            this.門市總倉.ReadOnly = true;
            // 
            // 名稱
            // 
            this.名稱.DataPropertyName = "名稱";
            this.名稱.HeaderText = "名稱";
            this.名稱.Name = "名稱";
            this.名稱.ReadOnly = true;
            // 
            // 庫存量
            // 
            this.庫存量.DataPropertyName = "庫存量";
            dataGridViewCellStyle1.Format = "0.###";
            this.庫存量.DefaultCellStyle = dataGridViewCellStyle1;
            this.庫存量.HeaderText = "庫存量";
            this.庫存量.Name = "庫存量";
            this.庫存量.ReadOnly = true;
            // 
            // 前週銷量
            // 
            this.前週銷量.DataPropertyName = "前週銷售量";
            dataGridViewCellStyle2.Format = "0.###";
            this.前週銷量.DefaultCellStyle = dataGridViewCellStyle2;
            this.前週銷量.HeaderText = "前週銷量";
            this.前週銷量.Name = "前週銷量";
            this.前週銷量.ReadOnly = true;
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(101, 11);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 10;
            this.laSCode.Text = "T001";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(154, 11);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 11;
            this.laSName.Text = "士林門市";
            // 
            // tbEAN
            // 
            this.tbEAN.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEAN.Location = new System.Drawing.Point(230, 43);
            this.tbEAN.MaxLength = 18;
            this.tbEAN.Name = "tbEAN";
            this.tbEAN.Size = new System.Drawing.Size(170, 27);
            this.tbEAN.TabIndex = 12;
            this.tbEAN.Text = "A01234567890123456";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(500, 43);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 27);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(419, 43);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 27);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // laStockCode
            // 
            this.laStockCode.AutoSize = true;
            this.laStockCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laStockCode.Location = new System.Drawing.Point(222, 79);
            this.laStockCode.Name = "laStockCode";
            this.laStockCode.Size = new System.Drawing.Size(155, 16);
            this.laStockCode.TabIndex = 15;
            this.laStockCode.Text = "A01234567890123456";
            // 
            // laStockName
            // 
            this.laStockName.AutoSize = true;
            this.laStockName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laStockName.Location = new System.Drawing.Point(163, 105);
            this.laStockName.Name = "laStockName";
            this.laStockName.Size = new System.Drawing.Size(72, 16);
            this.laStockName.TabIndex = 16;
            this.laStockName.Text = "測試商品";
            // 
            // laWarm
            // 
            this.laWarm.AutoSize = true;
            this.laWarm.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarm.ForeColor = System.Drawing.Color.Red;
            this.laWarm.Location = new System.Drawing.Point(386, 134);
            this.laWarm.Name = "laWarm";
            this.laWarm.Size = new System.Drawing.Size(136, 16);
            this.laWarm.TabIndex = 17;
            this.laWarm.Text = "本商品已跨店廢番";
            // 
            // UC10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Controls.Add(this.laWarm);
            this.Controls.Add(this.laStockName);
            this.Controls.Add(this.laStockCode);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tbEAN);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.cb2);
            this.Controls.Add(this.cb1);
            this.Controls.Add(this.cb0);
            this.Controls.Add(this.la6);
            this.Controls.Add(this.la5);
            this.Controls.Add(this.la4);
            this.Controls.Add(this.la3);
            this.Controls.Add(this.la2);
            this.Controls.Add(this.la1);
            this.Name = "UC10";
            this.Size = new System.Drawing.Size(784, 537);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Label la2;
        private System.Windows.Forms.Label la3;
        private System.Windows.Forms.Label la4;
        private System.Windows.Forms.Label la5;
        private System.Windows.Forms.Label la6;
        private System.Windows.Forms.CheckBox cb0;
        private System.Windows.Forms.CheckBox cb1;
        private System.Windows.Forms.CheckBox cb2;
        private System.Windows.Forms.DataGridView dgList;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.TextBox tbEAN;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label laStockCode;
        private System.Windows.Forms.Label laStockName;
        private System.Windows.Forms.DataGridViewTextBoxColumn 門市總倉;
        private System.Windows.Forms.DataGridViewTextBoxColumn 名稱;
        private System.Windows.Forms.DataGridViewTextBoxColumn 庫存量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 前週銷量;
        private System.Windows.Forms.Label laWarm;
    }
}
