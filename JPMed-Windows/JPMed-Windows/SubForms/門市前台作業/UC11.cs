﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;
using JPMed_Windows.Utility;
using log4net;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Excel = Microsoft.Office.Interop.Excel;
using JPMed_Windows.Entity;

namespace JPMed_Windows.SubForms.門市前台作業
{
    public partial class UC11 : UserControl
    {
        string formName = "門市前台作業系統";
        string empCodeReal = string.Empty;
        static string storeCode = Properties.Settings.Default.WerksCode.ToUpper();
        static string p_spbup = string.Empty;
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC11));
        SQLCreator sql;
        DBConnection dbConn;
        DataTable defaultDT = null;
        BindingSource source = new BindingSource();

        int _SHiNum, _SLoNum = 0;
        int _AHiNum, _ALoNum = 0;
        int _BHiNum, _BLoNum = 0;
        int _CHiNum, _CLoNum = 0;

        //是否已按下重新計算採購量
        bool IsCalORDERMClick = true;

        //所有RP補貨商品售價
        List<StorpPrice> allStropPriceList = new List<StorpPrice>();

        public UC11()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            dvList.AutoScrollOffset = new Point(10, 10);
            IntitalEvent();
            DefaultControl();
            IntitalControl();

            //setTestData();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            btnSearch.Click += btnSearch_Click;
            btnExportToExcel.Click += btnExportToExcel_Click;
            tbEmpCode.KeyUp += tbEmpCode_KeyUp;
            tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            btnCalORDERM.Click += new EventHandler(btnCalOrderM_Click);
            dvList.CellMouseDoubleClick += dvList_CellMouseDoubleClick;
            dvList.CellValueChanged += dvList_CellValueChanged;
            dvList.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dvList_DataBindingComplete);
            dvList.CellEndEdit += new DataGridViewCellEventHandler(dvList_CellEndEdit);
            dvList.CellBeginEdit += new DataGridViewCellCancelEventHandler(dvList_CellBeginEdit);
            //tbEan.KeyDown += tbEan_KeyDown;
        }

        /// <summary>
        /// EAN查詢事件
        /// </summary>
        private void tbEan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(tbEan.Text))
                    MessageBox.Show("請輸入[商品條碼(EAN)]!");
                else
                {
                    bool isFound = false;
                    foreach (DataGridViewRow row in dvList.Rows)
                    {
                        if (row.Cells["商品條碼EAN"].Value.ToString() == tbEan.Text)
                        {
                            dvList_CellMouseDoubleClick(row, new DataGridViewCellMouseEventArgs(0, row.Index, 0, 0,
                                new MouseEventArgs(MouseButtons.Left, 2, 0, 0, 0)));
                            isFound = true;
                            break;
                        }
                    }

                    if (!isFound) MessageBox.Show("查無商品資訊!");
                }
            }
        }

        //重新計算本週採購量
        void btnCalOrderM_Click(object sender, EventArgs e)
        {
            Int64 totalOrderM = 0;

            totalOrderM = CalOrderM();

            SaleRatioCheck();

            lblORDERM.Text = totalOrderM.ToString();

            IsCalORDERMClick = true;
        }

        /// <summary>
        /// 計算本次採購量
        /// 20170608 增加進銷比報表
        /// </summary>
        /// <returns></returns>
        private Int64 CalOrderM()
        {
            Int64 totalOrderM = 0;

            for (int i = 0; i < allStropPriceList.Count; i++)
            {
                totalOrderM
                    = totalOrderM +
                    allStropPriceList[i].PRICE *
                    Convert.ToInt32(Math.Round(Convert.ToDecimal(allStropPriceList[i].MENGE_C)));
            }

            return totalOrderM;
        }

        /// <summary>
        /// 進銷比檢查 
        /// 20170608 增加進銷比報表
        /// </summary>
        private void SaleRatioCheck()
        {
            int SALESM = 0;

            if (int.TryParse(lblSALESM.Text, out SALESM) && Convert.ToInt64(lblORDERM.Text) > Convert.ToInt32(lblSALESM.Text))
            {
                lblORDERM.ForeColor = Color.Red;
            }
            else
            {
                lblORDERM.ForeColor = Color.Black;
            }
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;
            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;
            datBADAT.Text = DateTime.Today.ToString("yyyy/MM/dd");
            datBADAT.Enabled = false;
            btnSearch.Enabled = false;
            //laEAN.Text = string.Empty;
            tbEan.Text = string.Empty;
            tbEan.Enabled = true;
            tbMAKTX.Text = string.Empty;
            cbMAABC.SelectedIndex = 0;
            cbClass1.SelectedIndex = 0;

            dvList.RowHeadersWidth = 20;
            btnExportToExcel.Enabled = false;
            btnCalORDERM.Enabled = false;
            dvList.DataSource = defaultDT;

            IsCalORDERMClick = true;
            allStropPriceList = new List<StorpPrice>();
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            string sqlCmd = string.Empty;
            DataTable tmpDT = new DataTable();

            try
            {
                sql = new SQLCreator("UC11");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得分店資料
                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
                else
                {
                    EnableControl(true, 0);
                    int term = PublicClass.GetIso8601WeekOfYear(DateTime.Now) - 1;
                    if (term == 0)
                    {
                        p_spbup = new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1).Year.ToString() +
                                  PublicClass.GetIso8601WeekOfYear(new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1)).ToString("00");
                    }
                    else
                    {
                        p_spbup = DateTime.Today.Year.ToString() + term.ToString("00");
                    }
                }

                //取得補貨範圍資料     20170615增加S類
                sqlCmd = sql.GetCommand("GetReplenRange");
                tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    foreach (DataRow item in tmpDT.Rows)
                    {
                        if (item["WMAAB"].ToString() == "S")
                        {
                            _SHiNum = Convert.ToInt32(item["HINUM"]);
                            _SLoNum = Convert.ToInt32(item["LONUM"]);
                        }
                        else if (item["WMAAB"].ToString() == "A")
                        {
                            _AHiNum = Convert.ToInt32(item["HINUM"]);
                            _ALoNum = Convert.ToInt32(item["LONUM"]);
                        }
                        else if (item["WMAAB"].ToString() == "B")
                        {
                            _BHiNum = Convert.ToInt32(item["HINUM"]);
                            _BLoNum = Convert.ToInt32(item["LONUM"]);
                        }
                        else if (item["WMAAB"].ToString() == "C")
                        {
                            _CHiNum = Convert.ToInt32(item["HINUM"]);
                            _CLoNum = Convert.ToInt32(item["LONUM"]);
                        }
                    }
                }

                DateTime nextRepDate = DateTime.Now;

                int dayOfWeek = Convert.ToInt16(DateTime.Now.DayOfWeek); //今日星期幾 0 ~ 6
                if (dayOfWeek > 0 && dayOfWeek < 4)
                {
                    //下個星期4

                    nextRepDate = DateTime.Today.AddDays(dayOfWeek * -1).AddDays(4);
                }
                else
                {
                    //下個星期1
                    nextRepDate = DateTime.Today.AddDays((dayOfWeek == 0 ? 7 : dayOfWeek) * -1).AddDays(8);
                }

                datBADAT.Text = nextRepDate.ToString("yyyy/MM/dd");
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (IsCalORDERMClick == false)
            {
                MessageBox.Show("請按下計算採購金額後再離開！");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("您確定關閉[補貨作業]功能?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls[1].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    if (_enbleControl)
                        tbEmpCode.Enabled = true;
                    else
                    {
                        tbEmpCode.Text = string.Empty;
                        tbEmpCode.Enabled = false;
                    }
                    break;

                case 1: //日期、查詢按鈕
                    datBADAT.Enabled = _enbleControl;
                    btnSearch.Enabled = _enbleControl;
                    btnExportToExcel.Enabled = _enbleControl;
                    btnCalORDERM.Enabled = _enbleControl;
                    break;
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    tbEmpCode.Focus();
                    tbEmpCode.Select(0, tbEmpCode.Text.Length);
                    laEmpName.Text = string.Empty;

                    EnableControl(false, 1);
                    EnableControl(false, 2);
                    btnExportToExcel.Enabled = false;
                    dvList.DataSource = defaultDT;
                    MessageBox.Show("資料庫中查無此員工編號!");
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    EnableControl(true, 1);
                    btnSearch.Focus();
                }
            }
            catch (Exception ex)
            {
                logger.Error("取得使用者名稱發生錯誤: " + ex.ToString());
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.Select(0, tbEmpCode.Text.Length);
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && ((TextBox)sender).SelectionLength == 0)
            {
                tbEmpCode.Select(0, tbEmpCode.Text.Length);
                GetEmpName(tbEmpCode.Text);
            }
        }

        /// <summary>
        /// 查詢
        /// </summary>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            Int64 totalOrderM = 0;
            allStropPriceList = new List<StorpPrice>();

            try
            {
                if (tbMATKL.Text.Length != 9 && tbMATKL.Text.Length > 0 && tbMATKL.Text.Split('*').Length != 3)
                {
                    MessageBox.Show("請依照以下規則查詢物料群組：\n查詢大分類：大分類號碼** \n查詢中分類：*中分類代碼*\n查詢小分類：**小分類代碼");
                    return;
                }

                //先清除控制項目再做查詢
                tbEan.Enabled = true;

                btnExportToExcel.Enabled = true;
                laWarn.Visible = false;

                #region MAABC 下拉選單
                string selectedMAABC = cbMAABC.Text;
                if (selectedMAABC == "全部")
                {
                    selectedMAABC = string.Empty;
                }
                else if (selectedMAABC.Length > 1)
                {
                    selectedMAABC = selectedMAABC.Substring(0, 1);
                }
                #endregion MAABC

                #region 取得總補貨資料

                string allSTORPsqlCmd =
                    SqlStatement.UC11.GetReplenAdvise(storeCode, datBADAT.Value.ToString("yyyyMMdd"), "", "", "", "", "");

                DataTable allDataDT = dbConn.GetDataTable(allSTORPsqlCmd);

                if (allDataDT.Rows.Count > 0)
                {
                    for (int i = 0; i < allDataDT.Rows.Count; i++)
                    {
                        StorpPrice storpItem = new StorpPrice();
                        storpItem.MATNR = allDataDT.Rows[i]["MATNR"].ToString();
                        storpItem.PRICE = Convert.ToInt32(allDataDT.Rows[i]["PRICE"].ToString());
                        storpItem.MENGE_C = allDataDT.Rows[i]["MENGE_C"].ToString();
                        allStropPriceList.Add(storpItem);
                    }
                }

                #endregion 取得總補貨資料

                #region Class1
                string selectedMATKLItem = cbClass1.Text;
                if (selectedMATKLItem == "全部")
                {
                    selectedMATKLItem = string.Empty;
                }
                else if (selectedMATKLItem.Length > 1)
                {
                    selectedMATKLItem = selectedMATKLItem.Substring(0, 1);
                }
                #endregion MAABC

                string sqlCmd = sql.GetCommand("GetReplenAdvise",
                    new string[]{
                        storeCode,
                        datBADAT.Value.ToString("yyyyMMdd"),
                        tbEan.Text.Trim(),
                        selectedMAABC,
                        tbMATKL.Text.Replace("*","___").Trim(),
                        tbMAKTX.Text,
                        selectedMATKLItem});

                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    string sqlGetLastSALESMcmd = SqlStatement.UC11.GetLastSALESM(datBADAT.Value.ToString("yyyyMMdd"), storeCode);
                    DataTable SalesMDT = dbConn.GetDataTable(sqlGetLastSALESMcmd);

                    if (SalesMDT.Rows.Count > 0)
                    {
                        lblSALESM.Text = SalesMDT.Rows[0]["TotalSALESM"].ToString();
                    }
                    else
                    {
                        lblSALESM.Text = "N/A";
                    }

                    foreach (DataRow item in tmpDT.Rows)
                    {
                        decimal MENGES = Convert.ToDecimal(item["MENGE_S"].ToString());
                        if (item["MAABC"].ToString() == "S")        //20170615增加S類
                        {
                            item["HINUM"] = Convert.ToInt32(Math.Round(MENGES * _SHiNum / 100, MidpointRounding.ToEven) + MENGES);
                            item["LONUM"] = Convert.ToInt32(MENGES - Math.Round(MENGES * _SLoNum / 100, MidpointRounding.ToEven)) >= 0 ? Convert.ToInt32(MENGES - Math.Round(MENGES * _SLoNum / 100, MidpointRounding.ToEven)) : 0;
                        }
                        else if (item["MAABC"].ToString() == "A")
                        {
                            item["HINUM"] = Convert.ToInt32(Math.Round(MENGES * _AHiNum / 100, MidpointRounding.ToEven) + MENGES);
                            item["LONUM"] = Convert.ToInt32(MENGES - Math.Round(MENGES * _ALoNum / 100, MidpointRounding.ToEven)) >= 0 ? Convert.ToInt32(MENGES - Math.Round(MENGES * _ALoNum / 100, MidpointRounding.ToEven)) : 0;
                        }
                        else if (item["MAABC"].ToString() == "B")
                        {
                            item["HINUM"] = Convert.ToInt32(Math.Round(MENGES * _BHiNum / 100, MidpointRounding.ToEven) + MENGES);
                            item["LONUM"] = Convert.ToInt32(MENGES - Math.Round(MENGES * _BLoNum / 100, MidpointRounding.ToEven)) >= 0 ? Convert.ToInt32(MENGES - Math.Round(MENGES * _BLoNum / 100, MidpointRounding.ToEven)) : 0;
                        }
                        else
                        {
                            item["HINUM"] = Convert.ToInt32(Math.Round(MENGES * _CHiNum / 100, MidpointRounding.ToEven) + MENGES);
                            item["LONUM"] = Convert.ToInt32(Math.Round(MENGES - MENGES * _CLoNum / 100, MidpointRounding.ToEven)) >= 0 ? Convert.ToInt32(MENGES - Math.Round(MENGES * _CLoNum / 100, MidpointRounding.ToEven)) : 0;
                        }
                    }

                    ////若查詢的日期小於今日，代表過去資料，該資料不可修改
                    if (DateTime.Now.Date >= datBADAT.Value.Date)
                    {
                        dvList.Columns["MENGE_C"].ReadOnly = true;
                        IsCalORDERMClick = true;
                    }
                    else
                    {
                        dvList.Columns["MENGE_C"].ReadOnly = false;
                        IsCalORDERMClick = false;
                    }

                    //計算本次採購金額
                    lblORDERM.Text = CalOrderM().ToString();

                    source.DataSource = tmpDT;
                    dvList.DataSource = source;
                    defaultDT = tmpDT.Clone();

                    if ((dvList.Rows[0].Cells["PROCD"].Value.ToString().Replace(" ", "").ToLower() != "x"))
                    {
                        dvList.Rows[0].Cells["MENGE_C"].Selected = true;

                        dvList.BeginEdit(false);
                    }
                    else
                    {
                        dvList.Columns["MENGE_C"].ReadOnly = true;
                    }

                    SaleRatioCheck();
                }
                else
                {
                    dvList.DataSource = defaultDT;
                    IsCalORDERMClick = true;
                    MessageBox.Show("查無相關資訊!");
                }
            }
            catch (Exception ex)
            {
                logger.Error("查詢命令發生錯誤: " + ex.ToString());
                MessageBox.Show("查詢命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 匯出Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Excel|*.xls";
            fileDialog.Title = "另存新檔";
            fileDialog.CreatePrompt = true;
            //fileDialog.InitialDirectory = @"C:\";
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK && fileDialog.FileName != "")
            {
                MemoryStream ms = DataToExcel(dvList);
                FileStream fs = new FileStream(fileDialog.FileName, FileMode.OpenOrCreate);
                ms.WriteTo(fs);
                fs.Close();
                ms.Close();

                MessageBox.Show("匯出Excel完成。");
            }
        }

        //DataGridView資料轉存Excel
        public static MemoryStream DataToExcel(DataGridView dv)
        {
            MemoryStream ms = new MemoryStream();

            IWorkbook workbook = new HSSFWorkbook();//Create an excel Workbook
            ISheet sheet = workbook.CreateSheet();//Create a work table in the table
            IRow headerRow = sheet.CreateRow(0); //To add a row in the table
            int columnIndex = 0;
            foreach (DataGridViewColumn column in dv.Columns)
            {
                if (column.Visible)
                {
                    headerRow.CreateCell(columnIndex, CellType.String).SetCellValue(column.HeaderText);
                    columnIndex++;
                }
            }
            int rowIndex = 1;

            foreach (DataGridViewRow row in dv.Rows)
            {
                columnIndex = 0;
                IRow dataRow = sheet.CreateRow(rowIndex);
                foreach (DataGridViewColumn column in dv.Columns)
                {
                    if (column.Visible)
                    {

                        ICell rowCell = dataRow.CreateCell(columnIndex, CellType.String);
                        //rowCell.SetCellType(CellType.Numeric);
                        decimal outDecimal = 0;
                        if (Decimal.TryParse(row.Cells[column.Index].Value.ToString(), out outDecimal))
                        {
                            string s = row.Cells[column.Index].Value.ToString().Replace(".000", "");
                            rowCell.SetCellValue(s);
                        }
                        else
                        {
                            rowCell.SetCellValue(row.Cells[column.Index].Value.ToString());
                        }

                        columnIndex++;
                    }
                }
                rowIndex++;

            }

            for (int i = 0; i < columnIndex; i++)
            {
                sheet.AutoSizeColumn(i);

            }

            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;

            return ms;
        }

        /// <summary>
        /// ABC顏色處理
        /// </summary>
        /// <param name="colorType">顏色類型(A/B/C)</param>
        /// <returns>Color Class</returns>
        Color GetColorByABC(string colorType)
        {
            if (colorType == "S")
            {
                return Color.FromArgb(166, 166, 255);//add S 紫色 20170615
            }
            else if (colorType == "A")
            {
                return Color.FromArgb(254, 177, 177);
            }
            else if (colorType == "B")
            {
                return Color.FromArgb(254, 254, 177);
            }
            else
            {
                return Color.FromArgb(186, 254, 177);
            }
        }

        #region DataGridView

        int oriValue = 0;

        /// <summary>
        /// Grid 滑鼠點擊事件
        /// </summary>
        private void dvList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                laWarn.Visible = false;
                if (e.RowIndex != -1)
                {
                    int rowIdx = e.RowIndex;

                    string year = dvList.Rows[rowIdx].Cells["BADAT"].Value.ToString().Substring(0, 4);
                    string month = dvList.Rows[rowIdx].Cells["BADAT"].Value.ToString().Substring(4, 2);
                    string day = dvList.Rows[rowIdx].Cells["BADAT"].Value.ToString().Substring(6, 2);
                    DateTime d = Convert.ToDateTime(year + "/" + month + "/" + day);

                    bool sapSign = (dvList.Rows[rowIdx].Cells["PROCD"].Value.ToString().Replace(" ", "").ToLower() == "x") ? true : false;

                    if (sapSign)
                    {
                        laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
                        laWarn.Visible = true;
                        dvList.Columns["MENGE_C"].ReadOnly = true;

                    }
                    else
                    {
                        //保留原數值
                        Int32.TryParse(dvList.Rows[rowIdx].Cells["MENGE_C"].Value.ToString(), out oriValue);
                        dvList.BeginEdit(true);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("滑鼠點擊命令發生錯誤: " + ex.ToString());
                MessageBox.Show("滑鼠點擊命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 欄位資料變更
        /// </summary>
        private void dvList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0)
                    return;

                if (dvList.Rows[e.RowIndex].Cells["MENGE_C"].ReadOnly)
                    return;

                int tmpInt;
                if (!int.TryParse(dvList.Rows[e.RowIndex].Cells["MENGE_C"].Value.ToString(), out tmpInt))
                    MessageBox.Show("確認量輸入非數值欄位，請輸入數值!");

                string rowMAABC = dvList.Rows[e.RowIndex].Cells["MAABC"].Value.ToString();
                string rowHINUM = dvList.Rows[e.RowIndex].Cells["HINUM"].Value.ToString();
                string rowLONUM = dvList.Rows[e.RowIndex].Cells["LONUM"].Value.ToString();
                string rowBADAT = dvList.Rows[e.RowIndex].Cells["BADAT"].Value.ToString();
                string rowMENGE_C = dvList.Rows[e.RowIndex].Cells["MENGE_C"].Value.ToString();
                string rowMATNR = dvList.Rows[e.RowIndex].Cells["MATNR"].Value.ToString();
                string currCRDAT = DateTime.Now.ToString("yyyyMMdd");
                string currCRTME = DateTime.Now.ToString("HHmmss");

                //20170316 增加修改前判斷SAP是否已修改
                string sqlCmd = SqlStatement.UC11.SelectSTORPData(storeCode, rowBADAT, rowMATNR);
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);

                if (tmpDT.Rows.Count == 1)
                {
                    if (tmpDT.Rows[0]["PROCD"].ToString().Replace(" ", "").ToLower() == "x")
                    {
                        dvList.Rows[e.RowIndex].Cells["MENGE_C"].ReadOnly = true;
                        MessageBox.Show("SAP系統已處理，不可修改");
                        //for (int i = 0; i < dvList.Rows.Count; i++)
                        //{
                        dvList.Columns["MENGE_C"].ReadOnly = true;
                        //dvList.Rows[i].Cells["PROCD"].Value = "X";
                        //}
                        return;
                    }
                }

                sqlCmd = SqlStatement.UC11.UpdateSTORP(storeCode, rowBADAT, rowMATNR, rowMENGE_C, rowHINUM, rowLONUM, empCodeReal, currCRDAT, currCRTME);
                if (dbConn.ExecSql(sqlCmd) == 1)
                {
                    MessageBox.Show("修改成功");
                    dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = GetColorByABC(rowMAABC);

                    var storpItem = allStropPriceList.SingleOrDefault(temp => temp.MATNR == rowMATNR);
                    if (storpItem != null)
                    {
                        storpItem.MENGE_C = rowMENGE_C;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("修改STORP發生錯誤: " + ex.ToString());
                MessageBox.Show("修改STORP發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 修改驗證
        /// </summary>
        private void dvList_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            decimal tempDecimal = 0;
            int tempInt;

            if (dvList.Columns["MENGE_C"].Index != e.ColumnIndex || dvList.Columns["MENGE_C"].ReadOnly || dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor != Color.White)
            {
                return;
            }

            //檢查是否SAP已處理
            if (dvList.Rows[e.RowIndex].Cells["PROCD"].Value.ToString().Replace(" ", "").ToLower() == "x")
            {
                e.Cancel = true;
                return;
            }

            if (!int.TryParse(e.FormattedValue.ToString(), out tempInt) || !Decimal.TryParse(e.FormattedValue.ToString(), out tempDecimal))
            {
                e.Cancel = true;
                MessageBox.Show("確認量應為整數數值欄位，請再次確認!");
            }

            decimal currHINUM = Convert.ToDecimal(dvList.Rows[e.RowIndex].Cells["HINUM"].Value.ToString());
            decimal currLONUM = Convert.ToDecimal(dvList.Rows[e.RowIndex].Cells["LONUM"].Value.ToString());

            if (tempDecimal > currHINUM || tempDecimal < currLONUM)
            {
                e.Cancel = true;
                MessageBox.Show("確認量必須介於最高補貨量及最低補貨量之間，請再次確認!");
            }
        }

        //開始編輯時，變更底色為白色
        void dvList_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //檢查是否SAP已處理
            if (dvList.Rows[e.RowIndex].Cells["PROCD"].Value.ToString().Replace(" ", "").ToLower() == "x")
            {
                e.Cancel = true;
            }
            else
            {
                //重新添加添加底色
                dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
            }
        }

        /// <summary>
        /// 結束編輯後，變更底色為相對應顏色
        /// </summary>
        void dvList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = GetColorByABC(dvList.Rows[e.RowIndex].Cells["MAABC"].Value.ToString());
            IsCalORDERMClick = false;
        }

        /// <summary>
        /// 資料繫結完成後
        /// </summary>
        void dvList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //依ABC類別改變顏色, 最高補貨量, 最低補貨量計算
            foreach (DataGridViewRow rowItem in dvList.Rows)
            {
                double MENGES = Convert.ToDouble(rowItem.Cells["MENGE_S"].Value.ToString());

                rowItem.DefaultCellStyle.BackColor = GetColorByABC(rowItem.Cells["MAABC"].Value.ToString());
            }
        }

        #endregion DataGridView

        /// <summary>
        /// 當畫面大小變更時，將dvList設定為可使用的大小
        /// </summary>
        private void UC11_Resize(object sender, EventArgs e)
        {
            dvList.Width = this.Width - 40;
            dvList.Height = this.Height - dvList.Location.Y - 40; ;
        }

        //前一次修改的Index
        int prevEditIndex = -1;

        private void dvList_KeyUp(object sender, KeyEventArgs e)
        {
            if (dvList.Rows.Count == 0 || dvList.SelectedRows[0].Cells["MENGE_C"].ReadOnly)
            {
                return;
            }

            if (e.KeyCode == Keys.Enter)
            {
                //若沒有任何選擇的列則不處理
                if (dvList.SelectedRows.Count == 0)
                    return;

                //若還有下一筆資料且與前一次修改的列不同時，繼續修改下一筆
                if (dvList.SelectedRows[0].Index + 1 <= dvList.Rows.Count && prevEditIndex != dvList.SelectedRows[0].Index)
                {
                    prevEditIndex = dvList.SelectedRows[0].Index;
                    dvList.BeginEdit(true);
                }
            }
        }
    }
}
