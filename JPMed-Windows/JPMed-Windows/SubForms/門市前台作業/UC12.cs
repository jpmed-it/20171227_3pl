﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JPMed_Windows.Utility;
using log4net;
using System.Globalization;

namespace JPMed_Windows.SubForms.門市前台作業
{
    public partial class UC12 : UserControl
    {
        string formName = "門市前台作業系統";
        string empCodeReal = string.Empty;
        static string storeCode = Properties.Settings.Default.WerksCode.ToUpper();
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC12));
        SQLCreator sql;
        DBConnection dbConn;
        DataTable defaultDT = null;
        BindingSource source = new BindingSource();
        DataTable GMOVEH; //物料異動資料表頭
        List<string> prdList = new List<string>();

        public UC12()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            tbEmpCode.KeyUp += tbEmpCode_KeyUp;
            tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            tbSN.KeyDown += tbSN_KeyDown;
            tbSN.TextChanged += tbSN_TextChanged;
            btnSearch.Click += btnSearch_Click;
            btnSave.Click += btnSave_Click;
            dvList.CellMouseDoubleClick += dvList_CellMouseDoubleClick;
            tbRealQty.KeyDown += tbRealQty_KeyUp;
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;
            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;
            cbSin.Enabled = false;
            laSdate.Text = string.Empty;
            laEdate.Text = string.Empty;
            tbSN.Enabled = false;
            tbSN.Text = string.Empty;
            btnSearch.Enabled = false;
            laEAN.Text = string.Empty;
            laNeedQty.Text = string.Empty;
            tbRealQty.Text = string.Empty;
            tbRealQty.Enabled = false;
            btnSave.Enabled = false;
            laCount.Text = string.Empty;
            laTotalQty.Text = string.Empty;
            laWarn.Visible = false;
            dvList.DataSource = defaultDT;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC12");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得特收日期
                string sqlCmd = sql.GetCommand("CheckSpecialIn", new string[] { storeCode });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    CultureInfo ci = new CultureInfo("zh-tw");
                    cbSin.Checked = true;
                    laSdate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLSD"].ToString()).ToString("D",ci);
                    laEdate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLDD"].ToString()).ToString("D",ci);
                }

                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
                else
                {
                    EnableControl(true, 0);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉[驗收發貨作業]功能?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls[1].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    if (_enbleControl)
                        tbEmpCode.Enabled = true;
                    else
                    {
                        tbEmpCode.Text = string.Empty;
                        tbEmpCode.Enabled = false;
                    }
                    break;

                case 1: //單號、查詢按鈕
                    if (!_enbleControl) tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;
                    btnSearch.Enabled = _enbleControl;
                    break;

                case 2: //清單、數量、輸入按鈕
                    if (dvList.Rows.Count == 0)
                    {
                        laWarn.Visible = false;
                        laTotalQty.Text = string.Empty;
                        laCount.Text = string.Empty;
                        laEAN.Text = string.Empty;
                        laNeedQty.Text = string.Empty;
                        tbRealQty.Text = string.Empty;
                        tbRealQty.Enabled = false;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        laWarn.Visible = false;
                        laTotalQty.Text = string.Empty;
                        laCount.Text = string.Empty;
                        laEAN.Text = string.Empty;
                        laNeedQty.Text = string.Empty;
                        tbRealQty.Text = string.Empty;
                        tbRealQty.Enabled = _enbleControl;
                        btnSave.Enabled = _enbleControl;
                    }
                    break;
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    tbEmpCode.Focus();
                    tbEmpCode.Select(0, tbEmpCode.Text.Length);
                    laEmpName.Text = string.Empty;

                    EnableControl(false, 1);
                    EnableControl(false, 2);
                    dvList.DataSource = defaultDT;
                    MessageBox.Show("資料庫中查無此員工編號!");
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    EnableControl(true, 1);
                    tbSN.Focus();
                }
            }
            catch (Exception ex)
            {
                logger.Error("取得使用者名稱發生錯誤: " + ex.ToString());
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.Select(0, tbEmpCode.Text.Length);
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && ((TextBox)sender).SelectionLength == 0)
            {
                tbEmpCode.Select(0, tbEmpCode.Text.Length);
                GetEmpName(tbEmpCode.Text);
            }
        }

        /// <summary>
        /// 輸入序號事件，自動查詢
        /// </summary>
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;
        private void tbSN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
                tbSN.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }
        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                btnSearch_Click(null, null);
                tbSN.SelectAll();
            }
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //先清空資訊
                prdList.Clear();
                string tmpVbeln = string.Empty;
                string sn = tbSN.Text;
                string sqlCmd = string.Empty;

                //先清除控制項目在做查詢
                dvList.DataSource = defaultDT;
                EnableControl(false, 2);

                if (string.IsNullOrEmpty(sn))
                {
                    MessageBox.Show("請輸入查詢單號");
                    return;
                }

                //先取得資訊
                sqlCmd = sql.GetCommand("GetGmovehInfo", new string[] { sn });
                GMOVEH = dbConn.GetDataTable(sqlCmd);
                if (GMOVEH.Rows.Count <= 0)
                {
                    //20160717新增需求，查不到單號者，需額外檢查VBELN
                    sqlCmd = sql.GetCommand("GetGmovetByVbeln", new string[] { sn });
                    DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (tmpDT.Rows.Count > 0)
                    {
                        string tmpSN = tmpDT.Rows[0]["EBELN"] == null ?
                                       tmpDT.Rows[0]["BEDNR"] == null ? "" : tmpDT.Rows[0]["BEDNR"].ToString() :
                                       tmpDT.Rows[0]["EBELN"].ToString();

                        if (string.IsNullOrEmpty(tmpSN))
                        {
                            MessageBox.Show("此單號查無對應單據，請確認!");
                            return;
                        }

                        tmpVbeln = sn;

                        #region ZRP1,ZRP3, ZRP5, VBELN 物流到貨作業檢查 及 到貨門市檢查
                        if (tmpDT.Rows[0]["BSART"].ToString() == "ZRP1"
                            || tmpDT.Rows[0]["BSART"].ToString() == "ZRP3"
                            || tmpDT.Rows[0]["BSART"].ToString() == "ZRP5"
                            || tmpDT.Rows[0]["BSART"].ToString() == "ZUB4")
                        {
                            sqlCmd = SqlStatement.UC12.GetWERKSByVBELN(sn);
                            DataTable dtWerks = dbConn.GetDataTable(sqlCmd);
                            if (dtWerks.Rows[0]["WERKS"].ToString() != storeCode)
                            {
                                MessageBox.Show("本門市非單據收貨門市(" + dtWerks.Rows[0]["WERKS"].ToString() + ")，請確認!");
                                tbSN.SelectAll();
                                tbSN.Focus();
                                return;
                            }

                            sqlCmd = SqlStatement.UC12.CheckGMOVETShip(sn, "AB");
                            DataTable dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                            if (dtProductShipCount.Rows.Count <= 0)
                            {
                                MessageBox.Show("物流到貨作業未操作");
                                tbSN.SelectAll();
                                tbSN.Focus();
                                return;
                            }
                        }
                        #endregion

                        prdList = (from row in tmpDT.AsEnumerable()
                                   select "'" + row["EAN11"].ToString() + "'").ToList();

                        sqlCmd = sql.GetCommand("GetGmovehInfo", new string[] { tmpSN });
                        GMOVEH = dbConn.GetDataTable(sqlCmd);
                    }
                    else
                    {
                        MessageBox.Show("此單號查無對應單據，請確認!");
                        return;
                    }
                }

                //從GMOVEH找出所有單據內容並填入dvList
                DataTable dtUnion = null;

                foreach (DataRow row in GMOVEH.Select())
                {
                    string bsart = row["BSART"].ToString(); //採購文件類型
                    string ebeln = row["EBELN"].ToString(); //採購文件號碼
                    string bednr = row["BEDNR"].ToString(); //需求追蹤號碼
                    string vbeln = row["EBELN"].ToString(); //預設值

                    string prdFilter = string.Empty;
                    if (prdList.Count > 0)
                    {
                        prdFilter = "A.EAN11 IN ({0})";
                        prdFilter = string.Format(prdFilter, string.Join(",", prdList.ToArray()));
                        vbeln = tbSN.Text; //填入正確的VBELN
                    }
                    else prdFilter = "1=1";

                    if (!string.IsNullOrEmpty(tmpVbeln))
                        prdFilter += string.Format(" AND B.VBELN = '{0}'", tmpVbeln);

                    sqlCmd = sql.GetCommand("GetSNInfo", new string[] { bsart, ebeln, bednr, prdFilter });
                    DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (tmpDT.Rows.Count > 0)
                    {
                        if (dtUnion == null) dtUnion = tmpDT;
                        else dtUnion = dtUnion.AsEnumerable().Union(tmpDT.AsEnumerable()).CopyToDataTable<DataRow>();
                    }
                }

                if (dtUnion != null && dtUnion.Rows.Count > 0)
                {
                    #region 3PL物流/自送 建單檢查
                    //若為 ZUB1 ，則出貨(641) 建單檢查
                    if (dtUnion.Rows[0]["BSART"].ToString() == "ZUB1" || dtUnion.Rows[0]["BSART"].ToString() == "ZUB3")
                    {
                        sqlCmd = SqlStatement.UC12.GetSTO2STO(GMOVEH.Rows[0]["EBELN"].ToString());
                        DataTable dtSTO2STO = dbConn.GetDataTable(sqlCmd);

                        if (dtSTO2STO.Rows.Count == 0)
                        {
                            MessageBox.Show("此單號查無相關資訊!");
                            tbSN.SelectAll();
                            tbSN.Focus();
                            return;
                        }
                        string queryBWART = string.Empty;
                        string popupMsg = string.Empty;

                        //檢查是否已出貨(自送調出 或 物流收貨)
                        sqlCmd = SqlStatement.UC12.CheckProductShip(GMOVEH.Rows[0]["EBELN"].ToString(), "641");
                        DataTable dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                        if (dtProductShipCount.Rows.Count <= 0)
                        {
                            MessageBox.Show("該品項無建單無法驗收");
                            tbSN.SelectAll();
                            tbSN.Focus();
                            return;
                        }

                        //若為物流派車，則增加檢查物流到貨(AB)
                        if (dtSTO2STO.Rows[0]["WMFLG"].ToString() == "X")
                        {
                            sqlCmd =
                                SqlStatement.UC12.CheckProductShip(GMOVEH.Rows[0]["EBELN"].ToString(), "AB");
                            dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                            if (dtProductShipCount.Rows.Count <= 0)
                            {
                                MessageBox.Show("物流到貨作業未操作");
                                tbSN.SelectAll();
                                tbSN.Focus();
                                return;
                            }
                        }
                    }

                    #endregion 3PL物流/自送 建單檢查

                    //若不等於收貨門市，需彈跳視窗提示
                    if (dtUnion.Rows[0]["BSART"].ToString() == "ZRE3" && dtUnion.Rows[0]["發貨門市"].ToString() != storeCode)
                    {
                        MessageBox.Show("本門市非單據發貨門市(" + dtUnion.Rows[0]["發貨門市"].ToString() + ")，請確認!");
                        dvList.DataSource = defaultDT;
                        return;

                    }
                    else if (dtUnion.Rows[0]["BSART"].ToString() != "ZRE3" && dtUnion.Rows[0]["收貨門市"].ToString() != storeCode)
                    {
                        MessageBox.Show("本門市非單據收貨門市(" + dtUnion.Rows[0]["收貨門市"].ToString() + ")，請確認!");
                        dvList.DataSource = defaultDT;
                        return;
                    }

                    #region ZUB4 物流收貨判斷

                    if (dtUnion.Rows[0]["BSART"].ToString() == "ZUB4")
                    {
                        sqlCmd =
                            SqlStatement.UC12.CheckProductShip(
                                GMOVEH.Rows[0]["EBELN"].ToString(), "AB"
                            );
                        DataTable dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                        if (dtProductShipCount.Rows.Count <= 0)
                        {
                            MessageBox.Show("物流到貨作業未操作");
                            tbSN.SelectAll();
                            tbSN.Focus();
                            return;
                        }
                    }

                    #endregion

                    source.DataSource = dtUnion;
                    dvList.DataSource = source;
                    defaultDT = dtUnion.Clone();

                    laCount.Text = dtUnion.AsEnumerable().Sum(q => decimal.Parse(q["應到數量"].ToString())).ToString("0.###");
                    laTotalQty.Text = dtUnion.AsEnumerable().Sum(q => decimal.Parse(q["實際量"].ToString())).ToString("0.###");

                    //添加事件
                    dvList.CellMouseDoubleClick += dvList_CellMouseDoubleClick;

                    //設定底色
                    foreach (DataGridViewRow row in dvList.Rows)
                    {
                        string rank = row.Cells["RANK"].Value.ToString();
                        Color color = rank == "1" ? Color.White :
                                      rank == "2" ? Color.LightPink :
                                      rank == "3" ? Color.LightGreen : Color.Red;

                        row.DefaultCellStyle.BackColor = color;
                    }
                }
                else
                {
                    dvList.DataSource = defaultDT;
                    MessageBox.Show("查無相關資訊!");
                }
            }
            catch (Exception ex)
            {
                logger.Error("查詢命令發生錯誤: " + ex.ToString());
                MessageBox.Show("查詢命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// Grid 滑鼠點擊事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                //還原所有底色
                foreach (DataGridViewRow row in dvList.Rows)
                {
                    string rank = row.Cells["RANK"].Value.ToString();
                    Color color = rank == "1" ? Color.White :
                                  rank == "2" ? Color.LightPink :
                                  rank == "3" ? Color.LightGreen : Color.Red;

                    row.DefaultCellStyle.BackColor = color;
                }

                if (e.RowIndex == -1)
                    return;

                //重新添加選擇項目底色
                dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightYellow;

                string snType = dvList.Rows[e.RowIndex].Cells["BSART"].Value.ToString();

                if ((snType == "ZRP2" || snType == "ZRP4" || snType == "ZFG1") && !cbSin.Checked)
                {
                    DateTime finalDate = DateTime.Parse(dvList.Rows[e.RowIndex].Cells["需求日期"].Value.ToString()).AddDays(5);
                    if (DateTime.Today > finalDate)
                    {
                        laWarn.Text = "產品已過收貨期限，不可作業";
                        laWarn.Visible = true;

                        laEAN.Text = string.Empty;
                        laNeedQty.Text = string.Empty;
                        tbRealQty.Text = string.Empty;
                        tbRealQty.Enabled = false;
                        btnSave.Enabled = false;
                        return;
                    }
                }

                if (dvList.Rows[e.RowIndex].Cells["CANEDIT"].Value.ToString() != "1")
                {
                    laWarn.Text = "產品已完成收貨，不可異動";
                    laWarn.Visible = true;

                    laEAN.Text = string.Empty;
                    laNeedQty.Text = string.Empty;
                    tbRealQty.Text = string.Empty;
                    tbRealQty.Enabled = false;
                    btnSave.Enabled = false;
                    return;
                }

                if (dvList.Rows[e.RowIndex].Cells["CANDO"].Value.ToString() != "1")
                {
                    laWarn.Text = "查無總倉對應單據，不可異動";
                    laWarn.Visible = true;

                    laEAN.Text = string.Empty;
                    laNeedQty.Text = string.Empty;
                    tbRealQty.Text = string.Empty;
                    tbRealQty.Enabled = false;
                    btnSave.Enabled = false;
                    return;
                }

                int rowIdx = e.RowIndex;
                laKey.Text = dvList.Rows[rowIdx].Cells["BSART"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["EBELN"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["EBELP"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["物料號碼"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["收貨門市"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["發貨門市"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["採購單數量"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["採購單計量單位"].Value.ToString() + ";" +
                             dvList.Rows[rowIdx].Cells["VBELN"].Value.ToString();

                laMaxQty.Text = Decimal.Parse(dvList.Rows[rowIdx].Cells["應到數量"].Value.ToString()).ToString();

                laEAN.Text = dvList.Rows[rowIdx].Cells["商品條碼EAN"].Value.ToString();
                laNeedQty.Text = decimal.Parse(dvList.Rows[rowIdx].Cells["應到數量"].Value.ToString()).ToString("0.###");

                tbRealQty.Text = Decimal.Parse(dvList.Rows[rowIdx].Cells["實際量"].Value.ToString()).ToString("0.###");

                tbRealQty.Enabled = true;
                btnSave.Enabled = true;
                laWarn.Visible = false;
                tbRealQty.SelectAll();
                tbRealQty.Focus();
            }
            catch (Exception ex)
            {
                logger.Error("滑鼠點擊命令發生錯誤: " + ex.ToString());
                MessageBox.Show("滑鼠點擊命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //調撥數量僅可介於 0 <= qty <= 應到(發)量
                int tempInt = 0;
                if (!Int32.TryParse(tbRealQty.Text, out tempInt))
                {
                    MessageBox.Show("[收發量]僅可輸入整數數值，請重新輸入!");
                    tbRealQty.SelectAll();
                    tbRealQty.Focus();
                    return;
                }
                else if (tempInt < 0 || tempInt > Decimal.Parse(laMaxQty.Text))
                {
                    MessageBox.Show("[收發量]需大於等於0且小於等於應到(發)量!\n 0 <= 收發量 <= " + Decimal.Parse(laMaxQty.Text).ToString("0.###"));
                    tbRealQty.SelectAll();
                    tbRealQty.Focus();
                    return;
                }

                DialogResult dialogResult = MessageBox.Show("您確定進行資料異動?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string bsart = laKey.Text.Split(';')[0];
                    string ebeln = laKey.Text.Split(';')[1];
                    string ebelp = laKey.Text.Split(';')[2];
                    string matnr = laKey.Text.Split(';')[3];
                    string ean11 = laEAN.Text;
                    string werks = laKey.Text.Split(';')[4];
                    string reswk = laKey.Text.Split(';')[5];
                    string empCode = empCodeReal; //tbEmpCode.Text;
                    string menge_p = laKey.Text.Split(';')[6];
                    string meins_p = laKey.Text.Split(';')[7];
                    string vbeln = laKey.Text.Split(';')[8].ToString();

                    string sqlCmd = sql.GetCommand("GetInsertCommand", new string[] { bsart, ebeln, ebelp, matnr, ean11, werks, reswk, empCode, menge_p, meins_p, tbRealQty.Text, vbeln });
                    if (dbConn.ExecSql(sqlCmd) == 0)
                    {
                        MessageBox.Show("未更新任何項目，請聯繫管理人員");
                        logger.Error("未更新任何項目，請聯繫管理人員");
                        logger.Info("SQL Command: " + sqlCmd);
                    }
                    else
                    {
                        laWarn.Visible = false;
                        laEAN.Text = string.Empty;
                        laNeedQty.Text = string.Empty;
                        tbRealQty.Text = string.Empty;
                        tbRealQty.Enabled = false;
                        btnSave.Enabled = false;

                        btnSearch_Click(null, null);
                        MessageBox.Show("更新成功!");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("儲存命令發生錯誤: " + ex.ToString());
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        private void tbRealQty_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbRealQty.Text) && e.KeyCode == Keys.Enter)
                btnSave_Click(null, null);
        }
    }
}
