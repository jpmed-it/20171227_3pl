﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using JPMed_Windows.Utility;
using log4net;
using System.Diagnostics;

namespace JPMed_Windows.SubForms.門市前台作業
{
    public partial class UC14 : UserControl
    {
        string formName = "門市前台作業系統";
        string empCodeReal = string.Empty;
        static string printProgramPath = Environment.CurrentDirectory + @"\PrintData\" + Properties.Settings.Default.PrintProgramName;
        static string storeCode = Properties.Settings.Default.WerksCode.ToUpper();
        static string p_spbup = string.Empty;
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC14));
        SQLCreator sql;
        DBConnection dbConn;
        DataTable defaultDT = null;

        public UC14()
        {
            InitializeComponent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;
            laMainStockName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            tbSdate.Text = DateTime.Today.ToString("yyyy-MM-dd");
            comboItems.Items.Clear();

            laEAN.Text = string.Empty;
            tbQty.Text = string.Empty;

            tbSdate.Enabled = false;
            comboItems.Enabled = false;
            tbQty.Enabled = false;
            btnSearch.Enabled = false;
            btnPrint.Enabled = false;
            btnSave.Enabled = false;
            btnDel.Enabled = false;

            laCount.Text = string.Empty;
            laTotalQty.Text = string.Empty;

            dvList.DataSource = defaultDT;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC14");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得總倉名稱
                string sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { "TDC2" }); //總倉固定為TDC1
                laMainStockName.Text = dbConn.GetDataTableRecData(sqlCmd);

                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
                else
                {
                    int term = PublicClass.GetIso8601WeekOfYear(DateTime.Now) - 1;
                    if (term == 0)
                    {
                        p_spbup = new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1).Year.ToString() +
                                  PublicClass.GetIso8601WeekOfYear(new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1)).ToString("00");
                    }
                    else
                    {
                        p_spbup = DateTime.Today.Year.ToString() + term.ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉[總部通知調撥回倉品]功能?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls[1].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //日期、查詢按鈕、列印、下拉選單
                    tbSdate.Enabled = _enbleControl;
                    btnSearch.Enabled = _enbleControl;
                    btnPrint.Enabled = _enbleControl;
                    comboItems.Enabled = _enbleControl;
                    break;

                case 2: //刪除按鈕
                    if (dvList.Rows.Count == 0) btnDel.Enabled = false;
                    else btnDel.Enabled = _enbleControl;

                    break;

                case 3: //清單、數量、輸入按鈕
                    if (dvList.Rows.Count == 0)
                    {
                        laWarn.Visible = false;
                        laTotalQty.Text = string.Empty;
                        laCount.Text = string.Empty;
                        laEAN.Text = string.Empty;
                        tbQty.Text = string.Empty;
                        tbQty.Enabled = false;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        laWarn.Visible = false;
                        laTotalQty.Text = string.Empty;
                        laCount.Text = string.Empty;
                        laEAN.Text = string.Empty;
                        tbQty.Text = string.Empty;
                        tbQty.Enabled = _enbleControl;
                        btnSave.Enabled = _enbleControl;
                    }
                    break;
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    tbEmpCode.Focus();
                    tbEmpCode.Select(0, tbEmpCode.Text.Length);
                    laEmpName.Text = string.Empty;

                    EnableControl(false, 1);
                    EnableControl(false, 2);
                    EnableControl(false, 3);
                    dvList.DataSource = defaultDT;
                    MessageBox.Show("資料庫中查無此員工編號!");
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    EnableControl(true, 1);
                    GetComboBoxItems();
                }
            }
            catch (Exception ex)
            {
                logger.Error("取得使用者名稱發生錯誤: " + ex.ToString());
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.Select(0, tbEmpCode.Text.Length);
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && ((TextBox)sender).SelectionLength == 0)
            {
                tbEmpCode.Select(0, tbEmpCode.Text.Length);
                GetEmpName(tbEmpCode.Text);
            }
        }

        /// <summary>
        /// 取得下拉選單內容
        /// </summary>
        private void GetComboBoxItems()
        {
            try
            {
                string searchDate = tbSdate.Value.ToString("yyyyMMdd");
                string sqlCmd = sql.GetCommand("GetS2OInfo", new string[] { searchDate, storeCode });

                //先清空項目
                dvList.DataSource = defaultDT;
                laEAN.Text = string.Empty;
                tbQty.Text = string.Empty;

                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    comboItems.DataSource = new BindingSource(tmpDT, null);
                    comboItems.DisplayMember = "Text";
                    comboItems.ValueMember = "Value";
                    comboItems.SelectedIndex = 0;
                    comboItems.Focus();
                }
                else
                {
                    comboItems.DataSource = new DataTable();
                    //tbSdate.Focus();
                    MessageBox.Show("該日期無相關調撥單據!");
                }
            }
            catch (Exception ex)
            {
                logger.Error("取得下拉選單內容發生錯誤: " + ex.ToString());
                MessageBox.Show("取得下拉選單內容發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboItems.SelectedIndex == -1)
                    MessageBox.Show("請選擇查詢單號!");
                else
                {
                    //先清除控制項目在做查詢
                    laEAN.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    btnDel.Enabled = false;
                    laCount.Text = string.Empty;
                    laTotalQty.Text = string.Empty;
                    laWarn.Visible = false;

                    string sqlCmd = sql.GetCommand("GetS2otdc1Data", new string[] { comboItems.SelectedValue.ToString(), p_spbup });
                    DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (tmpDT.Rows.Count > 0)
                    {
                        dvList.DataSource = tmpDT;
                        defaultDT = tmpDT.Clone();

                        laCount.Text = tmpDT.Rows.Count.ToString();
                        laTotalQty.Text = tmpDT.AsEnumerable().Sum(q => decimal.Parse(q[2].ToString())).ToString("0.###");

                        //非當天不可進行資料刪除
                        if (tbSdate.Value != DateTime.Today)
                        {
                            btnDel.Enabled = false;
                            laWarn.Text = "非當日作業，資料不可異動!";
                            laWarn.Visible = true;

                            //Disable Checkbox
                            foreach (DataGridViewRow row in dvList.Rows)
                                row.Cells["cb"].ReadOnly = true;

                            //移除事件
                            dvList.CellMouseDoubleClick -= dvList_CellMouseDoubleClick;
                        }
                        else
                        {
                            btnDel.Enabled = true;
                            laWarn.Visible = false;

                            //添加事件
                            dvList.CellMouseDoubleClick += dvList_CellMouseDoubleClick;
                        }
                    }
                    else
                    {
                        dvList.DataSource = defaultDT;
                        MessageBox.Show("查無相關資訊!");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("查詢命令發生錯誤: " + ex.ToString());
                MessageBox.Show("查詢命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// Grid 滑鼠點擊事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    //移除所有底色
                    foreach (DataGridViewRow row in dvList.Rows)
                        row.DefaultCellStyle.BackColor = Color.White;

                    //重新添加添加底色
                    dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Pink;

                    int rowIdx = e.RowIndex;
                    bool sapSign = dvList.Rows[rowIdx].Cells[1].Value.ToString() == "X" ? true : false;

                    laKey.Text = comboItems.SelectedValue.ToString() + ";" + dvList.Rows[rowIdx].Cells[10].Value.ToString() + ";" + dvList.Rows[rowIdx].Cells[11].Value.ToString();
                    laEAN.Text = dvList.Rows[rowIdx].Cells[2].Value.ToString();
                    tbQty.Text = decimal.Parse(dvList.Rows[rowIdx].Cells[3].Value.ToString()).ToString("0.###");

                    if (sapSign)
                    {
                        laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
                        laWarn.Visible = true;
                        tbQty.Enabled = false;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        laWarn.Visible = false;

                        if (tbSdate.Value != DateTime.Today)
                        {
                            tbQty.Enabled = false;
                            btnSave.Enabled = false;
                        }
                        else
                        {
                            tbQty.Enabled = true;
                            btnSave.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("滑鼠點擊命令發生錯誤: " + ex.ToString());
                MessageBox.Show("滑鼠點擊命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //調撥數量僅可介於 0 < qty < 399
                int tempInt = 0;
                if (!Int32.TryParse(tbQty.Text, out tempInt))
                {
                    MessageBox.Show("調撥數量僅可輸入整數數值，請重新輸入!");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }
                else if (tempInt <= 0 || tempInt >= 399)
                {
                    MessageBox.Show("調撥數量需大於0且小於399!\n 0 < 調撥數量 < 399");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                DialogResult dialogResult = MessageBox.Show("您確定異動調撥數量?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string bednr = laKey.Text.Split(';')[0];
                    string ean = laEAN.Text;
                    string werks = laKey.Text.Split(';')[1];
                    string reswk = laKey.Text.Split(';')[2];
                    string qty = tbQty.Text;
                    string chusn = empCodeReal; //tbEmpCode.Text;

                    string sqlCmd = sql.GetCommand("GetUpdateCommand", new string[] { bednr, ean, werks, reswk, qty, chusn });
                    if (dbConn.ExecSql(sqlCmd) == 0)
                    {
                        MessageBox.Show("未更新任何項目，請聯繫管理人員");
                        logger.Error("未更新任何項目，請聯繫管理人員");
                        logger.Info("SQL Command: " + sqlCmd);
                    }
                    else
                    {
                        laWarn.Visible = false;
                        laEAN.Text = string.Empty;
                        tbQty.Text = string.Empty;
                        tbQty.Enabled = false;
                        btnSearch_Click(null, null);
                        MessageBox.Show("修改成功!");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("儲存命令發生錯誤: " + ex.ToString());
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 0;
                string sqlCmd = string.Empty;
                foreach (DataGridViewRow row in dvList.Rows)
                {
                    if (Boolean.Parse(row.Cells[0].FormattedValue.ToString()))
                    {
                        count++;
                        sqlCmd += sql.GetCommand("GetDeleteCommand", new string[] { 
                        comboItems.SelectedValue.ToString(), //bednr
                        row.Cells[2].Value.ToString(), //ean
                        row.Cells[10].Value.ToString(), //werks
                        row.Cells[11].Value.ToString(), //reswk
                        empCodeReal //tbEmpCode.Text //chusn
                        });
                    }
                }

                if (count > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("您確定刪除" + count.ToString() + "筆資料?", "警告", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                        if (dbConn.ExecSql(sqlCmd) > 0)
                        {
                            laWarn.Visible = false;
                            laEAN.Text = string.Empty;
                            tbQty.Text = string.Empty;
                            tbQty.Enabled = false;
                            MessageBox.Show("刪除成功!");
                            btnSearch_Click(null, null);
                        }
                        else
                            MessageBox.Show("無資料受到影響，請聯繫系統管理員");
                }
                else
                    MessageBox.Show("請勾選欲刪除資料!");
            }
            catch (Exception ex)
            {
                logger.Error("刪除命令發生錯誤: " + ex.ToString());
                MessageBox.Show("刪除命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// Grid Edit Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvList_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dvList.IsCurrentCellDirty)
                dvList.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }
        private void dvList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dvList.Rows.Count > 0)
            {
                string procd = dvList.Rows[e.RowIndex].Cells["PROCD"].Value.ToString();
                if (procd == "X")
                {
                    DataGridViewCheckBoxCell cb = (DataGridViewCheckBoxCell)dvList.Rows[e.RowIndex].Cells["cb"];
                    if ((Boolean)cb.Value)
                    {
                        MessageBox.Show("此項目SAP已完成處理，不可進行刪除");
                        cb.Value = false;
                        dvList.RefreshEdit();
                        dvList.Refresh();
                    }
                };
            }
        }

        /// <summary>
        /// 列印物流單
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboItems.SelectedIndex == -1)
                    MessageBox.Show("請選擇列印單號!");
                else
                {
                    //22碼序號
                    string bednr = comboItems.SelectedValue.ToString();
                    ProcessStartInfo processInfo = new ProcessStartInfo(printProgramPath);
                    processInfo.Arguments = bednr;
                    using (Process process = new Process())
                    {
                        process.StartInfo = processInfo;
                        process.Start();
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("列印物流單發生錯誤: " + ex.ToString());
                MessageBox.Show("列印物流單發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 日期驗證
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSdate_Validated(object sender, EventArgs e)
        {
            if (dbConn != null)
                GetComboBoxItems();


            if (tbSdate.Value != DateTime.Today)
            {
                EnableControl(false, 2);
                EnableControl(false, 3);
            }
            else
            {
                EnableControl(true, 2);
                EnableControl(true, 3);
            }
        }
    }
}
