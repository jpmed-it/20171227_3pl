﻿namespace JPMed_Windows.SubForms.門市前台作業
{
    partial class UC15
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.laMainStockName = new System.Windows.Forms.Label();
            this.laMainStockCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.cb = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.退貨確認 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退貨至 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.商品條碼EAN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退貨量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.調出門市庫存量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.備註 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSGRU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WERKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESWK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LABOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RUECK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laTotalQty = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.laWarn = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.laCount = new System.Windows.Forms.Label();
            this.laEAN = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.comboItems = new System.Windows.Forms.ComboBox();
            this.tbSdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.la1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboMemo = new System.Windows.Forms.ComboBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.laStart = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // laMainStockName
            // 
            this.laMainStockName.AutoSize = true;
            this.laMainStockName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laMainStockName.Location = new System.Drawing.Point(202, 108);
            this.laMainStockName.Name = "laMainStockName";
            this.laMainStockName.Size = new System.Drawing.Size(40, 16);
            this.laMainStockName.TabIndex = 96;
            this.laMainStockName.Text = "總倉";
            // 
            // laMainStockCode
            // 
            this.laMainStockCode.AutoSize = true;
            this.laMainStockCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laMainStockCode.Location = new System.Drawing.Point(157, 108);
            this.laMainStockCode.Name = "laMainStockCode";
            this.laMainStockCode.Size = new System.Drawing.Size(46, 16);
            this.laMainStockCode.TabIndex = 95;
            this.laMainStockCode.Text = "TDC2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(99, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 94;
            this.label8.Text = "退貨至";
            // 
            // laKey
            // 
            this.laKey.AutoSize = true;
            this.laKey.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laKey.Location = new System.Drawing.Point(490, 136);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(0, 16);
            this.laKey.TabIndex = 93;
            this.laKey.Visible = false;
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cb,
            this.退貨確認,
            this.退貨至,
            this.商品條碼EAN,
            this.退貨量,
            this.品名,
            this.調出門市庫存量,
            this.備註,
            this.BSGRU,
            this.WERKS,
            this.RESWK,
            this.PROCD,
            this.LABOR,
            this.RUECK});
            this.dvList.Location = new System.Drawing.Point(14, 265);
            this.dvList.Name = "dvList";
            this.dvList.RowTemplate.Height = 24;
            this.dvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvList.Size = new System.Drawing.Size(759, 262);
            this.dvList.TabIndex = 90;
            // 
            // cb
            // 
            this.cb.HeaderText = "";
            this.cb.Name = "cb";
            this.cb.Width = 23;
            // 
            // 退貨確認
            // 
            this.退貨確認.DataPropertyName = "退貨確認";
            this.退貨確認.HeaderText = "退貨確認";
            this.退貨確認.Name = "退貨確認";
            this.退貨確認.ReadOnly = true;
            this.退貨確認.Width = 80;
            // 
            // 退貨至
            // 
            this.退貨至.DataPropertyName = "退貨至";
            this.退貨至.HeaderText = "退貨至";
            this.退貨至.Name = "退貨至";
            this.退貨至.ReadOnly = true;
            // 
            // 商品條碼EAN
            // 
            this.商品條碼EAN.DataPropertyName = "商品條碼(EAN)";
            this.商品條碼EAN.HeaderText = "商品條碼(EAN)";
            this.商品條碼EAN.Name = "商品條碼EAN";
            this.商品條碼EAN.ReadOnly = true;
            // 
            // 退貨量
            // 
            this.退貨量.DataPropertyName = "退貨量";
            dataGridViewCellStyle2.Format = "0.###";
            this.退貨量.DefaultCellStyle = dataGridViewCellStyle2;
            this.退貨量.HeaderText = "退貨量";
            this.退貨量.Name = "退貨量";
            this.退貨量.ReadOnly = true;
            // 
            // 品名
            // 
            this.品名.DataPropertyName = "品名";
            this.品名.HeaderText = "品名";
            this.品名.Name = "品名";
            this.品名.ReadOnly = true;
            // 
            // 調出門市庫存量
            // 
            this.調出門市庫存量.DataPropertyName = "調出門市庫存量";
            dataGridViewCellStyle3.Format = "0.###";
            this.調出門市庫存量.DefaultCellStyle = dataGridViewCellStyle3;
            this.調出門市庫存量.HeaderText = "庫存量";
            this.調出門市庫存量.Name = "調出門市庫存量";
            this.調出門市庫存量.ReadOnly = true;
            // 
            // 備註
            // 
            this.備註.DataPropertyName = "備註";
            this.備註.HeaderText = "備註";
            this.備註.Name = "備註";
            this.備註.ReadOnly = true;
            // 
            // BSGRU
            // 
            this.BSGRU.DataPropertyName = "BSGRU";
            this.BSGRU.HeaderText = "BSGRU";
            this.BSGRU.Name = "BSGRU";
            this.BSGRU.ReadOnly = true;
            this.BSGRU.Visible = false;
            // 
            // WERKS
            // 
            this.WERKS.DataPropertyName = "WERKS";
            this.WERKS.HeaderText = "WERKS";
            this.WERKS.Name = "WERKS";
            this.WERKS.ReadOnly = true;
            this.WERKS.Visible = false;
            // 
            // RESWK
            // 
            this.RESWK.DataPropertyName = "RESWK";
            this.RESWK.HeaderText = "RESWK";
            this.RESWK.Name = "RESWK";
            this.RESWK.ReadOnly = true;
            this.RESWK.Visible = false;
            // 
            // PROCD
            // 
            this.PROCD.DataPropertyName = "PROCD";
            this.PROCD.HeaderText = "PROCD";
            this.PROCD.Name = "PROCD";
            this.PROCD.ReadOnly = true;
            this.PROCD.Visible = false;
            // 
            // LABOR
            // 
            this.LABOR.DataPropertyName = "LABOR";
            this.LABOR.HeaderText = "LABOR";
            this.LABOR.Name = "LABOR";
            this.LABOR.ReadOnly = true;
            this.LABOR.Visible = false;
            // 
            // RUECK
            // 
            this.RUECK.DataPropertyName = "RUECK";
            this.RUECK.HeaderText = "RUECK";
            this.RUECK.Name = "RUECK";
            this.RUECK.ReadOnly = true;
            this.RUECK.Visible = false;
            // 
            // laTotalQty
            // 
            this.laTotalQty.AutoSize = true;
            this.laTotalQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laTotalQty.Location = new System.Drawing.Point(271, 237);
            this.laTotalQty.Name = "laTotalQty";
            this.laTotalQty.Size = new System.Drawing.Size(16, 16);
            this.laTotalQty.TabIndex = 89;
            this.laTotalQty.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(177, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 88;
            this.label7.Text = "目前總量：";
            // 
            // laWarn
            // 
            this.laWarn.AutoSize = true;
            this.laWarn.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarn.ForeColor = System.Drawing.Color.Red;
            this.laWarn.Location = new System.Drawing.Point(404, 237);
            this.laWarn.Name = "laWarn";
            this.laWarn.Size = new System.Drawing.Size(259, 16);
            this.laWarn.TabIndex = 92;
            this.laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
            this.laWarn.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(274, 11);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(66, 27);
            this.btnExit.TabIndex = 91;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(11, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 87;
            this.label6.Text = "目前總筆數：";
            // 
            // laCount
            // 
            this.laCount.AutoSize = true;
            this.laCount.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laCount.Location = new System.Drawing.Point(121, 237);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(16, 16);
            this.laCount.TabIndex = 86;
            this.laCount.Text = "0";
            // 
            // laEAN
            // 
            this.laEAN.AutoSize = true;
            this.laEAN.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEAN.Location = new System.Drawing.Point(227, 136);
            this.laEAN.Name = "laEAN";
            this.laEAN.Size = new System.Drawing.Size(41, 16);
            this.laEAN.TabIndex = 85;
            this.laEAN.Text = "T001";
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDel.Location = new System.Drawing.Point(407, 162);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(103, 27);
            this.btnDel.TabIndex = 84;
            this.btnDel.Text = "刪除(勾者)";
            this.btnDel.UseVisualStyleBackColor = true;
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbQty.Location = new System.Drawing.Point(168, 162);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(125, 27);
            this.tbQty.TabIndex = 82;
            this.tbQty.Text = "1111111111.000";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSave.Location = new System.Drawing.Point(299, 162);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(102, 27);
            this.btnSave.TabIndex = 83;
            this.btnSave.Text = "輸入(覆蓋)";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(99, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 81;
            this.label5.Text = "退貨量：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(11, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 79;
            this.label3.Text = "輸入資料：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(99, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 80;
            this.label4.Text = "商品條碼(EAN)：";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnPrint.Location = new System.Drawing.Point(387, 74);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(110, 27);
            this.btnPrint.TabIndex = 78;
            this.btnPrint.Text = "列印物流單";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(326, 74);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(55, 27);
            this.btnSearch.TabIndex = 77;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // comboItems
            // 
            this.comboItems.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboItems.FormattingEnabled = true;
            this.comboItems.Location = new System.Drawing.Point(239, 76);
            this.comboItems.Name = "comboItems";
            this.comboItems.Size = new System.Drawing.Size(79, 24);
            this.comboItems.TabIndex = 76;
            // 
            // tbSdate
            // 
            this.tbSdate.CalendarFont = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Location = new System.Drawing.Point(102, 74);
            this.tbSdate.Name = "tbSdate";
            this.tbSdate.Size = new System.Drawing.Size(131, 27);
            this.tbSdate.TabIndex = 75;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(11, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 74;
            this.label2.Text = "退貨日期：";
            // 
            // laEmpName
            // 
            this.laEmpName.AutoSize = true;
            this.laEmpName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEmpName.Location = new System.Drawing.Point(177, 45);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(56, 16);
            this.laEmpName.TabIndex = 73;
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEmpCode.Location = new System.Drawing.Point(63, 40);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 27);
            this.tbEmpCode.TabIndex = 72;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(11, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 71;
            this.label1.Text = "員編：";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(152, 15);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 70;
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(99, 15);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 69;
            this.laSCode.Text = "T001";
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(11, 15);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 68;
            this.la1.Text = "所在門市：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(99, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 16);
            this.label9.TabIndex = 97;
            this.label9.Text = "備註：";
            // 
            // comboMemo
            // 
            this.comboMemo.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboMemo.FormattingEnabled = true;
            this.comboMemo.Location = new System.Drawing.Point(161, 198);
            this.comboMemo.Name = "comboMemo";
            this.comboMemo.Size = new System.Drawing.Size(240, 24);
            this.comboMemo.TabIndex = 98;
            // 
            // btnCheck
            // 
            this.btnCheck.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCheck.Location = new System.Drawing.Point(407, 196);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(103, 27);
            this.btnCheck.TabIndex = 99;
            this.btnCheck.Text = "全部確認";
            this.btnCheck.UseVisualStyleBackColor = true;
            // 
            // laStart
            // 
            this.laStart.AutoSize = true;
            this.laStart.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laStart.ForeColor = System.Drawing.Color.Red;
            this.laStart.Location = new System.Drawing.Point(85, 202);
            this.laStart.Name = "laStart";
            this.laStart.Size = new System.Drawing.Size(16, 16);
            this.laStart.TabIndex = 100;
            this.laStart.Text = "*";
            this.laStart.Visible = false;
            // 
            // UC15
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.Controls.Add(this.laStart);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.comboMemo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.laMainStockName);
            this.Controls.Add(this.laMainStockCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.laTotalQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.laWarn);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.laEAN);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.comboItems);
            this.Controls.Add(this.tbSdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.la1);
            this.Name = "UC15";
            this.Size = new System.Drawing.Size(784, 537);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laMainStockName;
        private System.Windows.Forms.Label laMainStockCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.Label laTotalQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label laWarn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laCount;
        private System.Windows.Forms.Label laEAN;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox comboItems;
        private System.Windows.Forms.DateTimePicker tbSdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboMemo;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label laStart;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cb;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退貨確認;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退貨至;
        private System.Windows.Forms.DataGridViewTextBoxColumn 商品條碼EAN;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退貨量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 調出門市庫存量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 備註;
        private System.Windows.Forms.DataGridViewTextBoxColumn BSGRU;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS;
        private System.Windows.Forms.DataGridViewTextBoxColumn RESWK;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn LABOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn RUECK;
    }
}
