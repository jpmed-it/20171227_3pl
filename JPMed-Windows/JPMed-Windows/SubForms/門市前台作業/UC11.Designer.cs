﻿namespace JPMed_Windows.SubForms.門市前台作業
{
    partial class UC11
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.laKey = new System.Windows.Forms.Label();
            this.laWarn = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.datBADAT = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.la1 = new System.Windows.Forms.Label();
            this.btnExportToExcel = new System.Windows.Forms.Button();
            this.tbEan = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbMAABC = new System.Windows.Forms.ComboBox();
            this.tbMATKL = new System.Windows.Forms.TextBox();
            this.tbMAKTX = new System.Windows.Forms.TextBox();
            this.cbClass1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCalORDERM = new System.Windows.Forms.Button();
            this.lblORDERM = new System.Windows.Forms.Label();
            this.lblSALESM = new System.Windows.Forms.Label();
            this.MATKLItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EAN11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAABC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAKTX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HINUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_S = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LONUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MNG03 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MNG07 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_C = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShopLABST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TDCLABST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MATNR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EBELN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_Q = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BADAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // laKey
            // 
            this.laKey.AutoSize = true;
            this.laKey.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laKey.Location = new System.Drawing.Point(490, 132);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(0, 16);
            this.laKey.TabIndex = 64;
            this.laKey.Visible = false;
            // 
            // laWarn
            // 
            this.laWarn.AutoSize = true;
            this.laWarn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarn.ForeColor = System.Drawing.Color.Red;
            this.laWarn.Location = new System.Drawing.Point(416, 211);
            this.laWarn.Name = "laWarn";
            this.laWarn.Size = new System.Drawing.Size(259, 16);
            this.laWarn.TabIndex = 63;
            this.laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
            this.laWarn.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(553, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(122, 27);
            this.btnExit.TabIndex = 62;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            this.dvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MATKLItem,
            this.EAN11,
            this.MAABC,
            this.MAKTX,
            this.HINUM,
            this.MENGE_S,
            this.LONUM,
            this.MNG03,
            this.MNG07,
            this.MENGE_C,
            this.ShopLABST,
            this.TDCLABST,
            this.MATNR,
            this.EBELN,
            this.MENGE_Q,
            this.PROCD,
            this.BADAT,
            this.PRICE});
            this.dvList.Location = new System.Drawing.Point(3, 231);
            this.dvList.MinimumSize = new System.Drawing.Size(768, 300);
            this.dvList.MultiSelect = false;
            this.dvList.Name = "dvList";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvList.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dvList.RowTemplate.Height = 24;
            this.dvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvList.Size = new System.Drawing.Size(778, 300);
            this.dvList.TabIndex = 61;
            this.dvList.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dvList_CellValidating);
            this.dvList.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dvList_KeyUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(11, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 52;
            this.label5.Text = "品名：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(11, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 51;
            this.label4.Text = "商品條碼：";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(553, 73);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(122, 27);
            this.btnSearch.TabIndex = 48;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // datBADAT
            // 
            this.datBADAT.CalendarFont = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.datBADAT.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.datBADAT.Location = new System.Drawing.Point(102, 73);
            this.datBADAT.Name = "datBADAT";
            this.datBADAT.Size = new System.Drawing.Size(131, 27);
            this.datBADAT.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(11, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "需求日期：";
            // 
            // laEmpName
            // 
            this.laEmpName.AutoSize = true;
            this.laEmpName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEmpName.Location = new System.Drawing.Point(216, 45);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(56, 16);
            this.laEmpName.TabIndex = 44;
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEmpCode.Location = new System.Drawing.Point(102, 40);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 27);
            this.tbEmpCode.TabIndex = 43;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(11, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 42;
            this.label1.Text = "員編：";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(152, 15);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 41;
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(99, 15);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 40;
            this.laSCode.Text = "T001";
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(11, 15);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 39;
            this.la1.Text = "所在門市：";
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExportToExcel.Location = new System.Drawing.Point(553, 137);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(122, 27);
            this.btnExportToExcel.TabIndex = 72;
            this.btnExportToExcel.Text = "匯出EXCEL";
            this.btnExportToExcel.UseVisualStyleBackColor = true;
            // 
            // tbEan
            // 
            this.tbEan.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEan.Location = new System.Drawing.Point(102, 106);
            this.tbEan.MaxLength = 14;
            this.tbEan.Name = "tbEan";
            this.tbEan.Size = new System.Drawing.Size(131, 27);
            this.tbEan.TabIndex = 73;
            this.tbEan.Text = "4714504131332";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(324, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 16);
            this.label10.TabIndex = 74;
            this.label10.Text = "補貨量建議表";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(295, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 16);
            this.label3.TabIndex = 75;
            this.label3.Text = "ABC分類：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(295, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 16);
            this.label8.TabIndex = 76;
            this.label8.Text = "物料群組：";
            // 
            // cbMAABC
            // 
            this.cbMAABC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMAABC.FormattingEnabled = true;
            this.cbMAABC.Items.AddRange(new object[] {
            "全部",
            "S    熱賣商品",
            "A   重要商品",
            "B   商品 - 中度重要",
            "C   商品 - 低度重要",
            "X   特殊定番",
            "Y   待處分品",
            "Z   PB商品"});
            this.cbMAABC.Location = new System.Drawing.Point(382, 78);
            this.cbMAABC.Name = "cbMAABC";
            this.cbMAABC.Size = new System.Drawing.Size(131, 20);
            this.cbMAABC.TabIndex = 77;
            // 
            // tbMATKL
            // 
            this.tbMATKL.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbMATKL.Location = new System.Drawing.Point(382, 109);
            this.tbMATKL.MaxLength = 14;
            this.tbMATKL.Name = "tbMATKL";
            this.tbMATKL.Size = new System.Drawing.Size(131, 27);
            this.tbMATKL.TabIndex = 79;
            // 
            // tbMAKTX
            // 
            this.tbMAKTX.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbMAKTX.Location = new System.Drawing.Point(102, 137);
            this.tbMAKTX.MaxLength = 14;
            this.tbMAKTX.Name = "tbMAKTX";
            this.tbMAKTX.Size = new System.Drawing.Size(131, 27);
            this.tbMAKTX.TabIndex = 80;
            // 
            // cbClass1
            // 
            this.cbClass1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClass1.FormattingEnabled = true;
            this.cbClass1.Items.AddRange(new object[] {
            "全部",
            "1   藥品",
            "2   醫療用品",
            "3   保健食品",
            "4   專櫃彩妝保養",
            "5   一般彩妝保養",
            "6   生活用品",
            "7   食品"});
            this.cbClass1.Location = new System.Drawing.Point(382, 45);
            this.cbClass1.Name = "cbClass1";
            this.cbClass1.Size = new System.Drawing.Size(131, 20);
            this.cbClass1.TabIndex = 82;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(295, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 81;
            this.label6.Text = "大分類：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(11, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 16);
            this.label7.TabIndex = 83;
            this.label7.Text = "本次採購金額：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(295, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 16);
            this.label9.TabIndex = 84;
            this.label9.Text = "上週銷售金額：";
            // 
            // btnCalORDERM
            // 
            this.btnCalORDERM.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCalORDERM.Location = new System.Drawing.Point(553, 174);
            this.btnCalORDERM.Name = "btnCalORDERM";
            this.btnCalORDERM.Size = new System.Drawing.Size(122, 27);
            this.btnCalORDERM.TabIndex = 85;
            this.btnCalORDERM.Text = "計算採購金額";
            this.btnCalORDERM.UseVisualStyleBackColor = true;
            // 
            // lblORDERM
            // 
            this.lblORDERM.AutoSize = true;
            this.lblORDERM.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblORDERM.Location = new System.Drawing.Point(126, 178);
            this.lblORDERM.Name = "lblORDERM";
            this.lblORDERM.Size = new System.Drawing.Size(34, 16);
            this.lblORDERM.TabIndex = 86;
            this.lblORDERM.Text = "N/A";
            // 
            // lblSALESM
            // 
            this.lblSALESM.AutoSize = true;
            this.lblSALESM.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblSALESM.Location = new System.Drawing.Point(408, 178);
            this.lblSALESM.Name = "lblSALESM";
            this.lblSALESM.Size = new System.Drawing.Size(34, 16);
            this.lblSALESM.TabIndex = 87;
            this.lblSALESM.Text = "N/A";
            // 
            // MATKLItem
            // 
            this.MATKLItem.DataPropertyName = "CLASS1";
            this.MATKLItem.HeaderText = "大分類";
            this.MATKLItem.Name = "MATKLItem";
            this.MATKLItem.ReadOnly = true;
            this.MATKLItem.Width = 66;
            // 
            // EAN11
            // 
            this.EAN11.DataPropertyName = "EAN11";
            this.EAN11.HeaderText = "條碼";
            this.EAN11.MinimumWidth = 85;
            this.EAN11.Name = "EAN11";
            this.EAN11.ReadOnly = true;
            this.EAN11.Width = 85;
            // 
            // MAABC
            // 
            this.MAABC.DataPropertyName = "MAABC";
            dataGridViewCellStyle2.Format = "0.###";
            this.MAABC.DefaultCellStyle = dataGridViewCellStyle2;
            this.MAABC.HeaderText = "ABC分類";
            this.MAABC.MinimumWidth = 78;
            this.MAABC.Name = "MAABC";
            this.MAABC.ReadOnly = true;
            this.MAABC.Width = 78;
            // 
            // MAKTX
            // 
            this.MAKTX.DataPropertyName = "MAKTX";
            dataGridViewCellStyle3.Format = "0.###";
            this.MAKTX.DefaultCellStyle = dataGridViewCellStyle3;
            this.MAKTX.HeaderText = "品名";
            this.MAKTX.MinimumWidth = 54;
            this.MAKTX.Name = "MAKTX";
            this.MAKTX.ReadOnly = true;
            this.MAKTX.Width = 54;
            // 
            // HINUM
            // 
            this.HINUM.DataPropertyName = "HINUM";
            dataGridViewCellStyle4.NullValue = null;
            this.HINUM.DefaultCellStyle = dataGridViewCellStyle4;
            this.HINUM.HeaderText = "最高補貨量";
            this.HINUM.MinimumWidth = 90;
            this.HINUM.Name = "HINUM";
            this.HINUM.ReadOnly = true;
            this.HINUM.Width = 90;
            // 
            // MENGE_S
            // 
            this.MENGE_S.DataPropertyName = "MENGE_S";
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.MENGE_S.DefaultCellStyle = dataGridViewCellStyle5;
            this.MENGE_S.HeaderText = "建議量";
            this.MENGE_S.MinimumWidth = 66;
            this.MENGE_S.Name = "MENGE_S";
            this.MENGE_S.ReadOnly = true;
            this.MENGE_S.Width = 66;
            // 
            // LONUM
            // 
            this.LONUM.DataPropertyName = "LONUM";
            this.LONUM.HeaderText = "最低補貨量";
            this.LONUM.MinimumWidth = 90;
            this.LONUM.Name = "LONUM";
            this.LONUM.ReadOnly = true;
            this.LONUM.Width = 90;
            // 
            // MNG03
            // 
            this.MNG03.DataPropertyName = "MNG03";
            this.MNG03.HeaderText = "在途量";
            this.MNG03.Name = "MNG03";
            this.MNG03.ReadOnly = true;
            this.MNG03.Width = 66;
            // 
            // MNG07
            // 
            this.MNG07.DataPropertyName = "MNG07";
            this.MNG07.HeaderText = "90天週銷量";
            this.MNG07.Name = "MNG07";
            this.MNG07.ReadOnly = true;
            this.MNG07.Width = 90;
            // 
            // MENGE_C
            // 
            this.MENGE_C.DataPropertyName = "MENGE_C";
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.MENGE_C.DefaultCellStyle = dataGridViewCellStyle6;
            this.MENGE_C.HeaderText = "確認量";
            this.MENGE_C.MinimumWidth = 66;
            this.MENGE_C.Name = "MENGE_C";
            this.MENGE_C.Width = 66;
            // 
            // ShopLABST
            // 
            this.ShopLABST.DataPropertyName = "ShopLABST";
            dataGridViewCellStyle7.Format = "N0";
            this.ShopLABST.DefaultCellStyle = dataGridViewCellStyle7;
            this.ShopLABST.HeaderText = "門市庫存";
            this.ShopLABST.MinimumWidth = 78;
            this.ShopLABST.Name = "ShopLABST";
            this.ShopLABST.ReadOnly = true;
            this.ShopLABST.Width = 78;
            // 
            // TDCLABST
            // 
            this.TDCLABST.DataPropertyName = "TDCLABST";
            dataGridViewCellStyle8.Format = "N0";
            this.TDCLABST.DefaultCellStyle = dataGridViewCellStyle8;
            this.TDCLABST.HeaderText = "總倉庫存";
            this.TDCLABST.MinimumWidth = 78;
            this.TDCLABST.Name = "TDCLABST";
            this.TDCLABST.ReadOnly = true;
            this.TDCLABST.Width = 78;
            // 
            // MATNR
            // 
            this.MATNR.DataPropertyName = "MATNR";
            this.MATNR.HeaderText = "SAP商品編號";
            this.MATNR.MinimumWidth = 110;
            this.MATNR.Name = "MATNR";
            this.MATNR.ReadOnly = true;
            this.MATNR.Width = 110;
            // 
            // EBELN
            // 
            this.EBELN.DataPropertyName = "EBELN";
            this.EBELN.HeaderText = "SAP單號(採購單)";
            this.EBELN.MinimumWidth = 120;
            this.EBELN.Name = "EBELN";
            this.EBELN.ReadOnly = true;
            this.EBELN.Width = 120;
            // 
            // MENGE_Q
            // 
            this.MENGE_Q.DataPropertyName = "MENGE_Q";
            this.MENGE_Q.HeaderText = "供應商庫存量";
            this.MENGE_Q.MinimumWidth = 110;
            this.MENGE_Q.Name = "MENGE_Q";
            this.MENGE_Q.Width = 110;
            // 
            // PROCD
            // 
            this.PROCD.DataPropertyName = "PROCD";
            this.PROCD.HeaderText = "SAP處理註記";
            this.PROCD.MinimumWidth = 98;
            this.PROCD.Name = "PROCD";
            this.PROCD.ReadOnly = true;
            this.PROCD.Visible = false;
            this.PROCD.Width = 98;
            // 
            // BADAT
            // 
            this.BADAT.DataPropertyName = "BADAT";
            this.BADAT.HeaderText = "BADAT";
            this.BADAT.MinimumWidth = 69;
            this.BADAT.Name = "BADAT";
            this.BADAT.Visible = false;
            this.BADAT.Width = 69;
            // 
            // PRICE
            // 
            this.PRICE.DataPropertyName = "PRICE";
            this.PRICE.HeaderText = "PRICE";
            this.PRICE.Name = "PRICE";
            this.PRICE.ReadOnly = true;
            this.PRICE.Visible = false;
            this.PRICE.Width = 63;
            // 
            // UC11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.Controls.Add(this.lblSALESM);
            this.Controls.Add(this.lblORDERM);
            this.Controls.Add(this.btnCalORDERM);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbClass1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbMAKTX);
            this.Controls.Add(this.tbMATKL);
            this.Controls.Add(this.cbMAABC);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbEan);
            this.Controls.Add(this.btnExportToExcel);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.laWarn);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.datBADAT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.la1);
            this.Name = "UC11";
            this.Size = new System.Drawing.Size(784, 545);
            this.Resize += new System.EventHandler(this.UC11_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.Label laWarn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker datBADAT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Button btnExportToExcel;
        private System.Windows.Forms.TextBox tbEan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbMAABC;
        private System.Windows.Forms.TextBox tbMATKL;
        private System.Windows.Forms.TextBox tbMAKTX;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.ComboBox cbClass1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCalORDERM;
        private System.Windows.Forms.Label lblORDERM;
        private System.Windows.Forms.Label lblSALESM;
        private System.Windows.Forms.DataGridViewTextBoxColumn MATKLItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn EAN11;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAABC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAKTX;
        private System.Windows.Forms.DataGridViewTextBoxColumn HINUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_S;
        private System.Windows.Forms.DataGridViewTextBoxColumn LONUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn MNG03;
        private System.Windows.Forms.DataGridViewTextBoxColumn MNG07;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_C;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShopLABST;
        private System.Windows.Forms.DataGridViewTextBoxColumn TDCLABST;
        private System.Windows.Forms.DataGridViewTextBoxColumn MATNR;
        private System.Windows.Forms.DataGridViewTextBoxColumn EBELN;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_Q;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn BADAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRICE;
    }
}
