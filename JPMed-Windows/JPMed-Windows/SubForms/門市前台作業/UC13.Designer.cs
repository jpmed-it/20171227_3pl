﻿namespace JPMed_Windows.SubForms.門市前台作業
{
    partial class UC13
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.la1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.laEmpName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSdate = new System.Windows.Forms.DateTimePicker();
            this.comboItems = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.laEAN = new System.Windows.Forms.Label();
            this.laCount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.laTotalQty = new System.Windows.Forms.Label();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.cb = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EAN11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.調撥確認 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_R = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_GR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAKTX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WERKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UMMENGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LABST_OUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LABST_IN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WERKS1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESWK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExit = new System.Windows.Forms.Button();
            this.laWarn = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.btnCancelSelect = new System.Windows.Forms.Button();
            this.btnCheckSelect = new System.Windows.Forms.Button();
            this.btnCheck = new System.Windows.Forms.Button();
            this.cbDelivery = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(150, 11);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 14;
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(97, 11);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 13;
            this.laSCode.Text = "T001";
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(9, 11);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 12;
            this.la1.Text = "所在門市：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "員編：";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEmpCode.Location = new System.Drawing.Point(61, 36);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 27);
            this.tbEmpCode.TabIndex = 16;
            this.tbEmpCode.Text = "A1234567890";
            this.tbEmpCode.TextChanged += new System.EventHandler(this.tbEmpCode_TextChanged);
            this.tbEmpCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbEmpCode_KeyUp);
            // 
            // laEmpName
            // 
            this.laEmpName.AutoSize = true;
            this.laEmpName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEmpName.Location = new System.Drawing.Point(175, 41);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(56, 16);
            this.laEmpName.TabIndex = 17;
            this.laEmpName.Text = "王曉明";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(9, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "調撥日期：";
            // 
            // tbSdate
            // 
            this.tbSdate.CalendarFont = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Location = new System.Drawing.Point(100, 69);
            this.tbSdate.Name = "tbSdate";
            this.tbSdate.Size = new System.Drawing.Size(131, 27);
            this.tbSdate.TabIndex = 19;
            this.tbSdate.Validated += new System.EventHandler(this.tbSdate_Validated);
            // 
            // comboItems
            // 
            this.comboItems.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboItems.FormattingEnabled = true;
            this.comboItems.Location = new System.Drawing.Point(346, 72);
            this.comboItems.Name = "comboItems";
            this.comboItems.Size = new System.Drawing.Size(187, 24);
            this.comboItems.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(539, 70);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(55, 27);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnPrint.Location = new System.Drawing.Point(600, 70);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(110, 27);
            this.btnPrint.TabIndex = 22;
            this.btnPrint.Text = "列印物流單";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(9, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 23;
            this.label3.Text = "輸入資料：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(97, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "商品條碼(EAN)：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(97, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 26;
            this.label5.Text = "調撥數量：";
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbQty.Location = new System.Drawing.Point(191, 131);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(125, 27);
            this.tbQty.TabIndex = 27;
            this.tbQty.Text = "1111111111.000";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSave.Location = new System.Drawing.Point(322, 131);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(102, 27);
            this.btnSave.TabIndex = 28;
            this.btnSave.Text = "輸入(覆蓋)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDel.Location = new System.Drawing.Point(430, 131);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(103, 27);
            this.btnDel.TabIndex = 29;
            this.btnDel.Text = "刪除(勾者)";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // laEAN
            // 
            this.laEAN.AutoSize = true;
            this.laEAN.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEAN.Location = new System.Drawing.Point(225, 104);
            this.laEAN.Name = "laEAN";
            this.laEAN.Size = new System.Drawing.Size(41, 16);
            this.laEAN.TabIndex = 30;
            this.laEAN.Text = "T001";
            // 
            // laCount
            // 
            this.laCount.AutoSize = true;
            this.laCount.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laCount.Location = new System.Drawing.Point(119, 212);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(16, 16);
            this.laCount.TabIndex = 31;
            this.laCount.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(9, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 32;
            this.label6.Text = "目前總筆數：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(175, 212);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 33;
            this.label7.Text = "目前總量：";
            // 
            // laTotalQty
            // 
            this.laTotalQty.AutoSize = true;
            this.laTotalQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laTotalQty.Location = new System.Drawing.Point(269, 212);
            this.laTotalQty.Name = "laTotalQty";
            this.laTotalQty.Size = new System.Drawing.Size(16, 16);
            this.laTotalQty.TabIndex = 34;
            this.laTotalQty.Text = "0";
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle57.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle57.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle57.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle57.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle57.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle57.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle57;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cb,
            this.EAN11,
            this.調撥確認,
            this.MENGE_R,
            this.MENGE_GR,
            this.MAKTX,
            this.WERKS,
            this.UMMENGE,
            this.LABST_OUT,
            this.LABST_IN,
            this.PROCD,
            this.WERKS1,
            this.RESWK});
            this.dvList.Location = new System.Drawing.Point(12, 240);
            this.dvList.Name = "dvList";
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle63.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle63.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvList.RowHeadersDefaultCellStyle = dataGridViewCellStyle63;
            this.dvList.RowTemplate.Height = 24;
            this.dvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvList.Size = new System.Drawing.Size(759, 286);
            this.dvList.TabIndex = 35;
            this.dvList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvList_CellValueChanged);
            this.dvList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dvList_CellMouseDoubleClick);
            this.dvList.CurrentCellDirtyStateChanged += new System.EventHandler(this.dvList_CurrentCellDirtyStateChanged);
            // 
            // cb
            // 
            this.cb.HeaderText = "";
            this.cb.Name = "cb";
            this.cb.Width = 23;
            // 
            // EAN11
            // 
            this.EAN11.DataPropertyName = "商品條碼(EAN)";
            this.EAN11.HeaderText = "商品條碼(EAN)";
            this.EAN11.Name = "EAN11";
            this.EAN11.ReadOnly = true;
            this.EAN11.Width = 135;
            // 
            // 調撥確認
            // 
            this.調撥確認.DataPropertyName = "調撥確認";
            this.調撥確認.HeaderText = "調撥確認";
            this.調撥確認.Name = "調撥確認";
            this.調撥確認.ReadOnly = true;
            // 
            // MENGE_R
            // 
            this.MENGE_R.DataPropertyName = "調出數量";
            dataGridViewCellStyle58.Format = "0.###";
            this.MENGE_R.DefaultCellStyle = dataGridViewCellStyle58;
            this.MENGE_R.HeaderText = "調出數量";
            this.MENGE_R.Name = "MENGE_R";
            this.MENGE_R.ReadOnly = true;
            this.MENGE_R.Width = 80;
            // 
            // MENGE_GR
            // 
            this.MENGE_GR.DataPropertyName = "驗入數量";
            dataGridViewCellStyle59.Format = "0.###";
            this.MENGE_GR.DefaultCellStyle = dataGridViewCellStyle59;
            this.MENGE_GR.HeaderText = "驗入數量";
            this.MENGE_GR.Name = "MENGE_GR";
            this.MENGE_GR.ReadOnly = true;
            this.MENGE_GR.Width = 80;
            // 
            // MAKTX
            // 
            this.MAKTX.DataPropertyName = "品名";
            this.MAKTX.HeaderText = "品名";
            this.MAKTX.Name = "MAKTX";
            this.MAKTX.ReadOnly = true;
            this.MAKTX.Width = 80;
            // 
            // WERKS
            // 
            this.WERKS.DataPropertyName = "調入門市";
            this.WERKS.HeaderText = "調入門市";
            this.WERKS.Name = "WERKS";
            this.WERKS.ReadOnly = true;
            this.WERKS.Width = 80;
            // 
            // UMMENGE
            // 
            this.UMMENGE.DataPropertyName = "前週銷量";
            dataGridViewCellStyle60.Format = "0.###";
            this.UMMENGE.DefaultCellStyle = dataGridViewCellStyle60;
            this.UMMENGE.HeaderText = "前週銷量";
            this.UMMENGE.Name = "UMMENGE";
            this.UMMENGE.ReadOnly = true;
            this.UMMENGE.Width = 80;
            // 
            // LABST_OUT
            // 
            this.LABST_OUT.DataPropertyName = "調出門市庫存量";
            dataGridViewCellStyle61.Format = "0.###";
            this.LABST_OUT.DefaultCellStyle = dataGridViewCellStyle61;
            this.LABST_OUT.HeaderText = "調出門市庫存量";
            this.LABST_OUT.Name = "LABST_OUT";
            this.LABST_OUT.ReadOnly = true;
            // 
            // LABST_IN
            // 
            this.LABST_IN.DataPropertyName = "調入門市庫存量";
            dataGridViewCellStyle62.Format = "0.###";
            this.LABST_IN.DefaultCellStyle = dataGridViewCellStyle62;
            this.LABST_IN.HeaderText = "調入門市庫存量";
            this.LABST_IN.Name = "LABST_IN";
            this.LABST_IN.ReadOnly = true;
            // 
            // PROCD
            // 
            this.PROCD.DataPropertyName = "PROCD";
            this.PROCD.HeaderText = "PROCD";
            this.PROCD.Name = "PROCD";
            this.PROCD.ReadOnly = true;
            this.PROCD.Visible = false;
            // 
            // WERKS1
            // 
            this.WERKS1.DataPropertyName = "WERKS";
            this.WERKS1.HeaderText = "WERKS";
            this.WERKS1.Name = "WERKS1";
            this.WERKS1.ReadOnly = true;
            this.WERKS1.Visible = false;
            // 
            // RESWK
            // 
            this.RESWK.DataPropertyName = "RESWK";
            this.RESWK.HeaderText = "RESWK";
            this.RESWK.Name = "RESWK";
            this.RESWK.ReadOnly = true;
            this.RESWK.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(272, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(66, 27);
            this.btnExit.TabIndex = 36;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // laWarn
            // 
            this.laWarn.AutoSize = true;
            this.laWarn.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarn.ForeColor = System.Drawing.Color.Red;
            this.laWarn.Location = new System.Drawing.Point(427, 212);
            this.laWarn.Name = "laWarn";
            this.laWarn.Size = new System.Drawing.Size(259, 16);
            this.laWarn.TabIndex = 37;
            this.laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
            this.laWarn.Visible = false;
            // 
            // laKey
            // 
            this.laKey.AutoSize = true;
            this.laKey.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laKey.Location = new System.Drawing.Point(488, 104);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(0, 16);
            this.laKey.TabIndex = 38;
            this.laKey.Visible = false;
            // 
            // btnCancelSelect
            // 
            this.btnCancelSelect.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCancelSelect.Location = new System.Drawing.Point(539, 167);
            this.btnCancelSelect.Name = "btnCancelSelect";
            this.btnCancelSelect.Size = new System.Drawing.Size(147, 27);
            this.btnCancelSelect.TabIndex = 137;
            this.btnCancelSelect.Text = "取消確認(勾者)";
            this.btnCancelSelect.UseVisualStyleBackColor = true;
            this.btnCancelSelect.Click += new System.EventHandler(this.btnCancelSelect_Click);
            // 
            // btnCheckSelect
            // 
            this.btnCheckSelect.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCheckSelect.Location = new System.Drawing.Point(430, 167);
            this.btnCheckSelect.Name = "btnCheckSelect";
            this.btnCheckSelect.Size = new System.Drawing.Size(103, 27);
            this.btnCheckSelect.TabIndex = 136;
            this.btnCheckSelect.Text = "確認(勾者)";
            this.btnCheckSelect.UseVisualStyleBackColor = true;
            this.btnCheckSelect.Click += new System.EventHandler(this.btnCheckSelect_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCheck.Location = new System.Drawing.Point(321, 167);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(103, 27);
            this.btnCheck.TabIndex = 135;
            this.btnCheck.Text = "全部確認";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // cbDelivery
            // 
            this.cbDelivery.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cbDelivery.FormattingEnabled = true;
            this.cbDelivery.Location = new System.Drawing.Point(237, 71);
            this.cbDelivery.Name = "cbDelivery";
            this.cbDelivery.Size = new System.Drawing.Size(101, 24);
            this.cbDelivery.TabIndex = 138;
            this.cbDelivery.SelectedValueChanged += new System.EventHandler(this.cbDelivery_SelectedValueChanged);
            // 
            // UC13
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.Controls.Add(this.cbDelivery);
            this.Controls.Add(this.btnCancelSelect);
            this.Controls.Add(this.btnCheckSelect);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.laWarn);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.laTotalQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.laEAN);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.comboItems);
            this.Controls.Add(this.tbSdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.la1);
            this.Name = "UC13";
            this.Size = new System.Drawing.Size(784, 537);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker tbSdate;
        private System.Windows.Forms.ComboBox comboItems;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Label laEAN;
        private System.Windows.Forms.Label laCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label laTotalQty;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label laWarn;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.Button btnCancelSelect;
        private System.Windows.Forms.Button btnCheckSelect;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cb;
        private System.Windows.Forms.DataGridViewTextBoxColumn EAN11;
        private System.Windows.Forms.DataGridViewTextBoxColumn 調撥確認;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_R;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_GR;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAKTX;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS;
        private System.Windows.Forms.DataGridViewTextBoxColumn UMMENGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LABST_OUT;
        private System.Windows.Forms.DataGridViewTextBoxColumn LABST_IN;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RESWK;
        private System.Windows.Forms.ComboBox cbDelivery;
    }
}
