﻿namespace JPMed_Windows.SubForms.門市前台作業
{
    partial class UC16
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCheck = new System.Windows.Forms.Button();
            this.laRtnDisName = new System.Windows.Forms.Label();
            this.laRtnDisCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.cb = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.退貨確認 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退貨至 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.商品條碼EAN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退貨量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.調出門市庫存量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WERKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIFNR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laTotalQty = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.laWarn = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.laCount = new System.Windows.Forms.Label();
            this.laEAN = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.comboItems = new System.Windows.Forms.ComboBox();
            this.tbSdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.la1 = new System.Windows.Forms.Label();
            this.btnCheckSelect = new System.Windows.Forms.Button();
            this.btnCancelSelect = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCheck
            // 
            this.btnCheck.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCheck.Location = new System.Drawing.Point(298, 194);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(103, 27);
            this.btnCheck.TabIndex = 132;
            this.btnCheck.Text = "全部確認";
            this.btnCheck.UseVisualStyleBackColor = true;
            // 
            // laRtnDisName
            // 
            this.laRtnDisName.AutoEllipsis = true;
            this.laRtnDisName.AutoSize = true;
            this.laRtnDisName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laRtnDisName.Location = new System.Drawing.Point(253, 135);
            this.laRtnDisName.Name = "laRtnDisName";
            this.laRtnDisName.Size = new System.Drawing.Size(72, 16);
            this.laRtnDisName.TabIndex = 129;
            this.laRtnDisName.Text = "林口總倉";
            // 
            // laRtnDisCode
            // 
            this.laRtnDisCode.AutoEllipsis = true;
            this.laRtnDisCode.AutoSize = true;
            this.laRtnDisCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laRtnDisCode.Location = new System.Drawing.Point(168, 135);
            this.laRtnDisCode.Name = "laRtnDisCode";
            this.laRtnDisCode.Size = new System.Drawing.Size(91, 16);
            this.laRtnDisCode.TabIndex = 128;
            this.laRtnDisCode.Text = "A000000001";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(100, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 127;
            this.label8.Text = "退貨至";
            // 
            // laKey
            // 
            this.laKey.AutoSize = true;
            this.laKey.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laKey.Location = new System.Drawing.Point(490, 135);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(0, 16);
            this.laKey.TabIndex = 126;
            this.laKey.Visible = false;
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cb,
            this.退貨確認,
            this.退貨至,
            this.商品條碼EAN,
            this.退貨量,
            this.品名,
            this.調出門市庫存量,
            this.WERKS,
            this.LIFNR,
            this.PROCD});
            this.dvList.Location = new System.Drawing.Point(14, 264);
            this.dvList.Name = "dvList";
            this.dvList.RowTemplate.Height = 24;
            this.dvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvList.Size = new System.Drawing.Size(759, 262);
            this.dvList.TabIndex = 123;
            // 
            // cb
            // 
            this.cb.HeaderText = "";
            this.cb.Name = "cb";
            this.cb.Width = 23;
            // 
            // 退貨確認
            // 
            this.退貨確認.DataPropertyName = "退貨確認";
            this.退貨確認.HeaderText = "退貨確認";
            this.退貨確認.Name = "退貨確認";
            this.退貨確認.ReadOnly = true;
            this.退貨確認.Width = 80;
            // 
            // 退貨至
            // 
            this.退貨至.DataPropertyName = "退貨至";
            this.退貨至.HeaderText = "退貨至";
            this.退貨至.Name = "退貨至";
            this.退貨至.ReadOnly = true;
            // 
            // 商品條碼EAN
            // 
            this.商品條碼EAN.DataPropertyName = "商品條碼(EAN)";
            this.商品條碼EAN.HeaderText = "商品條碼(EAN)";
            this.商品條碼EAN.Name = "商品條碼EAN";
            this.商品條碼EAN.ReadOnly = true;
            // 
            // 退貨量
            // 
            this.退貨量.DataPropertyName = "退貨量";
            dataGridViewCellStyle2.Format = "0.###";
            this.退貨量.DefaultCellStyle = dataGridViewCellStyle2;
            this.退貨量.HeaderText = "退貨量";
            this.退貨量.Name = "退貨量";
            this.退貨量.ReadOnly = true;
            // 
            // 品名
            // 
            this.品名.DataPropertyName = "品名";
            this.品名.HeaderText = "品名";
            this.品名.Name = "品名";
            this.品名.ReadOnly = true;
            // 
            // 調出門市庫存量
            // 
            this.調出門市庫存量.DataPropertyName = "調出門市庫存量";
            dataGridViewCellStyle3.Format = "0.###";
            this.調出門市庫存量.DefaultCellStyle = dataGridViewCellStyle3;
            this.調出門市庫存量.HeaderText = "庫存量";
            this.調出門市庫存量.Name = "調出門市庫存量";
            this.調出門市庫存量.ReadOnly = true;
            // 
            // WERKS
            // 
            this.WERKS.DataPropertyName = "WERKS";
            this.WERKS.HeaderText = "WERKS";
            this.WERKS.Name = "WERKS";
            this.WERKS.ReadOnly = true;
            this.WERKS.Visible = false;
            // 
            // LIFNR
            // 
            this.LIFNR.DataPropertyName = "LIFNR";
            this.LIFNR.HeaderText = "LIFNR";
            this.LIFNR.Name = "LIFNR";
            this.LIFNR.ReadOnly = true;
            this.LIFNR.Visible = false;
            // 
            // PROCD
            // 
            this.PROCD.DataPropertyName = "PROCD";
            this.PROCD.HeaderText = "PROCD";
            this.PROCD.Name = "PROCD";
            this.PROCD.ReadOnly = true;
            this.PROCD.Visible = false;
            // 
            // laTotalQty
            // 
            this.laTotalQty.AutoSize = true;
            this.laTotalQty.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laTotalQty.Location = new System.Drawing.Point(271, 236);
            this.laTotalQty.Name = "laTotalQty";
            this.laTotalQty.Size = new System.Drawing.Size(16, 16);
            this.laTotalQty.TabIndex = 122;
            this.laTotalQty.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(177, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 121;
            this.label7.Text = "目前總量：";
            // 
            // laWarn
            // 
            this.laWarn.AutoSize = true;
            this.laWarn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarn.ForeColor = System.Drawing.Color.Red;
            this.laWarn.Location = new System.Drawing.Point(404, 236);
            this.laWarn.Name = "laWarn";
            this.laWarn.Size = new System.Drawing.Size(259, 16);
            this.laWarn.TabIndex = 125;
            this.laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
            this.laWarn.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(274, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(66, 27);
            this.btnExit.TabIndex = 124;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(11, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 120;
            this.label6.Text = "目前總筆數：";
            // 
            // laCount
            // 
            this.laCount.AutoSize = true;
            this.laCount.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laCount.Location = new System.Drawing.Point(121, 236);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(16, 16);
            this.laCount.TabIndex = 119;
            this.laCount.Text = "0";
            // 
            // laEAN
            // 
            this.laEAN.AutoSize = true;
            this.laEAN.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEAN.Location = new System.Drawing.Point(227, 109);
            this.laEAN.Name = "laEAN";
            this.laEAN.Size = new System.Drawing.Size(41, 16);
            this.laEAN.TabIndex = 118;
            this.laEAN.Text = "T001";
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDel.Location = new System.Drawing.Point(407, 161);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(103, 27);
            this.btnDel.TabIndex = 117;
            this.btnDel.Text = "刪除(勾者)";
            this.btnDel.UseVisualStyleBackColor = true;
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbQty.Location = new System.Drawing.Point(168, 161);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(125, 27);
            this.tbQty.TabIndex = 115;
            this.tbQty.Text = "1111111111.000";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSave.Location = new System.Drawing.Point(299, 161);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(102, 27);
            this.btnSave.TabIndex = 116;
            this.btnSave.Text = "輸入(覆蓋)";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(99, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 114;
            this.label5.Text = "退貨量：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(11, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 112;
            this.label3.Text = "輸入資料：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(99, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 113;
            this.label4.Text = "商品條碼(EAN)：";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnPrint.Location = new System.Drawing.Point(387, 73);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(110, 27);
            this.btnPrint.TabIndex = 111;
            this.btnPrint.Text = "列印退貨單";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(326, 73);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(55, 27);
            this.btnSearch.TabIndex = 110;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // comboItems
            // 
            this.comboItems.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboItems.FormattingEnabled = true;
            this.comboItems.Location = new System.Drawing.Point(239, 75);
            this.comboItems.Name = "comboItems";
            this.comboItems.Size = new System.Drawing.Size(79, 24);
            this.comboItems.TabIndex = 109;
            // 
            // tbSdate
            // 
            this.tbSdate.CalendarFont = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Location = new System.Drawing.Point(102, 73);
            this.tbSdate.Name = "tbSdate";
            this.tbSdate.Size = new System.Drawing.Size(131, 27);
            this.tbSdate.TabIndex = 108;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(11, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 107;
            this.label2.Text = "退貨日期：";
            // 
            // laEmpName
            // 
            this.laEmpName.AutoSize = true;
            this.laEmpName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEmpName.Location = new System.Drawing.Point(177, 44);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(56, 16);
            this.laEmpName.TabIndex = 106;
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEmpCode.Location = new System.Drawing.Point(63, 39);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 27);
            this.tbEmpCode.TabIndex = 105;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(11, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 104;
            this.label1.Text = "員編：";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(152, 14);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 103;
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(99, 14);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 102;
            this.laSCode.Text = "T001";
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(11, 14);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 101;
            this.la1.Text = "所在門市：";
            // 
            // btnCheckSelect
            // 
            this.btnCheckSelect.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCheckSelect.Location = new System.Drawing.Point(407, 194);
            this.btnCheckSelect.Name = "btnCheckSelect";
            this.btnCheckSelect.Size = new System.Drawing.Size(103, 27);
            this.btnCheckSelect.TabIndex = 133;
            this.btnCheckSelect.Text = "確認(勾者)";
            this.btnCheckSelect.UseVisualStyleBackColor = true;
            // 
            // btnCancelSelect
            // 
            this.btnCancelSelect.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCancelSelect.Location = new System.Drawing.Point(516, 194);
            this.btnCancelSelect.Name = "btnCancelSelect";
            this.btnCancelSelect.Size = new System.Drawing.Size(147, 27);
            this.btnCancelSelect.TabIndex = 134;
            this.btnCancelSelect.Text = "取消確認(勾者)";
            this.btnCancelSelect.UseVisualStyleBackColor = true;
            // 
            // UC16
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.Controls.Add(this.btnCancelSelect);
            this.Controls.Add(this.btnCheckSelect);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.laRtnDisName);
            this.Controls.Add(this.laRtnDisCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.laTotalQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.laWarn);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.laEAN);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.comboItems);
            this.Controls.Add(this.tbSdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.la1);
            this.Name = "UC16";
            this.Size = new System.Drawing.Size(784, 537);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label laRtnDisName;
        private System.Windows.Forms.Label laRtnDisCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.Label laTotalQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label laWarn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laCount;
        private System.Windows.Forms.Label laEAN;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox comboItems;
        private System.Windows.Forms.DateTimePicker tbSdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Button btnCheckSelect;
        private System.Windows.Forms.Button btnCancelSelect;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cb;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退貨確認;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退貨至;
        private System.Windows.Forms.DataGridViewTextBoxColumn 商品條碼EAN;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退貨量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 調出門市庫存量;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIFNR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROCD;
    }
}
