﻿namespace JPMed_Windows.SubForms.門市前台作業
{
    partial class UC12
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label12 = new System.Windows.Forms.Label();
            this.laEdate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.laSdate = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSin = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.laEAN = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.la1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.tbRealQty = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.laNeedQty = new System.Windows.Forms.Label();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.laTotalQty = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.laCount = new System.Windows.Forms.Label();
            this.laWarn = new System.Windows.Forms.Label();
            this.laMaxQty = new System.Windows.Forms.Label();
            this.商品條碼EAN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.應到數量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.當日收發量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.實際量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.備註 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSART = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EBELN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EBELP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料號碼 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.收貨門市 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.發貨門市 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.採購單數量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.採購單計量單位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.需求日期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RANK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VBELN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CANEDIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CANDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MATNR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VBELN1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(436, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 16);
            this.label12.TabIndex = 97;
            this.label12.Text = ")";
            // 
            // laEdate
            // 
            this.laEdate.AutoSize = true;
            this.laEdate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEdate.Location = new System.Drawing.Point(317, 70);
            this.laEdate.Name = "laEdate";
            this.laEdate.Size = new System.Drawing.Size(120, 16);
            this.laEdate.TabIndex = 96;
            this.laEdate.Text = "2016年11月15號";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(268, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 16);
            this.label11.TabIndex = 95;
            this.label11.Text = "迄日：";
            // 
            // laSdate
            // 
            this.laSdate.AutoSize = true;
            this.laSdate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSdate.Location = new System.Drawing.Point(145, 70);
            this.laSdate.Name = "laSdate";
            this.laSdate.Size = new System.Drawing.Size(120, 16);
            this.laSdate.TabIndex = 94;
            this.laSdate.Text = "2016年11月15號";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(91, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 16);
            this.label9.TabIndex = 93;
            this.label9.Text = "(起日：";
            // 
            // cbSin
            // 
            this.cbSin.AutoSize = true;
            this.cbSin.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cbSin.Location = new System.Drawing.Point(70, 72);
            this.cbSin.Name = "cbSin";
            this.cbSin.Size = new System.Drawing.Size(15, 14);
            this.cbSin.TabIndex = 92;
            this.cbSin.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(17, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 91;
            this.label8.Text = "特收：";
            // 
            // laKey
            // 
            this.laKey.AutoSize = true;
            this.laKey.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laKey.Location = new System.Drawing.Point(496, 133);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(0, 16);
            this.laKey.TabIndex = 90;
            this.laKey.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(280, 7);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(66, 27);
            this.btnExit.TabIndex = 89;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // laEAN
            // 
            this.laEAN.AutoSize = true;
            this.laEAN.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEAN.Location = new System.Drawing.Point(233, 131);
            this.laEAN.Name = "laEAN";
            this.laEAN.Size = new System.Drawing.Size(41, 16);
            this.laEAN.TabIndex = 88;
            this.laEAN.Text = "T001";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSave.Location = new System.Drawing.Point(348, 195);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(102, 27);
            this.btnSave.TabIndex = 86;
            this.btnSave.Text = "輸入(覆蓋)";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(105, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 16);
            this.label5.TabIndex = 84;
            this.label5.Text = "應到(退)量：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(105, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 83;
            this.label4.Text = "商品條碼(EAN)：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(17, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 82;
            this.label3.Text = "輸入資料：";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(280, 95);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(66, 27);
            this.btnSearch.TabIndex = 81;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // laEmpName
            // 
            this.laEmpName.AutoSize = true;
            this.laEmpName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEmpName.Location = new System.Drawing.Point(183, 42);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(56, 16);
            this.laEmpName.TabIndex = 78;
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEmpCode.Location = new System.Drawing.Point(69, 37);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 27);
            this.tbEmpCode.TabIndex = 77;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(17, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 76;
            this.label1.Text = "員編：";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(158, 12);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 75;
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(105, 12);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 74;
            this.laSCode.Text = "T001";
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(17, 12);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 73;
            this.la1.Text = "所在門市：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(17, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 16);
            this.label6.TabIndex = 99;
            this.label6.Text = "單號：";
            // 
            // tbSN
            // 
            this.tbSN.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSN.Location = new System.Drawing.Point(69, 95);
            this.tbSN.MaxLength = 22;
            this.tbSN.Name = "tbSN";
            this.tbSN.Size = new System.Drawing.Size(205, 27);
            this.tbSN.TabIndex = 100;
            this.tbSN.Text = "ZRP1T00120160720000001";
            // 
            // tbRealQty
            // 
            this.tbRealQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbRealQty.Location = new System.Drawing.Point(215, 195);
            this.tbRealQty.MaxLength = 14;
            this.tbRealQty.Name = "tbRealQty";
            this.tbRealQty.Size = new System.Drawing.Size(116, 27);
            this.tbRealQty.TabIndex = 102;
            this.tbRealQty.Text = "1111111111.000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(105, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 16);
            this.label2.TabIndex = 101;
            this.label2.Text = "本次收(退)量：";
            // 
            // laNeedQty
            // 
            this.laNeedQty.AutoSize = true;
            this.laNeedQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laNeedQty.Location = new System.Drawing.Point(200, 163);
            this.laNeedQty.Name = "laNeedQty";
            this.laNeedQty.Size = new System.Drawing.Size(52, 16);
            this.laNeedQty.TabIndex = 103;
            this.laNeedQty.Text = "10.000";
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.商品條碼EAN,
            this.品名,
            this.應到數量,
            this.當日收發量,
            this.實際量,
            this.備註,
            this.BSART,
            this.EBELN,
            this.EBELP,
            this.物料號碼,
            this.收貨門市,
            this.發貨門市,
            this.採購單數量,
            this.採購單計量單位,
            this.需求日期,
            this.RANK,
            this.VBELN,
            this.CANEDIT,
            this.CANDO,
            this.MATNR,
            this.VBELN1});
            this.dvList.Location = new System.Drawing.Point(11, 257);
            this.dvList.Name = "dvList";
            this.dvList.RowTemplate.Height = 24;
            this.dvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvList.Size = new System.Drawing.Size(759, 268);
            this.dvList.TabIndex = 108;
            // 
            // laTotalQty
            // 
            this.laTotalQty.AutoSize = true;
            this.laTotalQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laTotalQty.Location = new System.Drawing.Point(245, 235);
            this.laTotalQty.Name = "laTotalQty";
            this.laTotalQty.Size = new System.Drawing.Size(16, 16);
            this.laTotalQty.TabIndex = 107;
            this.laTotalQty.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(141, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 16);
            this.label7.TabIndex = 106;
            this.label7.Text = "已處理件數：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(8, 235);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 16);
            this.label10.TabIndex = 105;
            this.label10.Text = "總件數：";
            // 
            // laCount
            // 
            this.laCount.AutoSize = true;
            this.laCount.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laCount.Location = new System.Drawing.Point(79, 235);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(16, 16);
            this.laCount.TabIndex = 104;
            this.laCount.Text = "0";
            // 
            // laWarn
            // 
            this.laWarn.AutoSize = true;
            this.laWarn.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarn.ForeColor = System.Drawing.Color.Red;
            this.laWarn.Location = new System.Drawing.Point(348, 235);
            this.laWarn.Name = "laWarn";
            this.laWarn.Size = new System.Drawing.Size(280, 16);
            this.laWarn.TabIndex = 109;
            this.laWarn.Text = "該筆紀錄已過收貨期限，不可進行作業";
            this.laWarn.Visible = false;
            // 
            // laMaxQty
            // 
            this.laMaxQty.AutoSize = true;
            this.laMaxQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laMaxQty.Location = new System.Drawing.Point(496, 200);
            this.laMaxQty.Name = "laMaxQty";
            this.laMaxQty.Size = new System.Drawing.Size(0, 16);
            this.laMaxQty.TabIndex = 110;
            this.laMaxQty.Visible = false;
            // 
            // 商品條碼EAN
            // 
            this.商品條碼EAN.DataPropertyName = "商品條碼(EAN)";
            this.商品條碼EAN.HeaderText = "商品條碼(EAN)";
            this.商品條碼EAN.Name = "商品條碼EAN";
            this.商品條碼EAN.ReadOnly = true;
            this.商品條碼EAN.Width = 135;
            // 
            // 品名
            // 
            this.品名.DataPropertyName = "品名";
            this.品名.HeaderText = "品名";
            this.品名.Name = "品名";
            this.品名.ReadOnly = true;
            // 
            // 應到數量
            // 
            this.應到數量.DataPropertyName = "應到數量";
            dataGridViewCellStyle1.Format = "0.###";
            this.應到數量.DefaultCellStyle = dataGridViewCellStyle1;
            this.應到數量.HeaderText = "應到數量";
            this.應到數量.Name = "應到數量";
            this.應到數量.ReadOnly = true;
            // 
            // 當日收發量
            // 
            this.當日收發量.DataPropertyName = "當日收發量";
            dataGridViewCellStyle2.Format = "0.###";
            this.當日收發量.DefaultCellStyle = dataGridViewCellStyle2;
            this.當日收發量.HeaderText = "本次收發量";
            this.當日收發量.Name = "當日收發量";
            this.當日收發量.ReadOnly = true;
            this.當日收發量.Visible = false;
            // 
            // 實際量
            // 
            this.實際量.DataPropertyName = "實際量";
            dataGridViewCellStyle3.Format = "0.###";
            this.實際量.DefaultCellStyle = dataGridViewCellStyle3;
            this.實際量.HeaderText = "實際量";
            this.實際量.Name = "實際量";
            this.實際量.ReadOnly = true;
            // 
            // 備註
            // 
            this.備註.DataPropertyName = "備註";
            this.備註.HeaderText = "備註";
            this.備註.Name = "備註";
            this.備註.ReadOnly = true;
            // 
            // BSART
            // 
            this.BSART.DataPropertyName = "BSART";
            this.BSART.HeaderText = "BSART";
            this.BSART.Name = "BSART";
            this.BSART.ReadOnly = true;
            this.BSART.Visible = false;
            // 
            // EBELN
            // 
            this.EBELN.DataPropertyName = "EBELN";
            this.EBELN.HeaderText = "EBELN";
            this.EBELN.Name = "EBELN";
            this.EBELN.ReadOnly = true;
            this.EBELN.Visible = false;
            // 
            // EBELP
            // 
            this.EBELP.DataPropertyName = "EBELP";
            this.EBELP.HeaderText = "EBELP";
            this.EBELP.Name = "EBELP";
            this.EBELP.ReadOnly = true;
            this.EBELP.Visible = false;
            // 
            // 物料號碼
            // 
            this.物料號碼.DataPropertyName = "物料號碼";
            this.物料號碼.HeaderText = "物料號碼";
            this.物料號碼.Name = "物料號碼";
            this.物料號碼.ReadOnly = true;
            this.物料號碼.Visible = false;
            // 
            // 收貨門市
            // 
            this.收貨門市.DataPropertyName = "收貨門市";
            this.收貨門市.HeaderText = "收貨門市";
            this.收貨門市.Name = "收貨門市";
            this.收貨門市.ReadOnly = true;
            this.收貨門市.Visible = false;
            // 
            // 發貨門市
            // 
            this.發貨門市.DataPropertyName = "發貨門市";
            this.發貨門市.HeaderText = "發貨門市";
            this.發貨門市.Name = "發貨門市";
            this.發貨門市.ReadOnly = true;
            this.發貨門市.Visible = false;
            // 
            // 採購單數量
            // 
            this.採購單數量.DataPropertyName = "採購單數量";
            this.採購單數量.HeaderText = "採購單數量";
            this.採購單數量.Name = "採購單數量";
            this.採購單數量.ReadOnly = true;
            this.採購單數量.Visible = false;
            // 
            // 採購單計量單位
            // 
            this.採購單計量單位.DataPropertyName = "採購單計量單位";
            this.採購單計量單位.HeaderText = "採購單計量單位";
            this.採購單計量單位.Name = "採購單計量單位";
            this.採購單計量單位.ReadOnly = true;
            this.採購單計量單位.Visible = false;
            // 
            // 需求日期
            // 
            this.需求日期.DataPropertyName = "需求日期";
            this.需求日期.HeaderText = "需求日期";
            this.需求日期.Name = "需求日期";
            this.需求日期.ReadOnly = true;
            this.需求日期.Visible = false;
            // 
            // RANK
            // 
            this.RANK.DataPropertyName = "RANK";
            this.RANK.HeaderText = "RANK";
            this.RANK.Name = "RANK";
            this.RANK.ReadOnly = true;
            this.RANK.Visible = false;
            // 
            // VBELN
            // 
            this.VBELN.DataPropertyName = "VBELN";
            this.VBELN.HeaderText = "VBELN";
            this.VBELN.Name = "VBELN";
            this.VBELN.Visible = false;
            // 
            // CANEDIT
            // 
            this.CANEDIT.DataPropertyName = "CANEDIT";
            this.CANEDIT.HeaderText = "CANEDIT";
            this.CANEDIT.Name = "CANEDIT";
            this.CANEDIT.Visible = false;
            // 
            // CANDO
            // 
            this.CANDO.DataPropertyName = "CANDO";
            this.CANDO.HeaderText = "CANDO";
            this.CANDO.Name = "CANDO";
            this.CANDO.Visible = false;
            // 
            // MATNR
            // 
            this.MATNR.DataPropertyName = "MATNR";
            this.MATNR.HeaderText = "MATNR";
            this.MATNR.Name = "MATNR";
            this.MATNR.Visible = false;
            // 
            // VBELN1
            // 
            this.VBELN1.DataPropertyName = "VBELN1";
            this.VBELN1.HeaderText = "VBELN1";
            this.VBELN1.Name = "VBELN1";
            this.VBELN1.Visible = false;
            // 
            // UC12
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.Controls.Add(this.laMaxQty);
            this.Controls.Add(this.laWarn);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.laTotalQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.laNeedQty);
            this.Controls.Add(this.tbRealQty);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.laEdate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.laSdate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbSin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.laEAN);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.la1);
            this.Name = "UC12";
            this.Size = new System.Drawing.Size(784, 537);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label laEdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label laSdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbSin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label laEAN;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.TextBox tbRealQty;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label laNeedQty;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.Label laTotalQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label laCount;
        private System.Windows.Forms.Label laWarn;
        private System.Windows.Forms.Label laMaxQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn 商品條碼EAN;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 應到數量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 當日收發量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 實際量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 備註;
        private System.Windows.Forms.DataGridViewTextBoxColumn BSART;
        private System.Windows.Forms.DataGridViewTextBoxColumn EBELN;
        private System.Windows.Forms.DataGridViewTextBoxColumn EBELP;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料號碼;
        private System.Windows.Forms.DataGridViewTextBoxColumn 收貨門市;
        private System.Windows.Forms.DataGridViewTextBoxColumn 發貨門市;
        private System.Windows.Forms.DataGridViewTextBoxColumn 採購單數量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 採購單計量單位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 需求日期;
        private System.Windows.Forms.DataGridViewTextBoxColumn RANK;
        private System.Windows.Forms.DataGridViewTextBoxColumn VBELN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CANEDIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CANDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn MATNR;
        private System.Windows.Forms.DataGridViewTextBoxColumn VBELN1;
    }
}
