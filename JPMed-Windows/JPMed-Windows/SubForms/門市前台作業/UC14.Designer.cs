﻿namespace JPMed_Windows.SubForms.門市前台作業
{
    partial class UC14
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.laKey = new System.Windows.Forms.Label();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.cb = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EAN11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_R = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENGE_GR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAKTX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WERKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UMMENGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LABST_OUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LABST_IN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WERKS1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESWK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laTotalQty = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.laWarn = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.laCount = new System.Windows.Forms.Label();
            this.laEAN = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.comboItems = new System.Windows.Forms.ComboBox();
            this.tbSdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.la1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.laMainStockCode = new System.Windows.Forms.Label();
            this.laMainStockName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // laKey
            // 
            this.laKey.AutoSize = true;
            this.laKey.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laKey.Location = new System.Drawing.Point(490, 136);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(0, 16);
            this.laKey.TabIndex = 64;
            this.laKey.Visible = false;
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cb,
            this.EAN11,
            this.MENGE_R,
            this.MENGE_GR,
            this.MAKTX,
            this.WERKS,
            this.UMMENGE,
            this.LABST_OUT,
            this.LABST_IN,
            this.PROCD,
            this.WERKS1,
            this.RESWK});
            this.dvList.Location = new System.Drawing.Point(14, 235);
            this.dvList.Name = "dvList";
            this.dvList.RowTemplate.Height = 24;
            this.dvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvList.Size = new System.Drawing.Size(759, 292);
            this.dvList.TabIndex = 61;
            this.dvList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvList_CellValueChanged);
            this.dvList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dvList_CellMouseDoubleClick);
            this.dvList.CurrentCellDirtyStateChanged += new System.EventHandler(this.dvList_CurrentCellDirtyStateChanged);
            // 
            // cb
            // 
            this.cb.HeaderText = "";
            this.cb.Name = "cb";
            this.cb.Width = 23;
            // 
            // EAN11
            // 
            this.EAN11.DataPropertyName = "商品條碼(EAN)";
            this.EAN11.HeaderText = "商品條碼(EAN)";
            this.EAN11.Name = "EAN11";
            this.EAN11.ReadOnly = true;
            this.EAN11.Width = 135;
            // 
            // MENGE_R
            // 
            this.MENGE_R.DataPropertyName = "調出數量";
            dataGridViewCellStyle1.Format = "0.###";
            this.MENGE_R.DefaultCellStyle = dataGridViewCellStyle1;
            this.MENGE_R.HeaderText = "調撥數量";
            this.MENGE_R.Name = "MENGE_R";
            this.MENGE_R.ReadOnly = true;
            this.MENGE_R.Width = 80;
            // 
            // MENGE_GR
            // 
            this.MENGE_GR.DataPropertyName = "驗入數量";
            dataGridViewCellStyle2.Format = "0.###";
            this.MENGE_GR.DefaultCellStyle = dataGridViewCellStyle2;
            this.MENGE_GR.HeaderText = "驗入數量";
            this.MENGE_GR.Name = "MENGE_GR";
            this.MENGE_GR.ReadOnly = true;
            this.MENGE_GR.Visible = false;
            this.MENGE_GR.Width = 80;
            // 
            // MAKTX
            // 
            this.MAKTX.DataPropertyName = "品名";
            this.MAKTX.HeaderText = "品名";
            this.MAKTX.Name = "MAKTX";
            this.MAKTX.ReadOnly = true;
            this.MAKTX.Width = 80;
            // 
            // WERKS
            // 
            this.WERKS.DataPropertyName = "調入門市";
            this.WERKS.HeaderText = "調入門市";
            this.WERKS.Name = "WERKS";
            this.WERKS.ReadOnly = true;
            this.WERKS.Visible = false;
            this.WERKS.Width = 80;
            // 
            // UMMENGE
            // 
            this.UMMENGE.DataPropertyName = "前週銷量";
            dataGridViewCellStyle3.Format = "0.###";
            this.UMMENGE.DefaultCellStyle = dataGridViewCellStyle3;
            this.UMMENGE.HeaderText = "前週銷量";
            this.UMMENGE.Name = "UMMENGE";
            this.UMMENGE.ReadOnly = true;
            this.UMMENGE.Width = 80;
            // 
            // LABST_OUT
            // 
            this.LABST_OUT.DataPropertyName = "調出門市庫存量";
            dataGridViewCellStyle4.Format = "0.###";
            this.LABST_OUT.DefaultCellStyle = dataGridViewCellStyle4;
            this.LABST_OUT.HeaderText = "庫存量";
            this.LABST_OUT.Name = "LABST_OUT";
            this.LABST_OUT.ReadOnly = true;
            // 
            // LABST_IN
            // 
            this.LABST_IN.DataPropertyName = "調入門市庫存量";
            dataGridViewCellStyle5.Format = "0.###";
            this.LABST_IN.DefaultCellStyle = dataGridViewCellStyle5;
            this.LABST_IN.HeaderText = "調入門市庫存量";
            this.LABST_IN.Name = "LABST_IN";
            this.LABST_IN.ReadOnly = true;
            this.LABST_IN.Visible = false;
            // 
            // PROCD
            // 
            this.PROCD.DataPropertyName = "PROCD";
            this.PROCD.HeaderText = "PROCD";
            this.PROCD.Name = "PROCD";
            this.PROCD.ReadOnly = true;
            this.PROCD.Visible = false;
            // 
            // WERKS1
            // 
            this.WERKS1.DataPropertyName = "WERKS";
            this.WERKS1.HeaderText = "WERKS";
            this.WERKS1.Name = "WERKS1";
            this.WERKS1.ReadOnly = true;
            this.WERKS1.Visible = false;
            // 
            // RESWK
            // 
            this.RESWK.DataPropertyName = "RESWK";
            this.RESWK.HeaderText = "RESWK";
            this.RESWK.Name = "RESWK";
            this.RESWK.ReadOnly = true;
            this.RESWK.Visible = false;
            // 
            // laTotalQty
            // 
            this.laTotalQty.AutoSize = true;
            this.laTotalQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laTotalQty.Location = new System.Drawing.Point(271, 206);
            this.laTotalQty.Name = "laTotalQty";
            this.laTotalQty.Size = new System.Drawing.Size(16, 16);
            this.laTotalQty.TabIndex = 60;
            this.laTotalQty.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(177, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 59;
            this.label7.Text = "目前總量：";
            // 
            // laWarn
            // 
            this.laWarn.AutoSize = true;
            this.laWarn.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laWarn.ForeColor = System.Drawing.Color.Red;
            this.laWarn.Location = new System.Drawing.Point(429, 206);
            this.laWarn.Name = "laWarn";
            this.laWarn.Size = new System.Drawing.Size(259, 16);
            this.laWarn.TabIndex = 63;
            this.laWarn.Text = "該筆紀錄SAP已處理，不可進行異動";
            this.laWarn.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(274, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(66, 27);
            this.btnExit.TabIndex = 62;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(11, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 58;
            this.label6.Text = "目前總筆數：";
            // 
            // laCount
            // 
            this.laCount.AutoSize = true;
            this.laCount.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laCount.Location = new System.Drawing.Point(121, 206);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(16, 16);
            this.laCount.TabIndex = 57;
            this.laCount.Text = "0";
            // 
            // laEAN
            // 
            this.laEAN.AutoSize = true;
            this.laEAN.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEAN.Location = new System.Drawing.Point(227, 136);
            this.laEAN.Name = "laEAN";
            this.laEAN.Size = new System.Drawing.Size(41, 16);
            this.laEAN.TabIndex = 56;
            this.laEAN.Text = "T001";
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDel.Location = new System.Drawing.Point(432, 163);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(103, 27);
            this.btnDel.TabIndex = 55;
            this.btnDel.Text = "刪除(勾者)";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSave.Location = new System.Drawing.Point(324, 163);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(102, 27);
            this.btnSave.TabIndex = 54;
            this.btnSave.Text = "輸入(覆蓋)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbQty.Location = new System.Drawing.Point(193, 163);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(125, 27);
            this.tbQty.TabIndex = 53;
            this.tbQty.Text = "1111111111.000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(99, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 52;
            this.label5.Text = "調撥數量：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(99, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 51;
            this.label4.Text = "商品條碼(EAN)：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(11, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 50;
            this.label3.Text = "輸入資料：";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnPrint.Location = new System.Drawing.Point(387, 74);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(110, 27);
            this.btnPrint.TabIndex = 49;
            this.btnPrint.Text = "列印物流單";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(326, 74);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(55, 27);
            this.btnSearch.TabIndex = 48;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // comboItems
            // 
            this.comboItems.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboItems.FormattingEnabled = true;
            this.comboItems.Location = new System.Drawing.Point(239, 76);
            this.comboItems.Name = "comboItems";
            this.comboItems.Size = new System.Drawing.Size(79, 24);
            this.comboItems.TabIndex = 47;
            // 
            // tbSdate
            // 
            this.tbSdate.CalendarFont = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbSdate.Location = new System.Drawing.Point(102, 74);
            this.tbSdate.Name = "tbSdate";
            this.tbSdate.Size = new System.Drawing.Size(131, 27);
            this.tbSdate.TabIndex = 46;
            this.tbSdate.Validated += new System.EventHandler(this.tbSdate_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(11, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "調撥日期：";
            // 
            // laEmpName
            // 
            this.laEmpName.AutoSize = true;
            this.laEmpName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laEmpName.Location = new System.Drawing.Point(177, 45);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(56, 16);
            this.laEmpName.TabIndex = 44;
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tbEmpCode.Location = new System.Drawing.Point(63, 40);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 27);
            this.tbEmpCode.TabIndex = 43;
            this.tbEmpCode.Text = "A1234567890";
            this.tbEmpCode.TextChanged += new System.EventHandler(this.tbEmpCode_TextChanged);
            this.tbEmpCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbEmpCode_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(11, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 42;
            this.label1.Text = "員編：";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSName.Location = new System.Drawing.Point(152, 15);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(72, 16);
            this.laSName.TabIndex = 41;
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.AutoSize = true;
            this.laSCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laSCode.Location = new System.Drawing.Point(99, 15);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(41, 16);
            this.laSCode.TabIndex = 40;
            this.laSCode.Text = "T001";
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.la1.Location = new System.Drawing.Point(11, 15);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(88, 16);
            this.la1.TabIndex = 39;
            this.la1.Text = "所在門市：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(11, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 16);
            this.label8.TabIndex = 65;
            this.label8.Text = "調入總倉：";
            // 
            // laMainStockCode
            // 
            this.laMainStockCode.AutoSize = true;
            this.laMainStockCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laMainStockCode.Location = new System.Drawing.Point(94, 108);
            this.laMainStockCode.Name = "laMainStockCode";
            this.laMainStockCode.Size = new System.Drawing.Size(46, 16);
            this.laMainStockCode.TabIndex = 66;
            this.laMainStockCode.Text = "TDC2";
            // 
            // laMainStockName
            // 
            this.laMainStockName.AutoSize = true;
            this.laMainStockName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.laMainStockName.Location = new System.Drawing.Point(144, 108);
            this.laMainStockName.Name = "laMainStockName";
            this.laMainStockName.Size = new System.Drawing.Size(40, 16);
            this.laMainStockName.TabIndex = 67;
            this.laMainStockName.Text = "總倉";
            // 
            // UC14
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.Controls.Add(this.laMainStockName);
            this.Controls.Add(this.laMainStockCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.laTotalQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.laWarn);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.laEAN);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.comboItems);
            this.Controls.Add(this.tbSdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.la1);
            this.Name = "UC14";
            this.Size = new System.Drawing.Size(784, 537);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.Label laTotalQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label laWarn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laCount;
        private System.Windows.Forms.Label laEAN;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox comboItems;
        private System.Windows.Forms.DateTimePicker tbSdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label laMainStockCode;
        private System.Windows.Forms.Label laMainStockName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cb;
        private System.Windows.Forms.DataGridViewTextBoxColumn EAN11;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_R;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENGE_GR;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAKTX;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS;
        private System.Windows.Forms.DataGridViewTextBoxColumn UMMENGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LABST_OUT;
        private System.Windows.Forms.DataGridViewTextBoxColumn LABST_IN;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RESWK;
    }
}
