﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JPMed_Windows.Utility;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Reflection;

namespace JPMed_Windows
{
    public partial class MainForm : Form
    {
        bool quit = false;
        string formName = "門市前台作業系統";
        static string storeCode = Properties.Settings.Default.WerksCode;
        Process reportProcess = null;

        public MainForm()
        {
            InitializeComponent();
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("en-us");

            Load += new EventHandler(MainForm_Load);
            //this.ControlBox = false;

            foreach (ToolStripMenuItem dropDownItem in ms_menu.Items)
            {
                if (storeCode == "TDC1")
                {
                    if (dropDownItem.Text == "門市前台作業")
                    {
                        foreach (ToolStripMenuItem item in dropDownItem.DropDownItems)
                        {
                            if (item.Text != "驗收/退貨作業")
                                item.Enabled = false;
                        }
                    }
                }
                else if (storeCode == "T034")
                {
                    if (dropDownItem.Text == "門市前台作業")
                    {
                        foreach (ToolStripMenuItem item in dropDownItem.DropDownItems)
                        {
                            if (item.Text == "退貨作業")
                            {
                                foreach (ToolStripMenuItem subitem in item.DropDownItems)
                                {
                                    if (subitem.Text != "直退廠商作業")
                                        subitem.Enabled = false;
                                }
                            }
                            else if (item.Text != "驗收/退貨作業" && item.Text != "補貨作業")
                            {
                                item.Enabled = false;
                                foreach (ToolStripMenuItem subitem in item.DropDownItems)
                                    subitem.Enabled = false;
                            }
                        }
                    }
                }

            }
        }

        void MainForm_Load(object sender, EventArgs e)
        {
            //版本提示
            laVersion.Text = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
        }

        /// <summary>
        /// 離開 Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 離開ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定離開系統嗎?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                quit = true;
                this.Close();
            }
        }

        #region 庫存查詢 UC10
        private void 庫存查詢ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市查詢.UC10 uc = new JPMed_Windows.SubForms.門市查詢.UC10();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC10"].Controls["tbEAN"].Focus();
                this.Text = formName + " -- " + "庫存查詢";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 補貨作業 UC11
        private void 補貨作業ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市前台作業.UC11 uc = new JPMed_Windows.SubForms.門市前台作業.UC11();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC11"].Controls["tbEmpCode"].Focus();
                this.Text = formName + " -- " + "補貨作業";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 驗收發貨作業 UC12
        private void 驗收發貨作業ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市前台作業.UC12 uc = new JPMed_Windows.SubForms.門市前台作業.UC12();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC12"].Controls["tbEmpCode"].Focus();
                this.Text = formName + " -- " + "驗收/退貨作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 調撥門市作業 UC13
        private void 調撥門市作業ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市前台作業.UC13 uc = new JPMed_Windows.SubForms.門市前台作業.UC13();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC13"].Controls["tbEmpCode"].Focus();
                this.Text = formName + " -- " + "調撥門市作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 總部通知調撥回倉品 UC14
        private void 調撥總艙作業ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市前台作業.UC14 uc = new JPMed_Windows.SubForms.門市前台作業.UC14();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC14"].Controls["tbEmpCode"].Focus();
                this.Text = formName + " -- " + "總部通知調撥回倉品";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 門市退貨(有問題貨品)入退貨倉 UC15
        private void 退總倉作業ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市前台作業.UC15 uc = new JPMed_Windows.SubForms.門市前台作業.UC15();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC15"].Controls["tbEmpCode"].Focus();
                this.Text = formName + " -- " + "門市退貨(有問題貨品)入退貨倉";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 直退廠商作業 UC16
        private void 直退廠商作業ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_Windows.SubForms.門市前台作業.UC16 uc = new JPMed_Windows.SubForms.門市前台作業.UC16();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                panel0.Controls["UC16"].Controls["tbEmpCode"].Focus();
                this.Text = formName + " -- " + "直退廠商作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!quit)
            {
                DialogResult dialogResult = MessageBox.Show("您確定離開系統嗎?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult != DialogResult.Yes)
                    e.Cancel = true;
            }
        }

        [DllImport("user32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private void 補貨需求量與實到驗收量報表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string programPath = Environment.CurrentDirectory + @"\GoodsOnTheRoad\" + Properties.Settings.Default.GoodsOnTheRoad;
                ProcessStartInfo processInfo = new ProcessStartInfo(programPath);

                if (reportProcess == null || reportProcess.HasExited)
                {
                    reportProcess = new Process();
                    reportProcess.StartInfo = processInfo;
                    reportProcess.Start();
                }
                else
                {
                    IntPtr hWnd = reportProcess.MainWindowHandle;
                    if (hWnd != IntPtr.Zero)
                        SetForegroundWindow(hWnd);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("呼叫報表發生錯誤: " + ex.Message);
            }
        }

        private void 門市審單比較表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string programPath = Environment.CurrentDirectory + @"\RP_Report\" + Properties.Settings.Default.RP_Report;
                ProcessStartInfo processInfo = new ProcessStartInfo(programPath);

                if (reportProcess == null || reportProcess.HasExited)
                {
                    reportProcess = new Process();
                    reportProcess.StartInfo = processInfo;
                    reportProcess.Start();
                }
                else
                {
                    //IntPtr hWnd = reportProcess.MainWindowHandle;
                    //if (hWnd != IntPtr.Zero)
                    //    SetForegroundWindow(hWnd);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
