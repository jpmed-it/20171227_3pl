﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace JPMed_Windows.Uitlity
{
    public static class Common
    {
        public static void HandleAutoSize(this DataGridView dgv)
        {
            dgv.DataBindingComplete += (s, e) =>
            {
                var dg = (DataGridView)s;
                var width = dg.Columns.GetColumnsWidth(DataGridViewElementStates.None);
                var height = dg.Rows.GetRowsHeight(DataGridViewElementStates.None);

                dg.ClientSize = new Size(width, height);
            };
        }
    }
}
