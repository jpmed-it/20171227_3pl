﻿namespace JPMed_Windows
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ms_menu = new System.Windows.Forms.MenuStrip();
            this.門市查詢ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.庫存查詢ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.門市審單比較表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.門市前台作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.補貨作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.驗收發貨作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.調撥作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.調撥門市作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.調撥總艙作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退貨作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退總艙作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.直退廠商作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.門市列印作業ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.補貨需求量與實到驗收量報表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.離開ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel0 = new System.Windows.Forms.Panel();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.laVersion = new System.Windows.Forms.Label();
            this.ms_menu.SuspendLayout();
            this.panel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.SuspendLayout();
            // 
            // ms_menu
            // 
            this.ms_menu.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ms_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.門市查詢ToolStripMenuItem,
            this.門市前台作業ToolStripMenuItem,
            this.門市列印作業ToolStripMenuItem,
            this.離開ToolStripMenuItem});
            this.ms_menu.Location = new System.Drawing.Point(0, 0);
            this.ms_menu.Name = "ms_menu";
            this.ms_menu.Size = new System.Drawing.Size(784, 24);
            this.ms_menu.TabIndex = 0;
            this.ms_menu.Text = "menuStrip1";
            // 
            // 門市查詢ToolStripMenuItem
            // 
            this.門市查詢ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.庫存查詢ToolStripMenuItem,
            this.門市審單比較表ToolStripMenuItem});
            this.門市查詢ToolStripMenuItem.Name = "門市查詢ToolStripMenuItem";
            this.門市查詢ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.門市查詢ToolStripMenuItem.Text = "門市查詢";
            // 
            // 庫存查詢ToolStripMenuItem
            // 
            this.庫存查詢ToolStripMenuItem.Name = "庫存查詢ToolStripMenuItem";
            this.庫存查詢ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.庫存查詢ToolStripMenuItem.Text = "庫存查詢";
            this.庫存查詢ToolStripMenuItem.Click += new System.EventHandler(this.庫存查詢ToolStripMenuItem_Click);
            // 
            // 門市審單比較表ToolStripMenuItem
            // 
            this.門市審單比較表ToolStripMenuItem.Name = "門市審單比較表ToolStripMenuItem";
            this.門市審單比較表ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.門市審單比較表ToolStripMenuItem.Text = "門市審單比較表";
            this.門市審單比較表ToolStripMenuItem.Click += new System.EventHandler(this.門市審單比較表ToolStripMenuItem_Click);
            // 
            // 門市前台作業ToolStripMenuItem
            // 
            this.門市前台作業ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.補貨作業ToolStripMenuItem,
            this.驗收發貨作業ToolStripMenuItem,
            this.調撥作業ToolStripMenuItem,
            this.退貨作業ToolStripMenuItem});
            this.門市前台作業ToolStripMenuItem.Name = "門市前台作業ToolStripMenuItem";
            this.門市前台作業ToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.門市前台作業ToolStripMenuItem.Text = "門市前台作業";
            // 
            // 補貨作業ToolStripMenuItem
            // 
            this.補貨作業ToolStripMenuItem.Name = "補貨作業ToolStripMenuItem";
            this.補貨作業ToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.補貨作業ToolStripMenuItem.Text = "補貨作業";
            this.補貨作業ToolStripMenuItem.Click += new System.EventHandler(this.補貨作業ToolStripMenuItem_Click);
            // 
            // 驗收發貨作業ToolStripMenuItem
            // 
            this.驗收發貨作業ToolStripMenuItem.Name = "驗收發貨作業ToolStripMenuItem";
            this.驗收發貨作業ToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.驗收發貨作業ToolStripMenuItem.Text = "驗收/退貨作業";
            this.驗收發貨作業ToolStripMenuItem.Click += new System.EventHandler(this.驗收發貨作業ToolStripMenuItem_Click);
            // 
            // 調撥作業ToolStripMenuItem
            // 
            this.調撥作業ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.調撥門市作業ToolStripMenuItem,
            this.調撥總艙作業ToolStripMenuItem});
            this.調撥作業ToolStripMenuItem.Name = "調撥作業ToolStripMenuItem";
            this.調撥作業ToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.調撥作業ToolStripMenuItem.Text = "調撥作業";
            // 
            // 調撥門市作業ToolStripMenuItem
            // 
            this.調撥門市作業ToolStripMenuItem.Name = "調撥門市作業ToolStripMenuItem";
            this.調撥門市作業ToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.調撥門市作業ToolStripMenuItem.Text = "調撥門市作業";
            this.調撥門市作業ToolStripMenuItem.Click += new System.EventHandler(this.調撥門市作業ToolStripMenuItem_Click);
            // 
            // 調撥總艙作業ToolStripMenuItem
            // 
            this.調撥總艙作業ToolStripMenuItem.Name = "調撥總艙作業ToolStripMenuItem";
            this.調撥總艙作業ToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.調撥總艙作業ToolStripMenuItem.Text = "總部通知調撥回倉品";
            this.調撥總艙作業ToolStripMenuItem.Click += new System.EventHandler(this.調撥總艙作業ToolStripMenuItem_Click);
            // 
            // 退貨作業ToolStripMenuItem
            // 
            this.退貨作業ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.退總艙作業ToolStripMenuItem,
            this.直退廠商作業ToolStripMenuItem});
            this.退貨作業ToolStripMenuItem.Name = "退貨作業ToolStripMenuItem";
            this.退貨作業ToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.退貨作業ToolStripMenuItem.Text = "退貨作業";
            // 
            // 退總艙作業ToolStripMenuItem
            // 
            this.退總艙作業ToolStripMenuItem.Name = "退總艙作業ToolStripMenuItem";
            this.退總艙作業ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.退總艙作業ToolStripMenuItem.Text = "門市退貨(有問題貨品)入退貨倉";
            this.退總艙作業ToolStripMenuItem.Click += new System.EventHandler(this.退總倉作業ToolStripMenuItem_Click);
            // 
            // 直退廠商作業ToolStripMenuItem
            // 
            this.直退廠商作業ToolStripMenuItem.Name = "直退廠商作業ToolStripMenuItem";
            this.直退廠商作業ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.直退廠商作業ToolStripMenuItem.Text = "直退廠商作業";
            this.直退廠商作業ToolStripMenuItem.Click += new System.EventHandler(this.直退廠商作業ToolStripMenuItem_Click);
            // 
            // 門市列印作業ToolStripMenuItem
            // 
            this.門市列印作業ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.補貨需求量與實到驗收量報表ToolStripMenuItem});
            this.門市列印作業ToolStripMenuItem.Name = "門市列印作業ToolStripMenuItem";
            this.門市列印作業ToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.門市列印作業ToolStripMenuItem.Text = "門市列印作業";
            // 
            // 補貨需求量與實到驗收量報表ToolStripMenuItem
            // 
            this.補貨需求量與實到驗收量報表ToolStripMenuItem.Name = "補貨需求量與實到驗收量報表ToolStripMenuItem";
            this.補貨需求量與實到驗收量報表ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.補貨需求量與實到驗收量報表ToolStripMenuItem.Text = "補貨需求量與實到驗收量報表";
            this.補貨需求量與實到驗收量報表ToolStripMenuItem.Click += new System.EventHandler(this.補貨需求量與實到驗收量報表ToolStripMenuItem_Click);
            // 
            // 離開ToolStripMenuItem
            // 
            this.離開ToolStripMenuItem.Name = "離開ToolStripMenuItem";
            this.離開ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.離開ToolStripMenuItem.Text = "離開";
            this.離開ToolStripMenuItem.Click += new System.EventHandler(this.離開ToolStripMenuItem_Click);
            // 
            // panel0
            // 
            this.panel0.AutoScroll = true;
            this.panel0.AutoSize = true;
            this.panel0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel0.Controls.Add(this.laVersion);
            this.panel0.Controls.Add(this.pb1);
            this.panel0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel0.Location = new System.Drawing.Point(0, 24);
            this.panel0.Name = "panel0";
            this.panel0.Size = new System.Drawing.Size(784, 537);
            this.panel0.TabIndex = 1;
            // 
            // pb1
            // 
            this.pb1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pb1.Image = ((System.Drawing.Image)(resources.GetObject("pb1.Image")));
            this.pb1.Location = new System.Drawing.Point(304, 151);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(170, 148);
            this.pb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb1.TabIndex = 0;
            this.pb1.TabStop = false;
            // 
            // laVersion
            // 
            this.laVersion.Location = new System.Drawing.Point(11, 508);
            this.laVersion.Name = "laVersion";
            this.laVersion.Size = new System.Drawing.Size(160, 20);
            this.laVersion.TabIndex = 4;
            this.laVersion.Text = "ver: 1.0.0.0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panel0);
            this.Controls.Add(this.ms_menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.ms_menu;
            this.Name = "MainForm";
            this.Text = "門市前台作業系統";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ms_menu.ResumeLayout(false);
            this.ms_menu.PerformLayout();
            this.panel0.ResumeLayout(false);
            this.panel0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip ms_menu;
        private System.Windows.Forms.ToolStripMenuItem 門市查詢ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 庫存查詢ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 門市前台作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 補貨作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 驗收發貨作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 調撥作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 調撥門市作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 調撥總艙作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 門市列印作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 離開ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退貨作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退總艙作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 直退廠商作業ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 補貨需求量與實到驗收量報表ToolStripMenuItem;
        private System.Windows.Forms.Panel panel0;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.ToolStripMenuItem 門市審單比較表ToolStripMenuItem;
        private System.Windows.Forms.Label laVersion;
    }
}

