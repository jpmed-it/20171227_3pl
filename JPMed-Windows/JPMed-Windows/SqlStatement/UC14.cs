﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JPMed_Windows.Utility;

namespace JPMed_Windows.SqlStatement
{
    public class UC14 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetS2OInfo(string _createDate, string _reswkCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT DISTINCT
                         SUBSTRING(BEDNR, 17, 6) AS Text
                        ,BEDNR AS Value
                        FROM STO2TDC1
                        WHERE CRDAT = '{0}'
                        AND RESWK = '{1}'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)
                        ORDER BY 1");
            sb = sb.Replace("{0}", _createDate)
                   .Replace("{1}", _reswkCode);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetS2otdc1Data(string _bendr, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.PROCD
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '調出數量'
                        ,COALESCE(MENGE_GR, 0) AS '驗入數量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.RESWK = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '調入門市'
                        ,COALESCE(B.UMMENGE, 0) AS '前週銷量'
                        ,COALESCE((SELECT COALESCE(LABST, 0) FROM INVMC WHERE A.RESWK = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調出門市庫存量'
                        ,COALESCE((SELECT COALESCE(LABST, 0) FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調入門市庫存量'
                        ,A.WERKS
                        ,A.RESWK
                        FROM STO2TDC1 A
                        LEFT JOIN SALES B ON A.RESWK = B.WERKS AND A.MATNR = B.MATNR AND B.SPBUP = {1}
                        INNER JOIN ASSORT ON ASSORT.EAN11 = A.EAN11 AND ASSORT.WERKS = A.RESWK AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE BEDNR = '{0}'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)
                        ORDER BY A.EAN11");
            sb = sb.Replace("{0}", _bendr)
                   .Replace("{1}", _spbup);
            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _ean, string _werks, string _reswk, string _qty, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE STO2TDC1 SET MENGE_R = {0}, CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{0}", _qty)
                   .Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetDeleteCommand(string _bednr, string _ean, string _werks, string _reswk, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE STO2TDC1 SET DELFLG = 'X', TPROCD = 'C', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }
    }
}
