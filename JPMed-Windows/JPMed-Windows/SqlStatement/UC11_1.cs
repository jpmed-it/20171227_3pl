﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JPMed_Windows.Utility;

namespace JPMed_Windows.SqlStatement
{
    public class UC11_1 : SQLStatementCreator
    {
        //20170225 增加報廢品判斷
        public static string GetSugData(string _spbup, string _werks, string _ean11, string _lockPrd)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.EAN11 AS '商品條碼(EAN)'
                        ,A.MAKTX AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.EAN11 = EAN11 AND A.MATNR = MATNR AND A.WERKS = WERKS AND LGORT = '0001'), 0) AS '庫存量'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.EAN11 = EAN11 AND A.MATNR = MATNR AND WERKS = 'TDC2' AND LGORT = '0001'), 0) AS '總倉庫存'
                        ,COALESCE((SELECT SUM(UMMENGE) FROM SALES WHERE A.EAN11 = EAN11 AND A.MATNR = MATNR AND A.WERKS = WERKS AND SPBUP IN ({0})), 0) AS '前四週銷量'
                        ,CASE WHEN (A.BWSCL = '4' OR A.BWSCL = ' ' OR A.BWSCL IS NULL) THEN 'V' ELSE '' END AS '進倉貨'
                        ,A.MATNR
                        ,A.MEINS
                        FROM ASSORT A
                        INNER JOIN INVMC B ON A.WERKS = B.WERKS AND A.EAN11 = B.EAN11 AND A.MATNR = B.MATNR AND LGORT = '0001'
                        WHERE A.WERKS = '{1}' 
                        AND {2}
                        AND (SELECT TOP 1 LABST FROM INVMC WHERE A.EAN11 = EAN11 AND A.MATNR = MATNR AND A.WERKS = WERKS AND LGORT = '0001') <= 3
                        AND (SELECT COUNT(*) FROM SALES WHERE A.EAN11 = EAN11 AND A.MATNR = MATNR AND A.WERKS = WERKS) > 0
                        AND (A.DELFG != 'X' OR A.DELFG IS NULL)
                        AND (A.TMOUT != 'X' OR A.TMOUT IS NULL)
                        AND {3}
                        AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        ORDER BY 3 ASC, 4 DESC, 1 DESC");

            sb = sb.Replace("{0}", _spbup)
                   .Replace("{1}", _werks)
                   .Replace("{2}", string.IsNullOrEmpty(_ean11) ? "1=1" : string.Format("A.EAN11 NOT IN ({0})", _ean11))
                   .Replace("{3}", _lockPrd == "1" ? "(A.BWSCL = '4' OR A.BWSCL = ' ' OR A.BWSCL IS NULL)" : "1=1");

            return sb.ToString();
        }

        public static string GetInsertCommand(string _bednr, string _matnr, string _ean11, string _werks, string _badat, string _bsart_b, 
                                              string _mange_b, string _meins_b, string _empCode)
        {
            //先DEL在INSERT
            StringBuilder sb = new StringBuilder();
            sb.Append(@"DELETE REPLEN WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = 'TDC2';
                        INSERT INTO REPLEN (BEDNR, MATNR, EAN11, WERKS, RESWK,
                                            LGORT, CRDAT, CRTME, BADAT, BSART_B,
                                            BANFN, BNFPO, MENGE_B, MEINS_B, AFNAM,
                                            BSART_P, EBELN, EBELP, MENGE_P, MEINS_P,
                                            MENGE_GI, MENGE_GR, DELFLG, PROCD, TPROCD,
                                            CHUSN, CHDAT, CHTME) VALUES (
                                            '{0}', '{1}', '{2}', '{3}', 'TDC2',
                                            '0001', '{4}', '{5}', '{6}', '{7}',
                                            null, null, {8}, '{9}', '{10}',
                                            null, null, null, '{8}', '{9}',
                                            null, null, '', '', '',
                                            '{10}', '{4}', '{5}');");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _werks)
                   .Replace("{4}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{5}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{6}", _badat)
                   .Replace("{7}", _bsart_b)
                   .Replace("{8}", _mange_b)
                   .Replace("{9}", _meins_b)
                   .Replace("{10}", _empCode);

            return sb.ToString();
        }
    }
}
