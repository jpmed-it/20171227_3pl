﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JPMed_Windows.Utility;

namespace JPMed_Windows.SqlStatement
{
    public class UC11 : SQLStatementCreator
    {
        /// <summary>
        /// 取得門市名稱
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        /// <summary>
        /// 取得特定員工名稱
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        /// <summary>
        /// 取得最近一次上週銷量
        /// </summary>
        /// <param name="_BADAT">本次需求日期</param>
        /// <param name="_WERKS">店代號</param>
        /// <returns></returns>
        public static string GetLastSALESM(string _BADAT, string _WERKS)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"
            SELECT WERKS, BADAT, SUM(SALESM) as TotalSALESM FROM SALESRATIO
            WHERE BADAT = {@BADAT} AND WERKS = '{@WERKS}'
            GROUP BY WERKS, BADAT");

            sb.Replace("{@BADAT}", _BADAT);
            sb.Replace("{@WERKS}", _WERKS);

            return sb.ToString();
        }

        /// <summary>
        /// 查詢補貨建議量資料清單
        /// 20170608 增加進銷比報表 & 拆併單處理   20170615增加MAABC S熱賣商品改75 91行
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetReplenAdvise(string _WERKS, string _BADAT, string _EAN11, string _MAABC, string _MATKL, string _MAKTX, string _MATKLItem)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"
                        SELECT STORPData.*,REPLENData.EBELN FROM (
                        SELECT LEFT(STORP.MATKL,1) as CLASS1, STORP.BADAT, STORP.EAN11, STORP.MAABC, ASSORT.MAKTX
                        , STORP.HINUM , STORP.MENGE_S, STORP.LONUM, STORP.MENGE_C, ShopINVMC.LABST as ShopLABST
                        , TDCINVMC.LABST as TDCLABST, STORP.MATNR, STORP.MENGE_Q, STORP.PROCD
                        , STORP.MNG03, STORP.MNG07, ISNULL( PRICE.PRICE, 0) as PRICE 
                        FROM STORP 
                        INNER JOIN (SELECT * FROM ASSORT) as ASSORT ON ( STORP.MATNR = ASSORT.MATNR AND STORP.WERKS = ASSORT.WERKS )
                        INNER JOIN RRANGE ON (  RRANGE.WMAAB = CASE WHEN STORP.MAABC = 'S' THEN 'S' WHEN STORP.MAABC = 'A' THEN 'A' WHEN STORP.MAABC = 'B' THEN 'B' ELSE 'C' END)
                        LEFT JOIN  ( SELECT LABST, MATNR, WERKS, LGORT FROM INVMC ) as ShopINVMC on ( ShopINVMC.MATNR = STORP.MATNR AND ShopINVMC.WERKS = '{@WERKS}' AND ShopINVMC.LGORT = '0001')
                        LEFT JOIN  ( SELECT LABST, MATNR, WERKS, LGORT FROM INVMC ) as TDCINVMC on ( TDCINVMC.MATNR = STORP.MATNR AND TDCINVMC.WERKS = 'TDC2' AND TDCINVMC.LGORT = '0001')
						LEFT JOIN PRICE ON ASSORT.MATNR = PRICE.MATNR
                        Where STORP.WERKS = '{@WERKS}'
                        AND STORP.MAABC LIKE '%{@MAABC}%'
                        AND STORP.MATKL LIKE '{@MATKLItem}%'
                        AND STORP.MATKL LIKE '{@MATKL}'
                        AND STORP.BADAT ='{@BADAT}'
                        AND STORP.EAN11 LIKE '%{@EAN11}%'
                        AND ASSORT.MAKTX LIKE '%{@MAKTX}%'
                        AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL) ) AS STORPData 
                        LEFT JOIN ( SELECT ROW_NUMBER() OVER(PARTITION BY MATNR ORDER BY EBELN DESC) AS ROWID 
                        , MATNR, EBELN FROM REPLEN 
						WHERE REPLEN.BADAT = '{@BADAT}' AND REPLEN.WERKS = '{@WERKS}') as REPLENData 
                            ON STORPData.MATNR = REPLENData.MATNR AND REPLENData.ROWID = 1
                        ORDER BY (CASE STORPData.MAABC WHEN 'S' THEN 1 WHEN 'A' THEN 2 WHEN 'B' THEN 3 WHEN 'C' THEN 4 ELSE 5 END), STORPData.MAABC, STORPData.Class1");

            sb = sb.Replace("{@WERKS}", _WERKS);
            if (string.IsNullOrEmpty(_MATKL))
            {
                _MATKL = "%%";
            }
            sb = sb.Replace("{@MATKL}", _MATKL);
            sb = sb.Replace("{@BADAT}", _BADAT);
            sb = sb.Replace("{@EAN11}", _EAN11);
            sb = sb.Replace("{@MAKTX}", _MAKTX);
            sb = sb.Replace("{@MAABC}", _MAABC);
            sb = sb.Replace("{@MATKLItem}", _MATKLItem);

            return sb.ToString();
        }

        /// <summary>
        /// 查詢補貨補貨範圍資料
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetReplenRange()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT * FROM RRANGE");
            return sb.ToString();
        }

        /// <summary>
        /// 取得特定補貨資料
        /// </summary>
        /// <returns></returns>
        public static string SelectSTORPData(string _WERKS, string _BADAT, string _MATNR)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT * FROM [dbo].[STORP]
                       WHERE WERKS = '{@WERKS}'
                         AND BADAT = '{@BADAT}'
                         AND MATNR = '{@MATNR}'");
            sb.Replace("{@WERKS}", _WERKS);
            sb.Replace("{@BADAT}", _BADAT);
            sb.Replace("{@MATNR}", _MATNR);

            return sb.ToString();
        }

        /// <summary>
        /// 更新RP補貨資訊
        /// </summary>
        public static string UpdateSTORP(string _WERKS, string _BADAT, string _MATNR, string _MENGE_C, string _HINUM, string _LONUM, string _AFNAM, string _CRDAT, string _CRTME)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE [dbo].[STORP]
                        SET [MENGE_C] = '{@MENGE_C}'
	                        ,[HINUM] = '{@HINUM}'
                            ,[LONUM] = '{@LONUM}'
	                        ,[AFNAM] = '{@AFNAM}'
	                        ,[CRDAT] = '{@CRDAT}'
                            ,[CRTME] = '{@CRTME}'
                         WHERE WERKS = '{@WERKS}'
                         AND BADAT = '{@BADAT}'
                         AND MATNR = '{@MATNR}'");
            sb.Replace("{@WERKS}", _WERKS);
            sb.Replace("{@BADAT}", _BADAT);
            sb.Replace("{@MATNR}", _MATNR);
            sb.Replace("{@MENGE_C}", _MENGE_C);
            sb.Replace("{@HINUM}", _HINUM);
            sb.Replace("{@LONUM}", _LONUM);
            sb.Replace("{@AFNAM}", _AFNAM);
            sb.Replace("{@CRDAT}", _CRDAT);
            sb.Replace("{@CRTME}", _CRTME);

            return sb.ToString();
        }

        public static string CheckSpecialOpen(string _werksCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT CONVERT(date, REPLSD) AS 'REPLSD', CONVERT(date, REPLDD) AS 'REPLDD'
                        FROM REPLENCT
                        WHERE (DFLAG != 'X' OR DFLAG IS NULL)
                        AND WERKS = '{0}'
                        AND CONVERT(date, GETDATE()) BETWEEN REPLSD AND REPLDD
                        ORDER BY REPLDD DESC");
            sb = sb.Replace("{0}", _werksCode);
            return sb.ToString();
        }

        public static string GetReplenData(string _werksCode, string _badat, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.EAN11 AS '商品條碼(EAN)'
                        ,A.MENGE_B AS '現補量'
                        ,COALESCE((SELECT COALESCE(LABST, 0) FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
                        ,COALESCE(B.UMMENGE, 0) AS '前週銷量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,(SELECT TOP 1 CASE WHEN (BWSCL = '4' OR BWSCL IS NULL OR BWSCL = ' ') THEN 'V' ELSE '' END FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '進倉貨'
                        ,A.BEDNR
                        ,A.MATNR
                        ,A.WERKS
                        ,A.RESWK
                        ,A.PROCD
                        FROM REPLEN A
                        LEFT JOIN SALES B ON A.WERKS = B.WERKS AND A.EAN11 = B.EAN11 AND A.MATNR = B.MATNR AND B.SPBUP = {2}
                        WHERE (A.DELFLG != 'X' OR A.DELFLG IS NULL)
                        AND A.WERKS = '{0}'
                        AND A.BADAT = '{1}'
                        ORDER BY 2 DESC, 3 DESC, 4 ASC");
            sb = sb.Replace("{0}", _werksCode)
                   .Replace("{1}", _badat)
                   .Replace("{2}", _spbup);
            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _ean, string _werks, string _reswk, string _qty, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE REPLEN SET MENGE_B = {0}, CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{0}", _qty)
                   .Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetDeleteCommand(string _bednr, string _ean, string _werks, string _reswk, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE REPLEN SET DELFLG = 'X', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }
    }
}
