﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JPMed_Windows.Utility;

namespace JPMed_Windows.SqlStatement
{
    public class UC15 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetReason()
        {
            return @"SELECT '' AS Text, '' AS 'Value'
                     UNION ALL
                     SELECT BEZEI AS Text , BSGRU + ',' + LGORT AS 'Value' FROM REASON 
                     ORDER BY 2";
        }

        public static string GetRe2tdc1Info(string _createDate, string _reswkCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT DISTINCT
                         SUBSTRING(BEDNR, 17, 6) AS Text
                        ,BEDNR AS Value
                        FROM RE2TDC1
                        WHERE CRDAT = '{0}'
                        AND RESWK = '{1}'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)
                        ORDER BY 1");
            sb = sb.Replace("{0}", _createDate)
                   .Replace("{1}", _reswkCode);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetRe2tdc1Data(string _bendr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         CASE WHEN A.COMARK = 'X' THEN 'V' ELSE '' END AS '退貨確認'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '退貨至'
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(A.MENGE_R, 0) AS '退貨量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.RESWK = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT COALESCE(LABST, 0) FROM INVMC WHERE A.RESWK = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調出門市庫存量'
                        ,(SELECT TOP 1 BEZEI FROM REASON WHERE A.BSGRU = BSGRU) AS '備註'
                        ,A.BSGRU
                        ,A.WERKS
                        ,A.RESWK
                        ,A.PROCD
                        ,CASE WHEN B.LABOR = '001' THEN 1 ELSE 0 END AS LABOR
                        ,B.RUECK
                        FROM RE2TDC1 A
                        LEFT JOIN ASSORT B ON A.RESWK = B.WERKS AND A.EAN11 = B.EAN11
                        WHERE BEDNR = '{0}'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)
						AND (B.TMOUT = '' OR B.TMOUT IS NULL)
                        ORDER BY A.EAN11");
            sb = sb.Replace("{0}", _bendr);
            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _ean, string _werks, string _reswk, string _qty, string _chusn, string _bsgru, string _lgort)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2TDC1 SET MENGE_R = {0}, BSGRU = '{8}', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}', LGORT = '{LGORT}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{0}", _qty)
                   .Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{8}", _bsgru)
                   .Replace("{LGORT}", _lgort);

            return sb.ToString();
        }

        public static string GetDeleteCommand(string _bednr, string _ean, string _werks, string _reswk, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2TDC1 SET DELFLG = 'X', TPROCD = 'C', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetCheckCommand(string _bednr, string _ean, string _werks, string _reswk, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2TDC1 SET COMARK = 'X', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND RESWK = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _reswk)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }
    }
}
