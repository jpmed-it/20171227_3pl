﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JPMed_Windows.Utility;

namespace JPMed_Windows.SqlStatement
{
    public class UC16 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetRe2vendorInfo(string _createDate, string _werksCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT DISTINCT
						(SELECT CASE WHEN LEN(BEDNR) = 10 THEN CRTME ELSE SUBSTRING(BEDNR, 17, 6 ) END ) AS Text
                        ,BEDNR AS Value
                        FROM RE2VENDOR
                        WHERE CRDAT = '{0}'
                        AND WERKS = '{1}'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)
                        ORDER BY 1");
            sb = sb.Replace("{0}", _createDate)
                   .Replace("{1}", _werksCode);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetRe2tdc1Data(string _bendr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         CASE WHEN A.COMARK = 'X' THEN 'V' ELSE '' END AS '退貨確認'
                        ,(SELECT TOP 1 NAME1 FROM VENDOR WHERE A.LIFNR = LIFNR) AS '退貨至'
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(A.MENGE_R, 0) AS '退貨量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT COALESCE(LABST, 0) FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調出門市庫存量'
                        ,A.WERKS
                        ,A.LIFNR
                        ,A.PROCD
                        FROM RE2VENDOR A
						INNER JOIN ASSORT ON ASSORT.EAN11 = A.EAN11 AND ASSORT.WERKS = A.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE BEDNR = '{0}'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)
                        ORDER BY 2, 3");
            sb = sb.Replace("{0}", _bendr);
            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _ean, string _werks, string _lifnr, string _qty, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2VENDOR SET MENGE_R = {0}, CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND LIFNR = '{4}'");
            sb = sb.Replace("{0}", _qty)
                   .Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetDeleteCommand(string _bednr, string _ean, string _werks, string _lifnr, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2VENDOR SET DELFLG = 'X', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND LIFNR = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetCheckCommand(string _bednr, string _ean, string _werks, string _lifnr, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2VENDOR SET COMARK = 'X', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND LIFNR = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetUnCheckCommand(string _bednr, string _ean, string _werks, string _lifnr, string _chusn)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2VENDOR SET COMARK = ' ', CHUSN = '{5}', CHDAT = '{6}', CHTME = '{7}' WHERE BEDNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND LIFNR = '{4}'");
            sb = sb.Replace("{1}", _bednr)
                   .Replace("{2}", _ean)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr)
                   .Replace("{5}", _chusn)
                   .Replace("{6}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{7}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

    }
}
