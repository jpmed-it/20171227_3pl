﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JPMed_Windows.Utility;

namespace JPMed_Windows.SqlStatement
{
    public class UC10 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        //20170522 增加EAN12判斷
        public static string GetAssortInformation(string _storeCode, string _ean)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT MATNR, MAKTX, LABOR, RUECK, BWSCL, DELFG 
            FROM ASSORT 
            WHERE WERKS = '{0}'  AND ( EAN11 = '{1}' OR EAN12 = '{1}') 
            AND (TMOUT = '' OR TMOUT IS NULL)");
            sb = sb.Replace("{0}", _storeCode)
                   .Replace("{1}", _ean);

            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        //20170522 增加EAN12判斷
        public static string GetPrdSalesRecord(string _ean, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.WERKS AS '門市/總倉'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '名稱'
                        ,COALESCE(B.LABST, 0) AS '庫存量'
                        ,COALESCE(C.UMMENGE, 0) AS '前週銷售量'
                        FROM ASSORT A
                        LEFT JOIN INVMC B ON A.WERKS = B.WERKS AND A.EAN11 = B.EAN11 AND A.MATNR = B.MATNR AND B.LGORT = '0001'
                        LEFT JOIN SALES C ON A.WERKS = C.WERKS AND A.EAN11 = C.EAN11 AND C.SPBUP = {2}
                        WHERE A.EAN11 = '{1}' OR A.EAN12 = '{1}' AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        ORDER BY B.LABST DESC, C.UMMENGE, A.WERKS");

            sb = sb.Replace("{1}", _ean)
                   .Replace("{2}", _spbup);

            return sb.ToString();
        }
    }
}
