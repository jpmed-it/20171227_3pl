﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// 補貨作業
    /// </summary>
    public class Replen
    {
        #region Attributes
        /// <summary>
        /// 需求追蹤號碼
        /// PDA產生需求號碼 : 店號(4)+年(4)+月(2)+日(2) ex: ZRP1T0012016050500001 
        /// (類型 ZRP1 T001店 2016/05/05 固定補上000001）
        /// </summary>
        public string bednr { get; set; }

        /// <summary>
        /// 物料號碼
        /// </summary>
        public string matnr { get; set; }

        /// <summary>
        /// 國際貨品代碼 (EAN/UPC)
        /// </summary>
        public string ean11 { get; set; }

        /// <summary>
        /// 需求店代號
        /// 收貨店
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 發貨店代號
        /// 發貨店（調撥單使用） TDC1 固定
        /// </summary>
        public string reswk { get; set; }

        /// <summary>
        /// 儲存位置
        /// 固定為0001
        /// </summary>
        public string lgort { get; set; }

        /// <summary>
        /// 建立日期
        /// 需求建立日期
        /// </summary>
        public string crdat { get; set; }

        /// <summary>
        /// 建立時間
        /// 需求建立時間
        /// </summary>
        public string crtme { get; set; }

        /// <summary>
        /// 需求日期
        /// 需求補貨日（每周兩次捕貨週期日）
        /// </summary>
        public string badat { get; set; }

        /// <summary>
        /// 請購單文件類型
        /// 廠商=>非進倉貨ZRP2 / 自家=>進倉貨 ZRP1
        /// </summary>
        public string bsart_b { get; set; }

        /// <summary>
        /// 請購單號碼
        /// SAP產生請購單號
        /// </summary>
        public string banfn { get; set; }

        /// <summary>
        /// 請購單的項目號碼
        /// </summary>
        public string bnfpo { get; set; }

        /// <summary>
        /// 請購單數量
        /// </summary>
        public decimal menge_b { get; set; }

        /// <summary>
        /// 請購單計量單位
        /// </summary>
        public string meins_b { get; set; }

        /// <summary>
        /// 申請人名稱
        /// </summary>
        public string afnam { get; set; }

        /// <summary>
        /// 採購文件類型
        /// </summary>
        public string bsart_p { get; set; }

        /// <summary>
        /// 採購文件號碼
        /// </summary>
        public string ebeln { get; set; }

        /// <summary>
        /// 採購文件的項目號碼
        /// </summary>
        public decimal ebelp { get; set; }

        /// <summary>
        /// 採購單數量
        /// 就是現補量
        /// </summary>
        public decimal menge_p { get; set; }

        /// <summary>
        /// 採購單計量單位
        /// 填入基礎計量單位
        /// </summary>
        public string meins_p { get; set; }

        /// <summary>
        /// 已發貨數量
        /// 採購歷史紀錄的淨發貨量 （調撥單使用）
        /// </summary>
        public decimal menge_gi { get; set; }

        /// <summary>
        /// 已收貨數量
        /// 採購歷史紀錄的淨收貨量
        /// </summary>
        public decimal menge_gr { get; set; }

        /// <summary>
        /// 刪除註記
        /// </summary>
        public string delflg { get; set; }

        /// <summary>
        /// SAP 已處理註記
        /// </summary>
        public string procd { get; set; }

        /// <summary>
        /// 總倉處理註記
        /// 總倉已抓取資料做檢貨註記
        /// </summary>
        public string tprocd { get; set; }

        /// <summary>
        /// 最後修改者
        /// PC或PDA做修改更新時註記
        /// </summary>
        public string chusn { get; set; }

        /// <summary>
        /// 最後修改日期
        /// PC或PDA做修改更新時註記
        /// </summary>
        public string chdat { get; set; }

        /// <summary>
        /// 最後修改時間
        /// PC或PDA做修改更新時註記
        /// </summary>
        public string chtme { get; set; }
        #endregion

        public Replen() { }
    }

    /// <summary>
    /// 補貨期間特殊開放補貨控制表
    /// </summary>
    public class Replen_CT
    {
        #region Attributes
        /// <summary>
        /// 店代號
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 特開日期起
        /// </summary>
        public string replsd { get; set; }
        
        /// <summary>
        /// 特開日期迄
        /// 特開期間，以迄日為需求日一張單
        /// </summary>
        public string repldd { get; set; }

        /// <summary>
        /// 刪除旗標
        /// </summary>
        public string dflag { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public string crdat { get; set; }

        /// <summary>
        /// 建立日時間
        /// </summary>
        public string crtme { get; set; }

        /// <summary>
        /// 修改日期
        /// </summary>
        public string chdat { get; set; }

        /// <summary>
        /// 修改時間
        /// </summary>
        public string chtme { get; set; }
        #endregion

        public Replen_CT() { }
    }

    /// <summary>
    /// 員工主檔對應表
    /// </summary>
    public class ENMas
    {
        #region Attributes
        /// <summary>
        /// 員工編號
        /// 已離職員工要刪除 ，查不到就顯示錯誤不能執行
        /// </summary>
        public string empno { get; set; }

        /// <summary>
        /// 名稱
        /// </summary>
        public string name { get; set; }
        #endregion

        public ENMas() { }
    }
}
