﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// Error handle Code
    /// </summary>
    public class ErrorCode
    {
        #region Attributes
        /// <summary>
        /// 錯誤碼
        /// W開頭=Warning, E開頭=Error
        /// </summary>
        public string codeno { get; set; }
        
        /// <summary>
        /// 錯誤訊息1
        /// %1,%2, %3...傳入文字
        /// </summary>
        public string text1 { get; set; }

        /// <summary>
        /// 錯誤訊息2
        /// </summary>
        public string text2 { get; set; }

        /// <summary>
        /// 錯誤訊息3
        /// </summary>
        public string text3 { get; set; }

        /// <summary>
        /// 錯誤訊息4
        /// </summary>
        public string text4 { get; set; }
        #endregion

        public ErrorCode() { }
    }
}
