﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// 退貨原因
    /// </summary>
    public class Reason
    {
        #region Attributes
        /// <summary>
        /// 訂購原因
        /// TABLE: TBSGT
        /// </summary>
        public string bsgru {get;set;}

        /// <summary>
        /// 說明
        /// </summary>
        public string bezei { get; set; }
        #endregion

        public Reason() { }
    }
}
