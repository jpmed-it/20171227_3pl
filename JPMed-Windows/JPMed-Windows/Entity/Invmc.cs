﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// 庫存資訊
    /// </summary>
    public class Invmc
    {
        #region Attributes
        /// <summary>
        /// 物料群組
        /// </summary>
        public string matkl { get; set; }

        /// <summary>
        /// 舊物料號碼
        /// </summary>
        public string bismt { get; set; }

        /// <summary>
        /// 國際貨品代碼 (EAN/UPC)
        /// </summary>
        public string ean11 { get; set; }

        /// <summary>
        /// 物料號碼
        /// </summary>
        public string matnr { get; set; }

        /// <summary>
        /// 物料說明〈短文〉
        /// </summary>
        public string maktx { get; set; }
 
        /// <summary>
        /// 店號
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 店號名稱
        /// </summary>
        public string name1 { get; set; }

        /// <summary>
        /// 儲存地點
        /// </summary>
        public string lgort { get; set; }

        /// <summary>
        /// 儲存位置的說明
        /// </summary>
        public string lgobe { get; set; }
        
        /// <summary>
        /// 基礎計量單位
        /// </summary>
        public string meins { get; set; }

        /// <summary>
        /// 未限制使用的已估價庫存
        /// </summary>
        public decimal labst { get; set; } 
        #endregion

        public Invmc() { }
    }
}
