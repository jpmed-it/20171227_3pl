﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// SAP門市主檔
    /// </summary>
    public class Store
    {
        #region Attributes
        /// <summary>
        /// 店代號
        /// LOCNR
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 店代號說明
        /// </summary>
        public string name1 { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string ort01 { get; set; }

        /// <summary>
        /// 郵遞區號
        /// </summary>
        public string pstlz { get; set; }

        /// <summary>
        /// 排序欄位
        /// </summary>
        public string sortl { get; set; }

        /// <summary>
        /// 門牌號碼及街道
        /// </summary>
        public string stras_gp { get; set; }

        /// <summary>
        /// 電話號碼
        /// </summary>
        public string telf1 { get; set; }
        #endregion

        public Store() { }
    }
}
