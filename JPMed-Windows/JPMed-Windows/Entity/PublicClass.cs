﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace JPMed_Windows
{
    public class PublicClass
    {
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // If its Monday, Tuesday or Wednesday, then it'll be the same week# as whatever Thursday, Friday or Saturday are,  and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday) time = time.AddDays(3);

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        //轉換RUECK為相對應的退貨協議文字敘述
        public static string ConvertToRUECKString(string strRUECK)
        {
            List<string> canReturnList = new List<string>() { "01", "02", "03", "06", "07" };

            List<string> canNotReturnList = new List<string>() { "04", "05", "99" };

            if (canReturnList.Contains(strRUECK))
            {
                return "可退";
            }
            else if (canNotReturnList.Contains(strRUECK))
            {
                return "不可退";
            }
            else
            {
                return strRUECK;
            }
        }

    }
}
