﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// SAP鋪貨主檔資料
    /// </summary>
    public class Assort
    {
        #region Attributes
        /// <summary>
        /// 店代號
        /// LOCNR
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 物料號碼
        /// </summary>
        public string matnr { get; set; }

        /// <summary>
        /// 物料群組
        /// </summary>
        public string matkl { get; set; }

        /// <summary>
        /// 物料說明
        /// </summary>
        public string maktx { get; set; }

        /// <summary>
        /// 國際貨品代碼 (EAN/UPC)
        /// </summary>
        public string ean11 { get; set; }

        /// <summary>
        /// 舊物料號碼 
        /// </summary>
        public string bismt { get; set; }

        /// <summary>
        /// 供應來源
        /// 0:廠商貨, 1:自家貨
        /// </summary>
        public int bwscl { get; set; }

        /// <summary>
        /// 退貨協議
        /// 退換貨別:A01(可退貨), A04(買斷品), A05(自家商品), A06(一個月退食品, 可退貨), A07(可換貨,可退貨)
        /// </summary>
        public string rueck { get; set; }

        /// <summary>
        /// 貨源
        /// 補貨型態:1(依補貨流程), 3(產生採購單), 4(標準前移轉庫存)
        /// </summary>
        public string bwscl1 { get; set; }

        /// <summary>
        /// 基礎計量單位
        /// </summary>
        public string meins { get; set; }

        /// <summary>
        /// 跨店廢番
        /// PDA需檢查如果此欄位有值=X 則不能補貨(Cosmos的失效日期, 停進、停賣、停退、停止調撥)
        /// </summary>
        public string delfg { get; set; }

        /// <summary>
        /// 鋪貨清單失效
        /// PDA需檢查如果此欄位有值則不能補貨(目前未使用)
        /// </summary>
        public string tmout { get; set; }
        #endregion

        public Assort() { }
    }
}
