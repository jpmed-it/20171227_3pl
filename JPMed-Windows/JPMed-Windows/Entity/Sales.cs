﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// SAP sis週銷售量資料
    /// </summary>
    public class Sales
    {
        #region Attributes
        /// <summary>
        /// 過帳期間
        /// 201601 : 2016年的第一周
        /// 需要確認.net 函數算的週數與SAP是否相同
        /// </summary>
        public decimal spbup { get; set; }

        /// <summary>
        /// 店代號
        /// KUNNR
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 物料號碼
        /// </summary>
        public string matnr { get; set; }

        /// <summary>
        /// 國際貨品代碼 (EAN/UPC)
        /// </summary>
        public string ean11 { get; set; }

        /// <summary>
        /// 銷售量
        /// </summary>
        public decimal ummenge { get; set; }
        #endregion

        public Sales() { }
    }
}
