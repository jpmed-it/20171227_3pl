﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// 補貨建議商品售價
    /// 20170608 增加進銷比報表
    /// </summary>
    public class StorpPrice
    {
        public string MATNR { get; set; }

        public string MENGE_C { get; set; }

        public int PRICE { get; set; }
    }
}
