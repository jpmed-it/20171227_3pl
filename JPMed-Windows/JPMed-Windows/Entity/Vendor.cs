﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// SAP供應商資料
    /// </summary>
    public class Vendor
    {
        #region Attributes
        /// <summary>
        /// 供應商代號
        /// </summary>
        public string lifnr { get; set; }

        /// <summary>
        /// 供應商名稱
        /// </summary>
        public string name1 { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string ort01 { get; set; }

        /// <summary>
        /// 郵遞區號
        /// </summary>
        public string pstlz { get; set; }

        /// <summary>
        /// 簡稱
        /// </summary>
        public string sortl { get; set; }

        /// <summary>
        /// 門牌號碼及街道
        /// </summary>
        public string stras_gp { get; set; }

        /// <summary>
        /// 加值稅登記號碼
        /// </summary>
        public string stceg { get; set; }

        /// <summary>
        /// 電話號碼
        /// 聯絡人電話
        /// </summary>
        public string telf1 { get; set; }

        /// <summary>
        /// 聯絡人名稱
        /// 採購聯絡人
        /// </summary>
        public string name1_cp { get; set; }
        #endregion

        public Vendor() { }
    }
}
