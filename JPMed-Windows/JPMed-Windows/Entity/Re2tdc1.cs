﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Windows.Entity
{
    /// <summary>
    /// 退貨 退總倉作業
    /// </summary>
    public class Re2tdc1
    {
        #region Attributes
        /// <summary>
        /// 退貨追蹤號碼
        /// PDA產生需求號碼 : 店號(4)+年(4)+月(2)+日(2) ex: ZUR1T00120160505134835 (類型 ZUR1 T001店 2016/05/05 下午 13點 48分35秒的需求）
        /// </summary>
        public string bednr { get; set; }

        /// <summary>
        /// 物料號碼
        /// </summary>
        public string matnr { get; set; }

        /// <summary>
        /// 國際貨品代碼 (EAN/UPC)
        /// </summary>
        public string ean11 { get; set; }

        /// <summary>
        /// 退貨店代號
        /// 退貨店
        /// </summary>
        public string reswk { get; set; }

        /// <summary>
        /// 總倉代號
        /// 固定為TDC１總倉
        /// </summary>
        public string werks { get; set; }

        /// <summary>
        /// 儲存位置
        /// 固定為0009
        /// </summary>
        public string lgort { get; set; }

        /// <summary>
        /// 建立日期
        /// 退貨建立日期
        /// </summary>
        public string crdat { get; set; }

        /// <summary>
        /// 建立時間
        /// 134835  下午 13點 48分35秒
        /// </summary>
        public string crtme { get; set; }

        /// <summary>
        /// 申請人名稱
        /// 填入員工編號最後四碼
        /// </summary>
        public string afnam { get; set; }

        /// <summary>
        /// 採購文件類型
        /// 門市退貨回總倉 ZUR1
        /// </summary>
        public string bsart { get; set; }

        /// <summary>
        /// 採購文件號碼
        /// SAP產生請採購單號或調撥單號
        /// </summary>
        public string ebeln { get; set; }

        /// <summary>
        /// 採購文件的項目號碼
        /// </summary>
        public string ebelp { get; set; }

        /// <summary>
        /// 退貨數量
        /// </summary>
        public string menge_ｒ { get; set; }

        /// <summary>
        /// 退貨計量單位
        /// </summary>
        public string meins { get; set; }

        /// <summary>
        /// 已發貨數量
        /// 採購歷史紀錄的淨發貨量 （調撥單使用）
        /// </summary>
        public string menge_gi { get; set; }

        /// <summary>
        /// 已收貨數量
        /// 採購歷史紀錄的淨收貨量
        /// </summary>
        public string menge_gr { get; set; }

        /// <summary>
        /// 退貨確認
        /// 已經確認的SAP才拉回來建單
        /// </summary>
        public string comark { get; set; }

        /// <summary>
        /// 刪除註記
        /// </summary>
        public string delflg { get; set; }

        /// <summary>
        /// SAP 已處理註記
        /// </summary>
        public string procd { get; set; }

        /// <summary>
        /// 最後修改者
        /// PC或PDA做修改更新時註記
        /// </summary>
        public string chusn { get; set; }

        /// <summary>
        /// 最後修改日期
        /// PC或PDA做修改更新時註記
        /// </summary>
        public string chdat { get; set; }

        /// <summary>
        /// 最後修改時間
        /// PC或PDA做修改更新時註記
        /// </summary>
        public string chtme { get; set; }

        /// <summary>
        /// 訂購原因
        /// </summary>
        public string bsgru { get; set; }
        #endregion

        public Re2tdc1() { }
    }
}
