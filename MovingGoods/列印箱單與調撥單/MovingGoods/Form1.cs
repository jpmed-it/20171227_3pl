﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
//using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MovingGoods
{
    public partial class Form1 : Form
    {
        #region 所需全域變數
        string KeyCode_Box;
        string StoreCode;
        Codes Cs = new Codes();
        #endregion

        #region ※接收外部呼叫程式變數，重要。
        public Form1(string Num)
        {
            KeyCode_Box = Num;
            StoreCode = Num.Substring(4, 4);
            InitializeComponent();
            SheetType.Text = Cs.GetSheetType(Num);
        }
        #endregion

        #region ※Form1的表單讀取事件。
        private void Form1_Load(object sender, EventArgs e)
        {
           SetFormData(  Cs.GetPrintingData(KeyCode_Box,StoreCode));
           //PrintPanel(Print_Panel);
           //this.Dispose();
        }
        #endregion

        #region 所有列印相關程式

        #region ※預覽列印按鈕按下後的事件。
        private void Print_Btn_Click(object sender, EventArgs e)
        {
            PrintPanel(Print_Panel);
            PrintPanel(Print_Panel);
            this.Dispose();
        }
        #endregion

        #region 列印主程式
        private void PrintPanel(Panel pnl)
        {
            PrintDialog myPrintDialog = new PrintDialog();
            PrinterSettings values;
            values = myPrintDialog.PrinterSettings;
            myPrintDialog.Document = printDocument1;
            printDocument1.PrintController = new StandardPrintController();
            printDocument1.PrintPage +=
              new System.Drawing.Printing.PrintPageEventHandler(printDocument1_PrintPage);
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();
           // printDocument1.Print();
            printDocument1.Dispose();
        }
        #endregion

        #region ※printDocument1_PrintPage事件，重要！！
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            using (Bitmap bmp = new Bitmap(Print_Panel.ClientSize.Width, Print_Panel.ClientSize.Height))
            {
                Print_Panel.DrawToBitmap(bmp, Print_Panel.ClientRectangle);
                RectangleF bounds = e.PageSettings.PrintableArea;
                float factor = ((float)bmp.Height / (float)bmp.Width);
                e.Graphics.DrawImage(bmp, bounds.Left, bounds.Top,
                                          bounds.Width, factor * bounds.Width);
            }
        }
        #endregion

        #region 將列印資料填入畫面
        void SetFormData(Codes.PrintingData PD)
        {
            #region 由單劇類型決定要顯示的資料
            switch (PD.PrintType)
            {
                case "RE2TDC1": //退總倉
                    lab_Out.Text = "退貨門市：";
                    lab_Code.Text = "退貨單號：";
                    lab_Num.Text = "退貨數量：";
                    lab_In.Text = "退貨單位：";
                    lab_Date.Text = "退貨日期：";
                    break;
                case "RE2VENDOR": //直退廠商
                     lab_Out.Text = "退貨門市：";
                    lab_Code.Text = "退貨單號：";
                    lab_Num.Text = "退貨數量：";
                    lab_In.Text = "退貨廠商：";
                    lab_Date.Text = "退貨日期：";
                    break;
                case "STO2STO": //調撥至其他門市
                    lab_Out.Text = "調出門市：";
                    lab_Code.Text = "調撥單號：";
                    lab_Num.Text = "調出數量：";
                    lab_In.Text = "調入門市：";
                    lab_Date.Text = "調出日期：";
                    break;
                case "STO2TDC1 ": //調撥至總倉
                     lab_Out.Text = "調出門市：";
                    lab_Code.Text = "調撥單號：";
                    lab_Num.Text = "調出數量：";
                    lab_In.Text = "調入單位：";
                    lab_Date.Text = "調出日期：";
                    break;
                default:
                    break;
            }
            #endregion

            OutStore.Text = PD.GoesOut;
            BarCode.Text =string.Format("*{0}*",PD.Box_BarCode);
            Code.Text = PD.Box_BarCode;
            Code.Width = BarCode.Width;
            Num.Text = PD.TotalNum.ToString();
            In.Text = PD.GoesIn;
            Date.Text = PD.SheetDate;
        }
        #endregion

        #endregion

    }
}
