﻿namespace MovingGoods
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Print_Btn = new System.Windows.Forms.Button();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.Print_Panel = new System.Windows.Forms.Panel();
            this.SheetType = new System.Windows.Forms.Label();
            this.Code = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Label();
            this.lab_Date = new System.Windows.Forms.Label();
            this.In = new System.Windows.Forms.Label();
            this.lab_In = new System.Windows.Forms.Label();
            this.Num = new System.Windows.Forms.Label();
            this.lab_Num = new System.Windows.Forms.Label();
            this.BarCode = new System.Windows.Forms.Label();
            this.lab_Code = new System.Windows.Forms.Label();
            this.OutStore = new System.Windows.Forms.Label();
            this.lab_Out = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Print_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Print_Btn
            // 
            this.Print_Btn.Font = new System.Drawing.Font("標楷體", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.Print_Btn.Location = new System.Drawing.Point(718, 5);
            this.Print_Btn.Name = "Print_Btn";
            this.Print_Btn.Size = new System.Drawing.Size(75, 23);
            this.Print_Btn.TabIndex = 12;
            this.Print_Btn.Text = "列印預覽";
            this.Print_Btn.UseVisualStyleBackColor = true;
            this.Print_Btn.Click += new System.EventHandler(this.Print_Btn_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // Print_Panel
            // 
            this.Print_Panel.BackColor = System.Drawing.Color.White;
            this.Print_Panel.Controls.Add(this.label2);
            this.Print_Panel.Controls.Add(this.SheetType);
            this.Print_Panel.Controls.Add(this.Code);
            this.Print_Panel.Controls.Add(this.label12);
            this.Print_Panel.Controls.Add(this.label11);
            this.Print_Panel.Controls.Add(this.Date);
            this.Print_Panel.Controls.Add(this.lab_Date);
            this.Print_Panel.Controls.Add(this.In);
            this.Print_Panel.Controls.Add(this.lab_In);
            this.Print_Panel.Controls.Add(this.Num);
            this.Print_Panel.Controls.Add(this.lab_Num);
            this.Print_Panel.Controls.Add(this.BarCode);
            this.Print_Panel.Controls.Add(this.lab_Code);
            this.Print_Panel.Controls.Add(this.OutStore);
            this.Print_Panel.Controls.Add(this.lab_Out);
            this.Print_Panel.Location = new System.Drawing.Point(12, 34);
            this.Print_Panel.Name = "Print_Panel";
            this.Print_Panel.Size = new System.Drawing.Size(899, 396);
            this.Print_Panel.TabIndex = 15;
            // 
            // SheetType
            // 
            this.SheetType.AutoSize = true;
            this.SheetType.Font = new System.Drawing.Font("微軟正黑體", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SheetType.Location = new System.Drawing.Point(84, 16);
            this.SheetType.Name = "SheetType";
            this.SheetType.Size = new System.Drawing.Size(128, 47);
            this.SheetType.TabIndex = 27;
            this.SheetType.Text = "label1";
            // 
            // Code
            // 
            this.Code.AutoSize = true;
            this.Code.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Code.Location = new System.Drawing.Point(235, 197);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(0, 16);
            this.Code.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(469, 321);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(161, 16);
            this.label12.TabIndex = 25;
            this.label12.Text = "_________________";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體-ExtB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(381, 305);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 16);
            this.label11.TabIndex = 24;
            this.label11.Text = "打包人員：";
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Location = new System.Drawing.Point(515, 96);
            this.Date.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(71, 16);
            this.Date.TabIndex = 23;
            this.Date.Text = "label10";
            // 
            // lab_Date
            // 
            this.lab_Date.AutoSize = true;
            this.lab_Date.Font = new System.Drawing.Font("新細明體-ExtB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Date.Location = new System.Drawing.Point(415, 96);
            this.lab_Date.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_Date.Name = "lab_Date";
            this.lab_Date.Size = new System.Drawing.Size(59, 16);
            this.lab_Date.TabIndex = 22;
            this.lab_Date.Text = "日期：";
            // 
            // In
            // 
            this.In.AutoSize = true;
            this.In.Font = new System.Drawing.Font("標楷體", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.In.Location = new System.Drawing.Point(114, 305);
            this.In.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.In.Name = "In";
            this.In.Size = new System.Drawing.Size(62, 16);
            this.In.TabIndex = 21;
            this.In.Text = "label8";
            // 
            // lab_In
            // 
            this.lab_In.AutoSize = true;
            this.lab_In.Font = new System.Drawing.Font("新細明體-ExtB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_In.Location = new System.Drawing.Point(26, 305);
            this.lab_In.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_In.Name = "lab_In";
            this.lab_In.Size = new System.Drawing.Size(59, 16);
            this.lab_In.TabIndex = 20;
            this.lab_In.Text = "調入：";
            // 
            // Num
            // 
            this.Num.AutoSize = true;
            this.Num.Font = new System.Drawing.Font("標楷體", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Num.Location = new System.Drawing.Point(114, 239);
            this.Num.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Num.Name = "Num";
            this.Num.Size = new System.Drawing.Size(62, 16);
            this.Num.TabIndex = 19;
            this.Num.Text = "label6";
            // 
            // lab_Num
            // 
            this.lab_Num.AutoSize = true;
            this.lab_Num.Font = new System.Drawing.Font("新細明體-ExtB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Num.Location = new System.Drawing.Point(26, 239);
            this.lab_Num.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_Num.Name = "lab_Num";
            this.lab_Num.Size = new System.Drawing.Size(59, 16);
            this.lab_Num.TabIndex = 18;
            this.lab_Num.Text = "數量：";
            // 
            // BarCode
            // 
            this.BarCode.AutoSize = true;
            this.BarCode.BackColor = System.Drawing.Color.White;
            this.BarCode.Font = new System.Drawing.Font("Free 3 of 9", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BarCode.Location = new System.Drawing.Point(117, 156);
            this.BarCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.BarCode.Name = "BarCode";
            this.BarCode.Size = new System.Drawing.Size(0, 36);
            this.BarCode.TabIndex = 17;
            // 
            // lab_Code
            // 
            this.lab_Code.AutoSize = true;
            this.lab_Code.Font = new System.Drawing.Font("新細明體-ExtB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Code.Location = new System.Drawing.Point(26, 170);
            this.lab_Code.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_Code.Name = "lab_Code";
            this.lab_Code.Size = new System.Drawing.Size(59, 16);
            this.lab_Code.TabIndex = 16;
            this.lab_Code.Text = "單號：";
            // 
            // OutStore
            // 
            this.OutStore.AutoSize = true;
            this.OutStore.Font = new System.Drawing.Font("標楷體", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.OutStore.Location = new System.Drawing.Point(114, 96);
            this.OutStore.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OutStore.Name = "OutStore";
            this.OutStore.Size = new System.Drawing.Size(62, 16);
            this.OutStore.TabIndex = 15;
            this.OutStore.Text = "label2";
            // 
            // lab_Out
            // 
            this.lab_Out.AutoSize = true;
            this.lab_Out.Font = new System.Drawing.Font("新細明體-ExtB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Out.Location = new System.Drawing.Point(26, 96);
            this.lab_Out.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_Out.Name = "lab_Out";
            this.lab_Out.Size = new System.Drawing.Size(59, 16);
            this.lab_Out.TabIndex = 14;
            this.lab_Out.Text = "調出：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(23, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 35);
            this.label2.TabIndex = 71;
            this.label2.Text = "門市";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 874);
            this.Controls.Add(this.Print_Btn);
            this.Controls.Add(this.Print_Panel);
            this.Font = new System.Drawing.Font("標楷體", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "調/退貨箱單";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Print_Panel.ResumeLayout(false);
            this.Print_Panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Print_Btn;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Panel Print_Panel;
        private System.Windows.Forms.Label Code;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.Label lab_Date;
        private System.Windows.Forms.Label In;
        private System.Windows.Forms.Label lab_In;
        private System.Windows.Forms.Label Num;
        private System.Windows.Forms.Label lab_Num;
        private System.Windows.Forms.Label BarCode;
        private System.Windows.Forms.Label lab_Code;
        private System.Windows.Forms.Label OutStore;
        private System.Windows.Forms.Label lab_Out;
        private System.Windows.Forms.Label SheetType;
        private System.Windows.Forms.Label label2;
    }
}

