﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MovingGoods
{
    class Codes
    {
        #region 讀取列印相關資料
        public PrintingData GetPrintingData(string KeyCode, string StoreCode)
        {
            PrintingData PT = new PrintingData();
            PT.PrintType = GetTableName(KeyCode); //由搜尋碼來取得資料庫名稱
            PT.VD = new VendorData();//廠商資料

            string strBSART = KeyCode.Substring(0, 4); ;
            //20170422 Milk
            //若BEDNR等於10碼，代表手工單則須再查詢店代號 
            if (KeyCode.Replace(" ", "").Length ==10)
            {
                strBSART = GetBSARTByBEDNR(KeyCode.Replace(" ", ""));
                StoreCode = GetStoreCodeByBEDNR(KeyCode.Replace(" ", ""), strBSART, PT.PrintType);
            }

            //int LineNo = 1;
            int Count = 0;//算總量
            string LIFNR = "";


            string Comark = "";

            #region 依流程差異取得資料 (退總倉專用需要備註、退廠商的話收貨單位要改成廠商)
            string PS = ",GoesOut.Name1    AS 轉出單位 ,GoesIn.Name1      AS 轉入單位"; //收貨單位預設值為門市(倉別)
            string AdditionalTable = "LEFT JOIN STORE AS GoesOut ON " + PT.PrintType + ".RESWK=GoesOut.WERKS LEFT join STORE AS GoesIn ON " + PT.PrintType + ".WERKS=GoesIn.WERKS "; //查詢倉別及門市


            if (PT.PrintType == "RE2TDC1")//退總倉作業
            {
                PS += ",RE2TDC1.LGORT     AS 倉別";//退總倉要加查詢備註
                //AdditionalTable += "LEFT JOIN REASON ON " + PT.PrintType + ".BSGRU=REASON.BSGRU ";//查詢備註
                //20170313 增加報廢品檢查
                AdditionalTable += " INNER JOIN ASSORT GoesInASSORT ON ASSORT.EAN11 = GoesInASSORT.EAN11 AND (GoesInASSORT.TMOUT = '' OR GoesInASSORT.TMOUT IS NULL) AND GoesInASSORT.WERKS = GoesIn.WERKS ";
                Comark = "AND (RE2TDC1.COMARK='X' OR RE2TDC1.COMARK IS NULL)";
            }
            else if (PT.PrintType == "RE2VENDOR")
            {
                PS = ",GoesOut.Name1    AS 轉出單位 ,VENDOR.NAME1     AS 轉入單位 ,RE2VENDOR.LIFNR AS VENDORCODE";//退廠商轉入單位為廠商
                AdditionalTable = "LEFT JOIN STORE AS GoesOut ON " + PT.PrintType + ".WERKS=GoesOut.WERKS LEFT JOIN VENDOR ON RE2VENDOR.LIFNR=VENDOR.LIFNR";//查詢廠商
                //20170313 增加報廢品檢查
                AdditionalTable += " INNER JOIN ASSORT GoesInASSORT ON ASSORT.EAN11 = GoesInASSORT.EAN11 AND (GoesInASSORT.TMOUT = '' OR GoesInASSORT.TMOUT IS NULL) AND GoesInASSORT.WERKS = RE2VENDOR.WERKS ";
                Comark = "AND (RE2VENDOR.COMARK='X' OR RE2VENDOR.COMARK IS NULL)";
            }
            else if(PT.PrintType =="STO2STO" && strBSART=="ZUB1")
            {
                Comark = "AND (STO2STO.COMARK='X' OR STO2STO.COMARK IS NULL)";
            }
            else
            {
                AdditionalTable += " INNER JOIN ASSORT GoesInASSORT ON ASSORT.EAN11 = GoesInASSORT.EAN11 AND (GoesInASSORT.TMOUT = '' OR GoesInASSORT.TMOUT IS NULL) AND GoesInASSORT.WERKS = GoesIn.WERKS ";
            }


            #endregion



            try
            {
                //string strConn = ConfigurationManager.ConnectionStrings["PDADEV_ConnectString"].ConnectionString.ToString();
                string strConn = ConfigurationManager.ConnectionStrings["MovingGoods.Properties.Settings.PDADEV_ConnectString"].ConnectionString.ToString();
                DataTable dt = new DataTable();
                string QueryString = string.Format(@"
SELECT {0}.BEDNR        AS 退貨單條碼
      ,{0}.BSART AS 類型代碼
      ,ORT.BATXT AS 文件類型
      ,ASSORT.EAN11     AS 條碼
	  ,ASSORT.MAKTX     AS 品名
	  ,{0}.MENGE_R      AS 數量
	  ,ASSORT.MEINS     AS 單位
      ,{0}.CHDAT        AS 單據日期
      {1}
FROM {0} 
LEFT JOIN ASSORT ON {0}.MATNR=ASSORT.MATNR
JOIN ORDTYP AS ORT ON {0}.BSART=ORT.BSART
{2}


WHERE {0}.BEDNR=@ID AND ({0}.DELFLG <>'X' OR {0}.DELFLG IS NULL ) AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL) AND ASSORT.WERKS=@STORE {3}"
                    , PT.PrintType, PS, AdditionalTable, Comark);
                using (SqlConnection conn = new SqlConnection(strConn))
                {
                    using (SqlCommand command = new SqlCommand(QueryString, conn))
                    {
                        command.Parameters.AddWithValue("@ID", KeyCode);
                        command.Parameters.AddWithValue("@STORE", StoreCode);
                        conn.Open();

                        //dt.Columns.Add("序", typeof(string)); 20160701會議稱不需要
                        dt.Columns.Add("條碼", typeof(string));
                        dt.Columns.Add("品名", typeof(string));
                        dt.Columns.Add("數量", typeof(string));
                        //dt.Columns.Add("單位", typeof(string));

                        if (PT.PrintType == "RE2TDC1")
                            dt.Columns.Add("倉別", typeof(string));

                        DataRow dr;

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                if (string.IsNullOrEmpty(PT.Box_BarCode))
                                    PT.Box_BarCode = Convert.ToString(dataReader["退貨單條碼"]);

                                if (string.IsNullOrEmpty(PT.SheetDate))
                                    PT.SheetDate = Convert.ToString(dataReader["單據日期"]);

                                if (string.IsNullOrEmpty(PT.GoesOut))
                                    PT.GoesOut = Convert.ToString(dataReader["轉出單位"]);

                                if (string.IsNullOrEmpty(PT.GoesIn))
                                    PT.GoesIn = Convert.ToString(dataReader["轉入單位"]);

                                if (string.IsNullOrEmpty(PT.SheetType))
                                    PT.SheetType = string.Format("{0} {1}", Convert.ToString(dataReader["類型代碼"]), Convert.ToString(dataReader["文件類型"]));

                                if (PT.PrintType == "RE2VENDOR" && string.IsNullOrEmpty(LIFNR))
                                    LIFNR = Convert.ToString(dataReader["VENDORCODE"]);

                                dr = dt.NewRow();

                                //dr["序"] = LineNo;
                                dr["條碼"] = Convert.ToString(dataReader["條碼"]);
                                dr["品名"] = Convert.ToString(dataReader["品名"]);
                                dr["數量"] = Convert.ToString(Convert.ToInt32(dataReader["數量"]));
                                //dr["單位"] = Convert.ToString(dataReader["單位"]);

                                if (PT.PrintType == "RE2TDC1")
                                    dr["倉別"] = Convert.ToString(dataReader["倉別"]);

                                //LineNo++;
                                Count += Convert.ToInt32(dataReader["數量"]);

                                dt.Rows.Add(dr);
                            }
                            //DataRow dr1 = dt.NewRow();

                            //if (PT.PrintType == "RE2VENDOR" || PT.PrintType == "RE2TDC1")
                            //    dr1["條碼"] = "此張退貨單據";

                            //else
                            //    dr1["條碼"] = "此張調撥單據";

                            //dr1["品名"] = string.Format("({0}) 總件數共：", PT.Box_BarCode);
                            //dr1["數量"] = Count;
                            //dr1["單位"] = "件";

                            //dt.Rows.Add(dr1);
                        }
                    }
                }
                if (PT.PrintType == "RE2VENDOR")
                {
                    string QueryString2 = string.Format(@"
SELECT [LIFNR] AS 廠商編號
      ,[NAME1] AS 廠商名稱
      ,[STCEG] AS 廠商統編
      ,[TELF1] AS 廠商電話
      ,[SellerName] AS 廠商聯絡人
  FROM [dbo].[VENDOR]
 WHERE LIFNR=@li");
                    using (SqlConnection conn = new SqlConnection(strConn))
                    {
                        using (SqlCommand command = new SqlCommand(QueryString2, conn))
                        {
                            command.Parameters.AddWithValue("@li", LIFNR);
                            conn.Open();


                            using (SqlDataReader dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {

                                    PT.VD.VendorCode = Convert.ToString(dataReader["廠商編號"]);
                                    PT.VD.Vendor = Convert.ToString(dataReader["廠商名稱"]);
                                    PT.VD.VendorSTCEG = Convert.ToString(dataReader["廠商統編"]);
                                    PT.VD.VendorPhone = Convert.ToString(dataReader["廠商電話"]);
                                    PT.VD.VendorConnector = Convert.ToString(dataReader["廠商聯絡人"]);
                                }
                            }
                        }
                    }
                }
                if (PT.PrintType == "RE2VENDOR" || PT.PrintType == "RE2TDC1")
                {
                    if (Count == 0)
                        MessageBox.Show("無退貨資料");

                }
                PT.TotalNum = Count;
                PT.GoodDetail = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //System.Environment.Exit(System.Environment.ExitCode);
            return PT;

        }
        #endregion

        #region 判斷要讀取那個Table
        string GetTableName(string KeyCode)
        {
            string KeyCode_fix = KeyCode.Substring(0, 4);

            //20170422 Milk
            //若BEDNR等於10碼，代表手工單則須再查詢單型 
            if (KeyCode.Replace(" ","").Length == 10)
            {
                KeyCode_fix = GetBSARTByBEDNR(KeyCode.Replace(" ", ""));
            }
            string TableName = "";
            switch (KeyCode_fix)
            {
                case "ZUR1": //退總倉
                    TableName = "RE2TDC1";
                    break;
                case "ZRE3": //直退廠商
                    TableName = "RE2VENDOR";
                    break;
                case "ZUB1": //調撥至其他門市
                case "ZUB3":
                    TableName = "STO2STO";
                    break;
                case "ZUB2": //調撥至總倉
                    TableName = "STO2TDC1";
                    break;
                default:
                    break;
            }
            return TableName;
        }

        public string GetSheetType(string KeyCode)//20170921 新增功能
        {
            string KeyCode_fix = KeyCode.Substring(0, 4);

            //20170422 Milk
            //若BEDNR等於10碼，代表手工單則須再查詢單型 
            if (KeyCode.Replace(" ", "").Length == 10)
            {
                KeyCode_fix = GetBSARTByBEDNR(KeyCode.Replace(" ", ""));
            }
            string Type = "";
            switch (KeyCode_fix)
            {
                case "ZUR1": //退總倉
                    Type = "退總倉";
                    break;
                case "ZRE3": //直退廠商
                    Type = "直退廠商";
                    break;
                case "ZUB1": //調撥至其他門市
                case "ZUB3":
                    Type = "間調撥";
                    break;
                case "ZUB2": //調撥至總倉
                    Type = "調撥至總倉";
                    break;
                default:
                    break;
            }
            return Type;
        }
        /// <summary>
        /// 透過BEDNR向GMOVEH取得單型
        /// 20170422 Milk
        /// </summary>
        /// <param name="strBEDNR">追蹤單號</param>
        /// <returns></returns>
        string GetBSARTByBEDNR(string strBEDNR)
        {
            string strConn = ConfigurationManager.ConnectionStrings["MovingGoods.Properties.Settings.PDADEV_ConnectString"].ConnectionString.ToString();

            string QueryString = "SELECT BSART FROM GMOVEH WHERE BEDNR =@BEDNR";

            using (SqlConnection conn = new SqlConnection(strConn))
            {
                using (SqlCommand cmd = new SqlCommand(QueryString, conn))
                {
                    cmd.Parameters.AddWithValue("@BEDNR", strBEDNR);
                    conn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return dr[0].ToString();
                        }
                    }
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// 透過BEDNR取得店代號
        /// 20170422 Milk
        /// </summary>
        /// <param name="strBEDNR"></param>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        string GetStoreCodeByBEDNR(string strBEDNR, string strBSART, string strTableName)
        {
            string strConn = ConfigurationManager.ConnectionStrings["MovingGoods.Properties.Settings.PDADEV_ConnectString"].ConnectionString.ToString();

            string QueryString = "SELECT WERKS FROM {0} WHERE EBELN = @BEDNR AND BSART = @BSART";
            QueryString = string.Format(QueryString, strTableName);

            using (SqlConnection conn = new SqlConnection(strConn))
            {
                using (SqlCommand cmd = new SqlCommand(QueryString, conn))
                {
                    cmd.Parameters.AddWithValue("@BEDNR", strBEDNR);
                    cmd.Parameters.AddWithValue("@BSART", strBSART);
                    conn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return dr[0].ToString();
                        }
                    }
                }
            }

            return string.Empty;
        }
        #endregion

        #region 類別區
        public class PrintingData
        {
            public string Box_BarCode { get; set; }
            public string PrintType { get; set; }
            public DataTable GoodDetail { get; set; }
            public string SheetDate { get; set; }
            public string GoesOut { get; set; }
            public string GoesIn { get; set; }
            public int TotalNum { get; set; }
            public string SheetType { get; set; }
            public VendorData VD { get; set; }

        }

        public class VendorData
        {
            public string VendorCode { get; set; }
            public string Vendor { get; set; }
            public string VendorSTCEG { get; set; }
            public string VendorPhone { get; set; }
            public string VendorConnector { get; set; }
        }
        #endregion
    }
}
