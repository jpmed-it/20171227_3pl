﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace MovingGoods
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main(string[] FormNum)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //測試專用
            string[] FormNum1 = {    "ZUB1T02620171226161735" ,"ZUB2T00220161125133343" };
            //MessageBox.Show(new Form1(FormNum[0]).ToString());
            Application.Run(new Form1(FormNum[0]));
            Application.Run(new Form2(FormNum[0]));
        }

       
    }
}
