﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
//using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MovingGoods
{
    public partial class Form2 : Form
    {
        #region 所需全域變數
        string KeyCode_Box;
        string StoreCode;
        Codes Cs = new Codes();
        int TotalPages = 1;
        int row=0;//目前印完的列數
        int PageCount = 1;//當前列印頁面
        #endregion

        #region ※接收外部呼叫程式變數，重要。
        public Form2(string Num)
        {
            KeyCode_Box = Num;
            StoreCode = Num.Substring(4, 4);
            InitializeComponent();
            SheeType.Text = Cs.GetSheetType(Num);
        }
        #endregion

        #region ※Form2的表單讀取事件。
        private void Form2_Load(object sender, EventArgs e)
        {
           dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("細明體", 14, FontStyle.Bold);
           dataGridView1.DefaultCellStyle.Font = new Font("細明體", 14, FontStyle.Bold);
           SetFormData( Cs.GetPrintingData(KeyCode_Box,StoreCode));
            //PrintPanel(Print_Panel);
            //this.Dispose();
        }
        #endregion

        #region 將列印資料填入畫面
        void SetFormData(Codes.PrintingData PD) 
        {
            try
            {
                Out.Text = string.Format("ST4{0}-JP{0}{1}", StoreCode, PD.GoesOut);
                SheetType.Text = string.Format("");
                BarCode.Text = string.Format("*{0}*", PD.Box_BarCode);
                Code.Text = PD.Box_BarCode;
                SheetType.Text = PD.SheetType;
                if (PD.PrintType=="RE2VENDOR")
                {
                    Vendorname.Text = string.Format("{0}／{1}", PD.VD.VendorCode, PD.VD.Vendor);
                    VendorConnector.Text = string.Format("{0}／{1}", PD.VD.VendorPhone, PD.VD.VendorConnector);
                    Vendor_STCEG.Text = PD.VD.VendorSTCEG;
                }
                else
                {
                    Vendorname.Text = string.Format("{0}", PD.GoesIn);
                    VendorConnector.Text = "";
                    Vendor_STCEG.Text = "";
                }
            
                PrintDate.Text = DateTime.Now.ToShortDateString();
                SheetDate.Text = PD.SheetDate;
                dataGridView1.DataSource = PD.GoodDetail;
                //dataGridView1.RowsDefaultCellStyle.Font=new Font("細明體", 13, FontStyle.Regular);
                
                switch (PD.PrintType)
                {
                    case "RE2TDC1": //退總倉
                        lab_Out.Text = "退貨門市代號／名稱：";
                        lab_Type.Text = "退貨單別：";
                        lab_Vendor.Text = "收貨單位：";
                        lab_code.Text = "";
                        lab_Connector.Text = "";
                        lab_Title.Text = "日藥本舖股份有限公司國內退貨單";
                        SumTitle.Text=string.Format("此張退貨單據 ({0}) 總件數共：{1} 件",PD.Box_BarCode,PD.TotalNum);
                        break;
                    case "RE2VENDOR": //直退廠商
                        lab_Out.Text = "退貨門市代號／名稱：";
                        lab_Type.Text = "退貨單別：";
                        lab_Vendor.Text = "廠商名稱：";
                        lab_Title.Text = "日藥本舖股份有限公司國內退貨單";
                        lab_code.Text = "廠商統編：";
                        lab_Connector.Text = "廠商電話／連絡人：";
                        SumTitle.Text = string.Format("此張退貨單據 ({0}) 總件數共：{1} 件", PD.Box_BarCode, PD.TotalNum);
                        break;
                    case "STO2STO": //調撥至其他門市
                        lab_Out.Text = "調撥門市代號 / 名稱：";
                        lab_Type.Text = "調撥單別：";
                        lab_Vendor.Text = "收貨門市：";
                        lab_Connector.Text = "";
                        lab_code.Text = "";
                        lab_Title.Text = "日藥本舖股份有限公司國內調撥單";
                        SumTitle.Text = string.Format("此張調撥單據 ({0}) 總件數共：{1} 件", PD.Box_BarCode, PD.TotalNum);
                        break;
                    case "STO2TDC1": //調撥至總倉
                        lab_Out.Text = "調撥門市代號 / 名稱：";
                        lab_Type.Text = "調撥單別：";
                        lab_Vendor.Text = "收貨單位：";
                        lab_Connector.Text = "";
                        lab_code.Text = "";
                        lab_Title.Text = "日藥本舖股份有限公司國內調撥單";
                        SumTitle.Text = string.Format("此張調撥單據 ({0}) 總件數共：{1} 件", PD.Box_BarCode, PD.TotalNum);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion

        #region 所有列印相關程式

        #region ※預覽列印按鈕按下後的事件。
        private void button1_Click(object sender, EventArgs e)
        {
            PrintPanel(Print_Panel);
            //this.Dispose();
        }
        #endregion

        #region 列印主程式
        private void PrintPanel(Panel pnl)
        {
            PrintDialog myPrintDialog = new PrintDialog();
            PrinterSettings values;
            values = myPrintDialog.PrinterSettings;
            myPrintDialog.Document = printDocument1;
            printDocument1.PrintController = new StandardPrintController();
            printDocument1.PrintPage +=
              new System.Drawing.Printing.PrintPageEventHandler(printDocument1_PrintPage);
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();
            //printDocument1.Print(); 按鈕不再按下就列印，而要用預覽列印中的列印圖式來列印。
            printDocument1.Dispose();
        }
        #endregion

        #region ※printDocument1_PrintPage事件，重要！！
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (PageCount==1)
            {
                using (Bitmap bmp = new Bitmap(Print_Panel.Width, Print_Panel.Height))
                {
                    Print_Panel.DrawToBitmap(bmp, Print_Panel.ClientRectangle);
                    RectangleF bounds = e.PageSettings.PrintableArea;
                    float factor = ((float)bmp.Height / (float)bmp.Width);
                    e.Graphics.DrawImage(bmp, bounds.Left, bounds.Top,
                                              bounds.Width, factor * bounds.Width);
                }
                DrawHeader(e.Graphics,  537);
              
                if (dataGridView1.RowCount>13)
                {
                    //計算頁數
                    int n = (dataGridView1.RowCount - 13) / 26;
                    if ((dataGridView1.RowCount - 13) % 26 == 0)
                        TotalPages += n;
                    else
                        TotalPages += (n + 1);
                }

                DrawGridBody_FirstPage(e.Graphics, 560, PageCount);
               

                if (dataGridView1.RowCount > 13)
                { 
                    e.HasMorePages = true; 
                    PageCount++; 
                }
                    
            }
            else
            {
                DrawHeader(e.Graphics, 40);
                DrawGridBody_AddedPages(e.Graphics, 63, PageCount);
                    if (((DataTable)dataGridView1.DataSource).Rows.Count > row)
                    {
                        e.HasMorePages = true;
                        PageCount++;
                    }
                    else
                    {
                        e.HasMorePages = false;
                        row = 0;
                        PageCount = 1;
                    }
            }
        }
        #endregion

        #region 繪製物品清單相關程式

        #region 繪製物品清單的標題
        private void DrawHeader(Graphics g,  int y_value)
        {
            int x_value = 0;
            Font bold = new Font(this.Font, FontStyle.Bold);
            foreach (DataGridViewColumn dc in dataGridView1.Columns)
            {
                g.DrawString(dc.HeaderText, bold, Brushes.Black, (float)x_value, (float)y_value);//寫入標題
                x_value += dc.Width + 5;//設定寬度
            }
        }
        #endregion

        #region 繪製第一頁物品清單表身
        private void DrawGridBody_FirstPage(Graphics g, int y_value,int Page)
        {
            int x_value;
            row = 0;
            int ItemCount = 0;
            for (int i = 0; (i < 13) && ((i + row) < ((DataTable)dataGridView1.DataSource).Rows.Count); ++i)
            {
                DataRow dr = ((DataTable)dataGridView1.DataSource).Rows[i + row];
                x_value = 0;
                
                // draw a solid line
                g.DrawLine(Pens.Black, new Point(0, y_value), new Point(this.Width, y_value));

                foreach (DataGridViewColumn dc in dataGridView1.Columns)
                {
                    string text = dr[dc.DataPropertyName].ToString();
                    g.DrawString(text, this.Font, Brushes.Black, (float)x_value, (float)y_value + 10f);//寫入列資料
                    x_value += dc.Width + 5;//設定欄位寬度
                }

                y_value += 40;//寫入一列後設定高度
                ItemCount++;
                
            }
            g.DrawLine(Pens.Black, new Point(0, y_value), new Point(this.Width, y_value));
            g.DrawString("第 " + Page.ToString() + " 頁  本單共 " + TotalPages + " 頁  此頁共 " + ItemCount.ToString() + " 項  需求追蹤號碼 " + Code.Text, this.Font, Brushes.Black, 5, (float)y_value + 10f);//寫入頁數
            row += 13;//更新目前列印頁數
        }
        #endregion

        #region 繪製其他頁物品清單
        private void DrawGridBody_AddedPages(Graphics g, int y_value,int Page)
        {
            int x_value;
            int ItemCount = 0;
            for (int i = 0; (i < 26) && ((i + row) < ((DataTable)dataGridView1.DataSource).Rows.Count); ++i)
            {
                DataRow dr = ((DataTable)dataGridView1.DataSource).Rows[i+ row];
                x_value = 0;

                // draw a solid line
                g.DrawLine(Pens.Black, new Point(0, y_value), new Point(this.Width, y_value));

                foreach (DataGridViewColumn dc in dataGridView1.Columns)
                {
                    string text = dr[dc.DataPropertyName].ToString();
                    g.DrawString(text, this.Font, Brushes.Black, (float)x_value, (float)y_value + 10f);//寫入列中格子資料
                    x_value += dc.Width + 5;//設定欄位寬度
                }
                y_value += 40;//寫入一列後設定高度
                ItemCount++;
            }
            g.DrawLine(Pens.Black, new Point(0, y_value), new Point(this.Width, y_value));
            g.DrawString("第 " + Page.ToString() + " 頁  本單共 " + TotalPages + " 頁  此頁共 " + ItemCount.ToString() + " 項  需求追蹤號碼 " + Code.Text, this.Font, Brushes.Black, 5, (float)y_value + 10f);//寫入頁數
            row += 26;//更新目前列印頁數
        }
        #endregion

        #endregion

        #endregion
    }
}
