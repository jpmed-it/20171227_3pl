﻿namespace MovingGoods
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.Print_Panel = new System.Windows.Forms.Panel();
            this.SheeType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SumTitle = new System.Windows.Forms.Label();
            this.Out = new System.Windows.Forms.Label();
            this.SheetDate = new System.Windows.Forms.Label();
            this.PrintDate = new System.Windows.Forms.Label();
            this.Vendor_STCEG = new System.Windows.Forms.Label();
            this.VendorConnector = new System.Windows.Forms.Label();
            this.Vendorname = new System.Windows.Forms.Label();
            this.SheetType = new System.Windows.Forms.Label();
            this.lab_DheetDate = new System.Windows.Forms.Label();
            this.lab_Date = new System.Windows.Forms.Label();
            this.lab_Connector = new System.Windows.Forms.Label();
            this.lab_code = new System.Windows.Forms.Label();
            this.lab_Vendor = new System.Windows.Forms.Label();
            this.lab_Type = new System.Windows.Forms.Label();
            this.lab_Out = new System.Windows.Forms.Label();
            this.lab_Title = new System.Windows.Forms.Label();
            this.Code = new System.Windows.Forms.Label();
            this.BarCode = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.Print_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Print_Panel
            // 
            this.Print_Panel.AutoSize = true;
            this.Print_Panel.Controls.Add(this.label2);
            this.Print_Panel.Controls.Add(this.SheeType);
            this.Print_Panel.Controls.Add(this.label1);
            this.Print_Panel.Controls.Add(this.label9);
            this.Print_Panel.Controls.Add(this.label8);
            this.Print_Panel.Controls.Add(this.label7);
            this.Print_Panel.Controls.Add(this.SumTitle);
            this.Print_Panel.Controls.Add(this.Out);
            this.Print_Panel.Controls.Add(this.SheetDate);
            this.Print_Panel.Controls.Add(this.PrintDate);
            this.Print_Panel.Controls.Add(this.Vendor_STCEG);
            this.Print_Panel.Controls.Add(this.VendorConnector);
            this.Print_Panel.Controls.Add(this.Vendorname);
            this.Print_Panel.Controls.Add(this.SheetType);
            this.Print_Panel.Controls.Add(this.lab_DheetDate);
            this.Print_Panel.Controls.Add(this.lab_Date);
            this.Print_Panel.Controls.Add(this.lab_Connector);
            this.Print_Panel.Controls.Add(this.lab_code);
            this.Print_Panel.Controls.Add(this.lab_Vendor);
            this.Print_Panel.Controls.Add(this.lab_Type);
            this.Print_Panel.Controls.Add(this.lab_Out);
            this.Print_Panel.Controls.Add(this.lab_Title);
            this.Print_Panel.Controls.Add(this.Code);
            this.Print_Panel.Controls.Add(this.BarCode);
            this.Print_Panel.Location = new System.Drawing.Point(1, 34);
            this.Print_Panel.Name = "Print_Panel";
            this.Print_Panel.Size = new System.Drawing.Size(921, 558);
            this.Print_Panel.TabIndex = 0;
            // 
            // SheeType
            // 
            this.SheeType.AutoSize = true;
            this.SheeType.Font = new System.Drawing.Font("微軟正黑體", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SheeType.Location = new System.Drawing.Point(87, 38);
            this.SheeType.Name = "SheeType";
            this.SheeType.Size = new System.Drawing.Size(128, 47);
            this.SheeType.TabIndex = 69;
            this.SheeType.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 532);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(767, 12);
            this.label1.TabIndex = 68;
            this.label1.Text = "---------------------------------------------------------------------------------" +
    "---    物  品  明  細    -----------------------------------------------------------" +
    "-------------------------";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(533, 472);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(359, 19);
            this.label9.TabIndex = 57;
            this.label9.Text = "門市退貨人員確認：_________________";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(533, 416);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(359, 19);
            this.label8.TabIndex = 56;
            this.label8.Text = "門市驗單人員確認：_________________";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(467, 356);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(425, 19);
            this.label7.TabIndex = 55;
            this.label7.Text = "廠商 (倉庫) 收貨人員確認：_________________";
            // 
            // SumTitle
            // 
            this.SumTitle.AutoSize = true;
            this.SumTitle.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SumTitle.Location = new System.Drawing.Point(28, 295);
            this.SumTitle.Name = "SumTitle";
            this.SumTitle.Size = new System.Drawing.Size(129, 19);
            this.SumTitle.TabIndex = 67;
            this.SumTitle.Text = "此張____單據";
            // 
            // Out
            // 
            this.Out.AutoSize = true;
            this.Out.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Out.Location = new System.Drawing.Point(240, 119);
            this.Out.Name = "Out";
            this.Out.Size = new System.Drawing.Size(69, 19);
            this.Out.TabIndex = 60;
            this.Out.Text = "label12";
            // 
            // SheetDate
            // 
            this.SheetDate.AutoSize = true;
            this.SheetDate.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SheetDate.Location = new System.Drawing.Point(788, 146);
            this.SheetDate.Name = "SheetDate";
            this.SheetDate.Size = new System.Drawing.Size(69, 19);
            this.SheetDate.TabIndex = 66;
            this.SheetDate.Text = "label10";
            // 
            // PrintDate
            // 
            this.PrintDate.AutoSize = true;
            this.PrintDate.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.PrintDate.Location = new System.Drawing.Point(788, 119);
            this.PrintDate.Name = "PrintDate";
            this.PrintDate.Size = new System.Drawing.Size(59, 19);
            this.PrintDate.TabIndex = 65;
            this.PrintDate.Text = "label6";
            // 
            // Vendor_STCEG
            // 
            this.Vendor_STCEG.AutoSize = true;
            this.Vendor_STCEG.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vendor_STCEG.Location = new System.Drawing.Point(239, 226);
            this.Vendor_STCEG.Name = "Vendor_STCEG";
            this.Vendor_STCEG.Size = new System.Drawing.Size(59, 19);
            this.Vendor_STCEG.TabIndex = 64;
            this.Vendor_STCEG.Text = "label5";
            // 
            // VendorConnector
            // 
            this.VendorConnector.AutoSize = true;
            this.VendorConnector.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.VendorConnector.Location = new System.Drawing.Point(239, 199);
            this.VendorConnector.Name = "VendorConnector";
            this.VendorConnector.Size = new System.Drawing.Size(59, 19);
            this.VendorConnector.TabIndex = 63;
            this.VendorConnector.Text = "label4";
            // 
            // Vendorname
            // 
            this.Vendorname.AutoSize = true;
            this.Vendorname.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vendorname.Location = new System.Drawing.Point(239, 172);
            this.Vendorname.Name = "Vendorname";
            this.Vendorname.Size = new System.Drawing.Size(59, 19);
            this.Vendorname.TabIndex = 62;
            this.Vendorname.Text = "label3";
            // 
            // SheetType
            // 
            this.SheetType.AutoSize = true;
            this.SheetType.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SheetType.Location = new System.Drawing.Point(240, 146);
            this.SheetType.Name = "SheetType";
            this.SheetType.Size = new System.Drawing.Size(59, 19);
            this.SheetType.TabIndex = 61;
            this.SheetType.Text = "label2";
            // 
            // lab_DheetDate
            // 
            this.lab_DheetDate.AutoSize = true;
            this.lab_DheetDate.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_DheetDate.Location = new System.Drawing.Point(673, 146);
            this.lab_DheetDate.Name = "lab_DheetDate";
            this.lab_DheetDate.Size = new System.Drawing.Size(109, 19);
            this.lab_DheetDate.TabIndex = 59;
            this.lab_DheetDate.Text = "單據日期：";
            // 
            // lab_Date
            // 
            this.lab_Date.AutoSize = true;
            this.lab_Date.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Date.Location = new System.Drawing.Point(673, 120);
            this.lab_Date.Name = "lab_Date";
            this.lab_Date.Size = new System.Drawing.Size(109, 19);
            this.lab_Date.TabIndex = 58;
            this.lab_Date.Text = "列印日期：";
            // 
            // lab_Connector
            // 
            this.lab_Connector.AutoSize = true;
            this.lab_Connector.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Connector.Location = new System.Drawing.Point(28, 199);
            this.lab_Connector.Name = "lab_Connector";
            this.lab_Connector.Size = new System.Drawing.Size(187, 19);
            this.lab_Connector.TabIndex = 54;
            this.lab_Connector.Text = "廠商電話 / 連絡人：";
            // 
            // lab_code
            // 
            this.lab_code.AutoSize = true;
            this.lab_code.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_code.Location = new System.Drawing.Point(28, 226);
            this.lab_code.Name = "lab_code";
            this.lab_code.Size = new System.Drawing.Size(109, 19);
            this.lab_code.TabIndex = 53;
            this.lab_code.Text = "廠商統編：";
            // 
            // lab_Vendor
            // 
            this.lab_Vendor.AutoSize = true;
            this.lab_Vendor.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Vendor.Location = new System.Drawing.Point(28, 172);
            this.lab_Vendor.Name = "lab_Vendor";
            this.lab_Vendor.Size = new System.Drawing.Size(109, 19);
            this.lab_Vendor.TabIndex = 52;
            this.lab_Vendor.Text = "廠商名稱：";
            // 
            // lab_Type
            // 
            this.lab_Type.AutoSize = true;
            this.lab_Type.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Type.Location = new System.Drawing.Point(28, 146);
            this.lab_Type.Name = "lab_Type";
            this.lab_Type.Size = new System.Drawing.Size(69, 19);
            this.lab_Type.TabIndex = 51;
            this.lab_Type.Text = "單別：";
            // 
            // lab_Out
            // 
            this.lab_Out.AutoSize = true;
            this.lab_Out.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Out.Location = new System.Drawing.Point(28, 120);
            this.lab_Out.Name = "lab_Out";
            this.lab_Out.Size = new System.Drawing.Size(167, 19);
            this.lab_Out.TabIndex = 50;
            this.lab_Out.Text = "門市代號 / 名稱：";
            // 
            // lab_Title
            // 
            this.lab_Title.AutoSize = true;
            this.lab_Title.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_Title.Location = new System.Drawing.Point(300, 10);
            this.lab_Title.Name = "lab_Title";
            this.lab_Title.Size = new System.Drawing.Size(327, 26);
            this.lab_Title.TabIndex = 49;
            this.lab_Title.Text = "日藥本舖股份有限公司國內退貨單";
            // 
            // Code
            // 
            this.Code.AutoSize = true;
            this.Code.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Code.Location = new System.Drawing.Point(546, 86);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(0, 16);
            this.Code.TabIndex = 48;
            // 
            // BarCode
            // 
            this.BarCode.AutoSize = true;
            this.BarCode.Font = new System.Drawing.Font("Free 3 of 9", 36F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BarCode.Location = new System.Drawing.Point(429, 47);
            this.BarCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.BarCode.Name = "BarCode";
            this.BarCode.Size = new System.Drawing.Size(0, 36);
            this.BarCode.TabIndex = 47;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(12, 598);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(899, 274);
            this.dataGridView1.TabIndex = 46;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("標楷體", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.button1.Location = new System.Drawing.Point(766, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "列印預覽";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(26, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 35);
            this.label2.TabIndex = 70;
            this.label2.Text = "門市";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(923, 874);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Print_Panel);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Print_Panel.ResumeLayout(false);
            this.Print_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Print_Panel;
        private System.Windows.Forms.Label SheetDate;
        private System.Windows.Forms.Label PrintDate;
        private System.Windows.Forms.Label Vendor_STCEG;
        private System.Windows.Forms.Label VendorConnector;
        private System.Windows.Forms.Label Vendorname;
        private System.Windows.Forms.Label SheetType;
        private System.Windows.Forms.Label Out;
        private System.Windows.Forms.Label lab_DheetDate;
        private System.Windows.Forms.Label lab_Date;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lab_Connector;
        private System.Windows.Forms.Label lab_code;
        private System.Windows.Forms.Label lab_Vendor;
        private System.Windows.Forms.Label lab_Type;
        private System.Windows.Forms.Label lab_Out;
        private System.Windows.Forms.Label lab_Title;
        private System.Windows.Forms.Label Code;
        private System.Windows.Forms.Label BarCode;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label SumTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label SheeType;
        private System.Windows.Forms.Label label2;

    }
}