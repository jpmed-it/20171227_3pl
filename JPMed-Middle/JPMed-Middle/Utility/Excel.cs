﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NPOI.HSSF.UserModel;
using System.Windows.Forms;
using NPOI.SS.UserModel;

namespace JPMed_Middle.Utility
{
    public class ExcelExtension
    {
        //DataGridView資料轉存Excel
        public static MemoryStream DataToExcel(DataGridView dv)
        {
            MemoryStream ms = new MemoryStream();

            IWorkbook workbook = new HSSFWorkbook();//Create an excel Workbook
            ISheet sheet = workbook.CreateSheet();//Create a work table in the table
            IRow headerRow = sheet.CreateRow(0); //To add a row in the table
            int columnIndex = 0;
            foreach (DataGridViewColumn column in dv.Columns)
            {
                if (column.Visible)
                {
                    headerRow.CreateCell(columnIndex, CellType.String).SetCellValue(column.HeaderText);
                    columnIndex++;
                }
            }
            int rowIndex = 1;

            foreach (DataGridViewRow row in dv.Rows)
            {
                columnIndex = 0;
                IRow dataRow = sheet.CreateRow(rowIndex);
                foreach (DataGridViewColumn column in dv.Columns)
                {
                    if (column.Visible)
                    {

                        ICell rowCell = dataRow.CreateCell(columnIndex, CellType.String);
                        //rowCell.SetCellType(CellType.Numeric);
                        decimal outDecimal = 0;
                        if (Decimal.TryParse(row.Cells[column.Index].Value.ToString(), out outDecimal))
                        {
                            string s = row.Cells[column.Index].Value.ToString().Replace(".000", "");
                            rowCell.SetCellValue(s);
                        }
                        else
                        {
                            rowCell.SetCellValue(row.Cells[column.Index].Value.ToString());
                        }

                        columnIndex++;
                    }
                }
                rowIndex++;

            }

            for (int i = 0; i < columnIndex; i++)
            {
                sheet.AutoSizeColumn(i);

            }

            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;

            return ms;
        }
    }
}
