﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;

namespace JPMed_Middle.Utility
{
    public class DBConnection
    {
        private static ILog logger = LogManager.GetLogger(typeof(DBConnection));

        static string connectionStr = "Data Source={0};Initial Catalog={1};Persist Security Info=True;Max Pool Size=512;User ID={2};Password={3}";
        static string _ServerIp = Properties.Settings.Default.ServerIP;
        static string _DataBase = Properties.Settings.Default.DataBaseName;
        static string _User = Properties.Settings.Default.UserName;
        static string _Password = Properties.Settings.Default.Password;
        public static string _Connection;

        /// <summary>回傳時使用者欲得到的資料容器型別</summary>
        /// <remarks></remarks>
        public enum DataContainerType
        {
            DataSet = 0,
            DatatTable = 1
        }

        public DBConnection() 
        {
            _Connection = string.Format(connectionStr, _ServerIp, _DataBase, _User, PwdDecrypt.DecryptDES(_Password, "53536349", "94363535"));
        }

        public void ResetSettings()
        {
            Properties.Settings.Default.Reset();
            Properties.Settings.Default.Reload();
            _ServerIp = Properties.Settings.Default.ServerIP;
            _DataBase = Properties.Settings.Default.DataBaseName;
            _User = Properties.Settings.Default.UserName;
            _Password = Properties.Settings.Default.Password;
            _Connection = string.Format(connectionStr, _ServerIp, _DataBase, _User, PwdDecrypt.DecryptDES(_Password, "53536349", "94363535"));
        }

        /// <summary>
        /// Test Connecttion
        /// </summary>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public bool TestConnect(out string errMsg)
        {
            errMsg = string.Empty;

            if (String.IsNullOrEmpty(_Connection))
            {
                errMsg = "Can not get property:[DBConn], please check the parameter had been set!";
                return false;
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(_Connection))
                {
                    if (conn.State == ConnectionState.Closed)
                    {
                        try
                        {
                            conn.Open();
                            conn.Close();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            errMsg = ex.Message;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Execute SqlCmd
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        public int ExecSql(string sqlCmd)
        {
            SqlConnection sqlConnection = new SqlConnection(_Connection);
            if (sqlConnection == null)
                throw new Exception("SqlServer connection is null!");

            SqlCommand cmd = new SqlCommand(sqlCmd, sqlConnection);
            if (sqlConnection.State == ConnectionState.Closed)
                sqlConnection.Open();

            int result = cmd.ExecuteNonQuery();

            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();

            return result;
        }

        /// <summary>
        /// Get DataTable 0,0 Data
        /// </summary>
        /// <param name="transType"></param>
        /// <param name="sqlCmd"></param>
        /// <param name="memo"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string GetDataTableRecData(string sqlCmd)
        {
            try
            {
                DataTable tempDT = GetDataTable(sqlCmd);
                if (tempDT.Rows.Count > 0)
                    return tempDT.Rows[0][0].ToString();
                else
                    return "";
            }
            catch (Exception ex)
            {
                logger.Debug("SQL Command:" + sqlCmd);
                logger.Error(ex.Message);
                throw new Exception("Query Error:" + ex.Message);
            }
        }

        public DataTable GetDataTable(string queryString)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)GetDataTableOrDataset(queryString, DataContainerType.DatatTable, 0, 0, "");
            return dt;
        }

        /// <summary>透過查詢語法取得 DataTable or DataSet 物件</summary>
        /// <param name="queryString">查詢語法</param>
        /// <returns>DataTable or DataSet 物件, 依照傳入參數 DataContainerType 而不同</returns>
        /// <remarks></remarks>
        private object GetDataTableOrDataset(string queryString, DataContainerType ReturnType, int StartRecord, int MaxRecords, string srcTable)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(_Connection);

                if (sqlConnection == null)
                    throw new Exception("SqlServer connection is null!");

                if (ReturnType == DataContainerType.DataSet)
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    SqlCommand cmd = sqlConnection.CreateCommand();
                    cmd.CommandText = queryString;
                    da.SelectCommand = cmd;
                    sqlConnection.Open();

                    if (StartRecord == 0 && MaxRecords == 0 && srcTable == "")
                        da.Fill(ds);
                    else
                        da.Fill(ds, StartRecord, MaxRecords, srcTable);
                    
                    da.Dispose();
                    sqlConnection.Close();
                    return ds;
                }
                else if (ReturnType == DataContainerType.DatatTable)
                {
                    SqlCommand cmd = new SqlCommand(queryString, sqlConnection);
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    dt.Load(cmd.ExecuteReader());

                    if (sqlConnection.State == ConnectionState.Open)
                        sqlConnection.Close();

                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                logger.Debug("SQL Command:" + queryString);
                logger.Error(ex.Message);

                if (sqlConnection != null &&
                    sqlConnection.State == ConnectionState.Open)
                    sqlConnection.Close();

                throw new Exception(ex.Message);
            }
        }
    }
}
