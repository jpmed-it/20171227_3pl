﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;
using log4net;

namespace JPMed_Middle.Utility
{
    public class SQLCreator
    {
        #region Attributes
        private const string ns = "JPMed_Middle.SqlStatement";
        private static ILog logger = null;
        private SQLCreatorBase obj = null;
        protected Hashtable _Parameter = null;
        protected string Name = "";
        #endregion

        #region Constract
        public SQLCreator(string ClassName)
        {
            try
            {
                Type tp = Type.GetType(ns + "." + ClassName);
                obj = Activator.CreateInstance(Type.GetType(ns + "." + ClassName)) as SQLCreatorBase;
                logger = LogManager.GetLogger("[" + ClassName + "]");
                this.Name = ClassName;

                if (obj != null)
                {
                    _Parameter = new Hashtable();
                }
                else
                    throw new Exception("SQLCreator Create Error..");
            }
            catch (Exception ex)
            {
                logger.Error("Create SQLStatement Object [" + ClassName + "]  Error..\r\n" + ex.Message);
                throw new Exception("Create SQLStatement Object [" + ClassName + "]  Error..");
            }
        }

        public SQLCreator(Type Class)
        {
            try
            {
                obj = Activator.CreateInstance(Type.GetType(ns + "." + Class.Name), new Object[] { }) as SQLCreatorBase;
                logger = LogManager.GetLogger("[" + Class.Name + "]");
                this.Name = Class.Name;

                if (obj != null)
                {
                    _Parameter = new Hashtable();
                }
                else
                    throw new Exception("SQLCreator Create Error..");
            }
            catch (Exception ex)
            {
                logger.Error("Create SQLStatement Object [" + Class.Name + "]  Error..\r\n" + ex.Message);
                throw new Exception("Create SQLStatement Object [" + Class.Name + "]  Error..");
            }
        }
        #endregion

        #region SetParameter
        /// <summary>
        /// 設定傳入參數
        /// </summary>
        /// <param name="Name">參數名稱</param>
        /// <param name="Value">參數值</param>
        public void SetParameter(string Name, string Value)
        {
            _Parameter[Name] = Value;
        }

        public void SetParameter(string Name, object Value)
        {
            if (!_Parameter.ContainsKey(Name))
                _Parameter[Name] = Value;
        }

        #endregion

        #region GetParameter
        /// <summary>
        /// 取得參數
        /// </summary>
        /// <param name="Name">參數名稱</param>
        /// <returns></returns>
        protected  object GetParameter(string Name)
        {
            return _Parameter[Name] == null ? "" : _Parameter[Name];
        }

        #endregion

        #region Clear

        public void Clear()
        {
            _Parameter.Clear();
        }

        #endregion

        #region GetCommand
        /// <summary>
        /// 回傳SQL SqlCommand
        /// </summary>
        /// <param name="MethodName"></param>
        /// <returns></returns>
        public string GetCommand(string MethodName)
        {
            string m_Sql = string.Empty;
            MethodInfo method = null;

            try
            {
                SQLStatementCreator sc = ((SQLStatementCreator)obj);
                sc.SetParameter(_Parameter);
                method = obj.GetType().GetMethod(MethodName, BindingFlags.Public | BindingFlags.Static);
                m_Sql = method.Invoke(null, null).ToString();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return m_Sql;
        }

        /// <summary>
        ///  回傳SQL SqlCommand
        /// </summary>
        /// <param name="MethodName"></param>
        /// <param name="Parameter"></param>
        /// <returns></returns>
        public string GetCommand(string MethodName, object[] Parameter)
        {
            string m_Sql = string.Empty;
            MethodInfo method = null;
            try
            {
                SQLStatementCreator sc = ((SQLStatementCreator)obj);
                sc.SetParameter(_Parameter);
                method = obj.GetType().GetMethod(MethodName, BindingFlags.Public | BindingFlags.Static);
                m_Sql = method.Invoke(null, Parameter).ToString();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString(), ex);
            }
            return m_Sql;
        }
        #endregion
    }
}
