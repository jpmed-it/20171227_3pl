﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Middle.Entity
{
    public class Re2vct
    {
        /// <summary>
        /// 廠商編號
        /// </summary>
        public string LIFNR { get; set; }
        /// <summary>
        /// 門市代號
        /// </summary>
        public string WERKS { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public string CRDAT { get; set; }
        /// <summary>
        /// 建立日時間
        /// </summary>
        public string CRTME { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public string CHDAT { get; set; }
        /// <summary>
        /// 修改時間
        /// </summary>
        public string CHTME { get; set; }

        public Re2vct() { }

        public string CreateInsertCommand()
        {
            string result = @"INSERT INTO RE2VCT (LIFNR, CRDAT, CRTME, CHDAT, CHTME, WERKS) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')";
            return string.Format(result, LIFNR, CRDAT, CRTME, CHDAT, CHTME, WERKS);
        }
    }
}
