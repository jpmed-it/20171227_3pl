﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Middle.Entity
{
    public class Replenct
    {
        /// <summary>
        /// 店代號
        /// </summary>
        public string WERKS { get; set; }
        /// <summary>
        /// 特開日期起
        /// </summary>
        public string REPLSD { get; set; }
        /// <summary>
        /// 特開日期迄
        /// </summary>
        public string REPLDD { get; set; }
        /// <summary>
        /// 刪除旗標
        /// </summary>
        public string DFLAG { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public string CRDAT { get; set; }
        /// <summary>
        /// 建立日時間
        /// </summary>
        public string CRTME { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public string CHDAT { get; set; }
        /// <summary>
        /// 修改時間
        /// </summary>
        public string CHTME { get; set; }

        public Replenct() { }

        public string CreateInsertCommand()
        {
            string result =  @"INSERT INTO REPLENCT (WERKS, REPLSD, REPLDD, DFLAG, CRDAT, CRTME, CHDAT, CHTME) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')";
            return string.Format(result, WERKS, REPLSD, REPLDD, DFLAG, CRDAT, CRTME, CHDAT, CHTME);
        }

        public string RevertDataCommand()
        {
            string result = @"UPDATE REPLENCT SET DFLAG = ' ', CHDAT = '{0}', CHTME = '{1}' WHERE WERKS = '{2}' AND REPLSD = '{3}' AND REPLDD = '{4}'";
            return string.Format(result, CHDAT, CHTME, WERKS, REPLSD, REPLDD);
        }
    }
}
