﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Middle.Entity
{
    public class Alocat
    {
        public string WERKS { get; set; }
        public string CRDAT { get; set; }
        public string CRTME { get; set; }
        public string PRIOR { get; set; }
        public string MENGE { get; set; }
        public string DELFG { get; set; }
        public string CHDAT { get; set; }
        public string CHTME { get; set; }

        public Alocat() { }

        public string CreateInsertCommand()
        {
            string result = @"INSERT INTO ALOCAT (WERKS, CRDAT, CRTME, PRIOR, MENGE, DELFG, CHDAT, CHTME) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')";
            return string.Format(result, WERKS, CRDAT, CRTME, PRIOR, MENGE, DELFG, CHDAT, CHTME);
        }
    }
}
