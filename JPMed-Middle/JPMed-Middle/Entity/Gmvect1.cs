﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_Middle.Entity
{
    public class Gmvect1
    {
        /// <summary>
        /// 採購單號
        /// </summary>
        public string EBELN { get; set; }
        /// <summary>
        /// 刪除旗標
        /// </summary>
        public string DFLAG { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public string CRDAT { get; set; }
        /// <summary>
        /// 建立日時間
        /// </summary>
        public string CRTME { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public string CHDAT { get; set; }
        /// <summary>
        /// 修改時間
        /// </summary>
        public string CHTME { get; set; }

        public Gmvect1() { }

        public string CreateInsertCommand()
        {
            string result =  @"INSERT INTO GMVECT1 (EBELN, DFLAG, CRDAT, CRTME, CHDAT, CHTME) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')";
            return string.Format(result, EBELN, DFLAG, CRDAT, CRTME, CHDAT, CHTME);
        }

        public string RevertDataCommand()
        {
            string result = @"UPDATE GMVECT1 SET DFLAG = ' ', CHDAT = '{0}', CHTME = '{1}' WHERE EBELN = '{2}'";
            return string.Format(result, CHDAT, CHTME, EBELN);
        }
    }
}
