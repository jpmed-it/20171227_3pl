﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace JPMed_Middle
{
    public partial class MainForm : Form
    {
        bool quit = false;
        string formName = "MiddleDB 特殊控制 Menu 選單";
        Process reportProcess = null;

        public MainForm()
        {
            InitializeComponent();
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("en-us");

            //this.ControlBox = false;
        }

        private void 離開ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定離開系統嗎?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                quit = true;
                this.Close();
            }
        }

        private void 門市補貨控制表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                JPMed_Middle.SubForms.UC30 uc = new JPMed_Middle.SubForms.UC30();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == uc.Name)
                        return;
                    else
                        panel0.Controls.Remove(c);
                }

                panel0.Controls.Add(uc);
                this.Text = formName + " -- " + "門市補貨控制表";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void 直退廠商控制表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                JPMed_Middle.SubForms.UC31 uc = new JPMed_Middle.SubForms.UC31();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == uc.Name)
                        return;
                    else
                        panel0.Controls.Remove(c);
                }

                panel0.Controls.Add(uc);
                this.Text = formName + " -- " + "直退廠商控制表";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pDA上傳SAP的物料異動特放控制表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                JPMed_Middle.SubForms.UC32 uc = new JPMed_Middle.SubForms.UC32();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == uc.Name)
                        return;
                    else
                        panel0.Controls.Remove(c);
                }

                panel0.Controls.Add(uc);
                this.Text = formName + " -- " + "門市採購單特收(依日期)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void 總倉配貨控制表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                JPMed_Middle.SubForms.UC33 uc = new JPMed_Middle.SubForms.UC33();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == uc.Name)
                        return;
                    else
                        panel0.Controls.Remove(c);
                }

                panel0.Controls.Add(uc);
                this.Text = formName + " -- " + "總倉配貨控制表";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!quit)
            {
                DialogResult dialogResult = MessageBox.Show("您確定離開系統嗎?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult != DialogResult.Yes)
                    e.Cancel = true;
            }
        }

        private void 門市採購單特收依單號ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                JPMed_Middle.SubForms.UC34 uc = new JPMed_Middle.SubForms.UC34();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == uc.Name)
                        return;
                    else
                        panel0.Controls.Remove(c);
                }

                panel0.Controls.Add(uc);
                this.Text = formName + " -- " + "門市採購特收(依單號)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);
        private void 門市審單比較表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string programPath = Environment.CurrentDirectory + @"\RP_Report\" + Properties.Settings.Default.RP_Report;
                ProcessStartInfo processInfo = new ProcessStartInfo(programPath);

                if (reportProcess == null || reportProcess.HasExited)
                {
                    reportProcess = new Process();
                    reportProcess.StartInfo = processInfo;
                    reportProcess.Start();
                }
                else
                {
                    //IntPtr hWnd = reportProcess.MainWindowHandle;
                    //if (hWnd != IntPtr.Zero)
                    //    SetForegroundWindow(hWnd);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void 進銷比報表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                JPMed_Middle.SubForms.UC35 uc = new JPMed_Middle.SubForms.UC35();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == uc.Name)
                        return;
                    else
                        panel0.Controls.Remove(c);
                }

                panel0.Controls.Add(uc);
                this.Text = formName + " -- " + "進銷比報表";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
