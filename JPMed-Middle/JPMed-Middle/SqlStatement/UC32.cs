﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_Middle.Utility;

namespace JPMed_Middle.SqlStatement
{
    public class UC32 : SQLStatementCreator
    {
        public static string GetAllStore()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT WERKS, NAME1 FROM STORE");
            return sb.ToString();
        }

        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetGmvectInformation()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         WERKS AS '店代號'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE WERKS = A.WERKS) AS '店代號說明'
                        ,CONVERT(date, REPLSD) AS '特收日期起'
                        ,CONVERT(date, REPLDD) AS '特收日期迄'
                        ,DFLAG AS '刪除旗標'
                        FROM GMVECT A ORDER BY CONVERT(date, REPLDD) DESC, WERKS");

            return sb.ToString();
        }

        public static string DelRecord(string _werks, string _replsd, string _repldd, string _dflag)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE GMVECT SET DFLAG = 'X', CHDAT = '{4}', CHTME = '{5}' WHERE WERKS = '{0}' AND REPLSD = '{1}' AND REPLDD = '{2}' AND DFLAG = '{3}'");
            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _replsd)
                   .Replace("{2}", _repldd)
                   .Replace("{3}", _dflag)
                   .Replace("{4}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{5}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        /// <summary>
        /// 判斷可否建立[補貨期間特殊開放補貨控制表]
        /// 0:資料區間重複, 1:有同樣內容項目被刪除，可回復, 2:可新增
        /// </summary>
        /// <param name="_werks"></param>
        /// <param name="_replsd"></param>
        /// <param name="_repldd"></param>
        /// <returns></returns>
        public static string CheckDataExist(string _werks, string _replsd, string _repldd)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT dbo.CHECK_GMVECT('{0}', '{1}', '{2}')");
            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _replsd)
                   .Replace("{2}", _repldd);

            return sb.ToString();
        }
    }
}
