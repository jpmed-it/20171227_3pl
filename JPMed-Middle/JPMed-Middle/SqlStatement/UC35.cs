﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_Middle.Utility;

namespace JPMed_Middle.SqlStatement
{
    public class UC35 : SQLStatementCreator
    {
        /// <summary>
        /// 修改特定類別補貨控制表
        /// </summary>
        /// <returns></returns>
        public static string SearchRatio(string _BADATStart, string _BADATEnd, string _ClassType)
        {
            string classScript = string.Empty;
            string classGroupScript = string.Empty;

            //全部
            if (_ClassType == "0")
            {
                classScript = ", '*' as Class1, '*' as Class2";
                classGroupScript = string.Empty;
            }
            if (_ClassType == "1")
            {
                classScript = ", Class1, '*' as Class2";
                classGroupScript = ", Class1";
            }
            else if (_ClassType == "2")
            {
                classScript = ", Class1, Class2";
                classGroupScript = ", Class1, Class2";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT WERKS, BADAT {@ClassScript} 
              , ISNULL( CAST( SUM(ORDERM) AS VARCHAR), 'N/A') as ORDERM
              , ISNULL( CAST( SUM(SALESM) AS VARCHAR), 'N/A') as SALESM
			  , ISNULL( CASE WHEN SUM(SALESM) = 0 THEN 'N/A' ELSE CAST(  ROUND( CAST( SUM(ORDERM) / SUM(SALESM) * 100 as DECIMAL(10,0)) ,0) as VARCHAR) END, 'N/A') as RATIO
              FROM SALESRATIO
              WHERE BADAT >= '{@BADATStart}' AND BADAT <= '{@BADATEnd}'
              GROUP BY WERKS, BADAT {@ClassGroupScript} 
              ORDER BY WERKS, BADAT {@ClassGroupScript} ");
            sb.Replace("{@BADATStart}", _BADATStart);
            sb.Replace("{@BADATEnd}", _BADATEnd);
            sb.Replace("{@ClassScript}", classScript);
            sb.Replace("{@ClassGroupScript}", classGroupScript);
            return sb.ToString();
        }
    }
}
