﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_Middle.Utility;

namespace JPMed_Middle.SqlStatement
{
    public class UC31 : SQLStatementCreator
    {
        public static string GetRe2vctInfo()
        {
            return @"SELECT 
                     LIFNR AS '廠商編號'
                    ,(SELECT TOP 1 NAME1 FROM VENDOR WHERE A.LIFNR = LIFNR) AS '廠商名稱'
                    ,WERKS AS '門市代號'
                    FROM RE2VCT A ORDER BY LIFNR, WERKS";

        }

        public static string GetVendorInfo(string _lifnr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM VENDOR WHERE LIFNR = '{0}'");
            sb = sb.Replace("{0}", _lifnr);

            return sb.ToString();
        }

        public static string GetWerksInfo(string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT TOP 1 WERKS FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _werks);

            return sb.ToString();
        }

        public static string TruncateTable()
        {
            return @"DELETE RE2VCT";
        }
    }
}
