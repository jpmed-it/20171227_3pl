﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_Middle.Utility;

namespace JPMed_Middle.SqlStatement
{
    public class UC30 : SQLStatementCreator
    {
        /// <summary>
        /// 取得補貨範圍控制表
        /// </summary>
        /// <returns></returns>
        public static string GetRRANGE()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT WMAAB, HINUM, LONUM FROM RRANGE ORDER BY (CASE WMAAB WHEN 'S' THEN 1 WHEN 'A' THEN 2 WHEN 'B' THEN 3 WHEN 'C' THEN 4 END)");
            return sb.ToString();
        }

        /// <summary>
        /// 修改特定類別補貨控制表
        /// </summary>
        /// <returns></returns>
        public static string UpdateRRANGE(string _WMAAB, string _HINUM, string _LONUM)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RRANGE SET HINUM = {@HINUM}, LONUM = {@LONUM} WHERE WMAAB = '{@WMAAB}'");
            sb.Replace("{@WMAAB}", _WMAAB);
            sb.Replace("{@HINUM}", _HINUM);
            sb.Replace("{@LONUM}", _LONUM);
            return sb.ToString();
        }

        public static string GetAllStore()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT WERKS, NAME1 FROM STORE");
            return sb.ToString();
        }

        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetReplenctInformation()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         WERKS AS '店代號'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE WERKS = A.WERKS) AS '店代號說明'
                        ,CONVERT(date, REPLSD) AS '特開日期起'
                        ,CONVERT(date, REPLDD) AS '特開日期迄'
                        ,DFLAG AS '刪除旗標'
                        FROM REPLENCT A ORDER BY CONVERT(date, REPLDD) DESC, WERKS");

            return sb.ToString();
        }

        public static string DelRecord(string _werks, string _replsd, string _repldd, string _dflag)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE REPLENCT SET DFLAG = 'X', CHDAT = '{4}', CHTME = '{5}' WHERE WERKS = '{0}' AND REPLSD = '{1}' AND REPLDD = '{2}' AND DFLAG = '{3}'");
            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _replsd)
                   .Replace("{2}", _repldd)
                   .Replace("{3}", _dflag)
                   .Replace("{4}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{5}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        /// <summary>
        /// 判斷可否建立[補貨期間特殊開放補貨控制表]
        /// 0:資料區間重複, 1:有同樣內容項目被刪除，可回復, 2:可新增
        /// </summary>
        /// <param name="_werks"></param>
        /// <param name="_replsd"></param>
        /// <param name="_repldd"></param>
        /// <returns></returns>
        public static string CheckDataExist(string _werks, string _replsd, string _repldd)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT dbo.CHECK_REPLENCT('{0}', '{1}', '{2}')");
            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _replsd)
                   .Replace("{2}", _repldd);

            return sb.ToString();
        }
    }
}
