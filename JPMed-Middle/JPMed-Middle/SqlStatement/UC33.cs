﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_Middle.Utility;

namespace JPMed_Middle.SqlStatement
{
    public class UC33 : SQLStatementCreator
    {
        public static string GetAlocatInfo()
        {
            return @"SELECT 
                     WERKS AS '門市代號'
                    ,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '門市說明'
                    ,[PRIOR] AS '優先順序'
                    ,MENGE AS '固定分配量'
                    ,CRDAT AS '建立日期'
                    ,CRTME AS '建立時間'
                    FROM ALOCAT A WHERE DELFG != 'X' ORDER BY [PRIOR]";
        }

        public static string GetStoreInfo(string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _werks);

            return sb.ToString();
        }

        public static string TruncateTable()
        {
            string sqlCmd = @"UPDATE ALOCAT SET DELFG = 'X', CHDAT = '{0}', CHTME = '{1}' WHERE DELFG = ' '";
            return string.Format(sqlCmd, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"));
        }

        //public static string TruncateTable()
        //{
        //    string sqlCmd = @"DELETE ALOCAT";
        //    return sqlCmd;
        //}
    }
}
