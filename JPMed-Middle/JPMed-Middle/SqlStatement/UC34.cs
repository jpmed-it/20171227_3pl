﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_Middle.Utility;

namespace JPMed_Middle.SqlStatement
{
    public class UC34 : SQLStatementCreator
    {
        public static string GetGmvectInformation()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         EBELN AS '採購單號'
                        ,DFLAG AS '刪除旗標'
                        FROM GMVECT1 A ORDER BY EBELN");

            return sb.ToString();
        }

        public static string DelRecord(string _ebeln, string _dflag)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE GMVECT1 SET DFLAG = 'X', CHDAT = '{2}', CHTME = '{3}' WHERE EBELN = '{0}' AND DFLAG = '{1}'");
            sb = sb.Replace("{0}", _ebeln)
                   .Replace("{1}", _dflag)
                   .Replace("{2}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{3}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        /// <summary>
        /// 判斷可否建立[門市採購特收(依單號)]
        /// 0: 已存在, 1:有同樣內容項目被刪除，可回復, 2 OR '':可新增
        /// </summary>
        /// <param name="_ebeln"></param>
        /// <returns></returns>
        public static string CheckDataExist(string _ebeln)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                        CASE WHEN EBELN IS NOT NULL AND (DFLAG IS NULL OR DFLAG = '') THEN 0 
                             WHEN EBELN IS NOT NULL AND DFLAG = 'X' THEN 1
                             ELSE 2 END AS Result
                        FROM GMVECT1 
                        WHERE EBELN = '{0}'");
            sb = sb.Replace("{0}", _ebeln);
            return sb.ToString();
        }
    }
}
