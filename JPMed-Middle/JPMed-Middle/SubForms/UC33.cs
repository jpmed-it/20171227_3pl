﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data.OleDb;
using JPMed_Middle.Entity;
using JPMed_Middle.SqlStatement;
using JPMed_Middle.Utility;
using log4net;

namespace JPMed_Middle.SubForms
{
    public partial class UC33 : UserControl
    {
        string formName = "MiddleDB 特殊控制 Menu 選單";
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC33));
        SQLCreator sql;
        DBConnection dbConn;

        public UC33()
        {
            InitializeComponent();
            DefaultControl();
            IntitalControl();
            LoadData();
        }

        private void DefaultControl()
        {
            btnImport.Enabled = false;
        }

        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC33");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void LoadData()
        {
            string sqlCmd = sql.GetCommand("GetAlocatInfo");
            if (!string.IsNullOrEmpty(sqlCmd))
                dvList2.DataSource = dbConn.GetDataTable(sqlCmd);

            dvList.DataSource = null;
            btnImport.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[總倉配貨控制表]?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            ofd.Filter = "Excel 97-2003 活頁簿(*.xls)|*.xls|Excel 活頁簿(*.xlsx)|*.xlsx";
            ofd.FilterIndex = 2;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (ofd.CheckFileExists)
                    {
                        string filePath = ofd.FileName;
                        readFile(filePath);
                    }
                    else
                    {
                        logger.Error("Error: Could not read file from disk. Original");
                        MessageBox.Show("Error: Could not read file from disk. Original");
                        btnImport.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error: Could not read file from disk. Original error: " + ex.Message);
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    btnImport.Enabled = false;
                }
            }
        }

        private void readFile(string _filePath)
        {
            string Excel03ConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
            string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
            string conStr = string.Empty;
            string filePath = _filePath, sheetName = string.Empty;
            string header = "YES";
            string extension = Path.GetExtension(filePath);

            switch (extension)
            {
                case ".xls": //Excel 97-03
                    conStr = string.Format(Excel03ConString, filePath, header);
                    break;

                case ".xlsx": //Excel 07
                    conStr = string.Format(Excel07ConString, filePath, header);
                    break;
            }

            try
            {
                //Get the name of the First Sheet.
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        cmd.Connection = con;
                        con.Open();
                        DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        sheetName = dtExcelSchema.AsEnumerable().Where(q => q["TABLE_NAME"].ToString().Contains("$") && !(new string[] { "ColumnType$", "AttributeType$" }).Contains(q["TABLE_NAME"].ToString()))
                                                                .Select(q => q["TABLE_NAME"].ToString()).FirstOrDefault();
                        con.Close();
                    }
                }

                //Read Data from the First Sheet.
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        using (OleDbDataAdapter oda = new OleDbDataAdapter())
                        {
                            DataTable dt = new DataTable();
                            cmd.CommandText = "SELECT * From [" + sheetName + "]";
                            cmd.Connection = con;
                            con.Open();
                            oda.SelectCommand = cmd;
                            oda.Fill(dt);
                            con.Close();

                            if (dt.Rows.Count == 0)
                            {
                                MessageBox.Show("Excel中查無資料!");
                                logger.Warn("Excel中查無資料!");
                                return;
                            }

                            if (checkFileInformation(ref dt))
                                btnImport.Enabled = true;
                            else
                                btnImport.Enabled = false;

                            dvList.DataSource = dt;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Load file error: " + ex.ToString());
                MessageBox.Show("Load file error: " + ex.Message);
                btnImport.Enabled = false;
            }
        }

        private bool checkFileInformation(ref DataTable _tmpDT)
        {
            Dictionary<string, string> dirWerks = new Dictionary<string, string>();

            bool result = true;

            DataRow newRow;
            DataTable mainDT = new DataTable();
            mainDT.Columns.Add("門市代號", typeof(string));
            mainDT.Columns.Add("門市說明", typeof(string));
            mainDT.Columns.Add("優先順序", typeof(string));
            mainDT.Columns.Add("固定分配量", typeof(string));
            mainDT.Columns.Add("驗證結果", typeof(string));

            foreach (DataRow row in _tmpDT.Rows)
            {
                List<string> validateMsg = new List<string>();
                string werks = row["門市代號"].ToString();
                string seqNo = row["優先順序"].ToString();
                string qty = row["固定分配量"].ToString();

                newRow = mainDT.NewRow();
                newRow["門市代號"] = werks;
                newRow["優先順序"] = seqNo;
                newRow["固定分配量"] = qty;

                #region 門市代號 validate
                if (string.IsNullOrEmpty(werks) || werks == DBNull.Value.ToString())
                {
                    validateMsg.Add("門市代號不可為空");
                    result = false;
                }
                else
                {
                    string storeName = string.Empty;
                    if (!dirWerks.TryGetValue(werks, out storeName))
                    {
                        string sqlCmd = sql.GetCommand("GetStoreInfo", new string[] { werks });
                        storeName = dbConn.GetDataTableRecData(sqlCmd);

                        if (storeName == DBNull.Value.ToString() || string.IsNullOrEmpty(storeName))
                        {
                            validateMsg.Add("資料庫中查無此門市代號");
                            result = false;
                        }
                        else
                        {
                            newRow["門市說明"] = storeName;
                            dirWerks.Add(werks, storeName);
                        }
                    }
                    else newRow["門市說明"] = storeName;
                }
                #endregion

                #region 優先順序 validate
                int tempSeq = 0;
                if (!int.TryParse(seqNo, out tempSeq))
                {
                    validateMsg.Add("[優先順序]欄位請輸入數值");
                    result = false;
                }
                else
                {
                    if (tempSeq < 0 || tempSeq > 9999)
                    {
                        validateMsg.Add("[優先順序]欄位值須介於0~9999");
                        result = false;
                    }
                }
                #endregion

                #region 固定分配量 Validate
                int tempQty = 0;
                if (!int.TryParse(qty, out tempQty))
                {
                    validateMsg.Add("[固定分配量]欄位請輸入數值");
                    result = false;
                }
                #endregion

                if (validateMsg.Count > 0)
                    newRow["驗證結果"] = string.Join(Environment.NewLine, validateMsg.ToArray());

                mainDT.Rows.Add(newRow);
            }

            _tmpDT = mainDT;

            return result;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("確定繼續進行資料匯入:[總倉配貨表]?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    List<Alocat> addList = new List<Alocat>();
                    string sqlCmd = string.Empty;
                    sqlCmd = sql.GetCommand("TruncateTable");

                    foreach (DataGridViewRow row in dvList.Rows)
                    {
                        addList.Add(new Alocat()
                        {
                            WERKS = row.Cells[0].Value.ToString(),
                            PRIOR = row.Cells[2].Value.ToString(),
                            MENGE = row.Cells[3].Value.ToString(),
                            DELFG = " ",
                            CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                            CHTME = DateTime.Now.ToString("HHmmss"),
                            CRDAT = DateTime.Now.ToString("yyyyMMdd"),
                            CRTME = DateTime.Now.ToString("HHmmss")
                        });
                    }

                    dbConn.ExecSql(sqlCmd);
                    foreach (Alocat data in addList)
                        if (dbConn.ExecSql(data.CreateInsertCommand()) == 0)
                            logger.Warn("門市: " + data.WERKS + ", Add failed");

                    MessageBox.Show("作業完成");
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                logger.Error("匯入資料失敗: " + ex.ToString());
                MessageBox.Show(ex.Message);
            }
        }
    }
}
