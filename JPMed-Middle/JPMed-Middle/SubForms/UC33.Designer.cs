﻿namespace JPMed_Middle.SubForms
{
    partial class UC33
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.dvList2 = new System.Windows.Forms.DataGridView();
            this.門市代號 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.門市說明 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.優先順序 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.固定分配量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.建立日期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.建立時間 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvList2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnLoad.Location = new System.Drawing.Point(11, 249);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(100, 27);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "開啟檔案";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnImport
            // 
            this.btnImport.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnImport.Location = new System.Drawing.Point(117, 249);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(100, 27);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "資料匯入";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(223, 249);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 27);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            this.dvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Location = new System.Drawing.Point(12, 282);
            this.dvList.Name = "dvList";
            this.dvList.ReadOnly = true;
            this.dvList.RowTemplate.Height = 24;
            this.dvList.Size = new System.Drawing.Size(747, 237);
            this.dvList.TabIndex = 3;
            // 
            // dvList2
            // 
            this.dvList2.AllowUserToAddRows = false;
            this.dvList2.AllowUserToDeleteRows = false;
            this.dvList2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvList2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dvList2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvList2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.門市代號,
            this.門市說明,
            this.優先順序,
            this.固定分配量,
            this.建立日期,
            this.建立時間});
            this.dvList2.Location = new System.Drawing.Point(12, 14);
            this.dvList2.Name = "dvList2";
            this.dvList2.ReadOnly = true;
            this.dvList2.RowTemplate.Height = 24;
            this.dvList2.Size = new System.Drawing.Size(747, 229);
            this.dvList2.TabIndex = 4;
            // 
            // 門市代號
            // 
            this.門市代號.DataPropertyName = "門市代號";
            this.門市代號.HeaderText = "門市代號";
            this.門市代號.Name = "門市代號";
            this.門市代號.ReadOnly = true;
            // 
            // 門市說明
            // 
            this.門市說明.DataPropertyName = "門市說明";
            this.門市說明.HeaderText = "門市說明";
            this.門市說明.Name = "門市說明";
            this.門市說明.ReadOnly = true;
            // 
            // 優先順序
            // 
            this.優先順序.DataPropertyName = "優先順序";
            this.優先順序.HeaderText = "優先順序";
            this.優先順序.Name = "優先順序";
            this.優先順序.ReadOnly = true;
            // 
            // 固定分配量
            // 
            this.固定分配量.DataPropertyName = "固定分配量";
            dataGridViewCellStyle1.Format = "0.###";
            this.固定分配量.DefaultCellStyle = dataGridViewCellStyle1;
            this.固定分配量.HeaderText = "固定分配量";
            this.固定分配量.Name = "固定分配量";
            this.固定分配量.ReadOnly = true;
            // 
            // 建立日期
            // 
            this.建立日期.DataPropertyName = "建立日期";
            this.建立日期.HeaderText = "建立日期";
            this.建立日期.Name = "建立日期";
            this.建立日期.ReadOnly = true;
            // 
            // 建立時間
            // 
            this.建立時間.DataPropertyName = "建立時間";
            this.建立時間.HeaderText = "建立時間";
            this.建立時間.Name = "建立時間";
            this.建立時間.ReadOnly = true;
            // 
            // UC33
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Controls.Add(this.dvList2);
            this.Controls.Add(this.dvList);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnLoad);
            this.Name = "UC33";
            this.Size = new System.Drawing.Size(778, 531);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvList2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.DataGridView dvList2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 門市代號;
        private System.Windows.Forms.DataGridViewTextBoxColumn 門市說明;
        private System.Windows.Forms.DataGridViewTextBoxColumn 優先順序;
        private System.Windows.Forms.DataGridViewTextBoxColumn 固定分配量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 建立日期;
        private System.Windows.Forms.DataGridViewTextBoxColumn 建立時間;
    }
}
