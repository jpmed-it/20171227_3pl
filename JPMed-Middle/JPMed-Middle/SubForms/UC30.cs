﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JPMed_Middle.Entity;
using JPMed_Middle.Utility;
using log4net;

namespace JPMed_Middle.SubForms
{
    public partial class UC30 : UserControl
    {
        string formName = "MiddleDB 特殊控制 Menu 選單";
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC30));
        SQLCreator sql;
        DBConnection dbConn;

        public UC30()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            DefaultControl();
            IntitalControl();
        }

        private void DefaultControl()
        {

        }

        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC30");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                LoadData();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void LoadData()
        {
            string sqlCmd = sql.GetCommand("GetRRANGE");

            DataTable tmpDT = dbConn.GetDataTable(sqlCmd);

            //移除多餘的空白(音資料庫的欄位為CHAR(3)
            for (int i = 0; i < tmpDT.Rows.Count; i++)
            {
                tmpDT.Rows[i]["HINUM"] = tmpDT.Rows[i]["HINUM"].ToString().Replace(" ", "");
                tmpDT.Rows[i]["LONUM"] = tmpDT.Rows[i]["LONUM"].ToString().Replace(" ", "");
            }

            dvList.DataSource = tmpDT;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[門市補貨控制表]?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dvList.Rows.Count; i++)
                {
                    string WMAAB = dvList.Rows[i].Cells["WMAAB"].Value.ToString();
                    string HINUM = dvList.Rows[i].Cells["HINUM"].Value.ToString();
                    string LONUM = dvList.Rows[i].Cells["LONUM"].Value.ToString();

                    string sqlCmd = SqlStatement.UC30.UpdateRRANGE(WMAAB, HINUM, LONUM);
                    dbConn.ExecSql(sqlCmd);
                }
                MessageBox.Show("修改成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show("修改失敗" + ex.Message);
            }
        }

        private void dvList_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                return;
            }
            string columnName = dvList.Columns[e.ColumnIndex].HeaderText;
            int tempInt;
            decimal tempDecimal;
            if (!int.TryParse(e.FormattedValue.ToString(), out tempInt) || !Decimal.TryParse(e.FormattedValue.ToString(), out tempDecimal) || tempInt < 0 || tempDecimal < 0)
            {
                e.Cancel = true;
                MessageBox.Show(columnName + "請輸入正確數字！");
            }

            //20170614 S類與A類可設定的上限值為300，其餘100
            if (dvList.Rows[e.RowIndex].Cells[0].Value.ToString() == "S" && e.ColumnIndex == 1)
            {
                if (tempInt > 300)
                {
                    e.Cancel = true;
                    MessageBox.Show(columnName + "不可大於300%！");
                }
            }
            else if (dvList.Rows[e.RowIndex].Cells[0].Value.ToString() == "A" && e.ColumnIndex == 1)
            {
                if (tempInt > 300)
                {
                    e.Cancel = true;
                    MessageBox.Show(columnName + "不可大於300%！");
                }
            }
            else
            {
                if (tempInt > 100)
                {
                    e.Cancel = true;
                    MessageBox.Show(columnName + "不可大於100%！");
                }
            }
        }
    }
}
