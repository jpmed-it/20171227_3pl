﻿namespace JPMed_Middle.SubForms
{
    partial class UC35
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.dvList = new System.Windows.Forms.DataGridView();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbClass = new System.Windows.Forms.ComboBox();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.btnToExcel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.WERKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BADAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Class1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Class2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDERM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WERKS,
            this.BADAT,
            this.Class1,
            this.Class2,
            this.ORDERM,
            this.SALESM,
            this.RATIO});
            this.dvList.Location = new System.Drawing.Point(12, 165);
            this.dvList.Name = "dvList";
            this.dvList.ReadOnly = true;
            this.dvList.RowTemplate.Height = 24;
            this.dvList.Size = new System.Drawing.Size(749, 352);
            this.dvList.TabIndex = 0;
            this.dvList.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dvList_DataBindingComplete);
            // 
            // dpStartDate
            // 
            this.dpStartDate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dpStartDate.Location = new System.Drawing.Point(109, 43);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(150, 27);
            this.dpStartDate.TabIndex = 2;
            // 
            // dpEndDate
            // 
            this.dpEndDate.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dpEndDate.Location = new System.Drawing.Point(312, 43);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(150, 27);
            this.dpEndDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(38, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "日期：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(277, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "至";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(38, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "類別：";
            // 
            // cbClass
            // 
            this.cbClass.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Items.AddRange(new object[] {
            "全部",
            "大分類",
            "中分類"});
            this.cbClass.Location = new System.Drawing.Point(109, 77);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(150, 24);
            this.cbClass.TabIndex = 9;
            // 
            // BtnSearch
            // 
            this.BtnSearch.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.BtnSearch.Location = new System.Drawing.Point(621, 114);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(90, 27);
            this.BtnSearch.TabIndex = 13;
            this.BtnSearch.Text = "查詢";
            this.BtnSearch.UseVisualStyleBackColor = true;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // btnToExcel
            // 
            this.btnToExcel.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnToExcel.Location = new System.Drawing.Point(621, 74);
            this.btnToExcel.Name = "btnToExcel";
            this.btnToExcel.Size = new System.Drawing.Size(90, 27);
            this.btnToExcel.TabIndex = 14;
            this.btnToExcel.Text = "匯出Excel";
            this.btnToExcel.UseVisualStyleBackColor = true;
            this.btnToExcel.Click += new System.EventHandler(this.btnToExcel_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(621, 20);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(90, 27);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // WERKS
            // 
            this.WERKS.DataPropertyName = "WERKS";
            this.WERKS.HeaderText = "店別";
            this.WERKS.Name = "WERKS";
            this.WERKS.ReadOnly = true;
            // 
            // BADAT
            // 
            this.BADAT.DataPropertyName = "BADAT";
            this.BADAT.HeaderText = "補貨日期";
            this.BADAT.Name = "BADAT";
            this.BADAT.ReadOnly = true;
            // 
            // Class1
            // 
            this.Class1.DataPropertyName = "CLASS1";
            this.Class1.HeaderText = "大分類";
            this.Class1.Name = "Class1";
            this.Class1.ReadOnly = true;
            // 
            // Class2
            // 
            this.Class2.DataPropertyName = "CLASS2";
            this.Class2.HeaderText = "中分類";
            this.Class2.Name = "Class2";
            this.Class2.ReadOnly = true;
            // 
            // ORDERM
            // 
            this.ORDERM.DataPropertyName = "ORDERM";
            this.ORDERM.HeaderText = "本次採購金額";
            this.ORDERM.Name = "ORDERM";
            this.ORDERM.ReadOnly = true;
            // 
            // SALESM
            // 
            this.SALESM.DataPropertyName = "SALESM";
            this.SALESM.HeaderText = "上週銷售金額";
            this.SALESM.Name = "SALESM";
            this.SALESM.ReadOnly = true;
            // 
            // RATIO
            // 
            this.RATIO.DataPropertyName = "RATIO";
            this.RATIO.HeaderText = "占比(%)";
            this.RATIO.Name = "RATIO";
            this.RATIO.ReadOnly = true;
            // 
            // UC35
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.btnToExcel);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.BtnSearch);
            this.Controls.Add(this.cbClass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dpEndDate);
            this.Controls.Add(this.dpStartDate);
            this.Controls.Add(this.dvList);
            this.Name = "UC35";
            this.Size = new System.Drawing.Size(778, 531);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.DateTimePicker dpEndDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbClass;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.Button btnToExcel;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn WERKS;
        private System.Windows.Forms.DataGridViewTextBoxColumn BADAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Class1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Class2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDERM;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESM;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATIO;





    }
}
