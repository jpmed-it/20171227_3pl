﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JPMed_Middle.Entity;
using JPMed_Middle.Utility;
using log4net;
using System.IO;

namespace JPMed_Middle.SubForms
{
    public partial class UC35 : UserControl
    {
        string formName = "進銷比報表";
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC35));
        SQLCreator sql;
        DBConnection dbConn;
        DataTable defaultDT = null;
        BindingSource source = new BindingSource();

        public UC35()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            IntitalControl();
        }

        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC35");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                cbClass.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[進銷比報表]?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            if (dpStartDate.Value.Date > dpEndDate.Value.Date)
            {
                MessageBox.Show("起始日期不可大於結束日期");
                return;
            }

            string classType = cbClass.SelectedIndex.ToString();

            string sqlCmd = SqlStatement.UC35.SearchRatio(dpStartDate.Value.ToString("yyyyMMdd"),
                        dpEndDate.Value.ToString("yyyyMMdd"),classType);

            DataTable tmpDT = dbConn.GetDataTable(sqlCmd);

            source.DataSource = tmpDT;
                dvList.DataSource = source;
        }

        private void btnToExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Excel|*.xls";
            fileDialog.Title = "另存新檔";
            fileDialog.CreatePrompt = true;
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK && fileDialog.FileName != "")
            {
                MemoryStream ms = ExcelExtension.DataToExcel(dvList);
                FileStream fs = new FileStream(fileDialog.FileName, FileMode.OpenOrCreate);
                ms.WriteTo(fs);
                fs.Close();
                ms.Close();

                MessageBox.Show("匯出Excel完成。");
            }
        }

        private void dvList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //若為N/A，則文字顯示紅色
            foreach (DataGridViewRow rowItem in dvList.Rows)
            {
                if (rowItem.Cells["RATIO"].Value.ToString() == "N/A")
                {

                    rowItem.Cells["RATIO"].Style.ForeColor = Color.Red;
                }
            }
        }
    }
}
