﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JPMed_Middle.Entity;
using JPMed_Middle.Utility;
using log4net;

namespace JPMed_Middle.SubForms
{
    public partial class UC34 : UserControl
    {
        string formName = "MiddleDB 特殊控制 Menu 選單";
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC34));
        SQLCreator sql;
        DBConnection dbConn;

        public UC34()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            DefaultControl();
            IntitalControl();
        }

        private void DefaultControl()
        {
            rb0.Checked = true;
            tbSCode.Text = string.Empty;
        }

        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC34");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                LoadData();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void LoadData()
        {
            string sqlCmd = sql.GetCommand("GetGmvectInformation");

            DataTable tmpDT = new DataTable();
            dvList.DataSource = dbConn.GetDataTable(sqlCmd);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[門市採購特收(依單號)]?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 0;
                string sqlCmd = string.Empty;
                foreach (DataGridViewRow row in dvList.Rows)
                {
                    if (Boolean.Parse(row.Cells[0].FormattedValue.ToString()))
                    {
                        count++;
                        sqlCmd += sql.GetCommand("DelRecord", new string[] { 
                            row.Cells[1].FormattedValue.ToString(),
                            row.Cells[2].FormattedValue.ToString()
                        });
                    }
                }
               
                if (count > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("您確定刪除" + count.ToString() + "筆資料?", "警告", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                        if (dbConn.ExecSql(sqlCmd) > 0)
                        {
                            MessageBox.Show("刪除成功!");
                            LoadData();
                        }
                        else
                            MessageBox.Show("無資料受到影響，請聯繫系統管理員");
                }
            }
            catch (Exception ex)
            {
                logger.Error("刪除命令發生錯誤: " + ex.ToString());
                MessageBox.Show("刪除命令發生錯誤: " + ex.Message);
            }
        }

        private void dvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2)
                {
                    if (e.Value.ToString().Trim().ToUpper() == "X")
                        dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            catch (Exception ex)
            {
                logger.Error("系統錯誤: " + ex.ToString());
                MessageBox.Show("系統錯誤: " + ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable tmpDT = new DataTable();
                List<Gmvect1> addList = new List<Gmvect1>();
                List<Gmvect1> updateList = new List<Gmvect1>();
                List<string> errItem = new List<string>();

                string sqlCmd = string.Empty;
                string result = string.Empty;

                DialogResult dialogResult = MessageBox.Show("您確定新增:[門市採購特收(依單號)]?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    sqlCmd = sql.GetCommand("CheckDataExist", new string[] { tbSCode.Text });
                    result = dbConn.GetDataTableRecData(sqlCmd); //0:已存在, 1:可回復, 2or '':可新增
                    switch (result)
                    {
                        case "0":
                            errItem.Add("採購單號: " + tbSCode.Text + ", 已存在相同項目");
                            break;
                        case "1":
                            updateList.Add(new Gmvect1()
                            {
                                EBELN = tbSCode.Text.ToUpper(),
                                CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                                CHTME = DateTime.Now.ToString("HHmmss")
                            });
                            break;

                        case "":
                        case "2":
                            addList.Add(new Gmvect1()
                            {
                                EBELN = tbSCode.Text.ToUpper(),
                                DFLAG = " ",
                                CRDAT = DateTime.Now.ToString("yyyyMMdd"),
                                CRTME = DateTime.Now.ToString("HHmmss"),
                                CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                                CHTME = DateTime.Now.ToString("HHmmss")
                            });
                            break;
                    }

                    foreach (Gmvect1 data in addList)
                        if (dbConn.ExecSql(data.CreateInsertCommand()) == 0)
                            logger.Warn("採購單號: " + data.EBELN + ", Add Fail");

                    foreach (Gmvect1 data in updateList)
                        if (dbConn.ExecSql(data.RevertDataCommand()) == 0)
                            logger.Warn("採購單號: " + data.EBELN + ", Revert Fail");

                    if (errItem.Count > 0)
                    {
                        logger.Warn(string.Join("\n", errItem.ToArray()));
                        MessageBox.Show(string.Join("\n", errItem.ToArray()));
                    }

                    MessageBox.Show("作業完成!");
                }
            }
            catch (Exception ex)
            {
                logger.Error("系統錯誤: " + ex.ToString());
                MessageBox.Show("系統錯誤: " + ex.Message);
            }
            finally
            {
                LoadData();
            }
        }
    }
}
