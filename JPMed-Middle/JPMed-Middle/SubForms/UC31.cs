﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data.OleDb;
using JPMed_Middle.Entity;
using JPMed_Middle.SqlStatement;
using JPMed_Middle.Utility;
using log4net;
using System.Text.RegularExpressions;

namespace JPMed_Middle.SubForms
{
    public partial class UC31 : UserControl
    {
        string formName = "MiddleDB 特殊控制 Menu 選單";
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC31));
        SQLCreator sql;
        DBConnection dbConn;

        public UC31()
        {
            InitializeComponent();
            DefaultControl();
            IntitalControl();
            LoadData();
        }

        private void DefaultControl()
        {
            btnImport.Enabled = false;
        }

        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC31");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void LoadData()
        {
            string sqlCmd = sql.GetCommand("GetRe2vctInfo");
            if (!string.IsNullOrEmpty(sqlCmd))
            {
                DataTable dt = dbConn.GetDataTable(sqlCmd);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.AsEnumerable())
                    {
                        decimal tmpLifnr = 0;
                        string lifnr = row[0].ToString();

                        try
                        {
                            tmpLifnr = decimal.Parse(lifnr);
                            row[0] = tmpLifnr.ToString("#");
                        }
                        catch { }
                    }
                }

                dvList2.DataSource = dt;
            }

            dvList.DataSource = null;
            btnImport.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[直退廠商控制表]?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            ofd.Filter = "Excel 97-2003 活頁簿(*.xls)|*.xls|Excel 活頁簿(*.xlsx)|*.xlsx";
            ofd.FilterIndex = 2;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (ofd.CheckFileExists)
                    {
                        string filePath = ofd.FileName;
                        readFile(filePath);
                    }
                    else
                    {
                        logger.Error("Error: Could not read file from disk. Original");
                        MessageBox.Show("Error: Could not read file from disk. Original");
                        btnImport.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error: Could not read file from disk. Original error: " + ex.Message);
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    btnImport.Enabled = false;
                }
            }
        }

        private void readFile(string _filePath)
        {
            string Excel03ConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1};IMEX=1'";
            string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1};IMEX=1'";
            string conStr = string.Empty;
            string filePath = _filePath, sheetName = string.Empty;
            string header = "NO";
            string extension = Path.GetExtension(filePath);

            switch (extension)
            {
                case ".xls": //Excel 97-03
                    conStr = string.Format(Excel03ConString, filePath, header);
                    break;

                case ".xlsx": //Excel 07
                    conStr = string.Format(Excel07ConString, filePath, header);
                    break;
            }

            try
            {
                //Get the name of the First Sheet.
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        cmd.Connection = con;
                        con.Open();
                        DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        sheetName = dtExcelSchema.AsEnumerable().Where(q => q["TABLE_NAME"].ToString().Contains("$") && !(new string[] { "ColumnType$", "AttributeType$" }).Contains(q["TABLE_NAME"].ToString()))
                                                                .Select(q => q["TABLE_NAME"].ToString()).FirstOrDefault();
                        con.Close();
                    }
                }

                //Read Data from the First Sheet.
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        using (OleDbDataAdapter oda = new OleDbDataAdapter())
                        {
                            DataTable dt = new DataTable();
                            cmd.CommandText = "SELECT * From [" + sheetName + "]";
                            cmd.Connection = con;
                            con.Open();
                            oda.SelectCommand = cmd;
                            oda.Fill(dt);
                            con.Close();

                            if (dt.Rows.Count <= 1)
                            {
                                MessageBox.Show("Excel中查無資料!");
                                logger.Warn("Excel中查無資料!");
                                return;
                            }

                            if (checkFileInformation(ref dt))
                                btnImport.Enabled = true;
                            else
                                btnImport.Enabled = false;

                            dvList.DataSource = dt;
                            dvList.Columns["廠商編號_Formated"].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Load file error: " + ex.ToString());
                MessageBox.Show("Load file error: " + ex.Message);
                btnImport.Enabled = false;
            }
        }

        private bool checkFileInformation(ref DataTable _tmpDT)
        {
            Dictionary<string, string> dirVendor = new Dictionary<string, string>();
            Dictionary<string, string> dirWerks = new Dictionary<string, string>();

            bool result = true;
            DataTable tmpDT = new DataTable();
            tmpDT.Columns.Add("廠商編號", typeof(string));
            tmpDT.Columns.Add("廠商編號_Formated", typeof(string));
            tmpDT.Columns.Add("廠商名稱", typeof(string));
            tmpDT.Columns.Add("門市代號", typeof(string));
            tmpDT.Columns.Add("驗證結果", typeof(string));

            int rowIndex = 0;
            foreach (DataRow row in _tmpDT.Rows)
            {
                if (rowIndex == 0)
                {
                    rowIndex++;
                    continue;
                }

                string lifnr = row[0].ToString(); //廠商代碼
                string werks = row[1].ToString(); //門市代號

                DataRow newRow = tmpDT.NewRow();
                newRow["廠商編號"] = lifnr;
                newRow["廠商編號_Formated"] = lifnr;
                newRow["門市代號"] = werks;

                if (string.IsNullOrEmpty(lifnr))
                {
                    newRow["驗證結果"] = "廠商代碼不可為空值";
                    result = false;
                    tmpDT.Rows.Add(newRow);
                    continue;
                }

                if (string.IsNullOrEmpty(werks))
                {
                    newRow["驗證結果"] = "門市代號不可為空值";
                    result = false;
                    tmpDT.Rows.Add(newRow);
                    continue;
                }

                Regex regex = new Regex(@"^\d*$"); //廠商若為數字編碼，補滿10碼0，否則照舊填入
                if (regex.IsMatch(lifnr))
                {
                    lifnr = lifnr.PadLeft(10, '0');
                    newRow["廠商編號_Formated"] = lifnr;
                }

                string sqlCmd = string.Empty;
                string vendorName = string.Empty;
                string werksName = string.Empty;

                //廠商處理
                if (!dirVendor.TryGetValue(lifnr, out vendorName))
                {
                    sqlCmd = sql.GetCommand("GetVendorInfo", new string[] { lifnr });
                    vendorName = dbConn.GetDataTableRecData(sqlCmd);

                    if (vendorName == DBNull.Value.ToString() || string.IsNullOrEmpty(vendorName))
                    {
                        newRow["驗證結果"] = "資料庫中查無此廠商編號";
                        result = false;
                    }
                    else
                    {
                        newRow["廠商名稱"] = vendorName;
                        dirVendor.Add(lifnr, vendorName);
                    }
                }
                else newRow["廠商名稱"] = vendorName;

                //門市處理
                if (!dirWerks.TryGetValue(werks, out werksName))
                {
                    sqlCmd = sql.GetCommand("GetWerksInfo", new string[] { werks });
                    werksName = dbConn.GetDataTableRecData(sqlCmd);

                    if (werksName == DBNull.Value.ToString() || string.IsNullOrEmpty(werksName))
                    {
                        newRow["驗證結果"] += "\n 資料庫中查無此門市代號";
                        result = false;
                    }
                    else
                    {
                        newRow["門市代號"] = werks;
                        dirWerks.Add(werks, werksName);
                    }
                }
                else newRow["門市代號"] = werks;

                if (result) newRow["驗證結果"] = string.Empty;

                tmpDT.Rows.Add(newRow);
            }

            _tmpDT = tmpDT;
            return result;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("確定繼續進行資料匯入:[直退廠商控制表]?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    List<Re2vct> addList = new List<Re2vct>();
                    string sqlCmd = string.Empty;
                    sqlCmd = sql.GetCommand("TruncateTable");

                    foreach (DataGridViewRow row in dvList.Rows)
                    {
                        addList.Add(new Re2vct()
                        {
                            LIFNR = row.Cells["廠商編號_Formated"].Value.ToString(),
                            WERKS = row.Cells["門市代號"].Value.ToString(),
                            CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                            CHTME = DateTime.Now.ToString("HHmmss"),
                            CRDAT = DateTime.Now.ToString("yyyyMMdd"),
                            CRTME = DateTime.Now.ToString("HHmmss")
                        });
                    }

                    dbConn.ExecSql(sqlCmd);
                    foreach (Re2vct data in addList)
                        if (dbConn.ExecSql(data.CreateInsertCommand()) == 0)
                            logger.Warn("廠商: " + data.LIFNR + ", 門市代號: " + data.WERKS + ", Add failed");

                    MessageBox.Show("作業完成");
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                logger.Error("匯入資料失敗: " + ex.ToString());
                MessageBox.Show(ex.Message);
            }
        }
    }
}
