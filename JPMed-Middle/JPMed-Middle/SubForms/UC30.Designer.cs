﻿namespace JPMed_Middle.SubForms
{
    partial class UC30
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dvList = new System.Windows.Forms.DataGridView();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.WMAAB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HINUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LONUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).BeginInit();
            this.SuspendLayout();
            // 
            // dvList
            // 
            this.dvList.AllowUserToAddRows = false;
            this.dvList.AllowUserToDeleteRows = false;
            this.dvList.AllowUserToResizeColumns = false;
            this.dvList.AllowUserToResizeRows = false;
            this.dvList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WMAAB,
            this.HINUM,
            this.LONUM});
            this.dvList.Location = new System.Drawing.Point(27, 3);
            this.dvList.MultiSelect = false;
            this.dvList.Name = "dvList";
            this.dvList.RowTemplate.Height = 24;
            this.dvList.ShowEditingIcon = false;
            this.dvList.Size = new System.Drawing.Size(345, 251);
            this.dvList.TabIndex = 0;
            this.dvList.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dvList_CellValidating);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnOK.Location = new System.Drawing.Point(27, 281);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "確認";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExit.Location = new System.Drawing.Point(297, 281);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 27);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "離開";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // WMAAB
            // 
            this.WMAAB.DataPropertyName = "WMAAB";
            this.WMAAB.HeaderText = "ABC分類";
            this.WMAAB.Name = "WMAAB";
            this.WMAAB.ReadOnly = true;
            this.WMAAB.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // HINUM
            // 
            this.HINUM.DataPropertyName = "HINUM";
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.HINUM.DefaultCellStyle = dataGridViewCellStyle1;
            this.HINUM.HeaderText = "最高 (%)";
            this.HINUM.Name = "HINUM";
            this.HINUM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // LONUM
            // 
            this.LONUM.DataPropertyName = "LONUM";
            dataGridViewCellStyle2.Format = "N0";
            this.LONUM.DefaultCellStyle = dataGridViewCellStyle2;
            this.LONUM.HeaderText = "最低 (%)";
            this.LONUM.Name = "LONUM";
            this.LONUM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // UC30
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.dvList);
            this.Name = "UC30";
            this.Size = new System.Drawing.Size(778, 531);
            ((System.ComponentModel.ISupportInitialize)(this.dvList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dvList;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn WMAAB;
        private System.Windows.Forms.DataGridViewTextBoxColumn HINUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LONUM;




    }
}
