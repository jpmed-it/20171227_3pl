﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JPMed_Middle.Entity;
using JPMed_Middle.Utility;
using log4net;

namespace JPMed_Middle.SubForms
{
    public partial class UC32 : UserControl
    {
        string formName = "MiddleDB 特殊控制 Menu 選單";
        static string errMsg = string.Empty;
        static ILog logger = LogManager.GetLogger(typeof(UC32));
        SQLCreator sql;
        DBConnection dbConn;

        public UC32()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            DefaultControl();
            IntitalControl();
        }

        private void DefaultControl()
        {
            rb0.Checked = true;
            tbSCode.Text = string.Empty;
            tbSCode.Enabled = false;
            laSName.Text = string.Empty;
            dpStart.Text = DateTime.Today.ToString("yyyy-MM-dd");
            dpEnd.Text = DateTime.Today.ToString("yyyy-MM-dd");
        }

        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC32");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        logger.Error("連結資料庫發生錯誤: " + errMsg);
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ResetSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                LoadData();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private void LoadData()
        {
            string sqlCmd = sql.GetCommand("GetGmvectInformation");

            DataTable tmpDT = new DataTable();
            dvList.DataSource = dbConn.GetDataTable(sqlCmd);
        }

        private void radio_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radio = (RadioButton)sender;

            switch (radio.Name)
            {
                case "rb0":

                    if (radio.Checked)
                    {
                        tbSCode.Enabled = false;
                        tbSCode.Text = string.Empty;
                        laSName.Text = string.Empty;
                    }
                    else tbSCode.Enabled = true;
                    break;

                case "rb1":
                    if (!radio.Checked)
                    {
                        tbSCode.Enabled = false;
                        tbSCode.Text = string.Empty;
                        laSName.Text = string.Empty;
                    }
                    else tbSCode.Enabled = true;

                    break;
            }
        }

        private void tbSCode_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Length == 4)
            {
                string sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { tb.Text });
                tb.Text = tb.Text.ToUpper();
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text))
                {
                    MessageBox.Show("查無資料!");
                    laSName.Text = string.Empty;
                }
            }
            else laSName.Text = string.Empty;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[門市採購單特收(依日期)]?", "警告", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                this.Parent.Controls[0].Visible = true;
                this.Parent.Controls.Remove(this);
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> errList = new List<string>();

                int count = 0;
                string sqlCmd = string.Empty;
                foreach (DataGridViewRow row in dvList.Rows)
                {
                    if (Boolean.Parse(row.Cells[0].FormattedValue.ToString()))
                    {
                        count++;
                        sqlCmd += sql.GetCommand("DelRecord", new string[] { 
                            row.Cells[1].FormattedValue.ToString(),
                            DateTime.Parse(row.Cells[3].FormattedValue.ToString()).ToString("yyyyMMdd"),
                            DateTime.Parse(row.Cells[4].FormattedValue.ToString()).ToString("yyyyMMdd"),
                            row.Cells[5].FormattedValue.ToString()
                        });

                        DateTime replsd = DateTime.Parse(row.Cells[3].FormattedValue.ToString());
                        DateTime repldd = DateTime.Parse(row.Cells[4].FormattedValue.ToString());

                        if (replsd >= DateTime.Today && DateTime.Today <= repldd)
                            errList.Add(string.Format("第{0}筆資料因起訖日跨當日，不可進行刪除", count.ToString()));
                    }
                }

                if (errList.Count > 0)
                {
                    MessageBox.Show(string.Join("\n", errList.ToArray()));
                    return;
                }

                if (count > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("您確定刪除" + count.ToString() + "筆資料?", "警告", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                        if (dbConn.ExecSql(sqlCmd) > 0)
                        {
                            MessageBox.Show("刪除成功!");
                            LoadData();
                        }
                        else
                            MessageBox.Show("無資料受到影響，請聯繫系統管理員");
                }
            }
            catch (Exception ex)
            {
                logger.Error("刪除命令發生錯誤: " + ex.ToString());
                MessageBox.Show("刪除命令發生錯誤: " + ex.Message);
            }
        }

        private void dvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    DateTime endDate;
                    if (DateTime.TryParse(e.Value.ToString(), out endDate) && endDate < DateTime.Today)
                    {
                        dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                    }
                }
                else if (e.ColumnIndex == 5)
                {
                    if (e.Value.ToString().Trim().ToUpper() == "X")
                        dvList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            catch (Exception ex)
            {
                logger.Error("系統錯誤: " + ex.ToString());
                MessageBox.Show("系統錯誤: " + ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime sdate = DateTime.Parse(dpStart.Text);
                DateTime edate = DateTime.Parse(dpEnd.Text);

                if (sdate > edate)
                {
                    MessageBox.Show("結束日期不可大於開始日期!");
                    return;
                }

                if (sdate <= DateTime.Today)
                {
                    MessageBox.Show("開始日期僅可大於當日，請修改開始時間");
                    return;
                }

                DataTable tmpDT = new DataTable();
                List<Gmvect> addList = new List<Gmvect>();
                List<Gmvect> updateList = new List<Gmvect>();
                List<string> errItem = new List<string>();

                int type = rb0.Checked ? 0 : 1; //0:全店, 1:指定
                string sqlCmd = string.Empty;
                string result = string.Empty;

                DialogResult dialogResult = MessageBox.Show("您確定新增:[SAP物料異動特放控制表]?", "警告", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    switch (type)
                    {
                        case 0:

                            sqlCmd = sql.GetCommand("GetAllStore");
                            tmpDT = dbConn.GetDataTable(sqlCmd);

                            if (tmpDT.Rows.Count == 0)
                            {
                                MessageBox.Show("查無分店資訊，請聯絡系統管理者!");
                                return;
                            }

                            foreach (DataRow row in tmpDT.Rows)
                            {
                                result = string.Empty;
                                string scode = row[0].ToString().ToUpper();
                                string sname = row[1].ToString();

                                sqlCmd = sql.GetCommand("CheckDataExist", new string[] { scode, sdate.ToString("yyyyMMdd"), edate.ToString("yyyyMMdd") });
                                result = dbConn.GetDataTableRecData(sqlCmd); //0不可建立, 1更新刪除註記, 2可新增

                                switch (result)
                                {
                                    case "0":
                                        errItem.Add("店代號: " + scode + ", 日期與其他項目重疊，不可建立");
                                        break;

                                    case "1":
                                        updateList.Add(new Gmvect()
                                        {
                                            WERKS = scode,
                                            REPLSD = sdate.ToString("yyyyMMdd"),
                                            REPLDD = edate.ToString("yyyyMMdd"),
                                            CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                                            CHTME = DateTime.Now.ToString("HHmmss")
                                        });
                                        break;

                                    case "2":
                                        addList.Add(new Gmvect()
                                        {
                                            WERKS = scode,
                                            REPLSD = sdate.ToString("yyyyMMdd"),
                                            REPLDD = edate.ToString("yyyyMMdd"),
                                            DFLAG = " ",
                                            CRDAT = DateTime.Now.ToString("yyyyMMdd"),
                                            CRTME = DateTime.Now.ToString("HHmmss"),
                                            CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                                            CHTME = DateTime.Now.ToString("HHmmss")
                                        });
                                        break;
                                }
                            }

                            break;

                        case 1:

                            if (string.IsNullOrEmpty(laSName.Text))
                            {
                                MessageBox.Show("查無此店點名稱，請重新輸入!");
                                return;
                            }

                            sqlCmd = sql.GetCommand("CheckDataExist", new string[] { tbSCode.Text, sdate.ToString("yyyyMMdd"), edate.ToString("yyyyMMdd") });
                            result = dbConn.GetDataTableRecData(sqlCmd); //0不可建立, 1更新刪除註記, 2可新增
                            switch (result)
                            {
                                case "0":
                                    errItem.Add("店代號: " + tbSCode.Text + ", 日期與其他項目重疊，不可建立");
                                    break;

                                case "1":
                                    updateList.Add(new Gmvect()
                                    {
                                        WERKS = tbSCode.Text.ToUpper(),
                                        REPLSD = sdate.ToString("yyyyMMdd"),
                                        REPLDD = edate.ToString("yyyyMMdd"),
                                        CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                                        CHTME = DateTime.Now.ToString("HHmmss")
                                    });
                                    break;

                                case "2":
                                    addList.Add(new Gmvect()
                                    {
                                        WERKS = tbSCode.Text.ToUpper(),
                                        REPLSD = sdate.ToString("yyyyMMdd"),
                                        REPLDD = edate.ToString("yyyyMMdd"),
                                        DFLAG = " ",
                                        CRDAT = DateTime.Now.ToString("yyyyMMdd"),
                                        CRTME = DateTime.Now.ToString("HHmmss"),
                                        CHDAT = DateTime.Now.ToString("yyyyMMdd"),
                                        CHTME = DateTime.Now.ToString("HHmmss")
                                    });
                                    break;
                            }
                            break;
                    }

                    foreach (Gmvect data in addList)
                        if (dbConn.ExecSql(data.CreateInsertCommand()) == 0)
                            logger.Warn("分店: " + data.WERKS + ", Add Fail");

                    foreach (Gmvect data in updateList)
                        if (dbConn.ExecSql(data.RevertDataCommand()) == 0)
                            logger.Warn("分店: " + data.WERKS + ", Revert Fail");

                    if (errItem.Count > 0)
                    {
                        logger.Warn(string.Join("\n", errItem.ToArray()));
                        MessageBox.Show(string.Join("\n", errItem.ToArray()));
                    }

                    MessageBox.Show("作業完成!");
                }
            }
            catch (Exception ex)
            {
                logger.Error("系統錯誤: " + ex.ToString());
                MessageBox.Show("系統錯誤: " + ex.Message);
            }
            finally
            {
                LoadData();
            }
        }
    }
}
