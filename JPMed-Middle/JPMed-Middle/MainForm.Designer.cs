﻿namespace JPMed_Middle
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.管理部ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.總倉配貨控制表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.門市補貨控制表控制表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.門市審單比較表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.商品部ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.直退廠商控制表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.門市採購單特收依單號ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.離開ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel0 = new System.Windows.Forms.Panel();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.進銷比報表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.panel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.管理部ToolStripMenuItem,
            this.商品部ToolStripMenuItem,
            this.離開ToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(778, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // 管理部ToolStripMenuItem
            // 
            this.管理部ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.總倉配貨控制表ToolStripMenuItem,
            this.門市補貨控制表控制表ToolStripMenuItem,
            this.門市審單比較表ToolStripMenuItem,
            this.進銷比報表ToolStripMenuItem});
            this.管理部ToolStripMenuItem.Name = "管理部ToolStripMenuItem";
            this.管理部ToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.管理部ToolStripMenuItem.Text = "營業部";
            // 
            // 總倉配貨控制表ToolStripMenuItem
            // 
            this.總倉配貨控制表ToolStripMenuItem.Name = "總倉配貨控制表ToolStripMenuItem";
            this.總倉配貨控制表ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.總倉配貨控制表ToolStripMenuItem.Text = "1.總倉配貨控制表";
            this.總倉配貨控制表ToolStripMenuItem.Click += new System.EventHandler(this.總倉配貨控制表ToolStripMenuItem_Click);
            // 
            // 門市補貨控制表控制表ToolStripMenuItem
            // 
            this.門市補貨控制表控制表ToolStripMenuItem.Name = "門市補貨控制表控制表ToolStripMenuItem";
            this.門市補貨控制表控制表ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.門市補貨控制表控制表ToolStripMenuItem.Text = "2.門市補貨控制表";
            this.門市補貨控制表控制表ToolStripMenuItem.Click += new System.EventHandler(this.門市補貨控制表ToolStripMenuItem_Click);
            // 
            // 門市審單比較表ToolStripMenuItem
            // 
            this.門市審單比較表ToolStripMenuItem.Name = "門市審單比較表ToolStripMenuItem";
            this.門市審單比較表ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.門市審單比較表ToolStripMenuItem.Text = "3.門市審單比較表";
            this.門市審單比較表ToolStripMenuItem.Click += new System.EventHandler(this.門市審單比較表ToolStripMenuItem_Click);
            // 
            // 商品部ToolStripMenuItem
            // 
            this.商品部ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.直退廠商控制表ToolStripMenuItem,
            this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem,
            this.門市採購單特收依單號ToolStripMenuItem});
            this.商品部ToolStripMenuItem.Name = "商品部ToolStripMenuItem";
            this.商品部ToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.商品部ToolStripMenuItem.Text = "商品部";
            // 
            // 直退廠商控制表ToolStripMenuItem
            // 
            this.直退廠商控制表ToolStripMenuItem.Name = "直退廠商控制表ToolStripMenuItem";
            this.直退廠商控制表ToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.直退廠商控制表ToolStripMenuItem.Text = "1.直退廠商控制表";
            this.直退廠商控制表ToolStripMenuItem.Click += new System.EventHandler(this.直退廠商控制表ToolStripMenuItem_Click);
            // 
            // pDA上傳SAP的物料異動特放控制表ToolStripMenuItem
            // 
            this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem.Name = "pDA上傳SAP的物料異動特放控制表ToolStripMenuItem";
            this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem.Text = "2.門市採購單特收(依日期)";
            this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem.Click += new System.EventHandler(this.pDA上傳SAP的物料異動特放控制表ToolStripMenuItem_Click);
            // 
            // 門市採購單特收依單號ToolStripMenuItem
            // 
            this.門市採購單特收依單號ToolStripMenuItem.Name = "門市採購單特收依單號ToolStripMenuItem";
            this.門市採購單特收依單號ToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.門市採購單特收依單號ToolStripMenuItem.Text = "3.門市採購特收(依單號)";
            this.門市採購單特收依單號ToolStripMenuItem.Click += new System.EventHandler(this.門市採購單特收依單號ToolStripMenuItem_Click);
            // 
            // 離開ToolStripMenuItem
            // 
            this.離開ToolStripMenuItem.Name = "離開ToolStripMenuItem";
            this.離開ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.離開ToolStripMenuItem.Text = "離開";
            this.離開ToolStripMenuItem.Click += new System.EventHandler(this.離開ToolStripMenuItem_Click);
            // 
            // panel0
            // 
            this.panel0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel0.Controls.Add(this.pb1);
            this.panel0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel0.Location = new System.Drawing.Point(0, 24);
            this.panel0.Name = "panel0";
            this.panel0.Size = new System.Drawing.Size(778, 531);
            this.panel0.TabIndex = 1;
            // 
            // pb1
            // 
            this.pb1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pb1.Image = ((System.Drawing.Image)(resources.GetObject("pb1.Image")));
            this.pb1.Location = new System.Drawing.Point(308, 152);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(167, 154);
            this.pb1.TabIndex = 0;
            this.pb1.TabStop = false;
            // 
            // 進銷比報表ToolStripMenuItem
            // 
            this.進銷比報表ToolStripMenuItem.Name = "進銷比報表ToolStripMenuItem";
            this.進銷比報表ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.進銷比報表ToolStripMenuItem.Text = "4.進銷比報表";
            this.進銷比報表ToolStripMenuItem.Click += new System.EventHandler(this.進銷比報表ToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 555);
            this.Controls.Add(this.panel0);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "MiddleDB 特殊控制 Menu 選單";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem 管理部ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 門市補貨控制表控制表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 總倉配貨控制表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 商品部ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 直退廠商控制表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDA上傳SAP的物料異動特放控制表ToolStripMenuItem;
        private System.Windows.Forms.Panel panel0;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.ToolStripMenuItem 離開ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 門市採購單特收依單號ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 門市審單比較表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 進銷比報表ToolStripMenuItem;
    }
}

