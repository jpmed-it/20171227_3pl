﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC08 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetRe2vctInfo(string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         LIFNR AS Code
                        ,(SELECT TOP 1 NAME1 FROM VENDOR WHERE A.LIFNR = LIFNR) AS Name
                        ,LIFNR AS Value
                        FROM RE2VCT A 
                        WHERE WERKS = '{0}'
                        ORDER BY A.LIFNR");
            sb = sb.Replace("{0}", _werks);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetProduct(string _werks, string _ean11)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         MATNR
                        ,MEINS
                        ,MAKTX
                        ,EAN11
                        FROM ASSORT
                        WHERE WERKS = '{0}' AND (EAN11 = '{1}' OR EAN12 = '{1}')
                        AND (TMOUT = '' OR TMOUT IS NULL)
                        "); //20160919需求變更:抓EAN11 or EAN12

            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _ean11);
            return sb.ToString();
        }

        public static string GetCheckCommand(string _bednr, string _matnr, string _ean11, string _werks, string _lifnr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS COMARK, PROCD FROM RE2VENDOR WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND LIFNR = '{4}'");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr);
            return sb.ToString();
        }

        public static string GetInsertCommand(string _bednr, string _matnr, string _ean11, string _werks, string _lifnr, string _empCode, string _menge_r, string _meins)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"INSERT INTO RE2VENDOR (BEDNR, MATNR, EAN11, WERKS, LIFNR, LGORT, CRDAT, CRTME, AFNAM, BSART, MENGE_R, MEINS, COMARK, DELFLG, CHUSN, CHDAT, CHTME) VALUES (
                                               '{0}', '{1}', '{2}', '{3}', '{4}', '0001', '{5}', '{6}', '{7}', 'ZRE3', {8}, '{9}', ' ', ' ', '{10}', '{11}', '{12}')");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr)
                   .Replace("{5}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{6}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{7}", _empCode)
                   .Replace("{8}", _menge_r)
                   .Replace("{9}", _meins)
                   .Replace("{10}", _empCode)
                   .Replace("{11}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{12}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _matnr, string _ean11, string _werks, string _lifnr, string _empCode, string _menge_r)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2VENDOR SET MENGE_R = {5}, CHUSN = '{6}', CHDAT = '{7}', CHTME = '{8}' WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND LIFNR = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr)
                   .Replace("{5}", _menge_r)
                   .Replace("{6}", _empCode)
                   .Replace("{7}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{8}", DateTime.Now.ToString("HHmmss"));

            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr, string _matnr, string _ean11, string _werks, string _lifnr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS '退貨確認'
                        ,(SELECT TOP 1 NAME1 FROM VENDOR WHERE A.LIFNR = LIFNR) AS '退貨至'
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '退貨量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
                        FROM RE2VENDOR A
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND ASSORT.WERKS = A.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}'
                        AND A.MATNR = '{1}'
                        AND A.EAN11 = '{2}'
                        AND A.WERKS = '{3}'
                        AND A.LIFNR = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _lifnr);

            return sb.ToString();
        }

        /// <summary>
        /// 20160919需求變更:需檢查退貨之商品，是否存在於INFORED(主次供對應表)
        /// </summary>
        /// <param name="_lifnr"></param>
        /// <param name="_matnr"></param>
        /// <returns></returns>
        public static string CheckHasInfored(string _lifnr, string _matnr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT TOP 1 
                        COALESCE(LOEKZ, '') AS LOEKZ
                        FROM INFORED
                        WHERE LIFNR = '{0}'
                        AND MATNR = '{1}'");

            sb = sb.Replace("{0}", _lifnr)
                   .Replace("{1}", _matnr);

            return sb.ToString();
        }
    }
}
