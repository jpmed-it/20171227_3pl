﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC05 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetAllSrote(string _selfCode)
        {
            return @"SELECT WERKS + '-' + NAME1 AS Text ,WERKS AS 'Value' FROM STORE WHERE WERKS != '" + _selfCode + @"' AND WERKS != 'TDC1' AND WERKS != 'TDC2'
                     ORDER BY 2";
        }

        //20170225 增加報廢品判斷
        public static string GetProduct(string _werks, string _reswk, string _ean11)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         MATNR
                        ,MEINS
                        ,MAKTX
                        ,LABOR
                        ,RUECK
                        ,DELFG
                        ,EAN11
                        FROM ASSORT A
                        WHERE ( WERKS = '{0}' OR WERKS = '{1}' ) AND (EAN11 = '{2}' OR EAN12 = '{2}')
                        AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        "); //20160919需求變更:抓EAN11 or EAN12

            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _reswk)
                   .Replace("{2}", _ean11);

            return sb.ToString();
        }

        public static string GetCheckCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS COMARK,
                        PROCD FROM STO2STO 
                        WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' 
                        AND RESWK = '{3}' AND WERKS = '{4}'");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks);
            return sb.ToString();
        }

        public static string GetInsertCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks, string _empCode, string _menge_r, string _meins, string _rush, string _wmflg, string _bsart)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" IF NOT EXISTS (SELECT PROCD FROM STO2STO WHERE BEDNR = '{0}' AND RESWK = '{3}' AND WERKS <> '{4}')");
            sb.Append(" BEGIN ");
            sb.Append(@"INSERT INTO STO2STO (BEDNR, MATNR, EAN11, RESWK, WERKS, LGORT, CRDAT, CRTME, AFNAM, BSART, MENGE_R, MEINS, 
                        DELFLG, CHUSN, CHDAT, CHTME, RUSH, WMFLG) VALUES (
                        '{BEDNR}', '{MATNR}', '{EAN11}', '{RESWK}', '{WERKS}', '0001', '{CRDAT}', '{CRTME}', '{AFNAM}', '{BSART}', {MENGE_R}, '{MEINS}',
                        ' ', '{CHUSN}', '{CHDAT}', '{CHTME}', '{RUSH}', '{WMFLG}')");
            sb.Append(" END");
            sb = sb.Replace("{BEDNR}", _bednr)
                   .Replace("{MATNR}", _matnr)
                   .Replace("{EAN11}", _ean11)
                   .Replace("{RESWK}", _reswk)
                   .Replace("{WERKS}", _werks)
                   .Replace("{CRDAT}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{CRTME}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{AFNAM}", _empCode)
                   .Replace("{BSART}", _bsart)
                   .Replace("{MENGE_R}", _menge_r)
                   .Replace("{MEINS}", _meins)
                   .Replace("{CHUSN}", _empCode)
                   .Replace("{CHDAT}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{CHTME}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{RUSH}", _rush)
                   .Replace("{WMFLG}", _wmflg);//20170407增加派車註記及急單註記

            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks, string _empCode, string _menge_r, string _rush, string _wmflg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE STO2STO SET MENGE_R = {5}, CHUSN = '{6}', CHDAT = '{7}', CHTME = '{8}', RUSH = '{9}', WMFLG ='{10}' WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND RESWK = '{3}' AND WERKS = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks)
                   .Replace("{5}", _menge_r)
                   .Replace("{6}", _empCode)
                   .Replace("{7}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{8}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{9}", _rush)
                   .Replace("{10}", _wmflg);//20170407增加派車註記及急單註記

            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '調撥數量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.RESWK = WERKS AND A.EAN11 = EAN11) AS '品名'
						,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '調入門市'
						,COALESCE(B.UMMENGE, 0) AS '前週銷量'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.RESWK = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調出門市庫存量'
						,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調入門市庫存量'
                        FROM STO2STO A
						LEFT JOIN SALES B ON A.RESWK = B.WERKS AND A.MATNR = B.MATNR AND B.SPBUP = {5}
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}'
                        AND A.MATNR = '{1}'
                        AND A.EAN11 = '{2}'
                        AND A.RESWK = '{3}'
                        AND A.WERKS = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks)
                   .Replace("{5}", _spbup);

            return sb.ToString();
        }

        //20170523新增時判斷是否已有相同BEDNR
        public static string GetBEDNRCheckCommand(string _bednr, string _reswk, string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT PROCD FROM STO2STO WHERE BEDNR = '{0}' AND RESWK = '{1}' AND WERKS <> '{2}'");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _reswk)
                   .Replace("{2}", _werks);
            return sb.ToString();
        }
    }
}
