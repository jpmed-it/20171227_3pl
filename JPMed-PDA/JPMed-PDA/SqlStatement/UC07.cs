﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC07 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetReason()
        {
            return @"SELECT '' AS Text, '' AS 'Value'
                     UNION ALL
                     SELECT BEZEI AS Text , BSGRU + ',' + LGORT AS 'Value' FROM REASON 
                     ORDER BY 2";
        }

        //20170225 增加報廢品判斷
        public static string GetProduct(string _werks, string _ean11)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         MATNR
                        ,MEINS
                        ,MAKTX
                        ,LABOR
                        ,RUECK
                        ,EAN11
                        FROM ASSORT A
                        WHERE WERKS = '{0}' AND (EAN11 = '{1}' OR EAN12 = '{1}')
                        AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        "); //20160919需求變更:抓EAN11 or EAN12

            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _ean11);
            return sb.ToString();
        }

        public static string GetCheckCommand(string _bednr, string _matnr, string _ean11, string _reswk)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS 'COMARK', PROCD FROM RE2TDC1 WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND RESWK = '{3}' AND WERKS = '{4}'");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", "TDC2");
            return sb.ToString();
        }

        public static string GetInsertCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _bsgru, string _empCode, string _menge_r, string _meins, string _lgort)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"INSERT INTO RE2TDC1 (BEDNR, MATNR, EAN11, RESWK, WERKS, LGORT, CRDAT, CRTME, AFNAM, BSART, MENGE_R, MEINS, COMARK, DELFLG, CHUSN, CHDAT, CHTME, BSGRU) VALUES (
                                               '{0}', '{1}', '{2}', '{3}', 'TDC2', '{LGORT}', '{5}', '{6}', '{7}', 'ZUR1', {8}, '{9}', ' ', ' ', '{10}', '{11}', '{12}', '{4}')");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _bsgru)
                   .Replace("{5}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{6}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{7}", _empCode)
                   .Replace("{8}", _menge_r)
                   .Replace("{9}", _meins)
                   .Replace("{10}", _empCode)
                   .Replace("{11}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{12}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{LGORT}", _lgort);

            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _bsgru, string _empCode, string _menge_r, string _lgort)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE RE2TDC1 SET MENGE_R = {5}, BSGRU = '{4}', CHUSN = '{6}', CHDAT = '{7}', CHTME = '{8}', LGORT = '{LGORT}' WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND RESWK = '{3}' AND WERKS = 'TDC2'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _bsgru)
                   .Replace("{5}", _menge_r)
                   .Replace("{6}", _empCode)
                   .Replace("{7}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{8}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{LGORT}", _lgort);


            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr, string _matnr, string _ean11, string _reswk)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS '退貨確認'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '退貨至'
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '退貨量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.RESWK = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.RESWK = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
                        ,COALESCE((SELECT TOP 1 BEZEI FROM REASON WHERE A.BSGRU = BSGRU),'') AS '備註'
                        FROM RE2TDC1 A
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND ASSORT.WERKS = A.RESWK AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}'
                        AND A.MATNR = '{1}'
                        AND A.EAN11 = '{2}'
                        AND A.RESWK = '{3}'
                        AND A.WERKS = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", "TDC2");

            return sb.ToString();
        }

        /// <summary>
        /// 20160919需求變更:需檢查退貨之商品，是否存在於INFORED(主次供對應表)
        /// </summary>
        /// <param name="_lifnr"></param>
        /// <param name="_matnr"></param>
        /// <returns></returns>
        public static string CheckHasInfored(string _matnr, string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT TOP 1 
                        COALESCE(B.LIFNR, '') AS LIFNR
                        FROM INFORED A
                        INNER JOIN RE2VCT B ON (A.LIFNR = B.LIFNR AND B.WERKS = '{1}')
                        WHERE (A.LOEKZ = '' OR A.LOEKZ IS NULL)
                        AND A.MATNR = '{0}'");

            sb = sb.Replace("{0}", _matnr);
            sb = sb.Replace("{1}", _werks);

            return sb.ToString();
        }
    }
}
