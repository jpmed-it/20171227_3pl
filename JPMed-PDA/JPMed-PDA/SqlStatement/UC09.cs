﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;
using JPMed_PDA.Entity;

namespace JPMed_PDA.SqlStatement
{
    /// <summary>
    /// 物流收貨作業 UC09
    /// </summary>
    public class UC09 : SQLStatementCreator
    {
        /// <summary>
        /// 取得門市名稱
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        /// <summary>
        /// 取得員工名稱
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        /// <summary>
        /// 依單號取得門市對門市調撥資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetSTO2STOByBednrOrEbeln(string _Werks, string _Bednr )
        {
            string sqlScript = string.Format("SELECT * FROM STO2STO " +
            "WHERE RESWK = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _Bednr);

            return sqlScript;
        }

        /// <summary>
        /// 依單號取調撥至總倉調撥資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetSTO2TDC1ByBednrOrEbeln(string _Werks, string _Bednr)
        {
            string sqlScript = string.Format("SELECT * FROM STO2TDC1 " +
            "WHERE RESWK = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _Bednr);

            return sqlScript;
        }

        /// <summary>
        /// 依調撥至總倉調撥資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetRE2TDC1ByBednrOrEbeln(string _Werks, string _Bednr)
        {
            string sqlScript = string.Format("SELECT * FROM RE2TDC1 " +
            "WHERE RESWK = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _Bednr);

            return sqlScript;
        }


        /// <summary>
        /// 依單號物流異動資料
        /// </summary>
        /// <param name="_Ebeln"單號></param>
        /// <returns></returns>
        public static string GetGMOVET(string _Ebeln)
        {
            string sqlScript =
                string.Format("Select EBELN From GMOVET WHERE EBELN = '{0}' AND BWART = '641'", _Ebeln);
            return sqlScript;
        }

        public static string GetGmovehInfo(string _keyValue)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT BSART, EBELN, BEDNR FROM GMOVEH WHERE EBELN = '{0}' OR BEDNR = '{0}'");
            sb = sb.Replace("{0}", _keyValue);

            return sb.ToString();
        }

        /// <summary>
        /// 新增物料異動資料
        /// </summary>
        /// <param name="_bsart">單型</param>
        /// <param name="_ebeln">單號</param>
        /// <param name="_ebelp">項目號碼</param>
        /// <param name="_matnr">物料號碼</param>
        /// <param name="_ean11">國際貨品代碼</param>
        /// <param name="_werks">收貨店代號</param>
        /// <param name="_reswk">發貨店代號</param>
        /// <param name="_empCode">申請人代號</param>
        /// <param name="_menge_p">數量</param>
        /// <param name="meins_p">計量單位</param>
        /// <param name="_menge_m">異動數量</param>
        /// <param name="_vbeln">物流單號</param>
        /// <returns></returns>
        public static string InsertGMOVET(List<Gmovet> GmovetList, string empCode, string _CRDAT, string _CHTME)
        {
            StringBuilder sb = new StringBuilder();

            //TODO: 是否刪除原本未完成的單?
            //            string deleteCmd = @"DELETE GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND EBELP = '{2}' 
            //AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND MATNR = '{3}' AND EAN11 = '{4}' AND PROCD != 'X'; ";
            //            sb.Append(
            //                string.Format(deleteCmd, GmovetList[0].BSART, GmovetList[0].EBELN,
            //                GmovetList[0].EBELP, GmovetList[0].MATNR, GmovetList[0].EAN11));

            string insertCmdHeader = @"INSERT INTO GMOVET (BSART, EBELN, EBELP, CRDAT, MATNR,
                                                     EAN11, WERKS, AFNAM, MENGE_P, MEINS_P,
                                                     BWART, MENGE_M, MEINS_M, DELFLG, PROCD,
                                                     CHUSN, CHDAT, CHTME, VBELN) VALUES";
            sb.Append(insertCmdHeader);

            string insertCmd = @" ('{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', {8}, '{9}',
                                    '{10}', {11}, '{12}', '{13}', ' ','{14}', '{15}', '{16}', '{17}')";

            foreach (var item in GmovetList)
            {
                if (GmovetList.IndexOf(item) > 0)
                {
                    sb.Append(", ");
                }

                sb.Append(string.Format(insertCmd,
                    item.BSART,
                    item.EBELN,
                    item.EBELP,
                    _CRDAT,
                    item.MATNR,
                    item.EAN11,
                    item.RESWK,
                    empCode,
                    item.MENGE_P,
                    item.MEINS,
                    "641",
                    item.MENGE_M,
                    item.MEINS,
                    (Decimal.Parse(item.MENGE_M) == 0 ? "X" : " "),
                    empCode,
                    _CRDAT,
                    _CHTME,
                    item.VBELN));
            }

            sb.Append(";");

            return sb.ToString();
        }

        /// <summary>
        /// 計算調撥單中的商品資訊
        /// </summary>
        /// <param name="_bsart">單型</param>
        /// <param name="_ebeln">單號</param>
        /// <param name="_bednr">追蹤調撥號碼</param>
        /// <param name="_ean11">國際標準碼</param>
        /// <param name="_vbeln">物流單號</param>
        /// <returns></returns>
        public static string GetSNInfo(string _bsart, string _ebeln, string _bednr, string _ean11, string _vbeln)
        {
            StringBuilder sb = new StringBuilder();

            switch (_bsart.ToUpper())
            {
                case "ZUB1":
                case "ZUB3":
                    sb.Append(@"SELECT
                                 '@BSART_P' AS BSART
                                ,'@EBLEN' AS EBELN
								,A.EBELP AS EBELP
								,A.MATNR AS MATNR
								,A.EAN11 AS EAN11
								,A.WERKS AS WERKS
                                ,A.RESWK AS RESWK
                                ,A.MENGE_R AS MENGE_P
                                ,A.MEINS AS MEINS_P
								, A.MENGE_R as MENGE_M
								, A.MEINS as  MEINS_M
								, A.EBELN as VBELN
                                FROM STO2STO A 
                                INNER JOIN ASSORT 
								ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE 
								A.BSART = '@BSART_P'
								AND A.EBELN = '@EBLEN' 
								AND A.BEDNR = '@BEDNR'
                                AND A.DELFLG ='' ");

                    sb = sb.Replace("@BSART_P", _bsart)
                           .Replace("@EBLEN", _ebeln)
                           .Replace("@BEDNR", _bednr);

                    return sb.ToString();

                case "ZUB2":
                    sb.Append(@"SELECT
                                 '@BSART_P' AS BSART
                                ,'@EBLEN' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS MATNR
                                ,A.EAN11 AS EAN11
                                ,A.WERKS AS WERKS
                                ,A.RESWK AS RESWK
								,A.MENGE_R as MENGE_M
								,A.MEINS as  MEINS_M
                                ,A.MENGE_R AS MENGE_P
                                ,A.MEINS AS MEINS_P
								, A.EBELN as VBELN
                                FROM STO2TDC1 A 
                                INNER JOIN ASSORT 
								ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE 
								A.BSART = '@BSART_P'
								AND A.EBELN = '@EBLEN' 
								AND A.BEDNR = '@BEDNR'
                                AND A.DELFLG ='' ");

                    sb = sb.Replace("@BSART_P", _bsart)
                           .Replace("@EBLEN", _ebeln)
                           .Replace("@BEDNR", _bednr);

                    return sb.ToString();

                case "ZUR1":
                    sb.Append(@"SELECT
                                 '@BSART_P' AS BSART
                                ,'@EBLEN' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS MATNR
                                ,A.EAN11 AS EAN11
                                ,A.WERKS AS WERKS
                                ,A.RESWK AS RESWK
								,A.MENGE_R as MENGE_M
								,A.MEINS as  MEINS_M
                                ,A.MENGE_R AS MENGE_P
                                ,A.MEINS AS MEINS_P
								, A.EBELN as VBELN
                                FROM RE2TDC1 A 
                                INNER JOIN ASSORT 
								ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE 
								A.BSART = '@BSART_P'
								AND A.EBELN = '@EBLEN' 
								AND A.BEDNR = '@BEDNR'
                                AND A.DELFLG ='' ");

                    //20170810 修改
//                    sb.Append(@"SELECT
//                                 '@BSART_P' AS BSART
//                                ,'@EBLEN' AS EBELN
//                                ,A.BEDNR AS BEDNR
//                                ,A.EBELP AS EBELP
//                                ,A.MATNR AS MATNR
//                                ,A.EAN11 AS EAN11
//                                ,A.WERKS AS WERKS
//                                ,A.RESWK AS RESWK
//								,A.MENGE_R as MENGE_M
//								,A.MEINS as  MEINS_M
//                                ,A.MENGE_R AS MENGE_P
//                                ,A.MEINS AS MEINS_P
//								, A.EBELN as VBELN
//                                FROM RE2TDC1 A 
//                                INNER JOIN ASSORT 
//								ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
//                                WHERE 
//								A.BSART = '@BSART_P'
//								AND A.EBELN = '@EBLEN' 
//								AND A.BEDNR = '@BEDNR'
//                                AND A.DELFLG ='' ");

                    sb = sb.Replace("@BSART_P", _bsart)
                           .Replace("@EBLEN", _ebeln)
                           .Replace("@BEDNR", _bednr);

                    return sb.ToString();
            }

            return string.Empty;
        }
    }
}
