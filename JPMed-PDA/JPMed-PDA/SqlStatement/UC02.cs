﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC02 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetPrdInformation(string _werks, string _envCode)
        {
            string result = @"SELECT
                                COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS LABST, 
                                MATNR, MAKTX, DELFG 
                                FROM ASSORT A 
                                WHERE WERKS = '{0}' AND EAN11 = '{1}'
                                AND (A.TMOUT = '' OR A.TMOUT IS NULL)";
            return string.Format(result, _werks, _envCode);
        }

        public static string GetPOSMC_1(string _filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         POSMC.COMPANY 
                        ,POSMC.CREATE_DATE 
                        ,POSMC.MODIFIER
                        ,POSMC.MODI_DATE
                        ,POSMC.MC001
                        ,POSMC.MC002
                        ,POSMC.MC003
                        ,POSMC.MC004
                        ,POSMC.MC005
                        ,POSMC.MC006
                        ,POSMC.MC007
                        ,POSMC.MC009
                        ,POSMC.MC011
                        ,POSMC.MC019
                        ,POSMB.MB004
                        ,POSMB.MB012
                        ,POSMB.MB013
                        ,POSMF.MF004
                        FROM POSMC 
                        INNER JOIN POSMB ON POSMC.COMPANY = POSMB.COMPANY AND POSMC.MC001 = POSMB.MB001 AND POSMC.MC002 = POSMB.MB002 AND POSMC.MC003 = POSMB.MB003 
                        INNER JOIN POSMF ON POSMB.MB003 = POSMF.MF003
                        WHERE {0}
                        AND POSMB.MB013 >= '{1}'
                        AND POSMB.MB008='Y'  
                        ORDER BY MC003");
            
            sb = sb.Replace("{0}", _filter)
                   .Replace("{1}", DateTime.Today.ToString("yyyyMMdd"));

            return sb.ToString();
        }

        public static string GetPOSMC_2(string _filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         POSMC.COMPANY
                        ,POSMC.CREATE_DATE
                        ,POSMC.MODIFIER
                        ,POSMC.MODI_DATE
                        ,POSMC.MC001
                        ,POSMC.MC002
                        ,POSMC.MC003
                        ,POSMC.MC004
                        ,POSMC.MC005
                        ,POSMC.MC006
                        ,POSMC.MC007
                        ,POSMC.MC009
                        ,POSMC.MC011
                        ,POSMC.MC019
                        ,POSMB.MB004
                        ,POSMB.MB012
                        ,POSMB.MB013 
                        FROM POSMC 
                        INNER JOIN POSMB ON POSMC.COMPANY = POSMB.COMPANY AND POSMC.MC001 = POSMB.MB001 AND POSMC.MC002 = POSMB.MB002 AND POSMC.MC003 = POSMB.MB003 
                        WHERE {0} 
                        AND POSMB.MB013 >= '{1}' 
                        AND POSMB.MB008='Y'
                        ORDER BY MC003 ");

            sb = sb.Replace("{0}", _filter)
                   .Replace("{1}", DateTime.Today.ToString("yyyyMMdd"));

            return sb.ToString();
        }

        public static string GetPOSMC_3(string _filter, string _strCM_SKU)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         POSMC.COMPANY
                        ,POSMC.CREATE_DATE
                        ,POSMC.MODIFIER
                        ,POSMC.MODI_DATE
                        ,POSMC.MC001
                        ,POSMC.MC002
                        ,POSMC.MC003
                        ,POSMC.MC004
                        ,POSMC.MC005
                        ,POSMC.MC006
                        ,POSMC.MC007
                        ,POSMC.MC009
                        ,POSMC.MC011
                        ,POSMC.MC019
                        ,POSMB.MB004
                        ,POSMB.MB012
                        ,POSMB.MB013 
                        FROM POSMC 
                        INNER JOIN POSMB ON POSMC.COMPANY = POSMB.COMPANY AND POSMC.MC001 = POSMB.MB001 AND POSMC.MC002 = POSMB.MB002 AND POSMC.MC003 = POSMB.MB003 
                        WHERE {0} 
                        AND POSMB.MB013 >= '{1}' 
                        AND POSMB.MB008='Y'
                        AND POSMC.MC004='{2}'
                        ORDER BY MC003 ");

            sb = sb.Replace("{0}", _filter)
                   .Replace("{1}", DateTime.Today.ToString("yyyyMMdd"))
                   .Replace("{2}", _strCM_SKU);

            return sb.ToString();
        }

        public static string GetPOSMJ_1(string _filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         POSMJ.COMPANY
                        ,POSMJ.CREATE_DATE
                        ,POSMJ.MODIFIER
                        ,POSMJ.MODI_DATE
                        ,POSMJ.MJ001
                        ,POSMJ.MJ002
                        ,POSMJ.MJ003
                        ,POSMJ.MJ004
                        ,POSMJ.MJ005
                        ,POSMJ.MJ006
                        ,POSMJ.MJ009
                        ,POSMJ.MJ011
                        ,POSMI.MI004
                        ,POSMI.MI005
                        ,POSMI.MI006 
                        ,POSMF.MF004
                        FROM POSMJ 
                        INNER JOIN POSMI ON POSMJ.COMPANY = POSMI.COMPANY AND POSMJ.MJ001 = POSMI.MI001 AND POSMJ.MJ002 = POSMI.MI002 AND POSMJ.MJ003 = POSMI.MI003 
                        INNER JOIN  POSMF ON POSMI.MI003 = POSMF.MF003
                        WHERE ({0}) AND POSMI.MI006 >='{1}'  
                        ORDER BY POSMJ.MJ003");

            sb = sb.Replace("{0}", _filter)
                   .Replace("{1}", DateTime.Today.ToString("yyyyMMdd"));

            return sb.ToString();
        }

        public static string GetPOSMJ_2(string _filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         POSMJ.COMPANY
                        ,POSMJ.CREATE_DATE
                        ,POSMJ.MODIFIER
                        ,POSMJ.MODI_DATE
                        ,POSMJ.MJ001
                        ,POSMJ.MJ002
                        ,POSMJ.MJ003
                        ,POSMJ.MJ004
                        ,POSMJ.MJ005
                        ,POSMJ.MJ006
                        ,POSMJ.MJ009
                        ,POSMJ.MJ011
                        ,POSMI.MI004
                        ,POSMI.MI005
                        ,POSMI.MI006 
                        FROM POSMJ 
                        INNER JOIN POSMI ON POSMJ.COMPANY = POSMI.COMPANY AND POSMJ.MJ001 = POSMI.MI001 AND POSMJ.MJ002 = POSMI.MI002 AND POSMJ.MJ003 = POSMI.MI003 
                        WHERE ({0}) 
                        AND POSMI.MI006 >='{1}'
                        ORDER BY POSMJ.MJ003");

            sb = sb.Replace("{0}", _filter)
                   .Replace("{1}", DateTime.Today.ToString("yyyyMMdd"));

            return sb.ToString();
        }

        /// <summary>
        /// 以商品條碼,讀取鼎新商品貨號,截取鼎新品號,商品敘述,單位,單價成本....
        /// <param name="_eanCode">BarCode號碼</param>
        /// </summary>
        /// <returns></returns>
        public static string GetDataSystemPrd(string _eanCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"Select TOP 1 
                         INVMB.MB001
                        ,INVMB.MB002
                        ,INVMB.MB003
                        ,INVMB.MB013
                        ,INVMB.MB004
                        ,INVMB.MB039
                        ,INVMB.MB046
                        ,INVMB.MB049
                        ,INVMB.MB013
                        ,INVMB.MB005
                        ,INVMB.MB051
                        ,INVMB.MB081
                        ,INVMB.MB118
                        ,INVMH.MH001
                        ,INVMH.MH002 
                        ,INVMB.MB031
                        ,INVMB.MB032
                        ,INVMB.MB194
                        ,PURMA.MA002 
                        ,PURMA.MA003 
                        ,PURMA.MA005
                        ,PURMA.MA008 
                        ,PURMA.MA010 
                        ,PURMA.MA013 
                        ,PURMA.MA014 
                        ,PURMA.MA025 
                        ,PURMA.MA030
                        ,PURMA.MA044
                        ,PURMA.MA055 
                        ,INVMB.MB194 
                        ,INVMB.MB115 
                        FROM INVMB INVMB 
                        LEFT JOIN INVMH ON INVMB.MB001 = INVMH.MH001  
                        LEFT OUTER JOIN PURMA ON INVMB.MB032 = PURMA.MA001 
                        WHERE (INVMB.MB003 = '{0}' OR INVMB.MB001='{1}'  OR INVMH.MH002 = '{0}')");

            sb = sb.Replace("{0}", _eanCode)
                   .Replace("{1}", _eanCode.Trim());
            
            return sb.ToString();
        }

    }
}
