﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC05_1 : SQLStatementCreator
    {
        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '調撥數量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.RESWK = WERKS AND A.EAN11 = EAN11) AS '品名'
						,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '調入門市'
						,COALESCE(B.UMMENGE, 0) AS '前週銷量'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.RESWK = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調出門市庫存量'
						,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '調入門市庫存量'
                        FROM STO2STO A
						LEFT JOIN SALES B ON A.RESWK = B.WERKS AND A.MATNR = B.MATNR AND B.SPBUP = {5}
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}' AND (A.DELFLG IS NULL OR A.DELFLG != 'X')
                        ORDER BY A.EAN11");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{5}", _spbup);

            return sb.ToString();
        }
    }
}
