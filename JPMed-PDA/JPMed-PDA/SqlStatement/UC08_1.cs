﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC08_1 : SQLStatementCreator
    {
        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT DISTINCT
                         CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS '退貨確認'
                        ,(SELECT TOP 1 NAME1 FROM VENDOR WHERE A.LIFNR = LIFNR) AS '退貨至'
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '退貨量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
                        FROM RE2VENDOR A
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND ASSORT.WERKS = A.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}' AND (A.DELFLG IS NULL OR A.DELFLG != 'X')
                        ORDER BY 2,3");

            sb = sb.Replace("{0}", _bednr);

            return sb.ToString();
        }
    }
}
