﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class LABELS : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetRe2vctInfo(string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT 
                         LIFNR AS Code
                        ,(SELECT TOP 1 NAME1 FROM VENDOR WHERE A.LIFNR = LIFNR) AS Name
                        ,LIFNR AS Value
                        FROM RE2VCT A 
                        WHERE WERKS = '{0}'
                        ORDER BY A.LIFNR");
            sb = sb.Replace("{0}", _werks);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        /// <summary>
        /// 查詢銷貨主檔
        /// </summary>
        /// <param name="_werks"></param>
        /// <param name="_ean11"></param>
        /// <returns></returns>
        public static string GetProduct(string _werks, string _ean11)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT A.MATNR,A.MEINS,A.MAKTX,A.EAN11,A.MATKL,B.KSCHL2
                        FROM ASSORT A
                        LEFT JOIN MCGRP B ON A.MATKL=B.CLASS3
                        WHERE A.WERKS = '{0}' AND (EAN11 = '{1}' OR EAN12 = '{1}')
                        AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        "); //20160919需求變更:抓EAN11 or EAN12

            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _ean11);
            return sb.ToString();
        }

        //Modify By Milk
        /// <summary>
        /// 查詢補標主檔
        /// </summary>
        /// <param name="_matnr"></param>
        /// <param name="_ean11"></param>
        /// <param name="_werks"></param>
        /// <returns></returns>
        public static string GetCheckCommand(string _matnr, string _ean11, string _werks, string _pdaNum)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT *,CONVERT(INT,MENGE_M) MENGE FROM LABELS 
                        WHERE MATNR = '{0}' AND EAN11 = '{1}' AND WERKS = '{2}' AND PROCD <> 'X' AND PDANUM = '{3}' ");
            sb = sb.Replace("{0}", _matnr)
                   .Replace("{1}", _ean11)
                   .Replace("{2}", _werks)
                   .Replace("{3}", _pdaNum);
            return sb.ToString();
        }

        //Modify By Milk
        /// <summary>
        /// 新增補標主檔
        /// </summary>
        /// <param name="_bednr"></param>
        /// <param name="_crdat"></param>
        /// <param name="_werks"></param>
        /// <param name="_ean11"></param>
        /// <param name="_matnr"></param>
        /// <param name="_maktx"></param>
        /// <param name="_menge_m"></param>
        /// <param name="_matkl"></param>
        /// <param name="_chdat"></param>
        /// <param name="_chtme"></param>
        /// <returns></returns>
        public static string GetInsertCommand(string _bednr, string _crdat, string _werks, string _ean11, string _matnr, string _maktx, string _menge_m, string _matkl, string _chdat, string _chtme, string _padNum)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"INSERT INTO LABELS (BEDNR, CRDAT, WERKS, EAN11, MATNR, MAKTX, MENGE_M, MATKL, PROCD, CHDAT, CHTME, PDANUM) VALUES (
                                               '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '', '{8}', '{9}', '{10}' )");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _crdat)
                   .Replace("{2}", _werks)
                   .Replace("{3}", _ean11)
                   .Replace("{4}", _matnr)
                   .Replace("{5}", _maktx)
                   .Replace("{6}", _menge_m)
                   .Replace("{7}", _matkl)
                   .Replace("{8}", _chdat)
                   .Replace("{9}", _chtme)
                   .Replace("{10}", _padNum);

            return sb.ToString();
        }

        //Modify By Milk
        /// <summary>
        /// 更新補標主檔
        /// </summary>
        /// <param name="_bednr"></param>
        /// <param name="_matnr"></param>
        /// <param name="_ean11"></param>
        /// <param name="_werks"></param>
        /// <param name="_menge_m"></param>
        /// <param name="_chdat"></param>
        /// <param name="_chtme"></param>
        /// <returns></returns>
        public static string GetUpdateCommand(string _bednr, string _matnr, string _ean11, string _werks, string _menge_m, string _chdat, string _chtme, string _pdaNum)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE LABELS SET MENGE_M = '{4}' ,CHDAT = '{5}' ,CHTME = '{6}'  WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND WERKS = '{3}' AND PDANUM = '{7}' ");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _werks)
                   .Replace("{4}", _menge_m)
                   .Replace("{5}", _chdat)
                   .Replace("{6}", _chtme)
                   .Replace("{7}", _pdaNum);

            return sb.ToString();
        }

        //Modify By Milk
        /// <summary>
        /// 查詢補標主檔資料
        /// </summary>
        /// <param name="_werks"></param>
        /// <returns></returns>
        public static string GetSelectCommand(string _werks, string _pdaNum)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT TOP 20 EAN11 國際貨品代碼, MAKTX 物料說明,CONVERT(INT,MENGE_M) 印製量,MATNR 物料號碼 
                        FROM LABELS WHERE WERKS='{0}' AND PROCD <> 'X' AND PDANUM = '{1}' 
                        ORDER BY CHDAT DESC,CHTME DESC ");

            sb = sb.Replace("{0}", _werks)
                .Replace("{1}", _pdaNum);

            return sb.ToString();
        }

        /// <summary>
        /// 20160919需求變更:需檢查退貨之商品，是否存在於INFORED(主次供對應表)
        /// </summary>
        /// <param name="_lifnr"></param>
        /// <param name="_matnr"></param>
        /// <returns></returns>
        public static string CheckHasInfored(string _lifnr, string _matnr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT TOP 1 
                        COALESCE(LOEKZ, '') AS LOEKZ
                        FROM INFORED
                        WHERE LIFNR = '{0}'
                        AND MATNR = '{1}'");

            sb = sb.Replace("{0}", _lifnr)
                   .Replace("{1}", _matnr);

            return sb.ToString();
        }

        /// <summary>
        /// 取得最大流水號
        /// </summary>
        /// <param name="_bednr"></param>
        /// <param name="_crdat"></param>
        /// <param name="_werks"></param>
        /// <param name="_ean11"></param>
        /// <param name="_matnr"></param>
        /// <returns></returns>
        public static string GetMaxBEDNR(string _bednr, string _crdat, string _werks, string _ean11, string _matnr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT * FROM LABELS 
                        WHERE BEDNR LIKE '{0}%' AND CRDAT='{1}' AND WERKS='{2} AND EAN11='{3}' AND MATNR='{4}'
                        ORDER BY BEDNR DESC");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _crdat)
                   .Replace("{2}", _werks)
                   .Replace("{3}", _ean11)
                   .Replace("{4}", _matnr);

            return sb.ToString();
        }
    }
}
