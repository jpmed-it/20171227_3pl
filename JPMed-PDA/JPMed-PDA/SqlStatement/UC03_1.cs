﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC03_1 : SQLStatementCreator
    {
        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
//            sb.Append(@"SELECT
//                         A.EAN11 AS '商品條碼(EAN)'
//                        ,COALESCE(MENGE_B, 0) AS '現補量'
//                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
//                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
//						,COALESCE(B.UMMENGE, 0) AS '前週銷量'
//                        ,(SELECT CASE WHEN (BWSCL = '4' OR BWSCL IS NULL OR BWSCL = ' ') THEN 'V' ELSE '' END FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND A.MATNR = MATNR) AS '進倉貨'
//                        FROM REPLEN A
//						LEFT JOIN SALES B ON A.WERKS = B.WERKS AND A.MATNR = B.MATNR AND A.EAN11 = B.EAN11 AND A.WERKS = B.WERKS AND B.SPBUP = {5}
//                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
//                        WHERE A.BEDNR IN ({0})
//                        AND (A.DELFLG != 'X' OR A.DELFLG IS NULL)
//                        ORDER BY (A.CHDAT + A.CHTME) DESC");

            sb.Append(@"SELECT
                         A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_B, 0) AS '現補量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
						,COALESCE(B.UMMENGE, 0) AS '前週銷量'
                        ,(SELECT CASE WHEN (BWSCL = '4' OR BWSCL IS NULL OR BWSCL = ' ') THEN 'V' ELSE '' END FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND A.MATNR = MATNR) AS '進倉貨'
                        FROM REPLEN A
						LEFT JOIN SALES B ON A.WERKS = B.WERKS AND A.MATNR = B.MATNR AND A.EAN11 = B.EAN11 AND A.WERKS = B.WERKS AND B.SPBUP = {5}
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR IN ({0})
                        AND (A.DELFLG != 'X' OR A.DELFLG IS NULL)
                        ORDER BY (A.CHDAT + A.CHTME) DESC");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{5}", _spbup);

            return sb.ToString();
        }
    }
}
