﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC07_1 : SQLStatementCreator
    {
        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         CASE WHEN COMARK = 'X' THEN 'V' ELSE '' END AS '退貨確認'
                        ,(SELECT TOP 1 NAME1 FROM STORE WHERE A.WERKS = WERKS) AS '退貨至'
                        ,A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_R, 0) AS '退貨量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.RESWK = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.RESWK = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
                        ,COALESCE((SELECT TOP 1 BEZEI FROM REASON WHERE A.BSGRU = BSGRU),'') AS '備註'
                        FROM RE2TDC1 A
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND ASSORT.WERKS = A.RESWK AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}' AND (A.DELFLG IS NULL OR A.DELFLG != 'X')
                        ORDER BY A.EAN11 ");

            sb = sb.Replace("{0}", _bednr);

            return sb.ToString();
        }
    }
}
