﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC04 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string CheckSpecialIn(string _werksCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT CONVERT(date, REPLSD) AS 'REPLSD', CONVERT(date, REPLDD) AS 'REPLDD'
                        FROM GMVECT
                        WHERE (DFLAG != 'X' OR DFLAG IS NULL)
                        AND WERKS = '{0}'
                        AND CONVERT(date, GETDATE()) BETWEEN REPLSD AND REPLDD
                        ORDER BY REPLDD DESC");

            sb = sb.Replace("{0}", _werksCode);
            return sb.ToString();
        }

        public static string CheckSpecialIn2(string _ebenl)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT COUNT(*)
                        FROM GMVECT1
                        WHERE (DFLAG != 'X' OR DFLAG IS NULL)
                        AND EBELN = '{0}'");

            sb = sb.Replace("{0}", _ebenl);
            return sb.ToString();
        }

        public static string GetGmovehInfo(string _keyValue)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT BSART, EBELN, BEDNR FROM GMOVEH WHERE EBELN = '{0}' OR BEDNR = '{0}'");
            sb = sb.Replace("{0}", _keyValue);

            return sb.ToString();
        }

        public static string GetGMOVETStatus(string _WERKS, string _BWART, string _EBELN)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT WERKS, MATNR, EBELN, VBELN, MENGE_M "
                    + "FROM GMOVET"
                    + "WHERE WERKS = {WERKS} AND BWART = {BWART} AND EBELN = {EBELN}");

            sb = sb.Replace("{WERKS}", _WERKS);
            sb.Replace("{BWART}", _BWART);
            sb.Replace("{EBELN}", _EBELN);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetSNInfo(string _bsart, string _ebeln, string _bednr, string _prdFilter)
        {
            StringBuilder sb = new StringBuilder();

            switch (_bsart.ToUpper())
            {
                case "ZRP1":
                case "ZRP3":
                case "ZRP5":
//                    sb.Append(@"SELECT *, 
//                                CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
//                                     WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
//                                     WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
//                                     ELSE 4 END AS RANK
//                                FROM(
//                                SELECT
//                                 MAX(FNDATA.BSART) AS BSART
//                                ,MAX(FNDATA.EBELN) AS EBELN
//                                ,MAX(FNDATA.BEDNR) AS BEDNR
//                                ,MAX(FNDATA.EBELP) AS EBELP
//                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
//                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
//                                ,MAX(FNDATA.[商品條碼(EAN12)]) AS '商品條碼(EAN12)'
//                                ,MAX(FNDATA.[品名]) AS '品名'
//                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
//                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
//                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
//                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
//                                ,MAX(FNDATA.[備註]) AS '備註'
//                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
//                                ,FNDATA.VBELN
//                                ,CASE WHEN SUM(CASE WHEN FNDATA.BWART = 'AB' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS CANDO
//                                FROM(
//                                SELECT
//                                 '{0}' AS BSART
//                                ,'{1}' AS EBELN
//                                ,A.BEDNR AS BEDNR
//                                ,A.EBELP AS EBELP
//                                ,A.MATNR AS '物料號碼'
//                                ,A.EAN11 AS '商品條碼(EAN)'
//                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '商品條碼(EAN12)'
//                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
//                                ,A.WERKS AS '收貨門市'
//                                ,A.RESWK AS '發貨門市'
//                                ,A.MENGE_P AS '採購單數量'
//                                ,A.MEINS_P AS '採購單計量單位'
//                                ,'' AS '備註'
//                                ,CONVERT(date, BADAT) AS '需求日期'
//                                ,B.VBELN
//                                ,B.BWART
//                                FROM REPLEN A 
//                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP  AND A.EAN11 = B.EAN11
//								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
//                                WHERE A.BSART_P = '{0}' 
//                                AND A.EBELN = '{1}' 
//                                AND A.BEDNR = '{2}' 
//                                AND {3} 
//                                AND B.BSART = '{0}'
//                                AND B.EBELN = '{1}'
//								AND B.BWART = 'AB'
//                                AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)) FNDATA
//                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
//	                            INNER JOIN
//	                            (
//                                    SELECT DataOut.*, ISNULL(MENGE_M_RESWK,0) as '實際量', ISNULL( DataIn.CANEDIT, 1) as CANEDIT FROM
//		                            (
//			                            SELECT MATNR, VBELN, MENGE_M as '應到數量'
//			                            FROM GMOVET
//			                            WHERE BWART = '641' AND EBELN = '{1}' AND BSART = '{0}'
//		                            ) as DataOut
//		                            LEFT JOIN
//		                            (
//			                            SELECT MATNR, VBELN, MENGE_M as MENGE_M_RESWK
//			                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
//			                            FROM GMOVET
//			                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
//		                            ) as DataIn ON DataOut.MATNR = DataIn.MATNR AND DataIn.VBELN = DataOut.VBELN
//	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
//                                ORDER BY RANK, EBELP");

                    sb.Append(@"SELECT *, 
                                CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
                                     WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
                                     ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 MAX(FNDATA.BSART) AS BSART
                                ,MAX(FNDATA.EBELN) AS EBELN
                                ,MAX(FNDATA.BEDNR) AS BEDNR
                                ,MAX(FNDATA.EBELP) AS EBELP
                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
                                ,MAX(FNDATA.[商品條碼(EAN12)]) AS '商品條碼(EAN12)'
                                ,MAX(FNDATA.[品名]) AS '品名'
                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
                                ,MAX(FNDATA.[備註]) AS '備註'
                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
                                ,FNDATA.VBELN
                                ,CASE WHEN SUM(CASE WHEN FNDATA.BWART = 'AB' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS CANDO
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,B.VBELN
                                ,B.BWART
                                FROM REPLEN A 
                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP  AND A.EAN11 = B.EAN11
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART_P = '{0}' 
                                AND A.EBELN = '{1}' 
                                AND A.BEDNR = '{2}' 
                                AND {3} 
                                AND B.BSART = '{0}'
                                AND B.EBELN = '{1}'
								AND B.BWART = 'AB'
                                AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)) FNDATA
                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
	                            INNER JOIN
	                            (
                                    SELECT DataOut.*, ISNULL(MENGE_M_RESWK,0) as '實際量', ISNULL( DataIn.CANEDIT, 1) as CANEDIT FROM
		                            (
			                            SELECT MATNR, VBELN, MENGE_M as '應到數量'
			                            FROM GMOVET
			                            WHERE BWART = '641' AND EBELN = '{1}' AND BSART = '{0}'
		                            ) as DataOut
		                            LEFT JOIN
		                            (
			                            SELECT MATNR, VBELN, MENGE_M as MENGE_M_RESWK
			                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
			                            FROM GMOVET
			                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
		                            ) as DataIn ON DataOut.MATNR = DataIn.MATNR AND DataIn.VBELN = DataOut.VBELN
	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
                                ORDER BY RANK, EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                #region ZUB4
                case "ZUB4":
                    sb.Append(@"SELECT DATA.*, ISNULL(ALLDATA.CANEDIT,1) as CANEDIT,
                                ISNULL(ALLDATA.實際量,0) as '實際量',
                                CASE WHEN ISNULL(ALLDATA.實際量,0) = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.實際量 != 應到數量 THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
                                     WHEN ALLDATA.實際量 = 應到數量 THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
                                     ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                MAX(FNDATA.BSART) AS BSART
                                ,MAX(FNDATA.EBELN) AS EBELN
                                ,MAX(FNDATA.BEDNR) AS BEDNR
                                ,MAX(FNDATA.EBELP) AS EBELP
                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
                                ,MAX(FNDATA.[商品條碼(EAN12)]) AS '商品條碼(EAN12)'
                                ,MAX(FNDATA.[品名]) AS '品名'
                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
                                ,MAX(FNDATA.[採購單數量]) AS '應到數量'
                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
                                ,MAX(FNDATA.[備註]) AS '備註'
                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
                                ,FNDATA.VBELN
                                ,1 AS CANDO
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,A.EBELN as VBELN
                                ,B.BWART
                                FROM REPLEN A 
                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP AND A.EAN11 = B.EAN11 
                                    AND B.BSART = '{0}' AND B.EBELN = '{1}' AND B.BWART = 'AB'
                                    AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART_P = '{0}' 
                                AND A.EBELN = '{1}' 
                                AND A.BEDNR = '{2}' 
                                AND {3} ) FNDATA
                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
	                            LEFT JOIN
	                            (
		                            SELECT MATNR, VBELN, MENGE_M as '實際量'
		                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
		                            FROM GMOVET
		                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
                                ORDER BY RANK, EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();
                #endregion

                case "ZRP2":
                case "ZRP4":
                case "ZFG1":
                    sb.Append(@"SELECT DISTINCT *, 
                               CASE WHEN ALLDATA.[實際量] = 0 THEN 1 --(ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                    WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_P, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,COALESCE((SELECT TOP 1 VBELN FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND (DELFLG != 'X' OR DELFLG IS NULL) AND WERKS = 'TDC2' AND A.EAN11 = EAN11 AND BWART = '641'), '{1}') AS VBELN
                                ,COALESCE((SELECT TOP 1 CASE WHEN CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (PROCD != 'X' OR PROCD IS NULL) THEN 1 ELSE 0 END FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 1) AS CANEDIT
                                ,1 AS CANDO
                                FROM REPLEN A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL) 
                                WHERE BSART_P = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZUB1":
                case "ZUB3":
                    sb.Append(@"SELECT *, 
                              CASE WHEN ALLDATA.[實際量] = 0 THEN 1 --(ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                   WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                               WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                               ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND BWART = '101' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                ,'{1}' AS VBELN
                                ,COALESCE((SELECT TOP 1 CASE WHEN CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (PROCD != 'X' OR PROCD IS NULL) THEN 1 ELSE 0 END FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND BWART ='101' AND A.EBELP = EBELP AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 1) AS CANEDIT
                                ,1 AS CANDO
                                FROM STO2STO A 
                                INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZUB2":
                    sb.Append(@"SELECT DISTINCT *, 
                               CASE WHEN ALLDATA.[實際量] = 0 THEN 1 --(ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                    WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                ,'{1}' AS VBELN
                                ,COALESCE((SELECT TOP 1 CASE WHEN CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (PROCD != 'X' OR PROCD IS NULL) THEN 1 ELSE 0 END FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 1) AS CANEDIT
                                ,1 AS CANDO
                                FROM STO2TDC1 A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZUR1":
                    sb.Append(@"SELECT DISTINCT *, 
                                CASE WHEN ALLDATA.[實際量] = 0 THEN 1 --(ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                 WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                 ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,COALESCE((SELECT TOP 1 BEZEI FROM REASON WHERE A.BSGRU = BSGRU), '') AS '備註'
                                ,'' AS '需求日期'
                                ,'{1}' AS VBELN
                                ,COALESCE((SELECT TOP 1 CASE WHEN CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (PROCD != 'X' OR PROCD IS NULL) THEN 1 ELSE 0 END FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 1) AS CANEDIT
                                ,1 AS CANDO
                                FROM RE2TDC1 A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL) 
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZRE3":
                    sb.Append(@"SELECT *, 
                                CASE WHEN ALLDATA.[實際量] = 0 THEN 1 --(ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                 WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                 ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,'' AS '收貨門市'
                                ,A.WERKS AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                ,'{1}' AS VBELN
                                ,COALESCE((SELECT TOP 1 CASE WHEN CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (PROCD != 'X' OR PROCD IS NULL) THEN 1 ELSE 0 END FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 1) AS CANEDIT
                                ,1 AS CANDO
                                FROM RE2VENDOR A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();
            }

            return string.Empty;
        }

        public static string GetInsertCommand(string _bsart, string _ebeln, string _ebelp, string _matnr, string _ean11, string _werks,
                                              string _reswk, string _empCode, string _menge_p, string meins_p, string _menge_m, string _vbeln)
        {
            StringBuilder sb = new StringBuilder();

            string deleteCmd_zrp1 = @"DELETE GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND EBELP = '{2}' AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND MATNR = '{3}' AND EAN11 = '{4}' AND PROCD != 'X' AND BWART = '101' AND VBELN = '{5}'; ";
            string deleteCmd_zub1 = @"DELETE GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND EBELP = '{2}' AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND MATNR = '{3}' AND EAN11 = '{4}' AND PROCD != 'X' AND BWART = '101';";
            string deleteCmd = @"DELETE GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND EBELP = '{2}' AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND MATNR = '{3}' AND EAN11 = '{4}' AND PROCD != 'X'; ";
            string insertCmd = @"INSERT INTO GMOVET (BSART, EBELN, EBELP, CRDAT, MATNR,
                                                     EAN11, WERKS, AFNAM, MENGE_P, MEINS_P,
                                                     BWART, MENGE_M, MEINS_M, DELFLG, PROCD,
                                                     CHUSN, CHDAT, CHTME, VBELN) VALUES (
                                                     '{0}', '{1}', '{2}', '{3}', '{4}',
                                                     '{5}', '{6}', '{7}', {8}, '{9}',
                                                     '{10}', {11}, '{12}', '{13}', ' ',
                                                     '{14}', '{15}', '{16}', '{17}'); ";



            switch (_bsart.ToUpper())
            {
                case "ZRP1":
                case "ZRP3":
                case "ZRP5":
                case "ZUB4":
                    sb.Append(string.Format(deleteCmd_zrp1, _bsart, _ebeln, _ebelp, _matnr, _ean11, _vbeln));
                    //只收
                    sb.Append(string.Format(insertCmd, _bsart, _ebeln, _ebelp, DateTime.Now.ToString("yyyyMMdd"), _matnr, _ean11, _werks, _empCode, _menge_p, meins_p, "101", _menge_m, meins_p, (Decimal.Parse(_menge_m) == 0 ? "X" : " "), _empCode, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), _vbeln));
                    break;

                case "ZRP2":
                case "ZRP4":
                case "ZFG1":
                    sb.Append(string.Format(deleteCmd, _bsart, _ebeln, _ebelp, _matnr, _ean11));
                    //只收
                    sb.Append(string.Format(insertCmd, _bsart, _ebeln, _ebelp, DateTime.Now.ToString("yyyyMMdd"), _matnr, _ean11, _werks, _empCode, _menge_p, meins_p, "101", _menge_m, meins_p, (Decimal.Parse(_menge_m) == 0 ? "X" : " "), _empCode, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), _vbeln));
                    break;

                case "ZUB1":
                case "ZUB2":
                case "ZUB3":
                case "ZUR1":
                    sb.Append(string.Format(deleteCmd_zub1, _bsart, _ebeln, _ebelp, _matnr, _ean11));

                    //改為3段式收貨，不需再做發
                    //先發在收
                    //sb.Append(string.Format(insertCmd, _bsart, _ebeln, _ebelp, DateTime.Now.ToString("yyyyMMdd"), _matnr, _ean11, _reswk, _empCode, _menge_p, meins_p, "641", _menge_m, meins_p, (Decimal.Parse(_menge_m) == 0 ? "X" : " "), _empCode, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), _vbeln));
                    sb.Append(string.Format(insertCmd, _bsart, _ebeln, _ebelp, DateTime.Now.ToString("yyyyMMdd"), _matnr, _ean11, _werks, _empCode, _menge_p, meins_p, "101", _menge_m, meins_p, (Decimal.Parse(_menge_m) == 0 ? "X" : " "), _empCode, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), _vbeln));

                    break;

                case "ZRE3":
                    sb.Append(string.Format(deleteCmd, _bsart, _ebeln, _ebelp, _matnr, _ean11));
                    //只發
                    sb.Append(string.Format(insertCmd, _bsart, _ebeln, _ebelp, DateTime.Now.ToString("yyyyMMdd"), _matnr, _ean11, _reswk, _empCode, _menge_p, meins_p, "161", _menge_m, meins_p, (Decimal.Parse(_menge_m) == 0 ? "X" : " "), _empCode, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), _vbeln));
                    break;
            }

            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetSNInfoByEan(string _bsart, string _ebeln, string _bednr, string _ean11, string _vbeln)
        {
            StringBuilder sb = new StringBuilder();

            //增加AB檢查判斷
            switch (_bsart.ToUpper())
            {
                case "ZRP1":
                case "ZRP3":
                case "ZRP5":
                    sb.Append(@"SELECT 
                                 DATA.[商品條碼(EAN)]
                                ,DATA.[品名]
                                ,ALLDATA.[應到數量]
                                ,ALLDATA.[實際量]
                                ,DATA.[備註]
                                ,CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
                                      WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
                                      ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 MAX(FNDATA.BSART) AS BSART
                                ,MAX(FNDATA.EBELN) AS EBELN
                                ,MAX(FNDATA.BEDNR) AS BEDNR
                                ,MAX(FNDATA.EBELP) AS EBELP
                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
                                ,MAX(FNDATA.[商品條碼(EAN12)]) AS '商品條碼(EAN12)'
                                ,MAX(FNDATA.[品名]) AS '品名'
                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
                                ,MAX(FNDATA.[備註]) AS '備註'
                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
                                ,FNDATA.VBELN
                                ,CASE WHEN SUM(CASE WHEN FNDATA.BWART = 'AB' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS CANDO
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,B.VBELN
                                ,B.BWART
                                FROM REPLEN A 
                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP  AND A.EAN11 = B.EAN11
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART_P = '{0}' 
                                AND A.EBELN = '{1}' 
                                AND A.BEDNR = '{2}' 
                                AND A.EAN11 = '{3}' 
                                AND B.BSART = '{0}'
                                AND B.EBELN = '{1}'
								AND B.BWART = 'AB' 
                                AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)
                                AND B.VBELN = '{4}') FNDATA
                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
                                INNER JOIN
	                            (
                                    SELECT DataOut.*, ISNULL(MENGE_M_RESWK,0) as '實際量', ISNULL( DataIn.CANEDIT, 1) as CANEDIT FROM
		                            (
			                            SELECT MATNR, VBELN, MENGE_M as '應到數量'
			                            FROM GMOVET
			                            WHERE BWART = '641' AND EBELN = '{1}' AND BSART = '{0}'
		                            ) as DataOut
		                            LEFT JOIN
		                            (
			                            SELECT MATNR, VBELN, MENGE_M as MENGE_M_RESWK
			                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
			                            FROM GMOVET
			                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
		                            ) as DataIn ON DataOut.MATNR = DataIn.MATNR AND DataIn.VBELN = DataOut.VBELN
	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
                                ORDER BY RANK, EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11)
                           .Replace("{4}", _vbeln);
                    return sb.ToString();

                #region ZUB4
                case "ZUB4":
                    sb.Append(@"SELECT DATA.*, ISNULL(ALLDATA.CANEDIT,1) as CANEDIT,
                                ISNULL(ALLDATA.實際量,0) as '實際量',
                                CASE WHEN ISNULL(ALLDATA.實際量,0) = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.實際量 != 應到數量 THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
                                     WHEN ALLDATA.實際量 = 應到數量 THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
                                     ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 MAX(FNDATA.BSART) AS BSART
                                ,MAX(FNDATA.EBELN) AS EBELN
                                ,MAX(FNDATA.BEDNR) AS BEDNR
                                ,MAX(FNDATA.EBELP) AS EBELP
                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
                                ,MAX(FNDATA.[商品條碼(EAN12)]) AS '商品條碼(EAN12)'
                                ,MAX(FNDATA.[品名]) AS '品名'
                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
								,MAX(FNDATA.[採購單數量]) AS '應到數量'
                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
                                ,MAX(FNDATA.[備註]) AS '備註'
                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
                                ,FNDATA.VBELN
                                ,1 AS CANDO
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,A.EBELN as VBELN
                                ,B.BWART
                                FROM REPLEN A 
                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP AND A.EAN11 = B.EAN11 
                                    AND B.BSART = '{0}' AND B.EBELN = '{1}' AND B.BWART = 'AB'
                                    AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART_P = '{0}' 
                                AND A.EBELN = '{1}' 
                                AND A.BEDNR = '{2}' 
                                AND A.EAN11 = '{3}'  ) FNDATA
                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
	                            LEFT JOIN
	                            (
		                            SELECT MATNR, VBELN, MENGE_M as '實際量'
		                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
		                            FROM GMOVET
		                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
                                ORDER BY RANK, EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11);
                    return sb.ToString();
                #endregion

                case "ZRP2":
                case "ZRP4":
                case "ZFG1":
                    sb.Append(@"SELECT 
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                ,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                  WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                  ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_P, 0) AS '應到數量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) < CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                FROM REPLEN A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL) 
                                WHERE BSART_P = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND A.EAN11 = '{3}') ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11);
                    return sb.ToString();

                case "ZUB1":
                case "ZUB3":
                    sb.Append(@"SELECT
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                ,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                 WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                 ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND BWART = '101' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) < CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM STO2STO A 
                                INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART = '{0}' AND A.EBELN = '{1}' AND A.BEDNR = '{2}' AND A.EAN11 = '{3}') ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11);
                    return sb.ToString();

                case "ZUB2":
                    sb.Append(@"SELECT  
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                ,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                 WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                 ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) < CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM STO2TDC1 A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART = '{0}' AND A.EBELN = '{1}' AND A.BEDNR = '{2}' AND A.EAN11 = '{3}') ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11);
                    return sb.ToString();

                case "ZUR1":
                    sb.Append(@"SELECT 
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                ,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                 WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                 ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) < CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,COALESCE((SELECT TOP 1 BEZEI FROM REASON WHERE A.BSGRU = BSGRU), '') AS '備註'
                                ,'' AS '需求日期'
                                FROM RE2TDC1 A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL) 
                                WHERE A.BSART = '{0}' AND A.EBELN = '{1}' AND A.BEDNR = '{2}' AND A.EAN11 = '{3}') ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11);
                    return sb.ToString();

                case "ZRE3":
                    sb.Append(@"SELECT 
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                ,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                 WHEN (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                 ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,'' AS '收貨門市'
                                ,A.WERKS AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) < CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM RE2VENDOR A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART = '{0}' AND A.EBELN = '{1}' AND A.BEDNR = '{2}' AND A.EAN11 = '{3}') ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _ean11);
                    return sb.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// 由VBELN取回原始單號
        /// </summary>
        /// <param name="_vbeln"></param>
        /// <returns></returns>
        public static string GetGmovetByVbeln(string _vbeln)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         B.BSART
                        ,B.EBELN
                        ,B.BEDNR
                        ,A.EAN11
                        FROM GMOVET A
                        INNER JOIN GMOVEH B ON B.BSART = A.BSART AND B.EBELN = A.EBELN
                        WHERE VBELN = '{0}'
                        AND BWART = '641'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)");

            sb = sb.Replace("{0}", _vbeln);
            return sb.ToString();
        }

        //20170314 增加報廢品判斷
        public static string GetProduct(string _werks, string _ean11)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         MATNR
                        ,MEINS
                        ,MAKTX
                        ,LABOR
                        ,RUECK
                        ,DELFG
                        ,EAN11
                        FROM ASSORT A
                        WHERE WERKS = '{0}' AND (EAN11 = '{1}' OR EAN12 = '{1}')
                        AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        "); //20160919需求變更:抓EAN11 or EAN12

            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _ean11);

            return sb.ToString();
        }

        /// <summary>
        /// 商品出貨檢查
        /// </summary>
        /// <returns></returns>
        public static string CheckProductShip(string _VBELN, string _BWART)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM GMOVET WHERE EBELN = '{0}' AND BWART ='{1}' AND DELFLG != 'X'");

            sb = sb.Replace("{0}", _VBELN);
            sb = sb.Replace("{1}", _BWART);
            return sb.ToString();
        }

        //使用VBELN檢查到貨店是否已收貨
        public static string CheckGMOVETShip(string _VBELN, string _BWART)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM GMOVET WHERE VBELN = '{VBELN}' AND BWART ='{BWART}' AND DELFLG != 'X'");
            sb = sb.Replace("{VBELN}", _VBELN);
            sb = sb.Replace("{BWART}", _BWART);

            return sb.ToString();
        }

        /// <summary>
        /// 依物流單號查詢調撥資料
        /// </summary>
        /// <param name="_Vbeln">物流單號</param>
        public static string GetSTO2STO(string _Vbeln)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM STO2STO WHERE EBELN = '{0}'");

            sb = sb.Replace("{0}", _Vbeln);
            return sb.ToString();
        }

        /// <summary>
        /// 依單號取得到貨店代號
        /// </summary>
        /// <param name="_Vbeln"></param>
        /// <returns></returns>
        public static string GetWERKSByVBELN(string _Vbeln)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT REPLEN.WERKS, REPLEN.RESWK FROM REPLEN " +
            " INNER JOIN GMOVET ON REPLEN.EBELN = GMOVET.EBELN AND GMOVET.EBELP = REPLEN.EBELP AND BWART = '641'"
            + " WHERE VBELN = '{VBELN}'");

            sb = sb.Replace("{VBELN}", _Vbeln);
            return sb.ToString();
        }
    }
}
