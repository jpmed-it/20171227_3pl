﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;
using JPMed_PDA.Entity;

namespace JPMed_PDA.SqlStatement
{
    /// <summary>
    /// 物流到貨作業 UC10
    /// </summary>
    public class UC10 : SQLStatementCreator
    {
        /// <summary>
        /// 取得門市名稱
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        /// <summary>
        /// 取得員工名稱
        /// </summary>
        /// <param name="_code"></param>
        /// <returns></returns>
        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        /// <summary>
        /// 依單號物流異動資料
        /// </summary>
        /// <param name="_Vbeln">單號></param>
        /// <returns></returns>
        public static string GetGMOVET(string _Vbeln, string _Bwart)
        {
            string sqlScript =
                string.Format("Select * From GMOVET WHERE VBELN = '{0}' AND BWART = '{1}' AND DELFLG ='' ", _Vbeln, _Bwart);
            return sqlScript;
        }

        /// <summary>
        /// 依單號物流異動資料
        /// </summary>
        /// <param name="_Ebeln">單號></param>
        /// <returns></returns>
        public static string GetGMOVETByEBELN(string _EBELN, string _Bwart)
        {
            string sqlScript =
                string.Format("Select * From GMOVET WHERE EBELN = '{0}' AND BWART = '{1}' AND DELFLG ='' ", _EBELN, _Bwart);
            return sqlScript;
        }

        /// <summary>
        /// 依單號取得門市對門市調撥資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetSTO2STOByBednrOrEbeln(string _Werks, string _Bednr)
        {
            string sqlScript = string.Format("SELECT * FROM STO2STO " +
            "WHERE WERKS = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _Bednr);

            return sqlScript;
        }

        /// <summary>
        /// 依單號取調撥至總倉調撥資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetSTO2TDC1ByBednrOrEbeln(string _Werks, string _Bednr)
        {
            string sqlScript = string.Format("SELECT * FROM STO2TDC1 " +
            "WHERE RESWK = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _Bednr);

            return sqlScript;
        }

        /// <summary>
        /// 依調撥至總倉調撥資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetRE2TDC1ByBednrOrEbeln(string _Werks, string _Bednr)
        {
            string sqlScript = string.Format("SELECT * FROM RE2TDC1 " +
            "WHERE RESWK = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _Bednr);

            return sqlScript;
        }

        /// <summary>
        /// 查詢補貨資料
        /// </summary>
        /// <param name="_Werks"></param>
        /// <param name="_Bednr"></param>
        public static string GetREPLENByBednrOrEbeln(string _Werks, string _BEDNR)
        {
            string sqlScript = string.Format("SELECT *, MENGE_P as MENGE_M, EBELN as VBELN, BSART_P as BSART FROM REPLEN "
            + "WHERE WERKS = '{0}' AND ( BEDNR = '{1}' OR EBELN = '{1}') AND DELFLG ='' ", _Werks, _BEDNR);

            return sqlScript;
        }

        /// <summary>
        /// 依BEDNR或EBELN 取得物料異動單號或物流單號
        /// </summary>
        /// <param name="_keyValue"></param>
        /// <returns></returns>
        public static string GetGmovehInfo(string _keyValue)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT BSART, EBELN, BEDNR FROM GMOVEH WHERE EBELN = '{0}' OR BEDNR = '{0}'");
            sb = sb.Replace("{0}", _keyValue);

            return sb.ToString();
        }

        /// <summary>
        /// 新增物料異動資料
        /// </summary>
        /// <param name="_bsart">單型</param>
        /// <param name="_ebeln">單號</param>
        /// <param name="_ebelp">項目號碼</param>
        /// <param name="_matnr">物料號碼</param>
        /// <param name="_ean11">國際貨品代碼</param>
        /// <param name="_werks">收貨店代號</param>
        /// <param name="_reswk">發貨店代號</param>
        /// <param name="_empCode">申請人代號</param>
        /// <param name="_menge_p">數量</param>
        /// <param name="meins_p">計量單位</param>
        /// <param name="_menge_m">異動數量</param>
        /// <param name="_vbeln">物流單號</param>
        /// <returns></returns>
        public static string InsertGMOVET(List<Gmovet> GmovetList, string empCode, string _CRDAT, string _CHTME)
        {
            StringBuilder sb = new StringBuilder();

            //TODO: 是否刪除原本未完成的單?
            //            string deleteCmd = @"DELETE GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND EBELP = '{2}' 
            //AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND MATNR = '{3}' AND EAN11 = '{4}' AND PROCD != 'X'; ";
            //            sb.Append(
            //                string.Format(deleteCmd, GmovetList[0].BSART, GmovetList[0].EBELN,
            //                GmovetList[0].EBELP, GmovetList[0].MATNR, GmovetList[0].EAN11));

            string insertCmdHeader = @"INSERT INTO GMOVET (BSART, EBELN, EBELP, CRDAT, MATNR,
                                                     EAN11, WERKS, AFNAM, MENGE_P, MEINS_P,
                                                     BWART, MENGE_M, MEINS_M, DELFLG, PROCD,
                                                     CHUSN, CHDAT, CHTME, VBELN) VALUES";
            sb.Append(insertCmdHeader);

            string insertCmd = @" ('{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', {8}, '{9}',
                                    '{10}', {11}, '{12}', '{13}', ' ','{14}', '{15}', '{16}', '{17}')";

            foreach (var item in GmovetList)
            {
                if (GmovetList.IndexOf(item) > 0)
                {
                    sb.Append(", ");
                }

                sb.Append(string.Format(insertCmd,
                    item.BSART,
                    item.EBELN,
                    item.EBELP,
                    _CRDAT,
                    item.MATNR,
                    item.EAN11,
                    item.WERKS,
                    empCode,
                    item.MENGE_P,
                    item.MEINS,
                    item.BWART,
                    item.MENGE_M,
                    item.MEINS,
                    (Decimal.Parse(item.MENGE_M) == 0 ? "X" : " "),
                    empCode,
                    _CRDAT,
                    _CHTME,
                    item.VBELN));


            }

            sb.Append(";");

            return sb.ToString();
        }

        /// <summary>
        /// 由VBELN取回原始單號
        /// </summary>
        /// <param name="_vbeln"></param>
        /// <returns></returns>
        public static string GetGmovetByVbeln(string _vbeln)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         B.BSART
                        ,B.EBELN
                        ,B.BEDNR
                        ,A.EAN11
                        FROM GMOVET A
                        INNER JOIN GMOVEH B ON B.BSART = A.BSART AND B.EBELN = A.EBELN
                        WHERE VBELN = '{0}'
                        AND BWART = '641'
                        AND (DELFLG != 'X' OR DELFLG IS NULL)");

            sb = sb.Replace("{0}", _vbeln);
            return sb.ToString();
        }
    }
}
