﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC03 : SQLStatementCreator
    {
        public static string GetStoreNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME1 FROM STORE WHERE WERKS = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string GetEmployeeNameByCode(string _code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT NAME FROM ENMAS WHERE EMPNO = '{0}'");
            sb = sb.Replace("{0}", _code);
            return sb.ToString();
        }

        public static string CheckSpecialOpen(string _werksCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT CONVERT(date, REPLSD) AS 'REPLSD', CONVERT(date, REPLDD) AS 'REPLDD'
                        FROM REPLENCT
                        WHERE (DFLAG != 'X' OR DFLAG IS NULL)
                        AND WERKS = '{0}'
                        AND CONVERT(date, GETDATE()) BETWEEN REPLSD AND REPLDD
                        ORDER BY REPLDD DESC");
            sb = sb.Replace("{0}", _werksCode);
            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetProduct(string _werks, string _ean11, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                         A.MATNR
                        ,A.MEINS
                        ,A.MAKTX
                        ,A.BWSCL
                        ,A.DELFG
                        ,A.TMOUT
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS 'LABST'
                        ,COALESCE(B.UMMENGE, 0) AS 'UMMENGE'
                        ,A.EAN11 AS EAN11
                        FROM ASSORT A
                        LEFT JOIN SALES B ON A.WERKS = B.WERKS AND A.MATNR = B.MATNR AND A.EAN11 = B.EAN11 AND B.SPBUP = {2}
                        WHERE A.WERKS = '{0}' AND (A.EAN11 = '{1}' OR A.EAN12 = '{1}')
                        AND (A.TMOUT = '' OR A.TMOUT IS NULL)
                        "); //20160919需求變更:抓EAN11 or EAN12

            sb = sb.Replace("{0}", _werks)
                   .Replace("{1}", _ean11)
                   .Replace("{2}", _spbup);
            return sb.ToString();
        }

        public static string GetCheckCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT PROCD FROM REPLEN WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND RESWK = '{3}' AND WERKS = '{4}'");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks);
            return sb.ToString();
        }

        public static string GetInsertCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks, string _empCode, string _menge_b, string _meins, string _badat, string _bsart_b)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"INSERT INTO REPLEN (BEDNR, MATNR, EAN11, RESWK, WERKS, LGORT, CRDAT, CRTME, AFNAM, BSART_B, MENGE_B, MEINS_B, DELFLG, CHUSN, CHDAT, CHTME, MENGE_P, MEINS_P, BADAT) VALUES (
                                               '{0}', '{1}', '{2}', '{3}', '{4}', '0001', '{5}', '{6}', '{7}', '{13}', {8}, '{9}', ' ', '{10}', '{11}', '{12}', {8}, '{9}', '{14}')");
            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks)
                   .Replace("{5}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{6}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{7}", _empCode)
                   .Replace("{8}", _menge_b)
                   .Replace("{9}", _meins)
                   .Replace("{10}", _empCode)
                   .Replace("{11}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{12}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{13}", _bsart_b)
                   .Replace("{14}", _badat);

            return sb.ToString();
        }

        public static string GetUpdateCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks, string _empCode, string _menge_b, string _bsart_b)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"UPDATE REPLEN SET DELFLG = ' ', MENGE_B = {5}, MENGE_P = {5}, CHUSN = '{6}', CHDAT = '{7}', CHTME = '{8}', BSART_B = '{9}' WHERE BEDNR = '{0}' AND MATNR = '{1}' AND EAN11 = '{2}' AND RESWK = '{3}' AND WERKS = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks)
                   .Replace("{5}", _menge_b)
                   .Replace("{6}", _empCode)
                   .Replace("{7}", DateTime.Now.ToString("yyyyMMdd"))
                   .Replace("{8}", DateTime.Now.ToString("HHmmss"))
                   .Replace("{9}", _bsart_b);

            return sb.ToString();
        }

        //20170225 增加報廢品判斷
        public static string GetSelectCommand(string _bednr, string _matnr, string _ean11, string _reswk, string _werks, string _spbup)
        {
            StringBuilder sb = new StringBuilder();
//            sb.Append(@"SELECT
//                         A.EAN11 AS '商品條碼(EAN)'
//                        ,COALESCE(MENGE_B, 0) AS '現補量'
//                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
//                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
//						,COALESCE(B.UMMENGE, 0) AS '前週銷量'
//                        ,(SELECT CASE WHEN (BWSCL = '4' OR BWSCL IS NULL OR BWSCL = ' ') THEN 'V' ELSE '' END FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND A.MATNR = MATNR) AS '進倉貨'
//                        FROM REPLEN A
//						LEFT JOIN SALES B ON A.WERKS = B.WERKS AND A.MATNR = B.MATNR AND A.WERKS = B.WERKS AND B.SPBUP = {5}
//                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
//                        WHERE A.BEDNR = '{0}'
//                        AND A.MATNR = '{1}'
//                        AND A.EAN11 = '{2}'
//                        AND A.RESWK = '{3}'
//                        AND A.WERKS = '{4}'");

            sb.Append(@"SELECT
                         A.EAN11 AS '商品條碼(EAN)'
                        ,COALESCE(MENGE_B, 0) AS '現補量'
                        ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                        ,COALESCE((SELECT TOP 1 LABST FROM INVMC WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND LGORT = '0001'), 0) AS '庫存量'
						,COALESCE(B.UMMENGE, 0) AS '前週銷量'
                        ,(SELECT CASE WHEN (BWSCL = '4' OR BWSCL IS NULL OR BWSCL = ' ') THEN 'V' ELSE '' END FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11 AND A.MATNR = MATNR) AS '進倉貨'
                        FROM REPLEN A
						LEFT JOIN SALES B ON A.WERKS = B.WERKS AND A.MATNR = B.MATNR AND A.WERKS = B.WERKS AND B.SPBUP = {5}
                        INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                        WHERE A.BEDNR = '{0}'
                        AND A.MATNR = '{1}'
                        AND A.EAN11 = '{2}'
                        AND A.RESWK = '{3}'
                        AND A.WERKS = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks)
                   .Replace("{5}", _spbup);

            return sb.ToString();
        }

        public static string GetPrdInQty(string _bednr, string _matnr, string _ean11, string _reswk, string _werks)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT
                        COALESCE(MENGE_B, 0) AS '現補量'
                        FROM REPLEN A
                        WHERE A.BEDNR = '{0}'
                        AND A.MATNR = '{1}'
                        AND A.EAN11 = '{2}'
                        AND A.RESWK = '{3}'
                        AND A.WERKS = '{4}'");

            sb = sb.Replace("{0}", _bednr)
                   .Replace("{1}", _matnr)
                   .Replace("{2}", _ean11)
                   .Replace("{3}", _reswk)
                   .Replace("{4}", _werks);

            return sb.ToString();
        }
    }
}
