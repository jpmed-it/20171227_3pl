﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SqlStatement
{
    public class UC04_1 : SQLStatementCreator
    {
        //20170225 增加報廢品判斷
        public static string GetSNInfo(string _bsart, string _ebeln, string _bednr, string _prdFilter)
        {
            StringBuilder sb = new StringBuilder();

            switch (_bsart.ToUpper())
            {
                case "ZRP1":
                case "ZRP3":
                case "ZRP5":
                    sb.Append(@"SELECT *, 
                                CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
                                     WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
                                     ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 MAX(FNDATA.BSART) AS BSART
                                ,MAX(FNDATA.EBELN) AS EBELN
                                ,MAX(FNDATA.BEDNR) AS BEDNR
                                ,MAX(FNDATA.EBELP) AS EBELP
                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
                                ,MAX(FNDATA.[商品條碼(EAN12)]) AS '商品條碼(EAN12)'
                                ,MAX(FNDATA.[品名]) AS '品名'
                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
                                ,MAX(FNDATA.[備註]) AS '備註'
                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
                                ,FNDATA.VBELN
                                ,CASE WHEN SUM(CASE WHEN FNDATA.BWART = 'AB' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS CANDO
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,B.VBELN
                                 ,B.BWART
                                FROM REPLEN A 
                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP  AND A.EAN11 = B.EAN11
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART_P = '{0}' 
                                AND A.EBELN = '{1}' 
                                AND A.BEDNR = '{2}' 
                                AND {3} 
                                AND B.BSART = '{0}'
								AND B.BWART = 'AB'
                                AND B.EBELN = '{1}'
                                AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)) FNDATA
                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
	                            	                            INNER JOIN
	                            (
                                    SELECT DataOut.*, ISNULL(MENGE_M_RESWK,0) as '實際量', ISNULL( DataIn.CANEDIT, 1) as CANEDIT FROM
		                            (
			                            SELECT MATNR, VBELN, MENGE_M as '應到數量'
			                            FROM GMOVET
			                            WHERE BWART = '641' AND EBELN = '{1}' AND BSART = '{0}'
		                            ) as DataOut
		                            LEFT JOIN
		                            (
			                            SELECT MATNR, VBELN, MENGE_M as MENGE_M_RESWK
			                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
			                            FROM GMOVET
			                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
		                            ) as DataIn ON DataOut.MATNR = DataIn.MATNR AND DataIn.VBELN = DataOut.VBELN
	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
                                ORDER BY RANK, EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                #region ZUB4
                case "ZUB4":
                    sb.Append(@"SELECT DATA.*, ISNULL(ALLDATA.CANEDIT,1) as CANEDIT,
                                ISNULL(ALLDATA.實際量,0) as '實際量',
                                CASE WHEN ISNULL(ALLDATA.實際量,0) = 0 THEN 1 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                     WHEN ALLDATA.實際量 != 應到數量 THEN 2 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
                                     WHEN ALLDATA.實際量 = 應到數量 THEN 3 -- (ALLDATA.[當日收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
                                     ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 MAX(FNDATA.BSART) AS BSART
                                ,MAX(FNDATA.EBELN) AS EBELN
                                ,MAX(FNDATA.EBELP) AS EBELP
                                ,MAX(FNDATA.[物料號碼]) AS '物料號碼'
                                ,MAX(FNDATA.[商品條碼(EAN)]) AS '商品條碼(EAN)'
                                ,MAX(FNDATA.[品名]) AS '品名'
                                ,MAX(FNDATA.[收貨門市]) AS '收貨門市'
                                ,MAX(FNDATA.[發貨門市]) AS '發貨門市'
                                ,MAX(FNDATA.[採購單數量]) AS '採購單數量'
								,MAX(FNDATA.[採購單數量]) AS '應到數量'
                                ,MAX(FNDATA.[採購單計量單位]) AS '採購單計量單位'
                                ,MAX(FNDATA.[備註]) AS '備註'
                                ,MAX(FNDATA.[需求日期]) AS '需求日期'
                                ,FNDATA.VBELN
                                ,1 AS CANDO
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = ASSORT.EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                ,A.EBELN as VBELN
                                ,B.BWART
                                FROM REPLEN A 
                                LEFT JOIN GMOVET B ON A.EBELP = B.EBELP AND A.EAN11 = B.EAN11 
                                    AND B.BSART = '{0}' AND B.EBELN = '{1}' AND B.BWART = 'AB'
                                    AND (B.DELFLG != 'X' OR B.DELFLG IS NULL)
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE A.BSART_P = '{0}' 
                                AND A.EBELN = '{1}' 
                                AND A.BEDNR = '{2}' 
                                AND {3} ) FNDATA
                                GROUP BY FNDATA.VBELN, FNDATA.[商品條碼(EAN)]) as DATA
	                            LEFT JOIN
	                            (
		                            SELECT MATNR, VBELN, MENGE_M as '實際量'
		                            ,CASE WHEN ( PROCD = 'X' OR CONVERT(date, CRDAT) != CONVERT(date, GETDATE()) ) THEN 0 ELSE 1 END as CANEDIT 
		                            FROM GMOVET
		                            WHERE BWART = '101' AND EBELN = '{1}' AND BSART = '{0}'
	                            ) AS ALLDATA ON ALLDATA.MATNR = DATA.物料號碼 AND ALLDATA.VBELN = DATA.VBELN
                                ORDER BY RANK, EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();
                #endregion

                case "ZRP2":
                case "ZRP4":
                case "ZFG1":
                    sb.Append(@"SELECT 
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                --,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                  WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                  ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_P, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_P AS '採購單數量'
                                ,A.MEINS_P AS '採購單計量單位'
                                ,'' AS '備註'
                                ,CONVERT(date, BADAT) AS '需求日期'
                                FROM REPLEN A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART_P = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZUB1":
                case "ZUB3":
                    sb.Append(@"SELECT
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                --,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                  WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                  ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND BWART ='101' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM STO2STO A 
                                INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZUB2":
                    sb.Append(@"SELECT  
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                --,[本次收發量]
                                ,[實際量]
                                ,[備註]
                              ,CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                  WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                  ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM STO2TDC1 A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZUR1":
                    sb.Append(@"SELECT 
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                --,[本次收發量]
                                ,[實際量]
                                ,[備註]
                               ,CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                  WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                  ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,A.WERKS AS '收貨門市'
                                ,A.RESWK AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM RE2TDC1 A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();

                case "ZRE3":
                    sb.Append(@"SELECT 
                                 [商品條碼(EAN)]
                                ,[品名]
                                ,[應到數量]
                                --,[本次收發量]
                                ,[實際量]
                                ,[備註]
                                ,CASE WHEN ALLDATA.[實際量] = 0 THEN 1 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = 0 THEN 1
                                      WHEN ALLDATA.[實際量] !=  ALLDATA.[應到數量] THEN 2 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) !=  ALLDATA.[應到數量] THEN 2
	                                  WHEN ALLDATA.[實際量] = ALLDATA.[應到數量] THEN 3 -- (ALLDATA.[本次收發量] + ALLDATA.[實際量]) = ALLDATA.[應到數量] THEN 3
	                                  ELSE 4 END AS RANK
                                FROM(
                                SELECT
                                 '{0}' AS BSART
                                ,'{1}' AS EBELN
                                ,A.BEDNR AS BEDNR
                                ,A.EBELP AS EBELP
                                ,A.MATNR AS '物料號碼'
                                ,A.EAN11 AS '商品條碼(EAN)'
                                ,(SELECT TOP 1 EAN12 FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '商品條碼(EAN12)'
                                ,(SELECT TOP 1 MAKTX FROM ASSORT WHERE A.WERKS = WERKS AND A.EAN11 = EAN11) AS '品名'
                                ,'' AS '收貨門市'
                                ,A.WERKS AS '發貨門市'
                                ,COALESCE(MENGE_R, 0) AS '應到數量'
                                --,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) = CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '本次收發量'
                                ,COALESCE((SELECT SUM(MENGE_M) FROM GMOVET WHERE BSART = '{0}' AND EBELN = '{1}' AND A.EBELP = EBELP AND CONVERT(date, CRDAT) <= CONVERT(date, GETDATE()) AND (DELFLG != 'X' OR DELFLG IS NULL) AND A.WERKS = WERKS AND A.EAN11 = EAN11), 0) AS '實際量'
                                ,A.MENGE_R AS '採購單數量'
                                ,A.MEINS AS '採購單計量單位'
                                ,'' AS '備註'
                                ,'' AS '需求日期'
                                FROM RE2VENDOR A 
								INNER JOIN ASSORT ON ASSORT.MATNR = A.MATNR AND A.WERKS = ASSORT.WERKS AND (ASSORT.TMOUT = '' OR ASSORT.TMOUT IS NULL)
                                WHERE BSART = '{0}' AND EBELN = '{1}' AND BEDNR = '{2}' AND {3}) ALLDATA
                                ORDER BY RANK, ALLDATA.EBELP");

                    sb = sb.Replace("{0}", _bsart)
                           .Replace("{1}", _ebeln)
                           .Replace("{2}", _bednr)
                           .Replace("{3}", _prdFilter);
                    return sb.ToString();
            }

            return string.Empty;
        }
    }
}
