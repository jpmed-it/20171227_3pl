﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace JPMed_PDA.Utility
{
    public class SyncDatabaseTime
    {
        DBConnection dbConn;

        public SyncDatabaseTime()
        {
            dbConn = new DBConnection();
        }

        [DllImport("coredll.dll", SetLastError = true)]
        static extern bool SetLocalTime(ref SYSTEMTIME lpSystemTime);

        //This is the object you need to fill before you use SetLocalTime
        public struct SYSTEMTIME
        {
            public short year;
            public short month;
            public short dayOfWeek;
            public short day;
            public short hour;
            public short minute;
            public short second;
            public short milliseconds;
        }

        //And this is the final method to execute
        public static void SetSystemDateTime(DateTime dt)
        {
            SYSTEMTIME systime;
            systime.year = (short)dt.Year;
            systime.month = (short)dt.Month;
            systime.day = (short)dt.Day;

            systime.hour = (short)dt.Hour;
            systime.minute = (short)dt.Minute;
            systime.second = (short)dt.Second;
            systime.milliseconds = (short)dt.Millisecond;
            systime.dayOfWeek = (short)dt.DayOfWeek;

            SetLocalTime(ref systime);
        }

        //取得DB時間
        public bool GetDatabaseTime(ref string databaseTime)
        {
            if (!dbConn.TestConnect(out databaseTime))
                return false;
            else
            {
                databaseTime = dbConn.GetDataTableRecData("SELECT GETDATE()");
                return true;
            }
        }
    }
}
