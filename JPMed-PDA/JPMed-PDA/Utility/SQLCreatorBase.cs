﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;

namespace JPMed_PDA.Utility
{
    public abstract class SQLCreatorBase
    {
        private string MS_TO_ORACLE(string SQL_STATEMENT)
        {
            SQL_STATEMENT = SQL_STATEMENT.Replace("+  '", "||  '");
            SQL_STATEMENT = SQL_STATEMENT.Replace("+ '", "|| '");
            SQL_STATEMENT = SQL_STATEMENT.Replace("+'", "||'");
            SQL_STATEMENT = SQL_STATEMENT.Replace("'  +", "'  ||");
            SQL_STATEMENT = SQL_STATEMENT.Replace("' +", "' ||");
            SQL_STATEMENT = SQL_STATEMENT.Replace("'+", "'||");
            SQL_STATEMENT = SQL_STATEMENT.Replace(") + STR(", ") || STR(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" STR(", " TO_CHAR(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" str(", " TO_CHAR(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" ISNULL(", " NVL(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" isnull(", " NVL(");
            SQL_STATEMENT = SQL_STATEMENT.Replace("(ISNULL(", "(NVL(");
            SQL_STATEMENT = SQL_STATEMENT.Replace("(isnull(", "(NVL(");
            SQL_STATEMENT = SQL_STATEMENT.Replace("GETDATE()", "SYSDATE");
            SQL_STATEMENT = SQL_STATEMENT.Replace("getdate()", "SYSDATE");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" LEN(", " LENGTH(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" len(", " length(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" substring(", " substr(");
            SQL_STATEMENT = SQL_STATEMENT.Replace(" SUBSTRING(", " SUBSTR(");

            return SQL_STATEMENT;
        }
    }

    #region SQLStatementCreator

    public class SQLStatementCreator : SQLCreatorBase
    {
        protected static Hashtable _Parameter { get; set; }

        public SQLStatementCreator()
        {
            _Parameter = new Hashtable();
        }

        public void SetParameter(Hashtable Parameter)
        {
            _Parameter = Parameter;
        }

        #region GetParameter
        /// <summary>
        /// 取得參數
        /// </summary>
        /// <param name="Name">參數名稱</param>
        /// <returns></returns>
        protected static object GetParameter(string Name)
        {
            return _Parameter[Name] == null ? "" : _Parameter[Name];
        }

        #endregion

    }

    #endregion

}
