﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace JPMed_PDA.Utility
{
    /// <summary>
    /// 共用Function
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// 移動游標並全選
        /// </summary>
        /// <param name="c"></param>
        public static void SelectAndFocus(TextBox c)
        {
            c.Focus();
            c.SelectAll();
        }
    }
}
