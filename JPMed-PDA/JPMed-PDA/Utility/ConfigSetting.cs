﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace JPMed_PDA.Utility
{
    public class ConfigSettings
    {
        static string connectionStr = "Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}";

        /// <summary>
        /// DB連線字串
        /// </summary>
        public string DBConnString { get; set; }
        /// <summary>
        /// 分店代碼
        /// </summary>
        public string Werks { get; set; }
        /// <summary>
        /// PDA序號
        /// </summary>
        public string PdaSn { get; set; }
        /// <summary>
        /// WebService Url
        /// </summary>
        public string WebServiceUrl { get; set; }
        /// <summary>
        /// 是否開啟自動更新
        /// </summary>
        public string WmAutoUpdate { get; set; }
        /// <summary>
        /// 自動更新檢查檔案位置
        /// </summary>
        public string WmFileLocation { get; set; }
        /// <summary>
        /// ServerIP
        /// </summary>
        public string ServerIP { get; set; }
        /// <summary>
        /// 預設資料庫名稱
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 使用者密碼
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 查價程式連接字串1
        /// </summary>
        public string ConString1 { get; set; }

        /// <summary>
        /// 查價程式連接字串2
        /// </summary>
        public string ConString2 { get; set; }

        /// <summary>
        /// 查價程式連接字串3
        /// </summary>
        public string ConString3 { get; set; }

        /// <summary>
        /// 查價程式連接字串4
        /// </summary>
        public string ConString4 { get; set; }


        public ConfigSettings()
        {
            string programFolder = Path.GetDirectoryName(this.GetType().Assembly.GetModules()[0].FullyQualifiedName);
            string xmlFileFullPath = Path.Combine(programFolder, "Settings.xml");
            
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFileFullPath);
            
            XmlElement nodRoot = doc.DocumentElement;
            XmlNodeList nodeList = nodRoot.GetElementsByTagName("setting");
            if (nodeList.Count > 0)
            {
                foreach (XmlNode node in nodeList)
                {
                    switch (node.Attributes["name"].Value.ToLower())
                    {
                        case "padsn": PdaSn = node.Attributes["value"].Value; break;
                        case "werkscode": Werks = node.Attributes["value"].Value.ToUpper(); break;
                        case "serverip": ServerIP = node.Attributes["value"].Value; break;
                        case "databasename": DataBaseName = node.Attributes["value"].Value; break;
                        case "username": UserName = node.Attributes["value"].Value; break;
                        case "password": Password = node.Attributes["value"].Value; break;
                        case "webserviceuri": WebServiceUrl = node.Attributes["value"].Value; break;
                        case "wmautoupdate": WmAutoUpdate = node.Attributes["value"].Value; break;
                        case "wmfilelocation": WmFileLocation = node.Attributes["value"].Value; break;
                        case "constring1": ConString1 = node.Attributes["value"].Value; break;
                        case "constring2": ConString2 = node.Attributes["value"].Value; break;
                        case "constring3": ConString3 = node.Attributes["value"].Value; break;
                        case "constring4": ConString4 = node.Attributes["value"].Value; break;
                    }
                }

                DBConnString = string.Format(connectionStr, ServerIP, DataBaseName, UserName, PwdDecrypt.DecryptDES(Password, "53536349", "94363535"));
            }
            else DBConnString = string.Empty;
        }
        
    }
}
