﻿using System;
using System.Data;
using System.Data.SqlClient;
using JPMed_PDA.Utility;

namespace RichardPrice
{
    class CmsADODB
    {
        static ConfigSettings settings = new ConfigSettings();

        public CmsADODB()
        {

            //
            // TODO: 在此加入建構函式的程式碼
            //        
        }

        ///<summary >
        ///取得Connection String
        ///</summary>
        public static string GetConnectionString(String type1)
        {
            switch (type1)  /*status 只能為整數、長整數或字元變數.*/
            {
                case "1":
                    return settings.ConString1;
                case "2":
                    return settings.ConString2;
                case "3":
                    return settings.ConString3;
                default:
                    return settings.ConString4;
            }

        }

        ///<summary >
        ///取得Connection物件
        ///</summary>
        public static SqlConnection GetSqlConnection(String type1)
        {
            string ConnectionString = GetConnectionString(type1);
            SqlConnection conn = new SqlConnection(ConnectionString);
            return conn;
        }

        ///<summary >
        ///取得查詢資料(回傳DataTable)
        ///</summary>
        public static DataTable QueryGetTable(String type1, string strSql)
        {
            using (SqlConnection conn = GetSqlConnection(type1))
            {

                SqlDataAdapter adp = new SqlDataAdapter(strSql, conn);

                DataTable dt = new DataTable();
                adp.Fill(dt);

                return dt;
            }
        }

        ///<summary >
        ///執行SQL
        ///</summary>
        public static bool ExecuteSQL(String type1, string strSql)
        {
            using (SqlConnection conn = GetSqlConnection(type1))
            {
                bool flag;
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(strSql, conn);
                    cmd.CommandTimeout = 999;
                    int effect = cmd.ExecuteNonQuery();
                    flag = true;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    flag = false;
                    throw;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();

                }
                return flag;
            }
        }
    }
}
