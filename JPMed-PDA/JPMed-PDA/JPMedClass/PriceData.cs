﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace RichardPrice
{
    public class PriceData
    {
        /// <summary>
        /// 原售價
        /// </summary>
        public string OriginalPrice { set; get; }
        /// <summary>
        /// 現售價
        /// </summary>
        public string CurrentlyPrice { set; get; }
        /// <summary>
        /// 會員價
        /// </summary>
        public string MemberPrice { set; get; }
        /// <summary>
        /// 開始日
        /// </summary>
        public string StartDay { set; get; }
        /// <summary>
        /// 結束日
        /// </summary>
        public string EndDay { set; get; }
        /// <summary>
        /// 活動編號
        /// </summary>
        public string ActivityNumber { set; get; }
        /// <summary>
        /// 品號
        /// </summary>
        public string ProductNumber { set; get; }
        /// <summary>
        /// 品名
        /// </summary>
        public string ProductName { set; get; }
        /// <summary>
        /// 商品大類
        /// </summary>
        public string ProductType { set; get; }
        /// <summary>
        /// 備註
        /// </summary>
        public string PS { set; get; }
    }
}
