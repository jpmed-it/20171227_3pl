﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace RichardPrice
{
    public class Price
    {
        static string SQLStr;
        static string conType1 = "1";
        static string conType2 = "2";
        static int Position = 4;
        /// <summary>
        /// 商品主檔->原售價 MB051
        /// </summary>
        /// <param name="EAN">條碼</param>
        /// <returns></returns>
        public static DataTable OriginalPrice(string EAN)
        {
            DataTable dtINVMB;
            SQLStr = "select b.MB001, b.MB002, b.MB003, b.MB004, b.MB005, b.MB006, b.MB007, b.MB008, b.MB013, b.MB051, b.MB111 from INVMB as b left join INVMH as h on b.MB001=h.MH001 where (b.MB003='" + EAN + "' or h.MH002='" + EAN + "' or b.MB001='" + EAN + "') and (b.MB020!='S')";
            dtINVMB = CmsADODB.QueryGetTable(conType1, SQLStr);
            return dtINVMB;
        }

        /// <summary>
        /// 取得各種活動售價
        /// </summary>
        /// <param name="EAN">條碼</param>
        /// <param name="dayEnd">活動結束日</param>
        /// <param name="store">店代號</param>
        /// <returns></returns>
        public static PriceData GetPrice(string EAN, string dayEnd, string store)
        {
            DataTable dtINVMB;
            DataTable dtPOSMB;
            DataTable dtPOSMF;
            DataTable dtPOSML;
            PriceData PriceData1 = new PriceData();
            bool OnSale = true;
            string COSMOSStore = SAPChangeStore(store);
            dtINVMB = OriginalPrice(EAN);
            //原售價
            if (dtINVMB.Rows.Count > 0)
            {
                PriceData1.ProductType = GetType(dtINVMB.Rows[0]["MB005"].ToString());

                PriceData1.OriginalPrice = GetInt(dtINVMB.Rows[0]["MB051"].ToString());
                PriceData1.CurrentlyPrice = GetInt(dtINVMB.Rows[0]["MB051"].ToString());
                PriceData1.MemberPrice = GetInt(dtINVMB.Rows[0]["MB051"].ToString());
                PriceData1.StartDay = "";
                PriceData1.EndDay = "";
                PriceData1.ActivityNumber = "";
                PriceData1.ProductNumber = dtINVMB.Rows[0]["MB001"].ToString();
                PriceData1.ProductName = dtINVMB.Rows[0]["MB002"].ToString();
                //PriceData1.ProductCode = dtINVMB.Rows[0]["MB003"].ToString();
                PriceData1.PS = "";

                //個別商品特價
                //dtPOSMB = SalePrice(PriceData1.ProductNumber, dayEnd);
                //if (dtPOSMB.Rows.Count > 0)
                //{
                //    if (dtPOSMB.Rows[0]["MB022"].ToString() == "Y")
                //    {
                //        dtPOSMF = LimitedStore(dtPOSMB.Rows[0]["MB001"].ToString(), dtPOSMB.Rows[0]["MB003"].ToString(), COSMOSStore);
                //        if (dtPOSMF.Rows.Count > 0)
                //        {
                //            OnSale = true;
                //        }
                //        else
                //        {
                //            OnSale = false;
                //        }
                //    }
                //    else
                //    {
                //        OnSale = true;
                //    }
                //    if (OnSale == true)
                //    {
                //        PriceData1.CurrentlyPrice =Other.GetInt(dtPOSMB.Rows[0]["MC005"].ToString());
                //        PriceData1.MemberPrice = Other.GetInt(dtPOSMB.Rows[0]["MC006"].ToString());
                //        PriceData1.StartDay = Other.TakePosition(dtPOSMB.Rows[0]["MB012"].ToString(), Position);
                //        PriceData1.EndDay = Other.TakePosition(dtPOSMB.Rows[0]["MB013"].ToString(), Position);
                //        PriceData1.ActivityNumber = dtPOSMB.Rows[0]["MB001"].ToString();
                //        PriceData1.PS += dtPOSMB.Rows[0]["MB009"].ToString();
                //    }
                //}
                dtPOSMB = SalePriceList(PriceData1.ProductNumber, dayEnd, dayEnd, "", COSMOSStore);
                if (dtPOSMB.Rows.Count > 0)
                {
                    PriceData1.CurrentlyPrice = GetInt(dtPOSMB.Rows[0]["MC005"].ToString());
                    PriceData1.MemberPrice = GetInt(dtPOSMB.Rows[0]["MC006"].ToString());
                    PriceData1.StartDay = TakePosition(dtPOSMB.Rows[0]["MB012"].ToString(), Position);
                    PriceData1.EndDay = TakePosition(dtPOSMB.Rows[0]["MB013"].ToString(), Position);
                    PriceData1.ActivityNumber = dtPOSMB.Rows[0]["MB001"].ToString();
                    PriceData1.PS += dtPOSMB.Rows[0]["MB009"].ToString();
                }

                //商品類別特價->MB111品牌
                //dtPOSMB = TypePrice(dtINVMB.Rows[0]["MB111"].ToString(), dayEnd);
                //if (dtPOSMB.Rows.Count > 0)
                //{
                //    if (dtPOSMB.Rows[0]["MB022"].ToString() == "Y")
                //    {
                //        dtPOSMF = LimitedStore(dtPOSMB.Rows[0]["MB001"].ToString(), dtPOSMB.Rows[0]["MB003"].ToString(), COSMOSStore);
                //        if (dtPOSMF.Rows.Count > 0)
                //        {
                //            OnSale = true;
                //        }
                //        else
                //        {
                //            OnSale = false;
                //        }
                //    }
                //    else
                //    {
                //        OnSale = true;
                //    }
                //    if (OnSale == true)
                //    {
                //        PriceData1.CurrentlyPrice = Other.GetInt((Convert.ToDouble(PriceData1.CurrentlyPrice) * Convert.ToDouble(dtPOSMB.Rows[0]["MD007"])).ToString());
                //        PriceData1.MemberPrice = Other.GetInt((Convert.ToDouble(PriceData1.MemberPrice) * Convert.ToDouble(dtPOSMB.Rows[0]["MD008"])).ToString());
                //        PriceData1.StartDay = Other.TakePosition(dtPOSMB.Rows[0]["MB012"].ToString(), Position);
                //        PriceData1.EndDay = Other.TakePosition(dtPOSMB.Rows[0]["MB013"].ToString(), Position);
                //        PriceData1.ActivityNumber = dtPOSMB.Rows[0]["MB001"].ToString();
                //        PriceData1.PS += dtPOSMB.Rows[0]["MB009"].ToString();
                //    }
                //}
                dtPOSMB = TypePriceList(PriceData1.ProductNumber, dayEnd, dayEnd, "", COSMOSStore);
                if (dtPOSMB.Rows.Count > 0)
                {
                    PriceData1.CurrentlyPrice = GetInt((Convert.ToDouble(dtPOSMB.Rows[0]["MB051"]) * Convert.ToDouble(dtPOSMB.Rows[0]["MD007"])).ToString());
                    PriceData1.MemberPrice = GetInt((Convert.ToDouble(dtPOSMB.Rows[0]["MB051"]) * Convert.ToDouble(dtPOSMB.Rows[0]["MD008"])).ToString());
                    PriceData1.StartDay = TakePosition(dtPOSMB.Rows[0]["MB012"].ToString(), Position);
                    PriceData1.EndDay = TakePosition(dtPOSMB.Rows[0]["MB013"].ToString(), Position);
                    PriceData1.ActivityNumber = dtPOSMB.Rows[0]["MB001"].ToString();
                    PriceData1.PS += dtPOSMB.Rows[0]["MB009"].ToString();
                }

                ////組合品搭贈
                dtPOSMB = Combination(PriceData1.ProductNumber, dayEnd);
                if (dtPOSMB.Rows.Count > 0)
                {
                    //折扣
                    //固定組合 暫時不抓
                    if (dtPOSMB.Rows[0]["MI013"].ToString() == "1")
                    {
                        ////MI017 非會員價
                        //if (Convert.ToInt32(dtPOSMB.Rows[0]["MI017"].ToString())>0)
                        //{

                        //}
                        //else
                        //{
                        //    //MI019 非會員固定折扣率
                        //}
                    }
                    else if (dtPOSMB.Rows[0]["MI013"].ToString() == "3")
                    {
                        //3變動分量以上
                        //dtPOSML = TypePrice(dtPOSMB.Rows[0]["MI001"].ToString(), dtPOSMB.Rows[0]["MI003"].ToString());
                        dtPOSML = TypePriceList(dtPOSMB.Rows[0]["MI001"].ToString(), dtPOSMB.Rows[0]["MI003"].ToString(), dtPOSMB.Rows[0]["MI003"].ToString(), "", COSMOSStore);
                        if (dtPOSML.Rows.Count > 0)
                        {
                            if (Convert.ToDouble(dtPOSML.Rows[0]["ML007"]) > 0)
                            {
                                //X件X折

                            }
                        }
                    }
                    else if (dtPOSMB.Rows[0]["MI013"].ToString() == "2" || dtPOSMB.Rows[0]["MI013"].ToString() == "4")
                    {
                        //2變動分量組合 4增量折扣
                        //dtPOSML = TypePrice(dtPOSMB.Rows[0]["MI001"].ToString(), dtPOSMB.Rows[0]["MI003"].ToString());
                        dtPOSML = TypePriceList(dtPOSMB.Rows[0]["MI001"].ToString(), dtPOSMB.Rows[0]["MI003"].ToString(), dtPOSMB.Rows[0]["MI003"].ToString(), "", COSMOSStore);
                        if (dtPOSML.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(dtPOSML.Rows[0]["ML005"]) > 0)
                            {
                                //第X件特價
                            }
                            else if (Convert.ToDouble(dtPOSML.Rows[0]["ML007"]) > 0)
                            {
                                //第X件X折
                                //PriceData1.Explanation = "買一送一";
                            }
                            else if (Convert.ToInt32(dtPOSML.Rows[0]["ML004"]) == 0 && Convert.ToInt32(dtPOSML.Rows[0]["ML005"]) == 0 && Convert.ToDouble(dtPOSML.Rows[0]["ML007"]) == 0)
                            {
                                //買一送一
                                //PriceData1.Explanation = "買一送一";
                            }
                        }
                    }
                    else
                    {
                        //足量搭贈
                    }

                    PriceData1.StartDay = TakePosition(dtPOSMB.Rows[0]["MI005"].ToString(), Position);
                    PriceData1.EndDay = TakePosition(dtPOSMB.Rows[0]["MI006"].ToString(), Position);
                    PriceData1.ActivityNumber = dtPOSMB.Rows[0]["MI001"].ToString();
                    PriceData1.PS += dtPOSMB.Rows[0]["MI010"].ToString();
                }

            }
            else
            {
                PriceData1.OriginalPrice = "";
                PriceData1.CurrentlyPrice = "";
                PriceData1.MemberPrice = "";
                PriceData1.StartDay = "";
                PriceData1.EndDay = "";
                PriceData1.ActivityNumber = "";
                PriceData1.ProductNumber = "";
                PriceData1.ProductName = "";
                PriceData1.ProductType = "";
                PriceData1.PS = "";
            }

            return PriceData1;
        }

        /// <summary>
        /// 個別商品特價
        /// </summary>
        /// <param name="num1">品號</param>
        /// <param name="dayEnd">結束日</param>
        /// <returns></returns>
        //private static DataTable SalePrice(string num1, string dayEnd)
        //{
        //    DataTable dtPOSMB;
        //    SQLStr = "select B.MB001,B.MB003,B.MB009,B.MB012,B.MB013,B.MB022,C.MC005,C.MC006 from POSMB as B left join POSMC as C on C.MC001 = B.MB001 AND C.MC002 = B.MB002 AND C.MC003 = B.MB003  where C.MC004='" + num1 + "' AND B.MB012 <= '" + dayEnd + "' AND B.MB013 >= '" + dayEnd + "' AND B.MB008='Y' ORDER BY B.MB012 desc ";
        //    dtPOSMB = CmsADODB.QueryGetTable(conType1, SQLStr);
        //    return dtPOSMB;
        //}

        /// <summary>
        /// 個別商品特價限定專櫃
        /// </summary>
        /// <param name="Activity">活動代號</param>
        /// <param name="sale">特價代號</param>
        /// <param name="Store">店代號</param>
        /// <returns></returns>
        //private static DataTable LimitedStore(string Activity, string sale, string Store)
        //{
        //    DataTable dtPOSMF;
        //    SQLStr = "select MF004 from POSMF where MF001='" + Activity + "' AND MF003= '" + sale + "' and MF004='" + Store + "'";
        //    dtPOSMF = CmsADODB.QueryGetTable(conType1, SQLStr);
        //    return dtPOSMF;
        //}

        /// <summary>
        /// 商品類別特價
        /// </summary>
        /// <param name="num1">品號</param>
        /// <param name="dayEnd">結束日</param>
        /// <returns></returns>
        //private static DataTable TypePrice(string num1, string dayEnd)
        //{
        //    DataTable dtPOSMB;
        //    SQLStr = "select B.MB001,B.MB003,B.MB009,B.MB012,B.MB013,B.MB022,D.MD007,D.MD008 from POSMB as B left join POSMD as D on D.MD001 = B.MB001 AND D.MD002 = B.MB002 AND D.MD003 = B.MB003  where D.MD004='" + num1 + "' AND B.MB012 <= '" + dayEnd + "' AND B.MB013 >= '" + dayEnd + "' AND B.MB008='Y' ORDER BY B.MB012 desc ";
        //    dtPOSMB = CmsADODB.QueryGetTable(conType1, SQLStr);
        //    return dtPOSMB;
        //}

        /// <summary>
        /// 組合品搭贈
        /// </summary>
        /// <param name="num1">品號</param>
        /// <param name="dayEnd">結束日</param>
        /// <returns></returns>
        private static DataTable Combination(string Brands, string dayEnd)
        {
            DataTable dtPOSMB;
            SQLStr = "select I.MI001,I.MI003,I.MI005,I.MI006,I.MI010,I.MI013,I.MI017,I.MI018,I.MI019,I.MI020 from POSMI as I left join POSMJ as J on J.MJ001 = I.MI001 AND J.MJ002 = I.MI002 AND J.MJ003 = I.MI003  where J.MJ004='" + Brands + "' AND I.MI005 <= '" + dayEnd + "' AND I.MI006 >= '" + dayEnd + "' AND I.MI015='Y' ORDER BY J.MJ003 ";
            dtPOSMB = CmsADODB.QueryGetTable(conType1, SQLStr);
            return dtPOSMB;
        }

        /// <summary>
        /// 折扣-增量折扣
        /// </summary>
        /// <param name="Activity">活動代號</param>
        /// <param name="sale">特價代號</param>
        /// <returns></returns>
        private static DataTable OnSale(string Activity, string sale)
        {
            DataTable dtPOSMB;
            SQLStr = "select ML004, ML005, ML006, ML007, ML008 from POSML where ML001='" + Activity + "' AND ML003 = '" + sale + "'";
            dtPOSMB = CmsADODB.QueryGetTable(conType1, SQLStr);
            return dtPOSMB;
        }



        /// <summary>
        /// 商品大類
        /// </summary>
        /// <param name="data1"></param>
        /// <returns></returns>
        public static string GetType(string data1)
        {
            switch (data1)
            {
                case "1":
                    return "藥品";

                case "2":
                    return "醫療保健";

                case "3":
                    return "彩妝保養";

                case "4":
                    return "生活百貨";

                case "5":
                    return "食品";

                default:
                    return "其他";

            }
        }

        /// <summary>
        /// 個別商品特價清單
        /// </summary>
        /// <param name="num1">品號</param>
        /// <param name="dayEnd">結束日</param>
        /// <returns></returns>
        public static DataTable SalePriceList(string num1, string dayStart, string dayEnd, string act1, string store)
        {
            DataTable dtPOSMB;
            string WhereStr = "";
            if (num1 != "")
            {
                WhereStr = " and (M.MB003='" + num1 + "' or M.MB001='" + num1 + "' or H.MH002='" + num1 + "')";
            }
            if (act1 != "")
            {
                WhereStr = " and B.MB003='" + act1 + "'";
            }
            if (dayStart != "")
            {
                WhereStr += " and B.MB012<='" + dayStart + "'";
            }

            SQLStr = "select B.MB001,B.MB003,B.MB009,B.MB012,B.MB013,B.MB022,C.MC004,C.MC005,C.MC006,C.MC007,C.MC008,M.MB001 as id1,M.MB002 as Name1,M.MB003 as EAN,M.MB005 as Type1,M.MB051 from POSMB as B ";
            SQLStr += " left join POSMC as C on C.MC001 = B.MB001 AND C.MC002 = B.MB002 AND C.MC003 = B.MB003 ";
            SQLStr += " left join INVMB as M on M.MB001 = C.MC004 ";
            SQLStr += " left join INVMH as H on M.MB001 = H.MH001 ";
            SQLStr += " left join POSMF as F on B.MB001 = F.MF001 and B.MB003 = F.MF003 ";
            //SQLStr += " where B.MB013 >= '" + dayEnd + "' AND B.MB008='Y' AND (B.MB022='N' or F.MF004='" + store + "')  " + WhereStr;
            SQLStr += " where B.MB013 >= '" + dayEnd + "' AND B.MB008='Y' AND (B.MB022='N' or F.MF004='" + store + "') and (M.MB020!='S') " + WhereStr;
            SQLStr += " ORDER BY B.MB012 desc ";
            dtPOSMB = CmsADODB.QueryGetTable(conType1, SQLStr);
            return dtPOSMB;
        }

        /// <summary>
        /// 商品類別特價清單
        /// </summary>
        /// <param name="num1">品號</param>
        /// <param name="dayEnd">結束日</param>
        /// <returns></returns>
        public static DataTable TypePriceList(string num1, string dayStart, string dayEnd, string act1, string store)
        {
            DataTable dtPOSMB;
            string WhereStr = "";
            if (num1 != "")
            {
                WhereStr = " and (M.MB003='" + num1 + "' or M.MB001='" + num1 + "' or H.MH002='" + num1 + "')";
            }
            if (act1 != "")
            {
                WhereStr = " and B.MB003='" + act1 + "'";
            }
            if (dayStart != "")
            {
                WhereStr += " and B.MB012<='" + dayStart + "'";
            }

            SQLStr = "select B.MB001,B.MB003,B.MB009,B.MB012,B.MB013,B.MB022,D.MD004,D.MD005,D.MD006,D.MD007,D.MD008,M.MB001 as id1,M.MB002 as Name1,M.MB003 as EAN,M.MB005 as Type1,M.MB051 from POSMB as B ";
            SQLStr += " left join POSMD as D on D.MD001 = B.MB001 AND D.MD002 = B.MB002 AND D.MD003 = B.MB003 ";
            SQLStr += " left join INVMB as M on M.MB111 = D.MD004 ";
            SQLStr += " left join INVMH as H on M.MB001 = H.MH001 ";
            SQLStr += " left join POSMF as F on B.MB001 = F.MF001 and B.MB003 = F.MF003 ";
            //SQLStr += " where B.MB013 >= '" + dayEnd + "' AND B.MB008='Y' AND (B.MB022='N' or F.MF004='" + store + "') and M.MB001 is not null " + WhereStr;
            SQLStr += " where B.MB013 >= '" + dayEnd + "' AND B.MB008='Y' AND (B.MB022='N' or F.MF004='" + store + "') and M.MB001 is not null and (M.MB020!='S') " + WhereStr;
            SQLStr += " ORDER BY B.MB012 desc ";
            dtPOSMB = CmsADODB.QueryGetTable(conType1, SQLStr);
            return dtPOSMB;
        }

        /// <summary>
        /// 取右邊字串
        /// </summary>
        /// <param name="data1"></param>
        /// <param name="num1"></param>
        /// <returns></returns>
        private static string TakePosition(string data1, int num1)
        {
            if (data1.Length > num1)
            {
                return data1.Substring(data1.Length - num1, num1);
            }
            else
            {
                return data1;
            }
        }

        /// <summary>
        /// 四捨五入到整數
        /// </summary>
        /// <param name="data1"></param>
        /// <returns></returns>
        private static string GetInt(string data1)
        {
            return Math.Round(Convert.ToDouble(data1), 0).ToString();
        }

        /// <summary>
        /// SAP轉COSMOS店號
        /// </summary>
        /// <param name="data1"></param>
        /// <returns></returns>

        public static string SAPChangeStore(string data1)
        {
            string CosmsStoreName = "";

            if (!data1.Contains("ST")) 
            {
                switch (data1)
                {
                    case "G001":
                        CosmsStoreName = "ST3002";
                        break;
                    case "G002":
                        CosmsStoreName = "ST3003";
                        break;
                    case "G003":
                        CosmsStoreName = "ST3004";
                        break;
                    case "G004":
                        CosmsStoreName = "ST3005";
                        break;
                    default:
                        CosmsStoreName = "ST4" + data1.Substring(1, 3);
                        break;
                }
            }
            
            return CosmsStoreName;
        }
    }
}
