﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JPMed_PDA.Entity
{
    public class Gmovet
    {
        /// <summary>
        /// 排序
        /// 1:白色, 2:粉紅, 3:綠色, 4:紅色(錯誤)
        /// </summary>
        public string RANK { get; set; }
        /// <summary>
        /// 採購文件類型
        /// </summary>
        public string BSART { get; set; }
        /// <summary>
        /// 採購文件號碼
        /// </summary>
        public string EBELN { get; set; }
        /// <summary>
        /// 總倉分箱流水號
        /// </summary>
        public string VBELN { get; set; }
        /// <summary>
        /// 需求追蹤號碼
        /// </summary>
        public string BEDNR { get; set; }
        /// <summary>
        /// 採購文件的項目號碼
        /// </summary>
        public string EBELP { get; set; }
        /// <summary>
        /// 物料號碼
        /// </summary>
        public string MATNR { get; set; }
        /// <summary>
        /// 國際貨品代碼 (EAN/UPC)
        /// </summary>
        public string EAN11 { get; set; }
        /// <summary>
        /// 子代碼 (EAN/UPC)
        /// </summary>
        public string EAN12 { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        public string MAKTX { get; set; }
        /// <summary>
        /// 收貨店
        /// </summary>
        public string WERKS { get; set; }
        /// <summary>
        /// 發貨店
        /// </summary>
        public string RESWK { get; set; }
        /// <summary>
        /// 應到數量
        /// </summary>
        public string MENGE_R { get; set; }
        /// <summary>
        /// 當日處理量
        /// </summary>
        //public string MENGE_M_TODAY { get; set; }
        /// <summary>
        /// 採購單數量
        /// </summary>
        public string MENGE_P { get; set; }
        /// <summary>
        /// 已處理量
        /// </summary>
        public string MENGE_M { get; set; }
        /// <summary>
        /// 採購單計量單位
        /// </summary>
        public string MEINS { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string MEMO { get; set; }
        /// <summary>
        /// 需求日期(補貨單使用)
        /// </summary>
        public string BADAT { get; set; }
        /// <summary>
        /// 可否異動
        /// </summary>
        public bool CANEDIT { get; set; }
        /// <summary>
        /// ZRP1使用，有無總倉對應641單據, 0否, 1是
        /// </summary>
        public bool CANDO { get; set; }

        /// <summary>
        /// 異動類型
        /// </summary>
        public string BWART { get; set; }

        /// <summary>
        /// 刪除註記
        /// </summary>
        public string DELFLG { get; set; }

        /// <summary>
        /// SAP 已處理註記
        /// </summary>
        public string PROCD { get; set; }
    }
}
