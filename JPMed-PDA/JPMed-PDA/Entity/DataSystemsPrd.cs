﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace JPMed_PDA.Entity
{
    public class DataSystemsPrd
    {
        /// <summary>
        /// 鼎新品號
        /// </summary>
        public string strCMSKU { get; set; }
        /// <summary>
        /// 商品敘述
        /// </summary>
        public string strGoodsDESCP { get; set; }
        /// <summary>
        /// 商品主檔失效日
        /// </summary>
        public string strExpDate { get; set; }
        /// <summary>
        /// 供應商ID
        /// </summary>
        public string strSUP_ID { get; set; }
        /// <summary>
        /// 供應商簡稱
        /// </summary>
        public string strSUPName { get; set; }
        /// <summary>
        /// 供應商全名
        /// </summary>
        public string strSUP_FullName { get; set; }
        /// <summary>
        /// 供應商電話
        /// </summary>
        public string strSUP_TEL { get; set; }
        /// <summary>
        /// 供應商FAX
        /// </summary>
        public string strSUP_FAX { get; set; }
        /// <summary>
        /// 供應商連絡人
        /// </summary>
        public string strSUP_Contacter { get; set; }
        /// <summary>
        /// 供應商地址
        /// </summary>
        public string strSUP_ADD { get; set; }
        /// <summary>
        /// 供應商付款條件
        /// </summary>
        public string strSUP_PayCond { get; set; }
        /// <summary>
        /// 供應商課稅別
        /// </summary>
        public string strSUP_MA044 { get; set; }
        /// <summary>
        /// 供應商付款
        /// </summary>
        public string strSUP_PayCond_NO { get; set; }
        /// <summary>
        /// 供應商統編
        /// </summary>
        public string strSUPPLY_INVO { get; set; }
        /// <summary>
        /// 供應商發票聯數
        /// </summary>
        public string strSUPINVO_Count { get; set; }
        /// <summary>
        /// PT_Flag
        /// </summary>
        public string strPT_Flag { get; set; }
        /// <summary>
        /// 單位
        /// </summary>
        public string strGoodsUnit { get; set; }
        /// <summary>
        /// 單位成本
        /// </summary>
        public string decGoodsCost { get; set; }
        /// <summary>
        /// 商品大類
        /// </summary>
        public string strCLASS1 { get; set; }
        /// <summary>
        /// 商品主檔的條碼欄位
        /// </summary>
        public string strSKU_MB013 { get; set; }
        /// <summary>
        /// 國際條碼(規格)
        /// </summary>
        public string strSKU { get; set; }
        /// <summary>
        /// 原售價
        /// </summary>
        public string strSRC_AMT { get; set; }
        /// <summary>
        /// 最低補貨量
        /// </summary>
        public string intLimitQTY { get; set; }
        /// <summary>
        /// 缺貨旗標,目前以"INVMB.MB081(尺寸)"做為依據,1->表缺貨,空白及其它表貨可補 
        /// </summary>
        public string strNOGoods_MB081Flag { get; set; }
        /// <summary>
        /// 陸客專區旗標(426) 20150527 新增
        /// </summary>
        public string strMB118_426 { get; set; }
        /// <summary>
        /// 20151218新增退貨判斷,A01可退貨,A04買斷品,A05自家品,A06一個月可退食品
        /// </summary>
        public string strMB115 { get; set; }
    }
}
