﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using WmAutoUpdate;
using JPMed_PDA.Utility;
using System.Xml;
using System.Diagnostics;

namespace JPMed_PDA
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [MTAThread]
        static void Main()
        {
            string openAutoUpdate = new ConfigSettings().WmAutoUpdate;
            string fileLocatione = new ConfigSettings().WmFileLocation;

            if (openAutoUpdate == "0") Application.Run(new MainForm(true));
            else
            {
                string osversion = Environment.OSVersion.ToString();

                if (osversion.Contains("Windows CE"))
                {
                    Updater updater = new Updater(fileLocatione);
                    bool needSyncTime = updater.CheckForNewVersion();
                    Application.Run(new MainForm(needSyncTime));
                }
                else
                {
                    Version currentVersion = System.Reflection.Assembly.GetCallingAssembly().GetName().Version;
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(fileLocatione);
                    XmlNodeList modules = xDoc.GetElementsByTagName("download");
                    XmlNodeList versions = xDoc.GetElementsByTagName("version");

                    Version newVersion = new Version(
                      int.Parse(versions[0].Attributes["maj"].Value),
                      int.Parse(versions[0].Attributes["min"].Value),
                      int.Parse(versions[0].Attributes["bld"].Value),
                      int.Parse(versions[0].Attributes["rev"].Value));

                    if (currentVersion.CompareTo(newVersion) < 0)
                    {
                        MessageBox.Show("請手動下載最新版本!");
                        return;
                    }
                    
                    Application.Run(new MainForm(false));
                }
            }
        }
    }
}