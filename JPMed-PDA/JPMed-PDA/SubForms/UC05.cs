﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC05 : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        static string p_spbup = string.Empty; //前週
        SQLCreator sql;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        DataTable defaultDT = null;
        string snNoForStore = string.Empty; //目前序號歸屬門市
        string SnNo = "ZUB1" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;
        string rushX;
        string wmflgX;

        public UC05()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            btnSave.Click += btnSave_Click;
            btnShowAll.Click += btnShowAll_Click;
            tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            tbEmpCode.KeyUp += tbEmpCode_KeyUp;
            tbSN.TextChanged += tbSN_TextChanged;
            tbSN.KeyUp += tbSN_KeyUp;
            tbQty.KeyDown += tbQty_KeyDown;
            cboTransferCode.KeyDown += cboTransferCode_KeyDown;
            cbDeliveryBy3PL.Click += cbDeliveryBy3PL_Click;
            cbDeliveryBySelf.Click += cbDeliveryBySelf_Click;
            cboTransferCode.SelectedIndexChanged += cboTransferCode_SelectedIndexChanged;
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            laDate.Text = DateTime.Today.ToString("D");
            cboTransferCode.Items.Clear();
            cboTransferCode.Enabled = false;

            laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
            tbSN.Text = string.Empty;
            tbSN.Enabled = false;
            laProductName.Text = string.Empty;

            tbQty.Text = string.Empty;
            tbQty.Enabled = false;

            btnShowAll.Enabled = false;
            btnSave.Enabled = false;

            dgList.DataSource = defaultDT;
            cbDeliveryBy3PL.Enabled = false; //20170411增加派車狀態
            cbDeliveryBySelf.Enabled = false; //20170411增加急單狀態
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC05");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得所有門市資訊，排除自己
                string sqlCmd = sql.GetCommand("GetAllSrote", new string[] { storeCode });
                DataTable tmpDt = dbConn.GetDataTable(sqlCmd);
                if (tmpDt.Rows.Count > 0)
                {
                    cboTransferCode.DisplayMember = "Text";
                    cboTransferCode.ValueMember = "Value";
                    cboTransferCode.DataSource = tmpDt;
                    cboTransferCode.SelectedIndex = -1;
                }
                else
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無[門市]資訊，請聯絡資訊人員!");
                }

                //取得店家資訊
                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
                else
                {
                    int term = PublicClass.GetIso8601WeekOfYear(DateTime.Now) - 1;
                    if (term == 0)
                    {
                        p_spbup = new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1).Year.ToString() +
                                  PublicClass.GetIso8601WeekOfYear(new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1)).ToString("00");
                    }
                    else
                    {
                        p_spbup = DateTime.Today.Year.ToString() + term.ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    btnShowAll.Enabled = _enbleControl;
                    //cboTransferCode.SelectedIndex = -1;
                    cboTransferCode.Enabled = _enbleControl;
                    laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;
                    laProductName.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    cbDeliveryBy3PL.Checked = false; ;//20170412增加
                    cbDeliveryBy3PL.Enabled = _enbleControl; //20170411增加派車狀態
                    cbDeliveryBySelf.Checked = true;//20170412增加；20170919修正為先勾自送 by Joseph
                    cbDeliveryBySelf.Enabled = _enbleControl;//20170411增加急單狀態
                    break;
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[調撥門市作業]?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    EnableControl(false, 1);
                    tbEmpCode.Focus();
                    tbEmpCode.SelectAll();
                    laEmpName.Text = "查無此編號";
                    laEmpName.ForeColor = Color.Red;
                    dgList.DataSource = defaultDT;
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    laEmpName.ForeColor = Color.Black;
                    EnableControl(true, 1);
                    cboTransferCode.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.SelectAll();
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbEmpCode.SelectAll();
            }
        }

        /// <summary>
        /// 產品Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbSN.SelectAll();
            }
        }
        private void tbSN_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchProduct();
                tbSN.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 輸入數量後按Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !string.IsNullOrEmpty(tbQty.Text))
            {
                if (btnSave.Enabled)
                    btnSave_Click(null, null);
            }
        }

        /// <summary>
        /// 選擇門市後按ENTER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboTransferCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && cboTransferCode.SelectedIndex != -1)
            {
                tbSN.Focus();
            }
        }

        private void cboTransferCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboTransferCode.SelectedIndex != -1)
            {
                cbDeliveryBy3PL.Checked = false; //20170412 變更門市時將派車急單重設
                cbDeliveryBy3PL.Enabled = true;
                cbDeliveryBySelf.Checked = true; //20170919修正為先勾自送 by Joseph
                cbDeliveryBySelf.Enabled = true;
            }
        }

        /// <summary>
        /// 查詢產品
        /// </summary>
        private void SearchProduct()
        {
            try
            {
                if (string.IsNullOrEmpty(tbSN.Text))
                    return;

                string tmpEan = tbSN.Text.ToUpper();

                if (string.IsNullOrEmpty(tmpEan))
                    return;

                bool loop = true;
                while (loop)
                {
                    if (tmpEan.StartsWith("0") && tmpEan.Length > 0)
                        tmpEan = tmpEan.Substring(1);
                    else
                    {
                        loop = false;
                        if (tmpEan.Length == 0) return;
                        else tbSN.Text = tmpEan;
                    }
                }

                string toStoreNo = "";

                //檢查調入門市是否已選擇
                if (cboTransferCode.SelectedIndex != -1)
                {
                    toStoreNo = cboTransferCode.SelectedValue.ToString();
                }
                else
                {
                    laProductName.Text = "請輸入調入門市";
                    laProductName.ForeColor = Color.Red;
                    return;
                }

                string sqlCmd = sql.GetCommand("GetProduct", new string[] { storeCode, toStoreNo, tbSN.Text });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0 && tmpDT.Rows.Count >= 2)
                {
                    if (tmpDT.Rows[0]["DELFG"].ToString() == "X")
                    {
                        laProductName.Text = "此商品[已廢番]，請重新輸入!";
                        laProductName.ForeColor = Color.Red;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        laEan11.Text = tmpDT.Rows[0]["EAN11"].ToString().Trim(); //20160919需求變更:抓EAN11 or EAN12
                        if (string.IsNullOrEmpty(laEan11.Text))
                        {
                            laProductName.Text = "此代碼查無對應之EAN11值";
                            laProductName.ForeColor = Color.Red;
                            btnSave.Enabled = false;
                            return;
                        }

                        laKey.Text = tmpDT.Rows[0]["MATNR"].ToString() + ";" + tmpDT.Rows[0]["MEINS"].ToString();
                        laProductName.Text = tmpDT.Rows[0]["MAKTX"].ToString();
                        laProductName.ForeColor = Color.Black;
                        btnSave.Enabled = true;
                        tbQty.Focus();
                        //eanReal = tbSN.Text;
                        eanReal = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                    }
                }
                else
                {
                    laProductName.Text = "門市查無此商品!";
                    laProductName.ForeColor = Color.Red;
                    btnSave.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢產品發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cbDeliveryBy3PL.Checked == false && cbDeliveryBySelf.Checked == false) //20170407 當派車註記都沒選擇時
            {
                MessageBox.Show("[未選擇運送方式!");
                btnSave.Enabled = false;
                return;
            }

            try
            {
                //檢查調入門市是否已選擇
                string toStoreNo = "";
                if (cboTransferCode.SelectedIndex != -1)
                {
                    toStoreNo = cboTransferCode.SelectedValue.ToString();
                }

                string sqlCmd = sql.GetCommand("GetProduct", new string[] { storeCode, toStoreNo, tbSN.Text });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count != 2)
                {
                    MessageBox.Show("門市查無此商品!");
                    btnSave.Enabled = false;
                    return;
                }
                //if (eanReal != tbSN.Text)
                if (eanReal != laEan11.Text) //20160919需求變更:抓EAN11 or EAN12
                {
                    MessageBox.Show("[商品條碼]已異動，請重新進行資料查詢!");
                    btnSave.Enabled = false;
                    return;
                }

                if (cboTransferCode.SelectedIndex == -1 || cboTransferCode.SelectedValue == null || string.IsNullOrEmpty(cboTransferCode.SelectedValue.ToString()))
                {
                    MessageBox.Show("請選擇調入門市!");
                    return;
                }

                if (string.IsNullOrEmpty(tbQty.Text))
                {
                    MessageBox.Show("調撥數量不可為空");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                //調撥數量僅可介於 0 < qty < 399
                int tempInt = 0;
                try
                {
                    tempInt = Int32.Parse(tbQty.Text);
                    if (tempInt <= 0 || tempInt >= 399)
                    {
                        MessageBox.Show("調撥數量需大於0且小於399!\n 0 < 調撥數量 < 399");
                        tbQty.Select(0, tbQty.Text.Length);
                        tbQty.Focus();
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("調撥數量僅可輸入\n[整數數值]!");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                #region 準備異動資訊

                //單型，自送使用ZUB1, 派車ZUB3
                string _BSART = "ZUB1";

                //禾頡派車
                if (cbDeliveryBy3PL.Checked == true && cbDeliveryBySelf.Checked == false)
                {
                    wmflgX = "X";
                    rushX = "";
                    _BSART = "ZUB3";
                }

                //自送
                if (cbDeliveryBy3PL.Checked == false && cbDeliveryBySelf.Checked == true)
                {
                    wmflgX = "";
                    rushX = "X";
                    _BSART = "ZUB1";
                }
                //20170407 增加派車註記及急單註記上傳至Middle DB

                //0:新增, 1:覆蓋
                int modifyType;

                SnNo = SnNo.Substring(8, 8) == DateTime.Today.ToString("yyyyMMdd") ? SnNo : _BSART + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");

                if (string.IsNullOrEmpty(snNoForStore))
                    snNoForStore = cboTransferCode.SelectedValue.ToString();
                else
                {
                    if (snNoForStore != cboTransferCode.SelectedValue.ToString())
                    {
                        string tmpBednr = _BSART + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");
                        while (tmpBednr == SnNo)
                            tmpBednr = _BSART + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");

                        SnNo = tmpBednr;
                        snNoForStore = cboTransferCode.SelectedValue.ToString();
                    }
                }

                string matnr = laKey.Text.Split(';')[0];
                //string ean = tbSN.Text;
                string ean = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                string reswk = storeCode;
                string werks = cboTransferCode.SelectedValue.ToString();
                string empCode = empCodeReal; //tbEmpCode.Text;
                string qty = tbQty.Text;
                string meins = laKey.Text.Split(';')[1];
                string menge_r = tbQty.Text;
                string rush = rushX;//急單註記
                string wmflg = wmflgX;//禾頡派車註記

                sqlCmd = sql.GetCommand("GetCheckCommand", new string[] { SnNo, matnr, ean, reswk, werks });
                tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    if (_BSART == "ZUB1" && tmpDT.Rows[0]["COMARK"].ToString() == "V")
                    {
                        MessageBox.Show("此商品無法進行調撥：\n請確認是否已存在相同單據內容，且審核狀態[已確認]");
                        return;
                    }
                    else if (tmpDT.Rows[0]["PROCD"].ToString() == "X")
                    {
                        MessageBox.Show("此商品無法進行調撥：\n請確認是否已存在相同單據內容，且SAP審核狀態[已處理]");
                        return;
                    }
                    else
                    {
                        modifyType = 1;

                        //更新
                        sqlCmd = sql.GetCommand("GetUpdateCommand", new string[] { SnNo, matnr, ean, reswk, werks, empCode, menge_r, rush, wmflg });
                    }//20170407增加GetUpdateCommand, rush, wmflg                    
                }
                else
                {
                    modifyType = 0;

                    sqlCmd = sql.GetCommand("GetBEDNRCheckCommand", new string[] { SnNo, reswk, werks });
                    DataTable BednrCheckDT = dbConn.GetDataTable(sqlCmd);

                    if (BednrCheckDT.Rows.Count > 0)
                    {
                        MessageBox.Show("此單據已建立");
                        return;
                    }
                    //新增
                    sqlCmd = sql.GetCommand("GetInsertCommand", new string[] { SnNo, matnr, ean, reswk, werks, empCode, menge_r, meins, rush, wmflg, _BSART });
                }//20170407增加GetInsertCommand, rush, wmflg
                #endregion

                DialogResult dialogResult = MessageBox.Show(modifyType == 0 ? "您確定新增調撥單?" : "您確定覆蓋調撥數量？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (dialogResult == DialogResult.Yes)
                {
                    int dbReturnCode = dbConn.ExecSql(sqlCmd);
                    if (dbReturnCode == 0)
                    {
                        MessageBox.Show("未更新任何項目，請聯繫管理人員");
                    }
                    else if (dbReturnCode == -1)
                    {
                        MessageBox.Show("單據已建立");
                    }
                    else
                    {
                        laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                        tbSN.Text = string.Empty;
                        tbSN.Focus();

                        //cboTransferCode.SelectedIndex = -1;
                        tbQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        laProductName.Text = string.Empty;

                        sqlCmd = sql.GetCommand("GetSelectCommand", new string[] { SnNo, matnr, ean, reswk, werks, p_spbup });
                        DataTable sourceDT = dbConn.GetDataTable(sqlCmd);
                        source.DataSource = sourceDT;
                        dgList.DataSource = source;
                        source.ResetBindings(false);
                        BindDataGridStyle(sourceDT);
                        MessageBox.Show(modifyType == 0 ? "新增成功!" : "修改成功");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 完整顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                UC05_1 uc = new UC05_1(SnNo, p_spbup, ref dbConn);
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.GetType() == typeof(PictureBox))
                        c.Visible = false;
                    else if (c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = false;
                    else if (c.GetType() == typeof(UC05))
                        c.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        this.Parent.Controls.Remove(c);

                if (needAdd)
                    this.Parent.Controls.Add(uc);

                this.Parent.Parent.Text = formName + " -- " + "調撥門市(資料查詢)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "調撥數量":
                    case "前週銷量":
                    case "調出門市庫存量":
                    case "調入門市庫存量":
                        tb.Format = "0.###";
                        break;
                }

                tb.Width = 150;
                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }

        /// <summary>
        /// 派車註記設定 3PL新增需求20170411 必選且只能二選一
        /// </summary>
        private void cbDeliveryBy3PL_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("確定物流派車嗎?", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                cbDeliveryBy3PL.Checked = true;
                cbDeliveryBy3PL.Enabled = false;
                cbDeliveryBySelf.Checked = false; //20170919修正為先勾自送 by Joseph
                //cbDeliveryBySelf.Enabled = false; //20170919修正為先勾自送 by Joseph

                SnNo = "ZUB3" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");

                if (tbQty.Text != string.Empty)
                {
                    btnSave.Enabled = true;
                }
            }
            if (dialogResult == DialogResult.No)
            {
                cbDeliveryBy3PL.Checked = false;
            }
        }

        /// <summary>
        /// 自送註記
        /// </summary>
        private void cbDeliveryBySelf_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("確定自送嗎?", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                cbDeliveryBy3PL.Checked = false;
                cbDeliveryBy3PL.Enabled = false;
                cbDeliveryBySelf.Checked = true;
                cbDeliveryBySelf.Enabled = false;

                SnNo = "ZUB1" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");

                if (tbQty.Text != string.Empty)
                {
                    btnSave.Enabled = true;
                }
            }
            if (dialogResult == DialogResult.No)
            {
                cbDeliveryBySelf.Checked = false;
            }
        }
    }
}
