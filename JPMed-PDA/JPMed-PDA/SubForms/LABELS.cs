﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class LABELS : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        static string PdaSn = new ConfigSettings().PdaSn;
        SQLCreator sql;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        DataTable defaultDT = null;
        string snNoForVendor = string.Empty;
        static string[] IPAddr;
        string SnNo;
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;
        DataTable dt = new DataTable();

        public LABELS()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();

            SnNo = GetSnNo();  
        }

        /// <summary>
        /// 取得流水號
        /// </summary>
        /// <returns></returns>
        public string GetSnNo()
        {
            string _SNNo;
            string osversion = Environment.OSVersion.ToString();
            if (osversion.Contains("Windows CE"))
            {
                IPAddr = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString().Split('.');
                _SNNo = storeCode + IPAddr[2].PadLeft(3, '0') + IPAddr[3].PadLeft(3, '0') + DateTime.Now.ToString("yyyyMMddHHmmss");
            }
            else
            {

                // 取得所有 IP 位址
                foreach (var ipaddress in System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList)
                {
                    // 只取得IP V4的Address
                    if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        IPAddr = ipaddress.ToString().Split('.');
                    }
                }
                _SNNo = storeCode + IPAddr[2].PadLeft(3, '0') + IPAddr[3].PadLeft(3, '0') + DateTime.Now.ToString("yyyyMMddHHmmss");
            }

            return _SNNo;
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            //btnSave.Click += btnSave_Click;
            //btnShowAll.Click += btnShowAll_Click;
            //tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            //tbEmpCode.KeyUp += tbEmpCode_KeyUp;
            tbSN.TextChanged += tbSN_TextChanged;
            tbSN.KeyUp += tbSN_KeyUp;
            //tbQty.KeyDown += tbQty_KeyDown;
            //cbRtnDisCode.KeyDown += cbRtnDisCode_KeyDown;
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            //tbEmpCode.Text = string.Empty;
            //laEmpName.Text = string.Empty;

            laDate.Text = DateTime.Today.ToString("D");
            laProductName.Text = string.Empty;
            laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
            tbSN.Text = string.Empty;
            //tbSN.Enabled = false;

            //cbRtnDisCode.Items.Clear();
            //cbRtnDisCode.Enabled = false;

            //tbQty.Text = string.Empty;
            //tbQty.Enabled = false;

            //btnShowAll.Enabled = false;
            //btnSave.Enabled = false;

            dgList.DataSource = defaultDT;

            laSPDANo.Text = PdaSn;

        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("LABELS");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                ////取得廠商資訊
                //string sqlCmd = sql.GetCommand("GetRe2vctInfo", new string[] { storeCode });
                //DataTable tmpDt = dbConn.GetDataTable(sqlCmd);
                //if (tmpDt.Rows.Count > 0)
                //{
                //    DataTable sourceDT = new DataTable();
                //    sourceDT.Columns.Add("Text", typeof(string));
                //    sourceDT.Columns.Add("Value", typeof(string));
                //    foreach (DataRow row in tmpDt.AsEnumerable())
                //    {
                //        DataRow newRow = sourceDT.NewRow();
                //        newRow["Value"] = row["Value"].ToString();

                //        try { newRow["Text"] = Decimal.Parse(row["Code"].ToString()).ToString("#") + "- " + row["Name"].ToString(); }
                //        catch { newRow["Text"] = row["Code"].ToString() + "- " + row["Name"].ToString(); }

                //        sourceDT.Rows.Add(newRow);
                //    }

                //    //cbRtnDisCode.DisplayMember = "Text";
                //    //cbRtnDisCode.ValueMember = "Value";
                //    //cbRtnDisCode.DataSource = sourceDT;
                //    //cbRtnDisCode.SelectedIndex = -1;
                //}
                //else
                //{
                //    EnableControl(false, 0);
                //    MessageBox.Show("查無[直退廠商]資訊，請聯絡資訊人員!");
                //}

                //取得店家資訊
                string sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }

                //tbEmpCode.Focus();
                tbSN.Focus();
            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    //tbEmpCode.Text = string.Empty;
                    //tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    //btnShowAll.Enabled = _enbleControl;
                    laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;
                    laProductName.Text = string.Empty;
                    //cbRtnDisCode.SelectedIndex = -1;
                    //cbRtnDisCode.Enabled = _enbleControl;
                    //tbQty.Text = string.Empty;
                    //tbQty.Text = "1";
                    //tbQty.Enabled = _enbleControl;
                    //btnSave.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[行動補標作業]?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        ///// <summary>
        ///// 取得使用者名稱
        ///// </summary>
        ///// <param name="_empCode"></param>
        //private void GetEmpName(string _empCode)
        //{
        //    try
        //    {
        //        tbEmpCode.Text = tbEmpCode.Text.ToUpper();
        //        string empName = string.Empty;
        //        string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
        //        empName = dbConn.GetDataTableRecData(sqlCmd);

        //        if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
        //        {
        //            EnableControl(false, 1);
        //            tbEmpCode.Focus();
        //            tbEmpCode.SelectAll();
        //            laEmpName.Text = "查無此編號";
        //            laEmpName.ForeColor = Color.Red;
        //            dgList.DataSource = defaultDT;
        //        }
        //        else
        //        {
        //            laEmpName.Text = empName;
        //            empCodeReal = tbEmpCode.Text;
        //            //tbEmpCode.Enabled = false;
        //            laEmpName.ForeColor = Color.Black;
        //            EnableControl(true, 1);
        //            tbSN.Focus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
        //    }
        //}

        ///// <summary>
        ///// 使用者代碼change
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void tbEmpCode_TextChanged(object sender, EventArgs e)
        //{
        //    if (tbEmpCode.Text.Length == 6)
        //    {
        //        tbEmpCode.SelectAll();
        //        GetEmpName(tbEmpCode.Text);
        //    }
        //}

        //private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        //{
        //    AfterInput = ((TextBox)sender).Text.Length;
        //    aftStr = ((TextBox)sender).Text;

        //    if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
        //        AfterInput == tbSN.MaxLength || //MaxLength
        //        (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
        //    {
        //        SearchProduct();
        //        tbEmpCode.SelectAll();
        //    }
        //}

        /// <summary>
        /// 產品Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbSN.SelectAll();

                //  直接存入DB
                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    if (tbSN.Text != string.Empty)
                    {
                        btnSave_Click(null, null);
                    }
                    else
                    {
                        laEan11.Text = string.Empty;
                        laProductName.Text = "請輸入商品條碼(EAN)!";
                        laProductName.ForeColor = Color.Red;
                    }
                }
            }
        }
        private void tbSN_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchProduct();
                tbSN.SelectAll();

                //  直接存入DB
                if ((tbSN.Text != string.Empty) && (dt != null) && (dt.Rows.Count > 0))
                {
                    btnSave_Click(null, null);

                }

            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 輸入數量後按Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //if(btnSave.Enabled)
                if (tbSN.Text != string.Empty)
                {
                    btnSave_Click(null, null);
                }
                else
                {
                    laEan11.Text = string.Empty;
                    laProductName.Text = "請輸入商品條碼(EAN)!";
                    laProductName.ForeColor = Color.Red;
                }
            }
        }

        ///// <summary>
        ///// 選擇廠商後按ENTER
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void cbRtnDisCode_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        tbQty.Focus();
        //    }
        //}


        /// <summary>
        /// 查詢產品
        /// </summary>
        private void SearchProduct()
        {
            try
            {
                if (string.IsNullOrEmpty(tbSN.Text))
                    return;

                string tmpEan = tbSN.Text.ToUpper();

                if (string.IsNullOrEmpty(tmpEan))
                    return;

                bool loop = true;
                while (loop)
                {
                    if (tmpEan.StartsWith("0") && tmpEan.Length > 0)
                        tmpEan = tmpEan.Substring(1);
                    else
                    {
                        loop = false;
                        if (tmpEan.Length == 0) return;
                        else tbSN.Text = tmpEan;
                    }
                }

                string sqlCmd = sql.GetCommand("GetProduct", new string[] { storeCode, tbSN.Text });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count >= 2)
                {
                    laEan11.Text = string.Empty;
                    laProductName.Text = "主檔建立錯誤，有雙條碼!";
                    laProductName.ForeColor = Color.Red;
                    //btnSave.Enabled = false;
                    dt = null;
                    return;
                }
                else if (tmpDT.Rows.Count > 0)
                {
                    laEan11.Text = tmpDT.Rows[0]["KSCHL2"].ToString().Trim(); //20160919需求變更:抓EAN11 or EAN12
                    if (string.IsNullOrEmpty(laEan11.Text))
                    {
                        laEan11.Text = string.Empty;
                        laProductName.Text = "此代碼查無對應之分類值";
                        laProductName.ForeColor = Color.Red;
                        //btnSave.Enabled = false;
                        dt = null;
                        return;
                    }

                    laKey.Text = tmpDT.Rows[0]["MATNR"].ToString() + ";" + tmpDT.Rows[0]["MEINS"].ToString();
                    laProductName.Text = tmpDT.Rows[0]["MAKTX"].ToString();
                    laProductName.ForeColor = Color.Black;
                    //btnSave.Enabled = true;
                    //cbRtnDisCode.Focus();
                    //eanReal = tbSN.Text;
                    //eanReal = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                    dt = tmpDT;
                }
                else
                {
                    laEan11.Text = string.Empty;
                    laProductName.Text = "銷貨主檔查無此商品!";
                    laProductName.ForeColor = Color.Red;
                    //btnSave.Enabled = false;
                    dt = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢產品發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (eanReal != tbSN.Text)
                //if (eanReal != laEan11.Text) //20160919需求變更:抓EAN11 or EAN12
                //{
                //    MessageBox.Show("[商品條碼]已異動，請重新進行資料查詢!");
                //    //btnSave.Enabled = false;
                //    return;
                //}

                //if (cbRtnDisCode.SelectedIndex == -1 || cbRtnDisCode.SelectedValue == null || String.IsNullOrEmpty(cbRtnDisCode.SelectedValue.ToString()))
                //{
                //    MessageBox.Show("請選擇退貨廠商!");
                //    return;
                //}

                //if (string.IsNullOrEmpty(tbQty.Text))
                //{
                //    MessageBox.Show("印製量不可為空");
                //    tbQty.Select(0, tbQty.Text.Length);
                //    tbQty.Focus();
                //    return;
                //}

                ////退貨數量僅可介於 0 < qty < 399
                //decimal tmpDecimal = 0;
                //try
                //{
                //    tmpDecimal = decimal.Parse(tbQty.Text);
                //    if(tmpDecimal <= 0 || tmpDecimal >= 399)
                //    {
                //        MessageBox.Show("退貨數量需大於0且小於399!\n 0 < 退貨數量 < 399");
                //        tbQty.Select(0, tbQty.Text.Length);
                //        tbQty.Focus();
                //        return;
                //    }
                //}
                //catch
                //{
                //    MessageBox.Show("退貨數量僅可輸入\n[數值]及[小數]!");
                //    tbQty.Select(0, tbQty.Text.Length);
                //    tbQty.Focus();
                //    return;
                //}

                ////20160919需求變更:需檢查退貨之商品，是否存在於INFORED(主次供對應表)
                //string sqlCmd = sql.GetCommand("CheckHasInfored", new string[] { cbRtnDisCode.SelectedValue.ToString(), laKey.Text.Split(';')[0] });
                //DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                //if (tmpDT.Rows.Count == 0)
                //{
                //    MessageBox.Show("此商品查無主次供對應紀錄!");
                //    btnSave.Enabled = false;
                //    return;
                //}
                //else if(tmpDT.Rows[0][0].ToString() == "X")
                //{
                //    MessageBox.Show("此廠商非此商品之主供或次供，不允許退貨!");
                //    btnSave.Enabled = false;
                //    return;
                //}

                #region 準備異動資訊

                //0:新增, 1:覆蓋
                int modifyType;

                //IPAddr = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString().Split('.');
                //SnNo = storeCode + IPAddr[2].PadLeft(3, '0') + IPAddr[3].PadLeft(3, '0') + DateTime.Now.ToString("yyyyMMddHHmmss");
                SnNo = GetSnNo();
                //SnNo = SnNo.Substring(8, 8) == DateTime.Today.ToString("yyyyMMdd") ? SnNo : storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");

                //if (string.IsNullOrEmpty(snNoForVendor))
                //    snNoForVendor = cbRtnDisCode.SelectedValue.ToString();
                //else
                //{
                //    if (snNoForVendor != cbRtnDisCode.SelectedValue.ToString())
                //    {
                //        string tmpBednr = "ZRE3" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");
                //        while (tmpBednr == SnNo)
                //            tmpBednr = "ZRE3" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");

                //        SnNo = tmpBednr;
                //        snNoForVendor = cbRtnDisCode.SelectedValue.ToString();
                //    }
                //}

                string matnr = laKey.Text.Split(';')[0];
                string ean = tbSN.Text;
                //string ean = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                string werks = storeCode;
                //string lifnr = cbRtnDisCode.SelectedValue.ToString();
                string empCode = empCodeReal; //tbEmpCode.Text;
                //string qty = tbQty.Text;
                string meins = laKey.Text.Split(';')[1];
                string menge_m = "1";
                string crdate = DateTime.Now.ToString("yyyyMMdd");
                string maktx = string.Empty;
                string matkl = string.Empty;
                string chtme = DateTime.Now.ToString("HHmmss");

                string sqlCmd = sql.GetCommand("GetCheckCommand", new string[] { matnr, ean, werks, PdaSn });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    //    if (tmpDT.Rows[0]["COMARK"].ToString() == "V")
                    //    {
                    //        MessageBox.Show("此商品無法進行退貨：\n請確認是否已存在相同單據內容，且審核狀態[已確認]");
                    //        return;
                    //    }
                    //    else if (tmpDT.Rows[0]["PROCD"].ToString() == "X")
                    //    {
                    //        MessageBox.Show("此商品無法進行退貨：\n請確認是否已存在相同單據內容，且SAP審核狀態[已處理]");
                    //        return;
                    //    }
                    //    else
                    //    {
                    modifyType = 1;

                    //更新
                    sqlCmd = sql.GetCommand("GetUpdateCommand", new string[] { tmpDT.Rows[0]["BEDNR"].ToString(), matnr, ean, werks, (Convert.ToInt32(tmpDT.Rows[0]["MENGE"].ToString()) + 1).ToString(), crdate, chtme, PdaSn });
                    //    }
                }
                else
                {
                    modifyType = 0;
                    if (dt.Rows.Count > 0)
                    {
                        matnr = dt.Rows[0]["MATNR"].ToString();
                        maktx = dt.Rows[0]["MAKTX"].ToString().Replace("'","''");
                        matkl = dt.Rows[0]["KSCHL2"].ToString();
                    }

                    //新增
                    sqlCmd = sql.GetCommand("GetInsertCommand", new string[] { SnNo, crdate, werks, ean, matnr, maktx, menge_m, matkl, crdate, chtme, PdaSn });
                }
                #endregion

                //DialogResult dialogResult = MessageBox.Show(modifyType == 0 ? "您確定新增退貨單?" : "您確定覆蓋退貨數量？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                //if (dialogResult == DialogResult.Yes)
                //{
                if (dbConn.ExecSql(sqlCmd) == 0)
                {
                    MessageBox.Show("未更新任何項目，請聯繫管理人員");
                }
                else
                {
                    //        laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    //        tbSN.Text = string.Empty;
                    //        tbSN.Focus();

                    //        //cbRtnDisCode.SelectedIndex = -1;
                    //        tbQty.Text = string.Empty;
                    //        btnSave.Enabled = false;
                    //        laProductName.Text = string.Empty;

                    sqlCmd = sql.GetCommand("GetSelectCommand", new string[] { werks, PdaSn });
                    DataTable sourceDT = dbConn.GetDataTable(sqlCmd);
                    source.DataSource = sourceDT;
                    dgList.DataSource = source;
                    source.ResetBindings(false);
                    BindDataGridStyle(sourceDT);
                    //MessageBox.Show(modifyType == 0 ? "新增成功" : "修改成功!");
                    tbSN.Text = string.Empty;
                }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        ///// <summary>
        ///// 完整顯示
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void btnShowAll_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool needAdd = true;
        //        List<Control> rmc = new List<Control>();
        //        UC08_1 uc = new UC08_1(SnNo, ref dbConn);
        //        foreach (Control c in this.Parent.Controls)
        //        {
        //            if (c.GetType() == typeof(PictureBox))
        //                c.Visible = false;
        //            else if (c.Name == "laTimeStr" || c.Name == "laVersion")
        //                c.Visible = false;
        //            else if (c.GetType() == typeof(UC08))
        //                c.Visible = false;
        //            else if (c.Name == uc.Name)
        //            {
        //                c.Visible = true;
        //                needAdd = false;
        //            }
        //            else
        //                rmc.Add(c);
        //        }

        //        if (rmc.Count > 0)
        //            foreach (Control c in rmc)
        //                this.Parent.Controls.Remove(c);

        //        if (needAdd)
        //            this.Parent.Controls.Add(uc);

        //        this.Parent.Parent.Text = formName + " -- " + "直退場商(資料查詢)";
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "國際貨品代碼":
                        tb.Width = 90;
                        break;
                    case "物料說明":
                        tb.Width = 150;
                        break;
                    case "印製量":
                        tb.Width = 50;
                        break;
                    case "物料號碼":
                        tb.Width = 130;
                        break;
                }

                //if (col.ColumnName == "退貨確認") tb.Width = 80;
                //else tb.Width = 150;

                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
