﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC03 : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        static string p_spbup = string.Empty; //前週
        SQLCreator sql;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        DataTable defaultDT = null;
        DateTime nextRepDate; //系統預設交易日
        bool lockProduct = false;
        string SnNo = "ZRP1" + storeCode + "{0}000001";
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;

        public UC03()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            btnSave.Click += btnSave_Click;
            btnShowAll.Click += btnShowAll_Click;
            tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            tbEmpCode.KeyUp += tbEmpCode_KeyUp;
            tbSN.TextChanged += tbSN_TextChanged;
            tbSN.KeyUp += tbSN_KeyUp;
            tbQty.KeyDown += tbQty_KeyDown;
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            cbSopen.Checked = false;
            cbSopen.Enabled = false;
            laSdate.Text = string.Empty;
            laEdate.Text = string.Empty;
            laDate.Text = string.Empty;

            laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
            tbSN.Text = string.Empty;
            tbSN.Enabled = false;
            laProductName.Text = string.Empty;
            laStockNum.Text = string.Empty;
            laSpbup.Text = string.Empty;
            laInQty.Text = string.Empty;

            tbQty.Text = string.Empty;
            tbQty.Enabled = false;

            btnShowAll.Enabled = false;
            btnSave.Enabled = false;

            dgList.DataSource = defaultDT;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC03");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得特開日期 & 設定補貨日期
                string sqlCmd = sql.GetCommand("CheckSpecialOpen", new string[] { storeCode });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    cbSopen.Checked = true;
                    laSdate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLSD"].ToString()).ToString("D");
                    laEdate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLDD"].ToString()).ToString("D");
                    laDate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLDD"].ToString()).ToString("D");
                    nextRepDate = DateTime.Parse(tmpDT.Rows[0]["REPLDD"].ToString());
                }
                else
                {
                    int dayOfWeek = Convert.ToInt16(DateTime.Now.DayOfWeek); //今日星期幾
                    if (dayOfWeek > 0 && dayOfWeek < 4)
                    {
                        //下個星期4
                        laDate.Text = DateTime.Today.AddDays(dayOfWeek * -1).AddDays(4).ToString("D");
                        nextRepDate = DateTime.Today.AddDays(dayOfWeek * -1).AddDays(4);
                        lockProduct = true; //週四限進倉貨
                    }
                    else
                    {
                        //下個星期1
                        laDate.Text = DateTime.Today.AddDays((dayOfWeek == 0 ? 7 : dayOfWeek) * -1).AddDays(8).ToString("D");
                        nextRepDate = DateTime.Today.AddDays((dayOfWeek == 0 ? 7 : dayOfWeek) * -1).AddDays(8);
                    }
                }

                //設定序號
                SnNo = string.Format(SnNo, nextRepDate.ToString("yyyyMMdd"));

                //取得店家資訊
                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
                else
                {
                    int term = PublicClass.GetIso8601WeekOfYear(DateTime.Now) - 1;
                    if (term == 0)
                    {
                        p_spbup = new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1).Year.ToString() +
                                  PublicClass.GetIso8601WeekOfYear(new DateTime(DateTime.Today.Year, 01, 01).AddDays(-1)).ToString("00");
                    }
                    else
                    {
                        p_spbup = DateTime.Today.Year.ToString() + term.ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    btnShowAll.Enabled = _enbleControl;
                    laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;
                    laProductName.Text = string.Empty;
                    laStockNum.Text = string.Empty;
                    laSpbup.Text = string.Empty;
                    laInQty.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[補貨作業]?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    EnableControl(false, 1);
                    tbEmpCode.Focus();
                    tbEmpCode.SelectAll();
                    laEmpName.Text = "查無此編號";
                    laEmpName.ForeColor = Color.Red;
                    dgList.DataSource = defaultDT;
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    laEmpName.ForeColor = Color.Black;
                    EnableControl(true, 1);
                    tbSN.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.SelectAll();
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbEmpCode.SelectAll();
            }
        }

        /// <summary>
        /// 產品Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbSN.SelectAll();
            }
        }
        private void tbSN_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchProduct();
                tbSN.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 輸入數量後按Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !string.IsNullOrEmpty(tbQty.Text))
            {
                if (btnSave.Enabled)
                    btnSave_Click(null, null);
            }
        }

        /// <summary>
        /// 查詢產品
        /// </summary>
        private void SearchProduct()
        {
            try
            {
                string tmpEan = tbSN.Text.ToUpper();

                if (string.IsNullOrEmpty(tmpEan))
                    return;

                bool loop = true;
                while (loop)
                {
                    if (tmpEan.StartsWith("0") && tmpEan.Length > 0)
                        tmpEan = tmpEan.Substring(1);
                    else
                    {
                        loop = false;
                        if (tmpEan.Length == 0) return;
                        else tbSN.Text = tmpEan;
                    }
                }

                string sqlCmd = sql.GetCommand("GetProduct", new string[] { storeCode, tbSN.Text, p_spbup });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    //廢番
                    if (tmpDT.Rows[0]["DELFG"].ToString().Trim() == "X")
                    {
                        laProductName.Text = "此商品[已廢番]，請重新輸入!";
                        laProductName.ForeColor = Color.Red;
                        laStockNum.Text = string.Empty;
                        laSpbup.Text = string.Empty;
                        laInQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        return;
                    }

                    //鋪貨清單失效
                    if (tmpDT.Rows[0]["TMOUT"].ToString().Trim() == "X")
                    {
                        laProductName.Text = "此商品[鋪貨清單失效]不可使用，請重新輸入!";
                        laProductName.ForeColor = Color.Red;
                        laStockNum.Text = string.Empty;
                        laSpbup.Text = string.Empty;
                        laInQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        return;
                    }

                    //週四限進倉
                    if (lockProduct && tmpDT.Rows[0]["BWSCL"].ToString().Trim() != "4" && !string.IsNullOrEmpty(tmpDT.Rows[0]["BWSCL"].ToString()) && tmpDT.Rows[0]["BWSCL"].ToString() != " ")
                    {
                        laProductName.Text = "週四補貨限進倉貨，請重新輸入!";
                        laProductName.ForeColor = Color.Red;
                        laStockNum.Text = string.Empty;
                        laSpbup.Text = string.Empty;
                        laInQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        return;
                    }


                    laEan11.Text = tmpDT.Rows[0]["EAN11"].ToString().Trim(); //20160919需求變更:抓EAN11 or EAN12
                    if (string.IsNullOrEmpty(laEan11.Text))
                    {
                        laProductName.Text = "此代碼查無對應之EAN11值";
                        laProductName.ForeColor = Color.Red;
                        laStockNum.Text = string.Empty;
                        laSpbup.Text = string.Empty;
                        laInQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        return;
                    }

                    string realSnNo = SnNo;
                    //使單號與種類一致
                    if (!(tmpDT.Rows[0]["BWSCL"].ToString().Trim() == "4" || string.IsNullOrEmpty(tmpDT.Rows[0]["BWSCL"].ToString().Trim()) || tmpDT.Rows[0]["BWSCL"].ToString().Trim() == " "))
                        realSnNo = realSnNo.Replace("ZRP1", "ZRP2");

                    //sqlCmd = sql.GetCommand("GetPrdInQty", new string[] { realSnNo, tmpDT.Rows[0]["MATNR"].ToString(), tbSN.Text, "TDC1", storeCode });
                    sqlCmd = sql.GetCommand("GetPrdInQty", new string[] { realSnNo, tmpDT.Rows[0]["MATNR"].ToString(), laEan11.Text, "TDC2", storeCode }); //20160919需求變更:抓EAN11 or EAN12
                    string inQty = dbConn.GetDataTableRecData(sqlCmd);
                    laInQty.Text = string.IsNullOrEmpty(inQty) ? "0" : decimal.Parse(inQty).ToString("0.###");

                    laKey.Text = tmpDT.Rows[0]["MATNR"].ToString() + ";" + tmpDT.Rows[0]["MEINS"].ToString() + ";" + tmpDT.Rows[0]["BWSCL"].ToString().Trim();
                    laProductName.Text = tmpDT.Rows[0]["MAKTX"].ToString();
                    laProductName.ForeColor = Color.Black;
                    laStockNum.Text = decimal.Parse(tmpDT.Rows[0]["LABST"].ToString()).ToString("0.###");
                    laSpbup.Text = decimal.Parse(tmpDT.Rows[0]["UMMENGE"].ToString()).ToString("0.###");
                    btnSave.Enabled = true;
                    tbQty.Focus();
                    //eanReal = tbSN.Text;
                    eanReal = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                }
                else
                {
                    laProductName.Text = "門市查無此商品!";
                    laProductName.ForeColor = Color.Red;
                    laStockNum.Text = string.Empty;
                    laSpbup.Text = string.Empty;
                    laInQty.Text = string.Empty;
                    btnSave.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢產品發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (eanReal != tbSN.Text) 
                if (eanReal != laEan11.Text) //20160919需求變更:抓EAN11 or EAN12
                {
                    MessageBox.Show("[商品條碼]已異動，請重新進行資料查詢!");
                    btnSave.Enabled = false;
                    return;
                }

                if (string.IsNullOrEmpty(tbQty.Text))
                {
                    MessageBox.Show("調撥數量不可為空");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                //現補量僅可介於 0 < qty < 399
                int tempInt = 0;
                try
                {
                    tempInt =Int32.Parse(tbQty.Text);
                    if (tempInt <= 0 || tempInt >= 399)
                    {
                        MessageBox.Show("現補量需大於0且小於399!\n 0 < 現補量 < 399");
                        tbQty.Select(0, tbQty.Text.Length);
                        tbQty.Focus();
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("現補量僅可輸入\n[整數數值]!");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                #region 準備異動資訊

                //0:新增, 1:覆蓋
                int modifyType;

                #region 檢查日期是否變更，有則彈跳提示訊息[重新進入此畫面]
                DateTime tmpDate;
                //取得特開日期 & 設定補貨日期
                string sqlCommandd = sql.GetCommand("CheckSpecialOpen", new string[] { storeCode });
                DataTable tempDT = dbConn.GetDataTable(sqlCommandd);
                if (tempDT.Rows.Count > 0)
                    tmpDate = DateTime.Parse(tempDT.Rows[0]["REPLDD"].ToString());
                else
                {
                    int dayOfWeek = Convert.ToInt16(DateTime.Now.DayOfWeek); //今日星期幾
                    if (dayOfWeek > 0 && dayOfWeek < 4) tmpDate = DateTime.Today.AddDays(dayOfWeek * -1).AddDays(4);
                    else tmpDate = DateTime.Today.AddDays((dayOfWeek == 0 ? 7 : dayOfWeek) * -1).AddDays(8);
                }

                if (tmpDate != nextRepDate)
                {
                    MessageBox.Show("日期已變更，請關閉此功能後重新進入!");
                    return;
                }
                #endregion

                string matnr = laKey.Text.Split(';')[0];
                //string ean = tbSN.Text;
                string ean = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                string reswk = "TDC2";
                string werks = storeCode;
                string badat = nextRepDate.ToString("yyyyMMdd");
                string bsart_b = laKey.Text.Split(';')[2] == "4" || string.IsNullOrEmpty(laKey.Text.Split(';')[2]) || laKey.Text.Split(';')[2] == " " ? "ZRP1" : "ZRP2";
                string empCode = empCodeReal; //tbEmpCode.Text;
                string qty = tbQty.Text;
                string meins = laKey.Text.Split(';')[1];
                string menge_b = tbQty.Text;
                string realSnNo = SnNo;

                //使單號與種類一致
                if (!(laKey.Text.Split(';')[2] == "4" || string.IsNullOrEmpty(laKey.Text.Split(';')[2]) || laKey.Text.Split(';')[2] == " "))
                    realSnNo = realSnNo.Replace("ZRP1", "ZRP2");

                string sqlCmd = sql.GetCommand("GetCheckCommand", new string[] { realSnNo, matnr, ean, reswk, werks });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    if (tmpDT.Rows[0]["PROCD"].ToString() == "X")
                    {
                        MessageBox.Show("此商品無法進行補貨：\n請確認是否已存在相同單據內容，且SAP審核狀態[已處理]");
                        return;
                    }
                    else
                    {
                        modifyType = 1;

                        //更新
                        sqlCmd = sql.GetCommand("GetUpdateCommand", new string[] { realSnNo, matnr, ean, reswk, werks, empCode, menge_b, bsart_b });
                    }
                }
                else
                {
                    modifyType = 0;

                    //新增
                    sqlCmd = sql.GetCommand("GetInsertCommand", new string[] { realSnNo, matnr, ean, reswk, werks, empCode, menge_b, meins, badat, bsart_b });
                }
                #endregion

                DialogResult dialogResult = MessageBox.Show(modifyType == 0 ? "您確定新增補貨單?" : "您確定覆蓋補貨數量？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (dialogResult == DialogResult.Yes)
                {
                    

                    if (dbConn.ExecSql(sqlCmd) == 0)
                    {
                        MessageBox.Show("未更新任何項目，請聯繫管理人員");
                    }
                    else
                    {
                        laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                        tbSN.Text = string.Empty;
                        tbSN.Focus();

                        laStockNum.Text = string.Empty;
                        laSpbup.Text = string.Empty;
                        tbQty.Text = string.Empty;
                        laInQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        laProductName.Text = string.Empty;

                        sqlCmd = sql.GetCommand("GetSelectCommand", new string[] { realSnNo, matnr, ean, reswk, werks, p_spbup });
                        DataTable sourceDT = dbConn.GetDataTable(sqlCmd);
                        source.DataSource = sourceDT;
                        dgList.DataSource = source;
                        source.ResetBindings(false);
                        BindDataGridStyle(sourceDT);
                        MessageBox.Show(modifyType == 0 ? "新增成功!" : "修改成功");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 完整顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                //丟給後面頁面查詢時使用，組成SQL IN 語法 ('SN', 'SN')
                List<string> snList = new List<string>();
                snList.Add("'" + SnNo + "'");
                snList.Add("'" + SnNo.Replace("ZRP1", "ZRP2") + "'");

                #region 檢查日期是否變更，有則彈跳提示訊息[重新進入此畫面]
                DateTime tmpDate;
                //取得特開日期 & 設定補貨日期
                string sqlCommandd = sql.GetCommand("CheckSpecialOpen", new string[] { storeCode });
                DataTable tempDT = dbConn.GetDataTable(sqlCommandd);
                if (tempDT.Rows.Count > 0)
                    tmpDate = DateTime.Parse(tempDT.Rows[0]["REPLDD"].ToString());
                else
                {
                    int dayOfWeek = Convert.ToInt16(DateTime.Now.DayOfWeek); //今日星期幾
                    if (dayOfWeek > 0 && dayOfWeek < 4) tmpDate = DateTime.Today.AddDays(dayOfWeek * -1).AddDays(4);
                    else tmpDate = DateTime.Today.AddDays((dayOfWeek == 0 ? 7 : dayOfWeek) * -1).AddDays(8);
                }

                if (tmpDate != nextRepDate)
                {
                    MessageBox.Show("日期已變更，請關閉此功能後重新進入!");
                    return;
                }
                #endregion

                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                UC03_1 uc = new UC03_1(string.Join(",", snList.ToArray()), p_spbup, ref dbConn);
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.GetType() == typeof(PictureBox))
                        c.Visible = false;
                    else if (c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = false;
                    else if (c.GetType() == typeof(UC03))
                        c.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        this.Parent.Controls.Remove(c);

                if (needAdd)
                    this.Parent.Controls.Add(uc);

                this.Parent.Parent.Text = formName + " -- " + "補貨(資料查詢)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "現補量":
                    case "庫存量":
                    case "前週銷量":
                        tb.Format = "0.###";
                        break;
                }

                if (col.ColumnName == "進倉貨") tb.Width = 60;
                else tb.Width = 150;
                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
