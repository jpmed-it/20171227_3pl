﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;
using JPMed_PDA.Entity;

namespace JPMed_PDA.SubForms
{
    /// <summary>
    /// 物流到貨作業 UC10
    /// </summary>
    public partial class UC10 : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        DataTable defaultDT = null;
        string snNoForStore = string.Empty; //目前序號歸屬門市, 發貨門市
        string prevBENDR = string.Empty; //前次輸入的單號


        public UC10()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            tbEmpCode.TextChanged += new EventHandler(tbEmpCode_TextChanged);
            tbEmpCode.KeyUp += new KeyEventHandler(tbEmpCode_KeyUp);
            btnExit.Click += new EventHandler(btnExit_Click);
            btnShowAll.Click += new EventHandler(btnShowAll_Click);
            btnSave.Click += new EventHandler(btnSave_Click);
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            btnShowAll.Enabled = false;
            btnSave.Enabled = false;
            tbBENDR.Enabled = false;

            dgList.DataSource = defaultDT;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得店家資訊
                string sqlCmd = SqlStatement.UC09.GetStoreNameByCode(storeCode);
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    btnShowAll.Enabled = _enbleControl;
                    tbBENDR.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    Common.SelectAndFocus(tbBENDR);
                    break;
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult =
                MessageBox.Show("您確定關閉功能:[物流到貨作業]?", "警告",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        #region 使用者名稱相關
        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = SqlStatement.UC10.GetEmployeeNameByCode(tbEmpCode.Text);
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    EnableControl(false, 1);
                    tbEmpCode.Focus();
                    tbEmpCode.SelectAll();
                    laEmpName.Text = "查無此編號";
                    laEmpName.ForeColor = Color.Red;
                    dgList.DataSource = defaultDT;
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    laEmpName.ForeColor = Color.Black;
                    EnableControl(true, 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.SelectAll();
                GetEmpName(tbEmpCode.Text);
            }
        }

        /// <summary>
        /// 使用者代碼按下Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    tbEmpCode.SelectAll();
            //    GetEmpName(tbEmpCode.Text);
            //}
        }
        #endregion 使用者名稱相關

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> prdList = new List<string>();
                List<string> listKey = new List<string>();

                //GMOVET DataTable(特定單號的物流異動資料)
                DataTable gmovetDT;

                //檢查GMOVET單號
                if (tbBENDR.Text.Length == 0)
                { MessageBox.Show("請輸入單號"); return; }
                DataTable gmovehDT = new DataTable();

                string sqlCmd = string.Empty;
                string tmpSN = string.Empty;
                string vbeln = tbBENDR.Text;
                string eblen = string.Empty;

                //物流到貨只判斷 VBELN 20170506
                sqlCmd = SqlStatement.UC10.GetGmovetByVbeln(tbBENDR.Text);
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    tmpSN = tmpDT.Rows[0]["EBELN"] == null ?
                                   tmpDT.Rows[0]["BEDNR"] == null ? "" : tmpDT.Rows[0]["BEDNR"].ToString() :
                                   tmpDT.Rows[0]["EBELN"].ToString();

                    if (string.IsNullOrEmpty(tmpSN))
                    {
                        MessageBox.Show("此單號查無對應單據，請確認!");
                        return;
                    }

                    prdList = (from row in tmpDT.AsEnumerable()
                               select "'" + row["EAN11"].ToString() + "'").ToList();
                    sqlCmd = SqlStatement.UC10.GetGmovehInfo(tmpSN);
                    gmovehDT = dbConn.GetDataTable(sqlCmd);
                }
                else if (tbBENDR.Text.ToUpper().IndexOf("ZUB") != -1)
                {
                    sqlCmd = SqlStatement.UC10.GetGmovehInfo(tbBENDR.Text);
                    gmovehDT = dbConn.GetDataTable(sqlCmd);
                    if (gmovehDT.Rows.Count > 0)
                    {
                        tmpSN = gmovehDT.Rows[0]["BEDNR"].ToString();
                    }
                }
                else
                {
                    //額外從REPLEN(補貨資料中)抓取單型，判斷是否為ZUB4。若為ZUB4不做641檢查。
                    sqlCmd = SqlStatement.UC10.GetREPLENByBednrOrEbeln(storeCode, tbBENDR.Text);
                    gmovehDT = dbConn.GetDataTable(sqlCmd);
                    if (gmovehDT.Rows.Count == 0 || gmovehDT.Rows[0]["BSART_P"].ToString() != "ZUB4")
                    {
                        MessageBox.Show("此單號查無對應單據，請確認!");
                        return;
                    }
                    else if (gmovehDT.Rows[0]["BSART_P"].ToString() == "ZUB4")
                    {
                        tmpSN = tbBENDR.Text;
                    }
                }

                string strBSART = string.Empty;

                if (gmovehDT.Rows.Count > 0)
                {
                    strBSART = gmovehDT.Rows[0]["BSART"].ToString();

                    if (strBSART == "ZUB3")
                    {
                        sqlCmd = SqlStatement.UC10.GetSTO2STOByBednrOrEbeln(storeCode, tmpSN);
                    }
                    else if (strBSART == "ZUB2")
                    {
                        sqlCmd = SqlStatement.UC10.GetSTO2TDC1ByBednrOrEbeln(storeCode, tmpSN);
                    }
                    else if (strBSART == "ZUR1")
                    {
                        sqlCmd = SqlStatement.UC10.GetRE2TDC1ByBednrOrEbeln(storeCode, tmpSN);
                    }
                    //ZRP1, ZRP3, ZRP5 額外處理
                    else if (strBSART == "ZRP1" || strBSART == "ZRP3" 
                        || strBSART == "ZRP5" || strBSART=="ZUB4")
                    {
                        sqlCmd = SqlStatement.UC10.GetREPLENByBednrOrEbeln(storeCode, tmpSN);
                    }
                    else if (strBSART == "ZRE3" || strBSART == "ZUB1")
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號不允許");
                        return;
                    }
                    else
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("查無單號");
                        return;
                    }
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("查無單號");
                    return;
                }

                DataTable dt = dbConn.GetDataTable(sqlCmd);

                if (dt.Rows.Count > 0)
                {
                    if (strBSART == "ZUB1" && dt.Rows[0]["RUSH"].ToString() == "X")
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號不允許");
                        return;
                    }

                    if (strBSART != "ZRP1" && strBSART != "ZRP3" && strBSART != "ZRP5")
                    {
                        if (dt.Rows[0]["BSART"].ToString() == "ZRE3")
                        {
                            tbBENDR.SelectAll();
                            MessageBox.Show("單號不允許");
                            return;
                        }
                    }
                    
                    if (strBSART != "ZUB4" &&  strBSART.IndexOf("ZUB") != -1)
                    {
                        eblen = dt.Rows[0]["EBELN"].ToString();
                        sqlCmd = SqlStatement.UC10.GetGMOVETByEBELN(eblen, "AB");
                    }
                    else
                    {
                        sqlCmd = SqlStatement.UC10.GetGMOVET(vbeln, "AB");
                    }

                    gmovetDT = dbConn.GetDataTable(sqlCmd);

                    if (gmovetDT.Rows.Count > 0)
                    {
                        Common.SelectAndFocus(tbBENDR);
                        MessageBox.Show("單號重複");
                        return;
                    }
                    btnSave.Enabled = true;
                }
                else
                {
                    Common.SelectAndFocus(tbBENDR);
                    MessageBox.Show("查無單號");
                    return;
                }
                string currBSART = string.Empty;
                string currVBELN = string.Empty;
                if (strBSART == "ZRP1" || strBSART == "ZRP3" || strBSART == "ZRP5" || strBSART == "ZUB4")
                {
                    currBSART = strBSART;
                    currVBELN = vbeln;
                }
                else
                {
                    currBSART = dt.Rows[0]["BSART"].ToString();
                    currVBELN = dt.Rows[0]["EBELN"].ToString();

                }
                string currBEDNR = dt.Rows[0]["BEDNR"].ToString();
                string currEBELN = dt.Rows[0]["EBELN"].ToString();
                string currEAN11 = dt.Rows[0]["EAN11"].ToString();

                //若有該筆單號
                if (dt.Rows.Count > 0)
                {
                    if (strBSART == "ZUB4")
                    {
                        gmovetDT = dt;
                    }
                    else
                    {
                        if (strBSART.IndexOf("ZUB") != -1)
                        {
                            eblen = dt.Rows[0]["EBELN"].ToString();
                            sqlCmd = SqlStatement.UC10.GetGMOVETByEBELN(eblen, "641");
                        }
                        else
                        {
                            sqlCmd = SqlStatement.UC10.GetGMOVET(vbeln, "641");
                        }

                        gmovetDT = dbConn.GetDataTable(sqlCmd);

                        //若有相對應的商品異動出貨資料(GMOVET 641)
                        if (gmovetDT.Rows.Count > 0)
                        {
                            btnSave.Enabled = true;
                        }
                        else
                        {
                            Common.SelectAndFocus(tbBENDR);
                            MessageBox.Show("該品項無建單無法驗收");
                            btnSave.Enabled = false;
                            return;
                        }
                    }
                }
                else
                {
                    Common.SelectAndFocus(tbBENDR);
                    MessageBox.Show("該品項無建單無法驗收");
                    btnSave.Enabled = false;
                    return;
                }

                StringBuilder sb = new StringBuilder();

                List<Gmovet> GmovetList = new List<Gmovet>();

                for (int i = 0; i < gmovetDT.Rows.Count; i++)
                {
                    Gmovet objGmovet = new Gmovet();

                    objGmovet.BSART = currBSART;
                    objGmovet.EBELN = gmovetDT.Rows[i]["EBELN"].ToString();
                    objGmovet.EBELP = gmovetDT.Rows[i]["EBELP"].ToString();
                    objGmovet.MATNR = gmovetDT.Rows[i]["MATNR"].ToString();
                    objGmovet.EAN11 = gmovetDT.Rows[i]["EAN11"].ToString();
                    objGmovet.WERKS = storeCode;
                    objGmovet.BWART = "AB";
                    objGmovet.MENGE_P = gmovetDT.Rows[i]["MENGE_P"].ToString();
                    objGmovet.MEINS = gmovetDT.Rows[i]["MEINS_P"].ToString();
                    objGmovet.MENGE_M = gmovetDT.Rows[i]["MENGE_M"].ToString();
                    objGmovet.PROCD = string.Empty;
                    objGmovet.BEDNR = currBEDNR;
                    if (strBSART == "ZRP1" || strBSART == "ZRP3" 
                        || strBSART == "ZRP5" || strBSART == "ZUB4")
                    {
                        objGmovet.VBELN = gmovetDT.Rows[i]["VBELN"].ToString();
                    }
                    else
                    {
                        objGmovet.VBELN = currVBELN;
                    }
                    //若為ZUB4，則新增一筆641發貨紀錄再坐到貨記錄
                    if (objGmovet.BSART == "ZUB4")
                    {
                        Gmovet gmovet641 = new Gmovet();
                        gmovet641.BSART = currBSART;
                        gmovet641.EBELN = objGmovet.EBELN;
                        gmovet641.EBELP = objGmovet.EBELP;
                        gmovet641.MATNR = objGmovet.MATNR;
                        gmovet641.EAN11 = objGmovet.EAN11;
                        gmovet641.WERKS = "TDC2";
                        gmovet641.BWART = "641";
                        gmovet641.MENGE_P = objGmovet.MENGE_P;
                        gmovet641.MEINS = objGmovet.MEINS;
                        gmovet641.MENGE_M = objGmovet.MENGE_M;
                        gmovet641.PROCD = string.Empty;
                        gmovet641.BEDNR = currBEDNR;
                        gmovet641.VBELN = objGmovet.VBELN;
                        GmovetList.Add(gmovet641);
                    }
                    GmovetList.Add(objGmovet);
                }

                string CRDAT = DateTime.Now.ToString("yyyyMMdd");
                string CHTME = DateTime.Now.ToString("HHmmss");
                sqlCmd = SqlStatement.UC10.InsertGMOVET(GmovetList, empCodeReal, CRDAT, CHTME);
                dbConn.ExecSql(sqlCmd);

                //更新箱單號DataGrid
                DataTable BENDRDataTable = new DataTable();
                BENDRDataTable.Columns.Add(new DataColumn("單號"));
                BENDRDataTable.Rows.Add(tbBENDR.Text);

                dgList.DataSource = BENDRDataTable;
                BindDataGridStyle(BENDRDataTable);

                BENDRList.Add(tbBENDR.Text);

                lbBoxCount.Text = Convert.ToString(Convert.ToInt32(lbBoxCount.Text) + 1);
                Common.SelectAndFocus(tbBENDR);

            }
            catch (Exception ex)
            {
                MessageBox.Show("新增物流到貨資料錯誤: " + ex.Message);
            }
        }

        List<string> BENDRList = new List<string>();


        /// <summary>
        /// 完整顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            bool needAdd = true;
            List<Control> rmc = new List<Control>();

            UC10_1 uc = new UC10_1(BENDRList, ref dbConn);

            foreach (Control c in this.Parent.Controls)
            {
                if (c.GetType() == typeof(PictureBox))
                    c.Visible = false;
                else if (c.Name == "laTimeStr" || c.Name == "laVersion")
                    c.Visible = false;
                else if (c.GetType() == typeof(UC10))
                    c.Visible = false;
                else if (c.Name == uc.Name)
                {
                    c.Visible = true;
                    needAdd = false;
                }
                else
                    rmc.Add(c);
            }

            if (rmc.Count > 0)
                foreach (Control c in rmc)
                    this.Parent.Controls.Remove(c);

            if (needAdd)
                this.Parent.Controls.Add(uc);

            this.Parent.Parent.Text = formName + " -- " + "物流到貨作業(資料查詢)";
        }

        DateTime enterClickTime = new DateTime();

        private void tbBENDR_KeyUp(object sender, KeyEventArgs e)
        {
            List<string> prdList = new List<string>();
            List<string> listKey = new List<string>();

            //若單號修改過，則確認鈕不可按
            if (prevBENDR != tbBENDR.Text)
            {
                btnSave.Enabled = false;
            }

            prevBENDR = tbBENDR.Text;
            DataTable gmovehDT = new DataTable();
            string sqlCmd = string.Empty;
            string tmpSN = string.Empty;

            if (e.KeyCode == Keys.Enter)
            {

                //按下Enter後間隔一秒後再按下Enter才有反應，避免重複按下
                if (enterClickTime > DateTime.Now)
                    return;
                enterClickTime = DateTime.Now.Add(new TimeSpan(0, 0, 1));

                //檢查GMOVET單號
                if (tbBENDR.Text.Length == 0)
                { MessageBox.Show("請輸入單號"); return; }

                string vbeln = tbBENDR.Text;
                string eblen = string.Empty;

                //物流到貨只判斷 VBELN 20170506
                sqlCmd = SqlStatement.UC10.GetGmovetByVbeln(tbBENDR.Text);
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    tmpSN = tmpDT.Rows[0]["EBELN"] == null ?
                                   tmpDT.Rows[0]["BEDNR"] == null ? "" : tmpDT.Rows[0]["BEDNR"].ToString() :
                                   tmpDT.Rows[0]["EBELN"].ToString();

                    if (string.IsNullOrEmpty(tmpSN))
                    {
                        MessageBox.Show("此單號查無對應單據，請確認!");
                        return;
                    }

                    prdList = (from row in tmpDT.AsEnumerable()
                               select "'" + row["EAN11"].ToString() + "'").ToList();
                    sqlCmd = SqlStatement.UC10.GetGmovehInfo(tmpSN);
                    gmovehDT = dbConn.GetDataTable(sqlCmd);
                }
                else if (tbBENDR.Text.ToUpper().IndexOf("ZUB") != -1)
                {
                    sqlCmd = SqlStatement.UC10.GetGmovehInfo(tbBENDR.Text);
                    gmovehDT = dbConn.GetDataTable(sqlCmd);
                    if (gmovehDT.Rows.Count > 0)
                    {
                        tmpSN = gmovehDT.Rows[0]["BEDNR"].ToString();
                    }
                }
                else
                {
                    //額外從REPLEN(補貨資料中)抓取單型，判斷是否為ZUB4。若為ZUB4不做641檢查。
                    sqlCmd = SqlStatement.UC10.GetREPLENByBednrOrEbeln(storeCode, tbBENDR.Text);
                    gmovehDT = dbConn.GetDataTable(sqlCmd);
                    if (gmovehDT.Rows.Count == 0 || gmovehDT.Rows[0]["BSART_P"].ToString() != "ZUB4")
                    {
                        MessageBox.Show("此單號查無對應單據，請確認!");
                        return;
                    }
                    else if (gmovehDT.Rows[0]["BSART_P"].ToString() == "ZUB4")
                    {
                        tmpSN = tbBENDR.Text;
                    }
                }


                string strBSART = string.Empty;

                if (gmovehDT.Rows.Count > 0)
                {
                    strBSART = gmovehDT.Rows[0]["BSART"].ToString();

                    if (strBSART == "ZUB3")
                    {
                        sqlCmd = SqlStatement.UC10.GetSTO2STOByBednrOrEbeln(storeCode, tmpSN);
                    }
                    else if (strBSART == "ZUB2")
                    {
                        sqlCmd = SqlStatement.UC10.GetSTO2TDC1ByBednrOrEbeln(storeCode, tmpSN);
                    }
                    else if (strBSART == "ZUR1")
                    {
                        sqlCmd = SqlStatement.UC10.GetRE2TDC1ByBednrOrEbeln(storeCode, tmpSN);
                    }
                    //ZRP1, ZRP3, ZRP5 額外處理
                    else if (strBSART == "ZRP1" || strBSART == "ZRP3"
                        || strBSART == "ZRP5" || strBSART == "ZUB4")
                    {
                        sqlCmd = SqlStatement.UC10.GetREPLENByBednrOrEbeln(storeCode, tmpSN);
                    }
                    else if (strBSART == "ZRE3" || strBSART == "ZUB1")
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號不允許");
                        return;
                    }
                    else
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("查無單號");
                        return;
                    }
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("查無單號");
                    return;
                }

                DataTable dt = dbConn.GetDataTable(sqlCmd);
                if (dt.Rows.Count > 0)
                {

                    if (strBSART == "ZUB1" && dt.Rows[0]["RUSH"].ToString() == "X")
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號不允許");
                        return;
                    }

                    if (strBSART != "ZRP1" && strBSART != "ZRP3" && strBSART != "ZRP5" && strBSART != "ZUB4")
                    {
                        if (dt.Rows[0]["BSART"].ToString() == "ZRE3")
                        {
                            tbBENDR.SelectAll();
                            MessageBox.Show("單號不允許");
                            return;
                        }
                    }

                    if (strBSART != "ZUB4" && strBSART.IndexOf("ZUB") != -1)
                    {
                        eblen = dt.Rows[0]["EBELN"].ToString();
                        sqlCmd = SqlStatement.UC10.GetGMOVETByEBELN(eblen, "AB");
                    }
                    else
                    {
                        //檢查是否已收貨
                        sqlCmd = SqlStatement.UC10.GetGMOVET(vbeln, "AB");
                    }

                    DataTable gmovetDT = dbConn.GetDataTable(sqlCmd);

                    if (gmovetDT.Rows.Count > 0)
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號重複");
                        return;
                    }
                    btnSave.Enabled = true;
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("查無單號");
                    return;
                }

                //若有該筆單號 
                //ZUB4不做處理641建單檢查
                if (dt.Rows.Count > 0)
                {
                    if (strBSART != "ZUB4")
                    {
                        if (strBSART.IndexOf("ZUB") != -1)
                        {
                            eblen = dt.Rows[0]["EBELN"].ToString();
                            sqlCmd = SqlStatement.UC10.GetGMOVETByEBELN(eblen, "641");
                        }
                        else
                        {
                            sqlCmd = SqlStatement.UC10.GetGMOVET(vbeln, "641");
                        }
                        DataTable gmovetDT = dbConn.GetDataTable(sqlCmd);

                        //若有相對應的商品異動出貨資料(GMOVET 641)
                        if (gmovetDT.Rows.Count > 0)
                        {
                            btnSave.Enabled = true;
                        }
                        else
                        {
                            tbBENDR.SelectAll();
                            MessageBox.Show("該品項無建單無法驗收");
                            btnSave.Enabled = false;
                            return;
                        }
                    }
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("該品項無建單無法驗收");
                    btnSave.Enabled = false;
                    return;
                }
            }
        }

        /// <summary>
        /// DataGrid顯示處理
        /// </summary>
        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                tb.Width = 300;

                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
