﻿namespace JPMed_PDA.SubForms
{
    partial class UC06_1
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.dgList = new System.Windows.Forms.DataGrid();
            this.btnExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.laQty = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.laCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dgList
            // 
            this.dgList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgList.Location = new System.Drawing.Point(9, 62);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(300, 270);
            this.dgList.TabIndex = 136;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnExit.Location = new System.Drawing.Point(243, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 24);
            this.btnExit.TabIndex = 135;
            this.btnExit.Text = "離開";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(148, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.Text = "數量:";
            // 
            // laQty
            // 
            this.laQty.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.laQty.ForeColor = System.Drawing.Color.Black;
            this.laQty.Location = new System.Drawing.Point(203, 39);
            this.laQty.Name = "laQty";
            this.laQty.Size = new System.Drawing.Size(84, 20);
            this.laQty.Text = "100";
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label50.Location = new System.Drawing.Point(4, 39);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(62, 20);
            this.label50.Text = "總筆數:";
            // 
            // laCount
            // 
            this.laCount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.laCount.ForeColor = System.Drawing.Color.Black;
            this.laCount.Location = new System.Drawing.Point(72, 39);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(65, 20);
            this.laCount.Text = "100";
            // 
            // UC06_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laQty);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.laCount);
            this.Name = "UC06_1";
            this.Size = new System.Drawing.Size(318, 425);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dgList;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laQty;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label laCount;
    }
}
