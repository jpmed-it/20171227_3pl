﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC07 : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        SQLCreator sql;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        DataTable defaultDT = null;
        string SnNo = "ZUR1" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;

        public UC07()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            btnSave.Click += btnSave_Click;
            btnShowAll.Click += btnShowAll_Click;
            tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            tbEmpCode.KeyUp += tbEmpCode_KeyUp;
            tbSN.TextChanged += tbSN_TextChanged;
            tbSN.KeyUp += tbSN_KeyUp;
            tbQty.KeyDown += tbQty_KeyDown;
            cbMemo.KeyDown += cbMemo_KeyDown;
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            laDate.Text = DateTime.Today.ToString("D");
            laRtnDisCode.Text = "TDC2";
            laRtnDisName.Text = string.Empty;

            laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
            tbSN.Text = string.Empty;
            tbSN.Enabled = false;
            laProductName.Text = string.Empty;

            tbQty.Text = string.Empty;
            tbQty.Enabled = false;

            //laStart.Visible = false;
            cbMemo.Items.Clear();
            cbMemo.Enabled = false;

            btnShowAll.Enabled = false;
            btnSave.Enabled = false;

            dgList.DataSource = defaultDT;

        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC07");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //20170103 需求變更: 增加LABOR判斷 自家品/廠商品
                //預設空值
                lbl_REUCK.Text = string.Empty;

                //取得總倉名稱
                string sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { "TDC2" }); //總倉固定為TDC2
                laRtnDisName.Text = dbConn.GetDataTableRecData(sqlCmd);

                //取得退貨理由
                GetReason();

                //取得店家資訊
                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 取得備註下拉選單內容
        /// </summary>
        private void GetReason()
        {
            try
            {
                string sqlCmd = sql.GetCommand("GetReason");
                cbMemo.DataSource = dbConn.GetDataTable(sqlCmd);
                cbMemo.DisplayMember = "Text";
                cbMemo.ValueMember = "Value";
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得備註下拉選單內容發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    btnShowAll.Enabled = _enbleControl;
                    laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;
                    laProductName.Text = string.Empty;
                    cbMemo.SelectedIndex = -1;
                    cbMemo.Enabled = _enbleControl;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    //laStart.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[門市退貨(有問題貨品)入退貨倉]?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    EnableControl(false, 1);
                    tbEmpCode.Focus();
                    tbEmpCode.SelectAll();
                    laEmpName.Text = "查無此編號";
                    laEmpName.ForeColor = Color.Red;
                    dgList.DataSource = defaultDT;
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    laEmpName.ForeColor = Color.Black;
                    EnableControl(true, 1);
                    tbSN.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.SelectAll();
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbEmpCode.SelectAll();
            }
        }

        /// <summary>
        /// 產品Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbSN.SelectAll();
            }
        }
        private void tbSN_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchProduct();
                tbSN.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 查詢產品
        /// </summary>
        private void SearchProduct()
        {
            try
            {
                if (string.IsNullOrEmpty(tbSN.Text))
                    return;

                string tmpEan = tbSN.Text.ToUpper();

                if (string.IsNullOrEmpty(tmpEan))
                    return;

                bool loop = true;
                while (loop)
                {
                    if (tmpEan.StartsWith("0") && tmpEan.Length > 0)
                        tmpEan = tmpEan.Substring(1);
                    else
                    {
                        loop = false;
                        if (tmpEan.Length == 0) return;
                        else tbSN.Text = tmpEan;
                    }
                }

                string sqlCmd = sql.GetCommand("GetProduct", new string[] { storeCode, tbSN.Text });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    laEan11.Text = tmpDT.Rows[0]["EAN11"].ToString().Trim(); //20160919需求變更:抓EAN11 or EAN12
                    if (string.IsNullOrEmpty(laEan11.Text))
                    {
                        laProductName.Text = "此代碼查無對應之EAN11值";
                        laProductName.ForeColor = Color.Red;
                        btnSave.Enabled = false;
                        //laStart.Visible = false;
                        return;
                    }
                    
                    //string labor = tmpDT.Rows[0]["LABOR"].ToString(); //001:自家貨
                    //string reuck = tmpDT.Rows[0]["RUECK"].ToString(); //04:買斷品
                    //if (labor == "001" || reuck == "04") laStart.Visible = true;
                    //else laStart.Visible = false;

                    laKey.Text = tmpDT.Rows[0]["MATNR"].ToString() + ";" + tmpDT.Rows[0]["MEINS"].ToString();
                    laProductName.Text = tmpDT.Rows[0]["MAKTX"].ToString();
                    laProductName.ForeColor = Color.Black;

                    //20170515 LABOR顯示改為RUECK顯示
                    string reuck = PublicClass.ConvertToRUECKString(tmpDT.Rows[0]["RUECK"].ToString().Trim());
                    if (reuck.Length == 0)
                    {
                        MessageBox.Show("無維護，請通知商品部");
                        return;
                    }

                    lbl_REUCK.Text = reuck;


                    btnSave.Enabled = true;
                    tbQty.Focus();
                    //eanReal = tbSN.Text;
                    eanReal = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                }
                else
                {
                    //20170103 需求變更: 增加LABOR判斷 自家品/廠商品
                    //預設空值
                    lbl_REUCK.Text = string.Empty;

                    laProductName.Text = "門市查無此商品!";
                    laProductName.ForeColor = Color.Red;
                    btnSave.Enabled = false;
                    //laStart.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢產品發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 輸入數量後按Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cbMemo.Focus();
            }
        }

        /// <summary>
        /// 選擇廠商後按ENTER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbMemo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (btnSave.Enabled)
                    btnSave_Click(null, null);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (eanReal != tbSN.Text)
                if (eanReal != laEan11.Text) //20160919需求變更:抓EAN11 or EAN12
                {
                    MessageBox.Show("[商品條碼]已異動，請重新進行資料查詢!");
                    btnSave.Enabled = false;
                    return;
                }

                if ((cbMemo.SelectedValue == null || string.IsNullOrEmpty(cbMemo.SelectedValue.ToString())))
                {
                    MessageBox.Show("請選擇退貨備註!");
                    return;
                }

                if (string.IsNullOrEmpty(tbQty.Text))
                {
                    MessageBox.Show("退貨數量不可為空");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                string bsgru = cbMemo.SelectedValue.ToString().Split(',')[0];
                string lgort = cbMemo.SelectedValue.ToString().Split(',')[1];

                if (lbl_REUCK.Text == "不可退" && bsgru == "R02")
                {
                    MessageBox.Show("此商品不可退貨");
                    return;
                }

                //退貨數量僅可介於 0 < qty < 399
                int tempInt = 0;
                try
                {
                    tempInt = Int32.Parse(tbQty.Text);
                    if (tempInt <= 0 || tempInt >= 399)
                    {
                        MessageBox.Show("退貨數量需大於0且小於399!\n 0 < 退貨數量 < 399");
                        tbQty.Select(0, tbQty.Text.Length);
                        tbQty.Focus();
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("退貨數量僅可輸入\n[整數數值]");
                    tbQty.Select(0, tbQty.Text.Length);
                    tbQty.Focus();
                    return;
                }

                //20170109 不需判斷是否為直退商品
                string sqlCmd = string.Empty;
                DataTable tmpDT = new DataTable();


                #region 準備異動資訊

                //0:新增, 1:覆蓋
                int modifyType;

                SnNo = SnNo.Substring(8, 8) == DateTime.Today.ToString("yyyyMMdd") ? SnNo : "ZUR1" + storeCode + DateTime.Now.ToString("yyyyMMddHHmmss");
                string matnr = laKey.Text.Split(';')[0];
                //string ean = tbSN.Text;
                string ean = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
                string reswk = storeCode;
                //string bsgru = cbMemo.SelectedValue == null ? string.Empty : cbMemo.SelectedValue.ToString();
                string empCode = empCodeReal; //tbEmpCode.Text;
                string qty = tbQty.Text;
                string meins = laKey.Text.Split(';')[1];
                string menge_r = tbQty.Text;

                sqlCmd = sql.GetCommand("GetCheckCommand", new string[] { SnNo, matnr, ean, reswk });
                tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    if (tmpDT.Rows[0]["COMARK"].ToString() == "V")
                    {
                        MessageBox.Show("此商品無法進行退貨：\n請確認是否已存在相同單據內容，且審核狀態[已確認]");
                        return;
                    }
                    else if (tmpDT.Rows[0]["PROCD"].ToString() == "X")
                    {
                        MessageBox.Show("此商品無法進行退貨：\n請確認是否已存在相同單據內容，且SAP審核狀態[已處理]");
                        return;
                    }
                    else
                    {
                        modifyType = 1;

                        //更新
                        sqlCmd = sql.GetCommand("GetUpdateCommand", new string[] { SnNo, matnr, ean, reswk, bsgru, empCode, menge_r, lgort });
                    }
                }
                else
                {
                    modifyType = 0;

                    //新增
                    sqlCmd = sql.GetCommand("GetInsertCommand", new string[] { SnNo, matnr, ean, reswk, bsgru, empCode, menge_r, meins, lgort });
                }
                #endregion

                DialogResult dialogResult = MessageBox.Show(modifyType == 0 ? "您確定新增退貨單?" : "您確定覆蓋退貨數量？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (dialogResult == DialogResult.Yes)
                {
                    if (dbConn.ExecSql(sqlCmd) == 0)
                    {
                        MessageBox.Show("未更新任何項目，請聯繫管理人員");
                    }
                    else
                    {
                        laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                        tbSN.Text = string.Empty;
                        tbSN.Focus();

                        cbMemo.SelectedIndex = 0;
                        tbQty.Text = string.Empty;
                        btnSave.Enabled = false;
                        laProductName.Text = string.Empty;

                        sqlCmd = sql.GetCommand("GetSelectCommand", new string[] { SnNo, matnr, ean, reswk });
                        DataTable sourceDT = dbConn.GetDataTable(sqlCmd);
                        source.DataSource = sourceDT;
                        dgList.DataSource = source;
                        source.ResetBindings(false);
                        BindDataGridStyle(sourceDT);
                        MessageBox.Show(modifyType == 0 ? "新增成功" : "修改成功!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 完整顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                UC07_1 uc = new UC07_1(SnNo, ref dbConn);
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.GetType() == typeof(PictureBox))
                        c.Visible = false;
                    else if (c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = false;
                    else if (c.GetType() == typeof(UC07))
                        c.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        this.Parent.Controls.Remove(c);

                if (needAdd)
                    this.Parent.Controls.Add(uc);

                this.Parent.Parent.Text = formName + " -- " + "入退貨倉(資料查詢)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "退貨量":
                    case "庫存量":
                        tb.Format = "0.###";
                        break;
                }

                if (col.ColumnName == "退貨確認") tb.Width = 80;
                else tb.Width = 150;
                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
