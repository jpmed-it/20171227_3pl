﻿namespace JPMed_PDA.SubForms
{
    partial class UC03
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.label50 = new System.Windows.Forms.Label();
            this.laProductName = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgList = new System.Windows.Forms.DataGrid();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.laDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.laEdate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.laSdate = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSopen = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.laStockNum = new System.Windows.Forms.Label();
            this.laSpbup = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.laInQty = new System.Windows.Forms.Label();
            this.laEan11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label50.Location = new System.Drawing.Point(20, 203);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(55, 20);
            this.label50.Text = "品名:";
            // 
            // laProductName
            // 
            this.laProductName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laProductName.ForeColor = System.Drawing.Color.Black;
            this.laProductName.Location = new System.Drawing.Point(75, 203);
            this.laProductName.Name = "laProductName";
            this.laProductName.Size = new System.Drawing.Size(224, 20);
            this.laProductName.Text = "查無商品資訊";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.btnSave.Location = new System.Drawing.Point(228, 278);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 27);
            this.btnSave.TabIndex = 161;
            this.btnSave.Text = "輸入(覆蓋)";
            // 
            // dgList
            // 
            this.dgList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgList.Location = new System.Drawing.Point(3, 331);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(312, 67);
            this.dgList.TabIndex = 160;
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.tbQty.Location = new System.Drawing.Point(83, 277);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(118, 26);
            this.tbQty.TabIndex = 159;
            this.tbQty.Text = "1111111111.000";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(19, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 20);
            this.label7.Text = "現補量:";
            // 
            // laEmpName
            // 
            this.laEmpName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline);
            this.laEmpName.Location = new System.Drawing.Point(179, 65);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(123, 16);
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.tbEmpCode.Location = new System.Drawing.Point(65, 60);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 26);
            this.tbEmpCode.TabIndex = 157;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(10, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 20);
            this.label5.Text = "員編：";
            // 
            // tbSN
            // 
            this.tbSN.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.tbSN.Location = new System.Drawing.Point(145, 176);
            this.tbSN.MaxLength = 18;
            this.tbSN.Name = "tbSN";
            this.tbSN.Size = new System.Drawing.Size(157, 24);
            this.tbSN.TabIndex = 156;
            this.tbSN.Text = "A12345678909876522";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(19, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.Text = "商品條碼(EAN):";
            // 
            // btnShowAll
            // 
            this.btnShowAll.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnShowAll.Location = new System.Drawing.Point(243, 147);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(72, 24);
            this.btnShowAll.TabIndex = 155;
            this.btnShowAll.Text = "完整";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(10, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 20);
            this.label3.Text = "輸入資料:";
            // 
            // laDate
            // 
            this.laDate.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laDate.Location = new System.Drawing.Point(90, 128);
            this.laDate.Name = "laDate";
            this.laDate.Size = new System.Drawing.Size(149, 20);
            this.laDate.Text = "2016年04月01日";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(10, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.Text = "補貨日期:";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnExit.Location = new System.Drawing.Point(243, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 24);
            this.btnExit.TabIndex = 154;
            this.btnExit.Text = "離開";
            // 
            // laSName
            // 
            this.laSName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSName.Location = new System.Drawing.Point(65, 34);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(149, 20);
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSCode.Location = new System.Drawing.Point(10, 34);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(49, 20);
            this.laSCode.Text = "T001";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 20);
            this.label1.Text = "所在門市：";
            // 
            // laKey
            // 
            this.laKey.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laKey.ForeColor = System.Drawing.Color.Black;
            this.laKey.Location = new System.Drawing.Point(3, 405);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(129, 20);
            this.laKey.Visible = false;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(254, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 16);
            this.label12.Text = ")";
            // 
            // laEdate
            // 
            this.laEdate.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline);
            this.laEdate.Location = new System.Drawing.Point(137, 109);
            this.laEdate.Name = "laEdate";
            this.laEdate.Size = new System.Drawing.Size(120, 16);
            this.laEdate.Text = "2016年11月15號";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(88, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 16);
            this.label11.Text = "迄日：";
            // 
            // laSdate
            // 
            this.laSdate.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline);
            this.laSdate.Location = new System.Drawing.Point(137, 89);
            this.laSdate.Name = "laSdate";
            this.laSdate.Size = new System.Drawing.Size(120, 16);
            this.laSdate.Text = "2016年11月15號";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(83, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 16);
            this.label9.Text = "(起日：";
            // 
            // cbSopen
            // 
            this.cbSopen.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.cbSopen.Location = new System.Drawing.Point(62, 91);
            this.cbSopen.Name = "cbSopen";
            this.cbSopen.Size = new System.Drawing.Size(15, 14);
            this.cbSopen.TabIndex = 181;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(9, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.Text = "特開：";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(19, 227);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 20);
            this.label6.Text = "庫存量:";
            // 
            // laStockNum
            // 
            this.laStockNum.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laStockNum.ForeColor = System.Drawing.Color.Black;
            this.laStockNum.Location = new System.Drawing.Point(83, 227);
            this.laStockNum.Name = "laStockNum";
            this.laStockNum.Size = new System.Drawing.Size(232, 20);
            this.laStockNum.Text = "10.000";
            // 
            // laSpbup
            // 
            this.laSpbup.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSpbup.ForeColor = System.Drawing.Color.Black;
            this.laSpbup.Location = new System.Drawing.Point(101, 252);
            this.laSpbup.Name = "laSpbup";
            this.laSpbup.Size = new System.Drawing.Size(204, 20);
            this.laSpbup.Text = "10.000";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(20, 252);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 20);
            this.label13.Text = "前週銷量:";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(20, 305);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 20);
            this.label10.Text = "已入現補量:";
            // 
            // laInQty
            // 
            this.laInQty.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laInQty.ForeColor = System.Drawing.Color.Black;
            this.laInQty.Location = new System.Drawing.Point(125, 306);
            this.laInQty.Name = "laInQty";
            this.laInQty.Size = new System.Drawing.Size(190, 20);
            this.laInQty.Text = "10.000";
            // 
            // laEan11
            // 
            this.laEan11.Location = new System.Drawing.Point(90, 153);
            this.laEan11.Name = "laEan11";
            this.laEan11.Size = new System.Drawing.Size(57, 20);
            this.laEan11.Text = "Ean11";
            this.laEan11.Visible = false;
            // 
            // UC03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.Controls.Add(this.laEan11);
            this.Controls.Add(this.laInQty);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.laSpbup);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.laStockNum);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.laEdate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.laSdate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbSopen);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.laProductName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnShowAll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.laDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.label1);
            this.Name = "UC03";
            this.Size = new System.Drawing.Size(318, 425);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label laProductName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGrid dgList;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label laDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label laEdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label laSdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbSopen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laStockNum;
        private System.Windows.Forms.Label laSpbup;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label laInQty;
        private System.Windows.Forms.Label laEan11;
    }
}
