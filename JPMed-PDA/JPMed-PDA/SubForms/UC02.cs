﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Web.Services;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;
using JPMed_PDA.Entity;
using RichardPrice;

namespace JPMed_PDA.SubForms
{
    public partial class UC02 : UserControl
    {
        string formName = "倉管作業系統";
        static string errMsg = string.Empty;
        static string store = new ConfigSettings().Werks;
        JPMed_WebService.Service1 webSvc = null;
        SQLCreator sql;
        DBConnection dbConn;
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;

        public UC02()
        {
            InitializeComponent();

            laSCode.Text = store;
            if (string.IsNullOrEmpty(laSCode.Text))
                throw new Exception("無法取得設定檔中[門市代碼]資訊!");

            DefaultControl(true);
            IntitalControl();
        }

        public void DefaultControl(bool first)
        {
            if (first)
            {
                laSName.Text = string.Empty;
                tbSN.Text = string.Empty;
                laWarm.Visible = false;
                laWarm2.Visible = false;
            }
            
            laPrdQty.Text = string.Empty;
            laPrdSapSN.Text = string.Empty;
            laPrdName.Text = string.Empty;
            laPrdOriPrice.Text = string.Empty;
            laPrdPrice.Text = string.Empty;
            laPrdMbPrice.Text = string.Empty;
            laCampaignSdate.Text = string.Empty;
            laCampaignEdate.Text = string.Empty;
            laMemo.Text = string.Empty;
        }

        public void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC02");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                laSName.Text = GetWerksName(laSCode.Text);
            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        private string GetWerksName(string _werksCode)
        {
            string sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { laSCode.Text });
            string storeName = dbConn.GetDataTableRecData(sqlCmd);
            if (storeName == DBNull.Value.ToString())
                throw new Exception("資料庫中查無此分店代號(" + _werksCode + ")");

            return storeName;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[查價作業]?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;

                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            laWarm.Visible = false;
            laWarm2.Visible = false;
            string werksCode = laSCode.Text;
            string envCode = tbSN.Text.ToUpper();

            if(string.IsNullOrEmpty(envCode))
            {
                MessageBox.Show("請輸入產品條碼(EAN)!");
                DefaultControl(false);
                tbSN.SelectAll();
                tbSN.Focus();
                return;
            }

            bool loop = true;
            while (loop)
            {
                if (envCode.StartsWith("0") && envCode.Length > 0)
                    envCode = envCode.Substring(1);
                else
                {
                    loop = false;
                    if (envCode.Length == 0)
                    {
                        DefaultControl(false);
                        tbSN.SelectAll();
                        tbSN.Focus();
                        MessageBox.Show("請輸入有效之商品條碼(EAN)");
                        return;
                    }
                    else tbSN.Text = envCode;
                }
            }

            string sqlCmd = sql.GetCommand("GetPrdInformation", new string[] { werksCode, envCode });
            DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
            if (tmpDT.Rows.Count > 0)
            {
                laPrdQty.Text = decimal.Parse(tmpDT.Rows[0][0].ToString()).ToString("0.###");
                
                //SAP NO 去0
                string tmpSapSn = tmpDT.Rows[0][1].ToString().ToUpper();
                loop = true;
                while (loop)
                {
                    if (tmpSapSn.StartsWith("0") && tmpSapSn.Length > 0)
                        tmpSapSn = tmpSapSn.Substring(1);
                    else
                    {
                        loop = false;
                        laPrdSapSN.Text = tmpSapSn;
                    }
                }
                
                laPrdName.Text = tmpDT.Rows[0][2].ToString();
                laWarm2.Visible = tmpDT.Rows[0]["DELFG"].ToString() == "X";

                try
                {
                    //改用日藥IT開發之程式:查價作業
                    //GetProductInfoByWebService(tbSN.Text, "");
                    PriceData PriceData1 = new PriceData();
                    PriceData1 = Price.GetPrice(envCode, DateTime.Now.ToString("yyyyMMdd"), werksCode);
                    laPrdOriPrice.Text = PriceData1.OriginalPrice;
                    laPrdPrice.Text = PriceData1.CurrentlyPrice;
                    laPrdMbPrice.Text = PriceData1.MemberPrice;
                    laCampaignSdate.Text = PriceData1.StartDay;
                    laCampaignEdate.Text = PriceData1.EndDay;
                    laMemo.Text = PriceData1.PS;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("進行查價作業(FunctionCall)時發生錯誤： " + ex.Message);
                    tbSN.Focus();
                    tbSN.Select(0, tbSN.Text.Length);
                    laWarm.Text = "進行查價作業(FunctionCall)時發生錯誤： " + ex.Message;
                    laWarm.Visible = true;
                    DefaultControl(false);
                }

            }
            else
            {
                tbSN.Focus();
                tbSN.Select(0, tbSN.Text.Length);
                laWarm.Text = "查無相關產品資訊";
                laWarm.Visible = true;
                DefaultControl(false);
            }

            tbSN.Focus();
            tbSN.Select(0, tbSN.Text.Length);
        }

        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                btnSearch_Click(null, null);
                tbSN.SelectAll();
            }
        }
        private void tbSN_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
                tbSN.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 查詢產品活動代碼By 條碼 or 活動代碼
        /// </summary>
        /// <param name="_ean11"></param>
        /// <param name="_actNo"></param>
        /// <returns></returns>
        private void GetProductInfoByWebService(string _ean11, string _actNo)
        {
            string sqlCmd = string.Empty;
            string filter = string.Empty;
            DataSet dsTmp;
            DataSystemsPrd prdInfo = null;
            string errMsg = string.Empty;

            string strTmpSKU = _ean11; //輸入之條碼
            string strCM_SKU = string.Empty; //頂新料靠
            string strTmpPMCode = string.IsNullOrEmpty(_actNo) ? string.Empty : _actNo; //輸入之活動代碼
            string strPMCode = string.Empty;
            string strSTR = store; //門市代號
            string strSTYDescp = string.Empty; //商品品名
            string strCLASS1 = "0"; //商品大類
            string strSRC_AMT = "0"; //原售價
            string strSALE_AMT = "0"; //現售價
            string strSupName = string.Empty; //供應商名
            string strMC006_CMPrice = "0"; //會員價
            string strPMFMDate = string.Empty; //活動起日
            string strPMTODate = string.Empty; //活動迄日
            string strMemo = string.Empty; //活動名稱
            string strLocCM_SKU = string.Empty;
            string strLocSKU = string.Empty;
            string strCLASSDescp = string.Empty;
            bool bLocPM;

            laPrdOriPrice.Text = string.Empty;
            laPrdPrice.Text = string.Empty;
            laPrdMbPrice.Text = string.Empty;
            laCampaignSdate.Text = string.Empty;
            laCampaignEdate.Text = string.Empty;
            laMemo.Text = string.Empty;

            try
            {
                if (webSvc == null)
                {
                    webSvc = new JPMed_PDA.JPMed_WebService.Service1();
                    webSvc.Url = new ConfigSettings().WebServiceUrl;
                }

                if (!string.IsNullOrEmpty(strTmpSKU))
                {
                    prdInfo = GetDataSystemPrd(strTmpSKU, out errMsg);
                    if (prdInfo == null)
                    {
                        MessageBox.Show("連接資料庫發生問題,請稍候再試!若問題持續發生,請洽詢相關人員!! 錯誤訊息： " + errMsg);
                        return;
                    }

                    strCM_SKU = prdInfo.strCMSKU;
                    strLocCM_SKU = strCM_SKU.Trim();

                    if (strTmpSKU != prdInfo.strSKU) 
                        strTmpSKU = prdInfo.strSKU;

                    strLocSKU = strTmpSKU.Trim();
                    strSTYDescp = prdInfo.strGoodsDESCP; //商品品名
                    strSTYDescp = strSTYDescp.Length >= 26 ? strSTYDescp.Substring(0, 26) : strSTYDescp.Substring(0, strSTYDescp.Length);
                    strCLASS1 = prdInfo.strCLASS1; //商品大類
                    strSRC_AMT = prdInfo.strSRC_AMT; //原售單價
                    strSupName = prdInfo.strSUPName; //供應商名

                    if (strSALE_AMT.Trim() == "0" || strSALE_AMT.Trim() == "")
                        strSALE_AMT = strSRC_AMT.Trim();

                    switch (int.Parse(strCLASS1))
                    {
                        case 1: strCLASSDescp = "藥品"; break;
                        case 2: strCLASSDescp = "醫療保健"; break;
                        case 3: strCLASSDescp = "彩妝保養"; break;
                        case 4: strCLASSDescp = "生活百貨";break;
                        case 5: strCLASSDescp = "食品"; break;
                    }
                }

                #region TABLE:[POSMC]

                filter = string.Empty;
                if (!string.IsNullOrEmpty(strTmpSKU.Trim()) && string.IsNullOrEmpty(strTmpPMCode.Trim())) //條碼 查詢
                {
                    filter = string.Format("POSMC.MC004='{0}'", strCM_SKU);
                }
                else if (!string.IsNullOrEmpty(strTmpSKU.Trim()) && !string.IsNullOrEmpty(strTmpPMCode.Trim())) //條碼+活動代碼 查詢
                {
                    filter = string.Format("POSMC.MC004='{0}' AND POSMC.MC003='{1}'", strCM_SKU.Trim(), strTmpPMCode);
                }
                else if (string.IsNullOrEmpty(strTmpSKU.Trim()) && !string.IsNullOrEmpty(strTmpPMCode.Trim())) //活動代碼 查詢
                {
                    filter = string.Format("POSMC.MC003='{0}'", strTmpPMCode);
                }

                sqlCmd = sql.GetCommand("GetPOSMC_1", new string[] { filter });
                dsTmp = webSvc.uGetNeedData(1, sqlCmd, "LOG_POSMBC");

                #region 第一層
                if (dsTmp != null && dsTmp.Tables.Count > 0)
                {
                    if (dsTmp.Tables[0].Rows.Count > 0)
                    {
                        string strPMSTR = string.Empty;
                        List<string> strTmpFilter = new List<string>();
                        List<string> strNOTIN_PMCODE = new List<string>();

                        foreach (DataRow row in dsTmp.Tables[0].Rows)
                        {
                            strPMSTR = row["MF004"].ToString();
                            strTmpPMCode = row["MC003"].ToString();

                            if (strPMSTR.Trim() == strSTR.Trim()) strTmpFilter.Add("'" + strTmpPMCode + "'");
                            else strNOTIN_PMCODE.Add("'" + strTmpPMCode + "'");
                        }

                        //組合FILTER
                        if (strTmpFilter.Count > 0)
                            filter = "POSMC.MC003 IN(" + string.Join(",", strTmpFilter.ToArray()) + ")";
                        else
                            filter = "POSMC.MC004 = '" + strCM_SKU + "' AND POSMC.MC003 NOT IN (" + string.Join(",", strNOTIN_PMCODE.ToArray()) + ")";

                        //組SQL
                        if (string.IsNullOrEmpty(strCM_SKU.Trim()))
                            sqlCmd = sql.GetCommand("GetPOSMC_2", new string[] { filter });
                        else
                            sqlCmd = sql.GetCommand("GetPOSMC_3", new string[] { filter, strCM_SKU });
                        
                    }
                    else
                    {
                        sqlCmd = sql.GetCommand("GetPOSMC_2", new string[] { filter });
                    }
                }
                #endregion

                dsTmp = new DataSet();
                dsTmp = webSvc.uGetNeedData(1, sqlCmd, "LOG_POSMBC");

                #region 第二層
                if (dsTmp != null && dsTmp.Tables.Count > 0)
                {
                    if (dsTmp.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in dsTmp.Tables[0].Rows)
                        {
                            strPMCode = row["MC003"].ToString();
                            strCM_SKU = row["MC004"].ToString();

                            prdInfo = GetDataSystemPrd(strCM_SKU, out errMsg);
                            if (prdInfo == null)
                            {
                                MessageBox.Show("連接資料庫發生問題,請稍候再試!若問題持續發生,請洽詢相關人員(POSMC-2)!! 錯誤訊息： " + errMsg);
                                return;
                            }

                            strTmpSKU = prdInfo.strSKU; //國際條碼
                            strSTYDescp = prdInfo.strGoodsDESCP; //商品品名
                            strSTYDescp = strSTYDescp.Length >= 24 ? strSTYDescp.Substring(0, 24) : strSTYDescp.Substring(0, strSTYDescp.Length);

                            strCLASS1 = prdInfo.strCLASS1; //商品大類
                            strSRC_AMT = prdInfo.strSRC_AMT; //原售價
                            strSupName = prdInfo.strSUPName; //供應商名

                            switch (int.Parse(strCLASS1))
                            {
                                case 1: strCLASSDescp = "藥品"; break;
                                case 2: strCLASSDescp = "醫療保健"; break;
                                case 3: strCLASSDescp = "彩妝保養"; break;
                                case 4: strCLASSDescp = "生活百貨"; break;
                                case 5: strCLASSDescp = "食品"; break;
                            }

                            strSALE_AMT = row["MC005"].ToString(); //現售價
                            strSALE_AMT = decimal.Parse(strSALE_AMT).ToString("0.###");

                            strMC006_CMPrice = row["MC006"] == null ? "0" : row["MC006"].ToString(); //會員價
                            strMC006_CMPrice = decimal.Parse(strMC006_CMPrice).ToString("0.###");

                            strPMFMDate = row["MB012"].ToString(); //起始日
                            strPMTODate = row["MB013"].ToString(); //結束日

                            if (string.IsNullOrEmpty(strSALE_AMT.Trim()) || strSALE_AMT.Trim() == "0")
                            {
                                strSALE_AMT = prdInfo.strSRC_AMT.Trim();
                                strSALE_AMT = decimal.Parse(strSALE_AMT).ToString("0.###");
                            }

                            //Application.DoEvents();
                        }
                    }
                }
                #endregion

                #endregion

                #region TABLE:[POSMJ]

                if (!string.IsNullOrEmpty(strTmpSKU.Trim()) && string.IsNullOrEmpty(strTmpPMCode.Trim())) //條碼 查詢
                {
                    filter = string.Format("POSMJ.MJ004='{0}'", strCM_SKU);
                }
                else if (!string.IsNullOrEmpty(strTmpSKU.Trim()) && !string.IsNullOrEmpty(strTmpPMCode.Trim())) //條碼+活動代碼 查詢
                {
                    filter = string.Format("POSMJ.MJ004='{0}' AND POSMJ.MJ003='{1}'", strCM_SKU.Trim(), strTmpPMCode);
                }
                else if (string.IsNullOrEmpty(strTmpSKU.Trim()) && !string.IsNullOrEmpty(strTmpPMCode.Trim())) //活動代碼 查詢
                {
                    filter = string.Format("POSMJ.MJ003='{0}'", strTmpPMCode);
                }

                sqlCmd = sql.GetCommand("GetPOSMJ_1", new string[] { filter });

                dsTmp = new DataSet();
                dsTmp = webSvc.uGetNeedData(1, sqlCmd, "LOG_POSMBC");
                if (dsTmp == null || dsTmp.Tables.Count == 0 || dsTmp.Tables[0].Rows.Count == 0)
                {
                    //如果無資料，移除JOIN條件再取一次
                    sqlCmd = sql.GetCommand("GetPOSMJ_2", new string[] { filter });
                    dsTmp = new DataSet();
                    dsTmp = webSvc.uGetNeedData(1, sqlCmd, "LOG_POSMBC");
                }

                //所有活動促銷都找不到時,才帶出商品基本資料
                if (dsTmp == null || dsTmp.Tables.Count == 0)
                {
                    if (!string.IsNullOrEmpty(strTmpSKU.Trim()) && string.IsNullOrEmpty(strPMCode.Trim()))
                    {
                        if (string.IsNullOrEmpty(strSALE_AMT.Trim()) || strSALE_AMT.Trim() == "0")
                            strSALE_AMT = strSRC_AMT.Trim();

                        //Application.DoEvents();
                    }
                }

                if (dsTmp.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsTmp.Tables[0].Rows)
                    {
                        strPMCode = row["MJ003"].ToString(); //活動代碼(第二層)
                        strCM_SKU = row["MJ004"].ToString(); //鼎新商品品號

                        prdInfo = GetDataSystemPrd(strCM_SKU, out errMsg);
                        if (prdInfo == null)
                        {
                            MessageBox.Show("連接資料庫發生問題,請稍候再試!若問題持續發生,請洽詢相關人員(POSMJ-2)!! 錯誤訊息： " + errMsg);
                            return;
                        }

                        strTmpSKU = prdInfo.strSKU; //國際條碼
                        strSTYDescp = prdInfo.strGoodsDESCP; //商品品名
                        strCLASS1 = prdInfo.strCLASS1; //商品大類
                        strSRC_AMT = prdInfo.strSRC_AMT; //原售價

                        switch (int.Parse(strCLASS1))
                        {
                            case 1: strCLASSDescp = "藥品"; break;
                            case 2: strCLASSDescp = "醫療保健"; break;
                            case 3: strCLASSDescp = "彩妝保養"; break;
                            case 4: strCLASSDescp = "生活百貨"; break;
                            case 5: strCLASSDescp = "食品"; break;
                        }

                        strSALE_AMT = ""; //現售價
                        strPMFMDate = row["MI005"].ToString(); //起始日
                        strPMTODate = row["MI006"].ToString(); //結束日
                        strMemo = row["MI004"].ToString(); //活動名稱
                        bLocPM = true;

                        //Application.DoEvents();
                    }
                }
                else
                {
                    //所有活動促銷都找不到時,才帶出商品基本資料
                    if (!string.IsNullOrEmpty(strTmpSKU.Trim()) && string.IsNullOrEmpty(strPMCode.Trim()))
                    {
                        if (string.IsNullOrEmpty(strSALE_AMT.Trim()) || strSALE_AMT.Trim() == "0")
                            strSALE_AMT = strSRC_AMT.Trim();

                        //Application.DoEvents();
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢商品POS資訊時發生錯誤!錯誤訊息: " + ex.ToString());
            }
            finally
            {
                dsTmp = null;
                webSvc = null;
            }

            laPrdOriPrice.Text = string.IsNullOrEmpty(strSRC_AMT.Replace(" ", "")) || strSRC_AMT == "0" ? "" : decimal.Parse(strSRC_AMT.Trim()).ToString("0.###");
            laPrdPrice.Text = string.IsNullOrEmpty(strSALE_AMT.Replace(" ", "")) || strSALE_AMT == "0" ? "" : decimal.Parse(strSALE_AMT.Trim()).ToString("0.###");
            laPrdMbPrice.Text = string.IsNullOrEmpty(strMC006_CMPrice.Replace(" ", "")) || strMC006_CMPrice == "0" ? "" : decimal.Parse(strMC006_CMPrice.Trim()).ToString("0.###");
            laCampaignSdate.Text = string.IsNullOrEmpty(strPMFMDate) ? "" : strPMFMDate.Trim().Length >= 8 ? strPMFMDate.Trim().Substring(0, 8) : strPMFMDate.Trim().Substring(0, strPMFMDate.Trim().Length);
            laCampaignEdate.Text = string.IsNullOrEmpty(strPMTODate) ? "" : strPMTODate.Trim().Length >= 8 ? strPMTODate.Trim().Substring(0, 8) : strPMTODate.Trim().Substring(0, strPMTODate.Trim().Length);
            laMemo.Text = string.IsNullOrEmpty(strMemo) ? "" : strMemo;
        }

        /// <summary>
        /// 傳回鼎新品號名稱
        /// </summary>
        /// <param name="_ean11"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public DataSystemsPrd GetDataSystemPrd(string _ean11, out string errMsg)
        {
            try
            {
                if (webSvc == null)
                {
                    webSvc = new JPMed_PDA.JPMed_WebService.Service1();
                    webSvc.Url = new ConfigSettings().WebServiceUrl;
                }

                errMsg = string.Empty;
                DataSystemsPrd CRMSTYLE = new DataSystemsPrd();
                string sqlCmd = sql.GetCommand("GetDataSystemPrd", new string[] { _ean11 });
                using (DataSet ds = webSvc.uGetNeedData(1, sqlCmd, "INVMB"))
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            CRMSTYLE.strCMSKU = row["MB001"] == null ? "" : row["MB001"].ToString();
                            CRMSTYLE.strGoodsDESCP = row["MB002"] == null ? "未命名" : row["MB002"].ToString().Replace(",", "-").Replace("'", "''");
                            CRMSTYLE.strExpDate = row["MB031"] == null ? "" : row["MB031"].ToString();
                            CRMSTYLE.strSUP_ID = row["MB032"] == null ? "無供應商名" : row["MB032"].ToString();
                            CRMSTYLE.strSUPName = row["MA002"] == null ? "無供應商名" : row["MA002"].ToString().Trim().Length >= 10 ?
                                                                                        row["MA002"].ToString().Trim().Substring(0, 10) : 
                                                                                        row["MA002"].ToString().Trim().Substring(0, row["MA002"].ToString().Trim().Length);

                            CRMSTYLE.strSUP_FullName = row["MA003"] == null ? "無供應商-全名" : row["MA003"].ToString().Replace(",", "");
                            CRMSTYLE.strSUP_TEL = row["MA008"] == null ? "無供應商-電話" : row["MA008"].ToString();
                            CRMSTYLE.strSUP_FAX = row["MA010"] == null ? "無供應商-FAX" : row["MA010"].ToString();
                            CRMSTYLE.strSUP_Contacter = row["MA013"] == null ? "無供應商-連絡人" : row["MA013"].ToString();
                            CRMSTYLE.strSUP_ADD = row["MA014"] == null ? "無供應商-地址" : row["MA014"].ToString().Replace(",", "，");

                            CRMSTYLE.strSUP_PayCond = row["MA025"] == null ? "無供應商-付款條件" : row["MA025"].ToString().Replace(",", "，");
                            CRMSTYLE.strSUP_MA044 = row["MA044"] == null ? "" : row["MA044"].ToString();
                            CRMSTYLE.strSUP_PayCond_NO = row["MA055"] == null ? "無供應商-付款" : row["MA055"].ToString();
                            CRMSTYLE.strSUPPLY_INVO = row["MA005"] == null ? "12345678" : row["MA005"].ToString();
                            CRMSTYLE.strSUPINVO_Count = row["MA030"] == null ? "0" : row["MA030"].ToString();

                            CRMSTYLE.strPT_Flag = row["MB194"] == null ? "0" : row["MB194"].ToString();
                            CRMSTYLE.strGoodsUnit = row["MB004"] == null ? "EA" : row["MB004"].ToString();
                            CRMSTYLE.decGoodsCost = row["MB046"] == null ? "" : row["MB046"].ToString();
                            CRMSTYLE.strCLASS1 = row["MB005"] == null ? "" : row["MB005"].ToString();
                            CRMSTYLE.strSKU_MB013 = row["MB013"] == null ? "" : row["MB013"].ToString();

                            CRMSTYLE.strSKU = row["MB003"] == null ? (row["MB013"] == null ? "" : row["MB013"].ToString()) : row["MB003"].ToString();
                            CRMSTYLE.strSRC_AMT = row["MB051"] == null ? "" : row["MB051"].ToString();
                            CRMSTYLE.intLimitQTY = row["MB039"] == null ? "" : row["MB039"].ToString();
                            CRMSTYLE.strNOGoods_MB081Flag = row["MB081"] == null ? "" : row["MB081"].ToString();
                            CRMSTYLE.strMB118_426 = row["MB118"] == null ? "" : row["MB118"].ToString();

                            CRMSTYLE.strMB115 = row["MB115"] == null ? "A01" : row["MB115"].ToString();
                        }
                    }
                    else
                    {
                        CRMSTYLE.strCMSKU = _ean11.Trim();
                        CRMSTYLE.strGoodsDESCP = "未命名";
                        CRMSTYLE.strGoodsUnit = "";
                        CRMSTYLE.decGoodsCost = "0";
                        CRMSTYLE.strCLASS1 = "0";
                        CRMSTYLE.strSKU = _ean11.Trim();
                        CRMSTYLE.strSRC_AMT = "0";
                        CRMSTYLE.intLimitQTY = "0";
                        CRMSTYLE.strSUPName = "";
                        CRMSTYLE.strSUP_FullName = "";
                        CRMSTYLE.strSUP_TEL = "";
                        CRMSTYLE.strSUP_FAX = "";
                        CRMSTYLE.strSUP_Contacter = "";
                        CRMSTYLE.strSUP_ADD = "";
                        CRMSTYLE.strSUP_PayCond = "";
                        CRMSTYLE.strSUP_ID = "";
                        CRMSTYLE.strPT_Flag = "0";
                        CRMSTYLE.strSUPPLY_INVO = "";
                        CRMSTYLE.strSUPINVO_Count = "0";
                        CRMSTYLE.strSUP_MA044 = "";
                        CRMSTYLE.strNOGoods_MB081Flag = "";
                    }
                }

                return CRMSTYLE;
            }
            catch (Exception ex)
            {
                errMsg = ex.ToString() + "->目前資料庫正忙碌中,請稍後再執行";
                return null;
            }
        }
    }
}
