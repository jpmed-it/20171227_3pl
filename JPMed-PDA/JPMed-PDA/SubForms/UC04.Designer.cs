﻿namespace JPMed_PDA.SubForms
{
    partial class UC04
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.laNeedQty = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.laEdate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.laSdate = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSin = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.laProductName = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgList = new System.Windows.Forms.DataGrid();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbGmo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgList2 = new System.Windows.Forms.DataGrid();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.laRealQty = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.laEan11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // laNeedQty
            // 
            this.laNeedQty.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laNeedQty.ForeColor = System.Drawing.Color.Black;
            this.laNeedQty.Location = new System.Drawing.Point(99, 215);
            this.laNeedQty.Name = "laNeedQty";
            this.laNeedQty.Size = new System.Drawing.Size(66, 20);
            this.laNeedQty.Text = "399.000";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(11, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.Text = "應到(退)量:";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(254, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 16);
            this.label12.Text = ")";
            // 
            // laEdate
            // 
            this.laEdate.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline);
            this.laEdate.Location = new System.Drawing.Point(137, 97);
            this.laEdate.Name = "laEdate";
            this.laEdate.Size = new System.Drawing.Size(120, 16);
            this.laEdate.Text = "2016年11月15號";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(88, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 16);
            this.label11.Text = "迄日：";
            // 
            // laSdate
            // 
            this.laSdate.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline);
            this.laSdate.Location = new System.Drawing.Point(137, 77);
            this.laSdate.Name = "laSdate";
            this.laSdate.Size = new System.Drawing.Size(120, 16);
            this.laSdate.Text = "2016年11月15號";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(83, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 16);
            this.label9.Text = "(起日：";
            // 
            // cbSin
            // 
            this.cbSin.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.cbSin.Location = new System.Drawing.Point(62, 79);
            this.cbSin.Name = "cbSin";
            this.cbSin.Size = new System.Drawing.Size(15, 14);
            this.cbSin.TabIndex = 212;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(9, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.Text = "特收：";
            // 
            // laKey
            // 
            this.laKey.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laKey.ForeColor = System.Drawing.Color.Black;
            this.laKey.Location = new System.Drawing.Point(3, 383);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(129, 20);
            this.laKey.Visible = false;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label50.Location = new System.Drawing.Point(12, 195);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(45, 20);
            this.label50.Text = "品名:";
            // 
            // laProductName
            // 
            this.laProductName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laProductName.ForeColor = System.Drawing.Color.Black;
            this.laProductName.Location = new System.Drawing.Point(57, 195);
            this.laProductName.Name = "laProductName";
            this.laProductName.Size = new System.Drawing.Size(234, 20);
            this.laProductName.Text = "查無商品資訊";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.btnSave.Location = new System.Drawing.Point(228, 235);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 27);
            this.btnSave.TabIndex = 211;
            this.btnSave.Text = "輸入(覆蓋)";
            // 
            // dgList
            // 
            this.dgList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgList.Location = new System.Drawing.Point(3, 278);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(312, 55);
            this.dgList.TabIndex = 210;
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.tbQty.Location = new System.Drawing.Point(116, 236);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(98, 26);
            this.tbQty.TabIndex = 209;
            this.tbQty.Text = "399.000";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(11, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 20);
            this.label7.Text = "本次收(退)量:";
            // 
            // laEmpName
            // 
            this.laEmpName.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Underline);
            this.laEmpName.Location = new System.Drawing.Point(179, 53);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(123, 16);
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular);
            this.tbEmpCode.Location = new System.Drawing.Point(65, 48);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 26);
            this.tbEmpCode.TabIndex = 208;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(10, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 20);
            this.label5.Text = "員編：";
            // 
            // tbSN
            // 
            this.tbSN.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.tbSN.Location = new System.Drawing.Point(137, 171);
            this.tbSN.MaxLength = 18;
            this.tbSN.Name = "tbSN";
            this.tbSN.Size = new System.Drawing.Size(157, 24);
            this.tbSN.TabIndex = 207;
            this.tbSN.Text = "A12345678909876522";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(11, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.Text = "商品條碼(EAN):";
            // 
            // btnShowAll
            // 
            this.btnShowAll.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnShowAll.Location = new System.Drawing.Point(243, 145);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(72, 24);
            this.btnShowAll.TabIndex = 206;
            this.btnShowAll.Text = "完整";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(10, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 20);
            this.label3.Text = "輸入資料:";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnExit.Location = new System.Drawing.Point(243, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 24);
            this.btnExit.TabIndex = 205;
            this.btnExit.Text = "離開";
            // 
            // laSName
            // 
            this.laSName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSName.Location = new System.Drawing.Point(65, 26);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(149, 20);
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSCode.Location = new System.Drawing.Point(10, 26);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(49, 20);
            this.laSCode.Text = "T001";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(8, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 20);
            this.label1.Text = "所在門市：";
            // 
            // tbGmo
            // 
            this.tbGmo.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.tbGmo.Location = new System.Drawing.Point(65, 117);
            this.tbGmo.MaxLength = 22;
            this.tbGmo.Name = "tbGmo";
            this.tbGmo.Size = new System.Drawing.Size(184, 24);
            this.tbGmo.TabIndex = 228;
            this.tbGmo.Text = "A123456789098765222222";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(11, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 20);
            this.label2.Text = "單號：";
            // 
            // dgList2
            // 
            this.dgList2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgList2.Location = new System.Drawing.Point(3, 351);
            this.dgList2.Name = "dgList2";
            this.dgList2.Size = new System.Drawing.Size(312, 55);
            this.dgList2.TabIndex = 230;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(3, 336);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(181, 17);
            this.label10.Text = "前次收(退)結果:";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(2, 263);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(181, 17);
            this.label13.Text = "收(退)結果:";
            // 
            // laRealQty
            // 
            this.laRealQty.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laRealQty.ForeColor = System.Drawing.Color.Black;
            this.laRealQty.Location = new System.Drawing.Point(246, 214);
            this.laRealQty.Name = "laRealQty";
            this.laRealQty.Size = new System.Drawing.Size(64, 20);
            this.laRealQty.Text = "399.000";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label15.Location = new System.Drawing.Point(160, 214);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 20);
            this.label15.Text = "已收(退)量:";
            // 
            // laEan11
            // 
            this.laEan11.Location = new System.Drawing.Point(88, 149);
            this.laEan11.Name = "laEan11";
            this.laEan11.Size = new System.Drawing.Size(100, 20);
            this.laEan11.Text = "laEan11";
            this.laEan11.Visible = false;
            // 
            // UC04
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Thistle;
            this.Controls.Add(this.laEan11);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.dgList2);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.laRealQty);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbGmo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laNeedQty);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.laEdate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.laSdate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbSin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laKey);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.laProductName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnShowAll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.label1);
            this.Name = "UC04";
            this.Size = new System.Drawing.Size(318, 425);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label laNeedQty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label laEdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label laSdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbSin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label laProductName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGrid dgList;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbGmo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGrid dgList2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label laRealQty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label laEan11;
    }
}
