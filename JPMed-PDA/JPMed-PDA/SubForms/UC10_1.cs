﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC10_1 : UserControl
    {
        string formName = "倉管作業系統 -- 物流到貨作業";
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        BindingSource source = new BindingSource();
        DataTable tmpDT;

        public UC10_1(List<string> keys, ref DBConnection dbConn)
        {
            InitializeComponent();

            try
            {
                //更新下方DataGrid
                DataTable BENDRDataTable = new DataTable();
                BENDRDataTable.Columns.Add(new DataColumn("單號"));
                for (int i = 0; i < keys.Count; i++)
                {
                    BENDRDataTable.Rows.Add(keys[i]);
                }
                dgList.DataSource = BENDRDataTable;
                BindDataGridStyle(BENDRDataTable);

                laCount.Text = Convert.ToString(keys.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢資料發生錯誤: " + ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉:[物流到貨(資料查詢)]作業?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;

                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == typeof(UC10).Name)
                    {
                        control.Visible = true;

                        foreach (Control controlitem in control.Controls)
                        {
                            if (controlitem.Name == "tbSN")
                            {
                                controlitem.Focus();
                                ((TextBox)controlitem).SelectAll();
                                break;
                            }
                        }
                        break;
                    }
                }

                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == this.Name)
                    {
                        this.Parent.Controls.Remove(control);
                        break;
                    }
                }
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                tb.Width = 300;

                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
