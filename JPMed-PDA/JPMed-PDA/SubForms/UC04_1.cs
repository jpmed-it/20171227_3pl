﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC04_1 : UserControl
    {
        string formName = "倉管作業系統 -- 驗收/退貨作業";
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        SQLCreator sql = new SQLCreator("UC04_1");
        BindingSource source = new BindingSource();
        DataTable tmpDT;

        public UC04_1(List<string> _key, ref DBConnection dbConn)
        {
            InitializeComponent();

            try
            {
                DataTable dtUnion = null;
                foreach (string keyValue in _key)
                {
                    string sqlCmd = sql.GetCommand("GetSNInfo", new string[] { keyValue.Split(';')[0], keyValue.Split(';')[1], keyValue.Split(';')[2], keyValue.Split(';')[3] });
                    tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (dtUnion == null) dtUnion = tmpDT;
                    else dtUnion = dtUnion.AsEnumerable().Union(tmpDT.AsEnumerable()).CopyToDataTable<DataRow>();
                }

                source.DataSource = dtUnion;
                dgList.DataSource = source;
                source.ResetBindings(false);
                BindDataGridStyle(dtUnion);

                laCount.Text = dtUnion.AsEnumerable().Sum(r => Decimal.Parse(r["應到數量"].ToString())).ToString("0.###");
                //laQty.Text = tmpDT.AsEnumerable().Sum(r => Decimal.Parse(r["本次收發量"].ToString()) + Decimal.Parse(r["實際量"].ToString())).ToString("0.###");
                laQty.Text = dtUnion.AsEnumerable().Sum(r => Decimal.Parse(r["實際量"].ToString())).ToString("0.###");
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢資料發生錯誤: " + ex.ToString());
            }
        }

        public UC04_1(string _bsart, string _ebeln, string _bednr, string _prdFilter, ref DBConnection dbConn)
        {
            InitializeComponent();

            try
            {
                string sqlCmd = sql.GetCommand("GetSNInfo", new string[] { _bsart, _ebeln, _bednr, _prdFilter });
                tmpDT = dbConn.GetDataTable(sqlCmd);
                source.DataSource = tmpDT;
                dgList.DataSource = source;
                source.ResetBindings(false);
                BindDataGridStyle(tmpDT);

                laCount.Text = tmpDT.AsEnumerable().Sum(r => Decimal.Parse(r["應到數量"].ToString())).ToString("0.###");
                //laQty.Text = tmpDT.AsEnumerable().Sum(r => Decimal.Parse(r["本次收發量"].ToString()) + Decimal.Parse(r["實際量"].ToString())).ToString("0.###");
                laQty.Text = tmpDT.AsEnumerable().Sum(r => Decimal.Parse(r["實際量"].ToString())).ToString("0.###");
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢資料發生錯誤: " + ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉:[收發貨(資料查詢)]作業?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                
                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == typeof(UC04).Name)
                    {
                        control.Visible = true;

                        foreach (Control controlitem in control.Controls)
                        {
                            if (controlitem.Name == "tbSN")
                            {
                                controlitem.Focus();
                                ((TextBox)controlitem).SelectAll();
                                break;
                            }
                        }
                        break;
                    }
                }

                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == this.Name)
                    {
                        this.Parent.Controls.Remove(control);
                        break;
                    }
                }
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                //DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                MyDataGridTextBoxColumn tb = new MyDataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;
                tb.dv = dt.DefaultView;
                tb.sw = "RANK";

                switch (col.ColumnName.ToUpper())
                {
                    case "應到數量":
                    case "本次收發量":
                    case "實際量":
                        tb.Format = "0.###";
                        break;
                }

                if (col.ColumnName.ToUpper() == "RANK" || col.ColumnName.ToUpper() == "CANEDIT" || col.ColumnName.ToUpper() == "CANDO") tb.Width = 0;
                else tb.Width = 150;

                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }

        public class MyDataGridTextBoxColumn : DataGridTextBoxColumn
        {
            public DataView dv = null;
            public String sw = "";
            protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
            {
                try
                {
                    switch (dv[rowNum].Row[sw].ToString())
                    {
                        case "1": backBrush = new System.Drawing.SolidBrush(Color.White); break;
                        case "2": backBrush = new System.Drawing.SolidBrush(Color.LightPink); break;
                        case "3": backBrush = new System.Drawing.SolidBrush(Color.LightGreen); break;
                        case "4": backBrush = new System.Drawing.SolidBrush(Color.Red); break;
                        default: break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("重新繪製表格發生錯誤: " + ex.Message);
                }
                finally
                {
                    base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
                }
            }
        }
    }
}
