﻿namespace JPMed_PDA.SubForms
{
    partial class UC02
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.laPrdQty = new System.Windows.Forms.Label();
            this.laPrdSapSN = new System.Windows.Forms.Label();
            this.laPrdName = new System.Windows.Forms.Label();
            this.laPrdOriPrice = new System.Windows.Forms.Label();
            this.laPrdPrice = new System.Windows.Forms.Label();
            this.laPrdMbPrice = new System.Windows.Forms.Label();
            this.laCampaignSdate = new System.Windows.Forms.Label();
            this.laCampaignEdate = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.laWarm = new System.Windows.Forms.Label();
            this.laMemo = new System.Windows.Forms.Label();
            this.laWarm2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 20);
            this.label1.Text = "所在門市：";
            // 
            // laSCode
            // 
            this.laSCode.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSCode.Location = new System.Drawing.Point(10, 39);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(49, 20);
            this.laSCode.Text = "T001";
            // 
            // laSName
            // 
            this.laSName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSName.Location = new System.Drawing.Point(65, 39);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(149, 20);
            this.laSName.Text = "士林門市";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnExit.Location = new System.Drawing.Point(227, 13);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 24);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "離開";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnSearch.Location = new System.Drawing.Point(227, 94);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(72, 24);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "查詢";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(10, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 20);
            this.label2.Text = "商品條碼(EAN)：";
            // 
            // tbSN
            // 
            this.tbSN.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.tbSN.Location = new System.Drawing.Point(10, 94);
            this.tbSN.MaxLength = 18;
            this.tbSN.Name = "tbSN";
            this.tbSN.Size = new System.Drawing.Size(204, 24);
            this.tbSN.TabIndex = 0;
            this.tbSN.Text = "A123456789098765";
            this.tbSN.TextChanged += new System.EventHandler(this.tbSN_TextChanged);
            this.tbSN.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSN_KeyUp);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(10, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 20);
            this.label3.Text = "庫存量：";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(10, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.Text = "SAP物料編碼：";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(10, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 20);
            this.label5.Text = "品名：";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(10, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 20);
            this.label6.Text = "原售價：";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(10, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 20);
            this.label7.Text = "現售價：";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(10, 255);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 20);
            this.label8.Text = "會員時價：";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(10, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 20);
            this.label9.Text = "活動期：";
            // 
            // laPrdQty
            // 
            this.laPrdQty.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laPrdQty.Location = new System.Drawing.Point(80, 155);
            this.laPrdQty.Name = "laPrdQty";
            this.laPrdQty.Size = new System.Drawing.Size(219, 20);
            this.laPrdQty.Text = "10";
            // 
            // laPrdSapSN
            // 
            this.laPrdSapSN.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laPrdSapSN.Location = new System.Drawing.Point(119, 175);
            this.laPrdSapSN.Name = "laPrdSapSN";
            this.laPrdSapSN.Size = new System.Drawing.Size(180, 20);
            this.laPrdSapSN.Text = "A123456789009876";
            // 
            // laPrdName
            // 
            this.laPrdName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.laPrdName.Location = new System.Drawing.Point(65, 195);
            this.laPrdName.Name = "laPrdName";
            this.laPrdName.Size = new System.Drawing.Size(234, 20);
            this.laPrdName.Text = "衛生紙";
            // 
            // laPrdOriPrice
            // 
            this.laPrdOriPrice.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.laPrdOriPrice.Location = new System.Drawing.Point(80, 215);
            this.laPrdOriPrice.Name = "laPrdOriPrice";
            this.laPrdOriPrice.Size = new System.Drawing.Size(219, 20);
            this.laPrdOriPrice.Text = "10";
            // 
            // laPrdPrice
            // 
            this.laPrdPrice.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.laPrdPrice.Location = new System.Drawing.Point(80, 235);
            this.laPrdPrice.Name = "laPrdPrice";
            this.laPrdPrice.Size = new System.Drawing.Size(219, 20);
            this.laPrdPrice.Text = "10";
            // 
            // laPrdMbPrice
            // 
            this.laPrdMbPrice.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.laPrdMbPrice.Location = new System.Drawing.Point(93, 255);
            this.laPrdMbPrice.Name = "laPrdMbPrice";
            this.laPrdMbPrice.Size = new System.Drawing.Size(206, 20);
            this.laPrdMbPrice.Text = "10";
            // 
            // laCampaignSdate
            // 
            this.laCampaignSdate.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laCampaignSdate.Location = new System.Drawing.Point(80, 275);
            this.laCampaignSdate.Name = "laCampaignSdate";
            this.laCampaignSdate.Size = new System.Drawing.Size(80, 20);
            this.laCampaignSdate.Text = "20160101";
            // 
            // laCampaignEdate
            // 
            this.laCampaignEdate.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laCampaignEdate.Location = new System.Drawing.Point(177, 275);
            this.laCampaignEdate.Name = "laCampaignEdate";
            this.laCampaignEdate.Size = new System.Drawing.Size(86, 20);
            this.laCampaignEdate.Text = "20160101";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(156, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 20);
            this.label10.Text = "~";
            // 
            // laWarm
            // 
            this.laWarm.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laWarm.ForeColor = System.Drawing.Color.Red;
            this.laWarm.Location = new System.Drawing.Point(10, 128);
            this.laWarm.Name = "laWarm";
            this.laWarm.Size = new System.Drawing.Size(204, 20);
            this.laWarm.Text = "查無相關資訊";
            // 
            // laMemo
            // 
            this.laMemo.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laMemo.Location = new System.Drawing.Point(65, 295);
            this.laMemo.Name = "laMemo";
            this.laMemo.Size = new System.Drawing.Size(234, 20);
            this.laMemo.Text = "新春好禮大放送";
            // 
            // laWarm2
            // 
            this.laWarm2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laWarm2.ForeColor = System.Drawing.Color.Red;
            this.laWarm2.Location = new System.Drawing.Point(10, 128);
            this.laWarm2.Name = "laWarm2";
            this.laWarm2.Size = new System.Drawing.Size(204, 20);
            this.laWarm2.Text = "本商品已跨店廢番";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(10, 295);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 20);
            this.label11.Text = "備註:";
            // 
            // UC02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Controls.Add(this.label11);
            this.Controls.Add(this.laWarm2);
            this.Controls.Add(this.laMemo);
            this.Controls.Add(this.laWarm);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.laCampaignEdate);
            this.Controls.Add(this.laCampaignSdate);
            this.Controls.Add(this.laPrdMbPrice);
            this.Controls.Add(this.laPrdPrice);
            this.Controls.Add(this.laPrdOriPrice);
            this.Controls.Add(this.laPrdName);
            this.Controls.Add(this.laPrdSapSN);
            this.Controls.Add(this.laPrdQty);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.label1);
            this.Name = "UC02";
            this.Size = new System.Drawing.Size(318, 425);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label laPrdQty;
        private System.Windows.Forms.Label laPrdSapSN;
        private System.Windows.Forms.Label laPrdName;
        private System.Windows.Forms.Label laPrdOriPrice;
        private System.Windows.Forms.Label laPrdPrice;
        private System.Windows.Forms.Label laPrdMbPrice;
        private System.Windows.Forms.Label laCampaignSdate;
        private System.Windows.Forms.Label laCampaignEdate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label laWarm;
        private System.Windows.Forms.Label laMemo;
        private System.Windows.Forms.Label laWarm2;
        private System.Windows.Forms.Label label11;
    }
}
