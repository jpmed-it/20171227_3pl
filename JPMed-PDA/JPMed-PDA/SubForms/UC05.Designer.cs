﻿namespace JPMed_PDA.SubForms
{
    partial class UC05
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.label50 = new System.Windows.Forms.Label();
            this.laProductName = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgList = new System.Windows.Forms.DataGrid();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboTransferCode = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.laDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.laEan11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbDeliveryBy3PL = new System.Windows.Forms.CheckBox();
            this.cbDeliveryBySelf = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label50.Location = new System.Drawing.Point(20, 218);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(55, 20);
            this.label50.Text = "品名：";
            // 
            // laProductName
            // 
            this.laProductName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laProductName.ForeColor = System.Drawing.Color.Black;
            this.laProductName.Location = new System.Drawing.Point(75, 218);
            this.laProductName.Name = "laProductName";
            this.laProductName.Size = new System.Drawing.Size(224, 20);
            this.laProductName.Text = "查無商品資訊";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular);
            this.btnSave.Location = new System.Drawing.Point(228, 242);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 27);
            this.btnSave.TabIndex = 140;
            this.btnSave.Text = "輸入(覆蓋)";
            // 
            // dgList
            // 
            this.dgList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgList.Location = new System.Drawing.Point(3, 275);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(312, 83);
            this.dgList.TabIndex = 139;
            // 
            // tbQty
            // 
            this.tbQty.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular);
            this.tbQty.Location = new System.Drawing.Point(96, 243);
            this.tbQty.MaxLength = 14;
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(118, 26);
            this.tbQty.TabIndex = 138;
            this.tbQty.Text = "1111111111.000";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(19, 245);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 20);
            this.label7.Text = "調撥數量:";
            // 
            // cboTransferCode
            // 
            this.cboTransferCode.Location = new System.Drawing.Point(90, 115);
            this.cboTransferCode.Name = "cboTransferCode";
            this.cboTransferCode.Size = new System.Drawing.Size(200, 23);
            this.cboTransferCode.TabIndex = 137;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(10, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 20);
            this.label6.Text = "調入門市:";
            // 
            // laEmpName
            // 
            this.laEmpName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline);
            this.laEmpName.Location = new System.Drawing.Point(179, 61);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(123, 16);
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular);
            this.tbEmpCode.Location = new System.Drawing.Point(65, 56);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 26);
            this.tbEmpCode.TabIndex = 136;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(10, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 20);
            this.label5.Text = "員編：";
            // 
            // tbSN
            // 
            this.tbSN.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.tbSN.Location = new System.Drawing.Point(145, 189);
            this.tbSN.MaxLength = 18;
            this.tbSN.Name = "tbSN";
            this.tbSN.Size = new System.Drawing.Size(157, 24);
            this.tbSN.TabIndex = 135;
            this.tbSN.Text = "A12345678909876522";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(19, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.Text = "商品條碼(EAN)：";
            // 
            // btnShowAll
            // 
            this.btnShowAll.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnShowAll.Location = new System.Drawing.Point(243, 160);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(72, 24);
            this.btnShowAll.TabIndex = 134;
            this.btnShowAll.Text = "完整";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(10, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 20);
            this.label3.Text = "輸入資料：";
            // 
            // laDate
            // 
            this.laDate.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laDate.Location = new System.Drawing.Point(90, 87);
            this.laDate.Name = "laDate";
            this.laDate.Size = new System.Drawing.Size(149, 20);
            this.laDate.Text = "2016年04月01日";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(10, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.Text = "調撥日期:";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnExit.Location = new System.Drawing.Point(243, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 24);
            this.btnExit.TabIndex = 133;
            this.btnExit.Text = "離開";
            // 
            // laSName
            // 
            this.laSName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSName.Location = new System.Drawing.Point(65, 32);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(149, 20);
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSCode.Location = new System.Drawing.Point(10, 32);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(49, 20);
            this.laSCode.Text = "T001";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 20);
            this.label1.Text = "所在門市：";
            // 
            // laKey
            // 
            this.laKey.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laKey.ForeColor = System.Drawing.Color.Black;
            this.laKey.Location = new System.Drawing.Point(18, 388);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(129, 20);
            this.laKey.Visible = false;
            // 
            // laEan11
            // 
            this.laEan11.Location = new System.Drawing.Point(96, 164);
            this.laEan11.Name = "laEan11";
            this.laEan11.Size = new System.Drawing.Size(100, 20);
            this.laEan11.Text = "laEan11";
            this.laEan11.Visible = false;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(10, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 20);
            this.label8.Text = "物流派車：";
            // 
            // cbDeliveryBy3PL
            // 
            this.cbDeliveryBy3PL.Location = new System.Drawing.Point(100, 140);
            this.cbDeliveryBy3PL.Name = "cbDeliveryBy3PL";
            this.cbDeliveryBy3PL.Size = new System.Drawing.Size(36, 20);
            this.cbDeliveryBy3PL.TabIndex = 155;
            // 
            // cbDeliveryBySelf
            // 
            this.cbDeliveryBySelf.Location = new System.Drawing.Point(195, 140);
            this.cbDeliveryBySelf.Name = "cbDeliveryBySelf";
            this.cbDeliveryBySelf.Size = new System.Drawing.Size(43, 20);
            this.cbDeliveryBySelf.TabIndex = 157;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(138, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 20);
            this.label9.Text = "自送：";
            // 
            // UC05
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.Controls.Add(this.cbDeliveryBySelf);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbDeliveryBy3PL);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laEan11);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.laProductName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboTransferCode);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnShowAll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.laDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laKey);
            this.Name = "UC05";
            this.Size = new System.Drawing.Size(318, 425);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label laProductName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGrid dgList;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboTransferCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label laDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.Label laEan11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cbDeliveryBy3PL;
        private System.Windows.Forms.CheckBox cbDeliveryBySelf;
        private System.Windows.Forms.Label label9;
    }
}
