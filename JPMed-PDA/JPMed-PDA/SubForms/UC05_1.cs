﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC05_1 : UserControl
    {
        string formName = "倉管作業系統 -- 調撥門市作業";
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        SQLCreator sql = new SQLCreator("UC05_1");
        BindingSource source = new BindingSource();

        public UC05_1(string _bender, string _p_spbup, ref DBConnection dbConn)
        {
            InitializeComponent();

            try
            {
                string sqlCmd = sql.GetCommand("GetSelectCommand", new string[] { _bender, _p_spbup });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                source.DataSource = tmpDT;
                dgList.DataSource = source;
                source.ResetBindings(false);
                BindDataGridStyle(tmpDT);

                laCount.Text = tmpDT.Rows.Count.ToString();
                laQty.Text = tmpDT.AsEnumerable().Sum(r => Decimal.Parse(r["調撥數量"].ToString())).ToString("0.###");
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢資料發生錯誤: " + ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉:[調撥門市(資料查詢)]作業?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == typeof(UC05).Name)
                    {
                        control.Visible = true;

                        foreach (Control controlitem in control.Controls)
                        {
                            if (controlitem.Name == "tbSN")
                            {
                                controlitem.Focus();
                                ((TextBox)controlitem).SelectAll();
                                break;
                            }
                        }
                        break;
                    }
                }

                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == this.Name)
                    {
                        this.Parent.Controls.Remove(control);
                        break;
                    }
                }
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "調撥數量":
                    case "前週銷量":
                    case "調出門市庫存量":
                    case "調入門市庫存量":
                        tb.Format = "0.###";
                        break;
                }

                tb.Width = 150;
                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
