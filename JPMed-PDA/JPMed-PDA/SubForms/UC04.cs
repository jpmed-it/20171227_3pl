﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;
using JPMed_PDA.Entity;

namespace JPMed_PDA.SubForms
{
    public partial class UC04 : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        SQLCreator sql;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        BindingSource source2 = new BindingSource();
        DataTable defaultDT = null;
        DataTable GMOVEH;
        DataTable allData;
        Gmovet gmo;
        string key;
        int BeforeInput;
        int AfterInput;
        string befStr;
        string aftStr;
        List<string> prdList = new List<string>();
        List<string> listKey = new List<string>();

        public UC04()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            btnExit.Click += btnExit_Click;
            btnSave.Click += btnSave_Click;
            btnShowAll.Click += btnShowAll_Click;

            tbEmpCode.TextChanged += tbEmpCode_TextChanged;
            tbEmpCode.KeyUp += tbEmpCode_KeyUp;

            tbGmo.TextChanged += tbGmo_TextChanged;
            tbGmo.KeyUp += tbGmo_KeyUp;

            tbSN.TextChanged += tbSN_TextChanged;
            tbSN.KeyUp += tbSN_KeyUp;

            tbQty.KeyDown += tbQty_KeyDown;
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            cbSin.Checked = false;
            cbSin.Enabled = false;
            laSdate.Text = string.Empty;
            laEdate.Text = string.Empty;

            tbGmo.Text = string.Empty;
            tbGmo.Enabled = false;

            laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
            tbSN.Text = string.Empty;
            tbSN.Enabled = false;
            laProductName.Text = string.Empty;

            laNeedQty.Text = string.Empty;
            laRealQty.Text = string.Empty;

            tbQty.Text = string.Empty;
            tbQty.Enabled = false;

            btnShowAll.Enabled = false;
            btnSave.Enabled = false;

            dgList.DataSource = defaultDT;
            dgList2.DataSource = defaultDT;

            //暫時移除
            label15.Visible = true; // false;
            laRealQty.Visible = true; // false;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                sql = new SQLCreator("UC04");
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得特收日期
                string sqlCmd = sql.GetCommand("CheckSpecialIn", new string[] { storeCode });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                if (tmpDT.Rows.Count > 0)
                {
                    cbSin.Checked = true;
                    laSdate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLSD"].ToString()).ToString("D");
                    laEdate.Text = DateTime.Parse(tmpDT.Rows[0]["REPLDD"].ToString()).ToString("D");
                }

                //取得店家資訊
                sqlCmd = sql.GetCommand("GetStoreNameByCode", new string[] { storeCode });
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    tbGmo.Text = string.Empty;
                    tbGmo.Enabled = _enbleControl;

                    laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;

                    laProductName.Text = string.Empty;
                    laNeedQty.Text = string.Empty;
                    laRealQty.Text = string.Empty;

                    tbQty.Text = string.Empty;
                    tbQty.Enabled = _enbleControl;

                    btnShowAll.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    break;

                case 2:
                    tbGmo.Text = string.Empty;
                    tbGmo.Enabled = _enbleControl;
                    break;

                case 3:

                    laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                    tbSN.Text = string.Empty;
                    tbSN.Enabled = _enbleControl;

                    laProductName.Text = string.Empty;
                    laNeedQty.Text = string.Empty;
                    laRealQty.Text = string.Empty;

                    tbQty.Text = string.Empty;
                    tbQty.Enabled = _enbleControl;

                    btnShowAll.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    break;

            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉功能:[驗收/退貨作業]?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = sql.GetCommand("GetEmployeeNameByCode", new string[] { tbEmpCode.Text });
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    EnableControl(false, 1);
                    tbEmpCode.Focus();
                    tbEmpCode.SelectAll();
                    laEmpName.Text = "查無此編號";
                    laEmpName.ForeColor = Color.Red;
                    dgList.DataSource = defaultDT;
                    dgList2.DataSource = defaultDT;
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    laEmpName.ForeColor = Color.Black;
                    EnableControl(true, 2);
                    tbGmo.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.SelectAll();
                GetEmpName(tbEmpCode.Text);
            }
        }
        private void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbEmpCode.SelectAll();
            }
        }

        /// <summary>
        /// 單號change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbGmo_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbGmo.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                GmoSearch(true);
                tbGmo.SelectAll();
            }
        }
        private void tbGmo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GmoSearch(true);
                tbGmo.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 查詢單號
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GmoSearch(bool clearDGList)
        {
            try
            {
                //先清空資訊
                prdList.Clear();
                listKey.Clear();

                if (string.IsNullOrEmpty(tbGmo.Text))
                    return;

                string tmpVbeln = string.Empty;
                string sn = tbGmo.Text;
                string sqlCmd = string.Empty;

                //先清除控制項目在做查詢
                allData = null;

                if (clearDGList)
                {
                    dgList.DataSource = defaultDT;
                    dgList2.DataSource = defaultDT;
                }

                EnableControl(false, 3);

                if (string.IsNullOrEmpty(sn))
                {
                    MessageBox.Show("請輸入查詢單號");
                    return;
                }

                //先取得資訊
                sqlCmd = sql.GetCommand("GetGmovehInfo", new string[] { sn });
                GMOVEH = dbConn.GetDataTable(sqlCmd);

                if (GMOVEH.Rows.Count <= 0)
                {
                    //20160717新增需求，查不到單號者，需額外檢查VBELN
                    sqlCmd = sql.GetCommand("GetGmovetByVbeln", new string[] { sn });
                    DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (tmpDT.Rows.Count > 0)
                    {
                        string tmpSN = tmpDT.Rows[0]["EBELN"] == null ?
                                       tmpDT.Rows[0]["BEDNR"] == null ? "" : tmpDT.Rows[0]["BEDNR"].ToString() :
                                       tmpDT.Rows[0]["EBELN"].ToString();

                        if (string.IsNullOrEmpty(tmpSN))
                        {
                            MessageBox.Show("此單號查無對應單據，請確認!");
                            return;
                        }

                        tmpVbeln = sn;

                        #region ZRP1,ZRP3, ZRP5, VBELN 物流到貨作業檢查 及 到貨門市檢查
                        if (tmpDT.Rows[0]["BSART"].ToString() == "ZRP1" || tmpDT.Rows[0]["BSART"].ToString() == "ZRP3"
                            || tmpDT.Rows[0]["BSART"].ToString() == "ZRP5" || tmpDT.Rows[0]["BSART"].ToString() == "ZUB4")
                        {
                            sqlCmd = SqlStatement.UC04.GetWERKSByVBELN(sn);
                            DataTable dtWerks = dbConn.GetDataTable(sqlCmd);
                            if (dtWerks.Rows[0]["WERKS"].ToString() != storeCode)
                            {
                                MessageBox.Show("本門市非單據收貨門市(" + dtWerks.Rows[0]["WERKS"].ToString() + ")，請確認!");
                                tbGmo.SelectAll();
                                tbGmo.Focus();
                                return;
                            }

                            sqlCmd = SqlStatement.UC04.CheckGMOVETShip(sn, "AB");
                            DataTable dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                            if (dtProductShipCount.Rows.Count <= 0)
                            {
                                MessageBox.Show("物流到貨作業未操作");
                                tbGmo.SelectAll();
                                tbGmo.Focus();
                                return;
                            }
                        }
                        #endregion

                        prdList = (from row in tmpDT.AsEnumerable()
                                   select "'" + row["EAN11"].ToString() + "'").ToList();

                        sqlCmd = sql.GetCommand("GetGmovehInfo", new string[] { tmpSN });
                        GMOVEH = dbConn.GetDataTable(sqlCmd);
                    }
                    else
                    {
                        MessageBox.Show("此單號查無對應單據，請確認!");
                        return;
                    }
                }

                //從GMOVEH找出所有單據內容
                foreach (DataRow row in GMOVEH.Select())
                {
                    string bsart = row["BSART"].ToString(); //採購文件類型
                    string ebeln = row["EBELN"].ToString(); //採購文件號碼
                    string bednr = row["BEDNR"].ToString(); //需求追蹤號碼
                    string vbeln = row["EBELN"].ToString(); //預設值

                    string prdFilter = string.Empty;

                    if (prdList.Count > 0)
                    {
                        prdFilter = "A.EAN11 IN ({0})";
                        prdFilter = string.Format(prdFilter, string.Join(",", prdList.ToArray()));
                        vbeln = tbGmo.Text; //填入正確的VBELN
                    }
                    else prdFilter = "1=1";

                    if (!string.IsNullOrEmpty(tmpVbeln))
                        prdFilter += string.Format(" AND B.VBELN = '{0}'", tmpVbeln);

                    key = bsart + ";" + ebeln + ";" + bednr + ";" + prdFilter;

                    //如果為ZRP1 或 ZRP2，可能為同一採購文件號碼，需額外處理
                    if (bsart == "ZRP1" || bsart == "ZRP3" || bsart == "ZRP5" || bsart == "ZRP2" || bsart == "ZRP4" || bsart == "ZFG1")
                        listKey.Add(key);

                    sqlCmd = sql.GetCommand("GetSNInfo", new string[] { bsart, ebeln, bednr, prdFilter });
                    DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                    if (tmpDT.Rows.Count > 0)
                    {
                        if (allData == null) allData = tmpDT;
                        else allData = allData.AsEnumerable().Union(tmpDT.AsEnumerable()).CopyToDataTable<DataRow>();
                    }
                }

                //檢查單號是否存在
                if (allData == null || allData.Rows.Count <= 0)
                {
                    MessageBox.Show("此單號查無相關資訊!");
                    tbGmo.SelectAll();
                    tbGmo.Focus();
                }
                else
                {
                    #region 3PL物流/自送 建單檢查
                    //若為 ZUB1自送 或ZUB3 派車 ，則出貨(641) 建單檢查
                    if (allData.Rows[0]["BSART"].ToString() == "ZUB1" || allData.Rows[0]["BSART"].ToString() == "ZUB3")
                    {
                        sqlCmd = SqlStatement.UC04.GetSTO2STO(GMOVEH.Rows[0]["EBELN"].ToString());
                        DataTable dtSTO2STO = dbConn.GetDataTable(sqlCmd);

                        if (dtSTO2STO.Rows.Count == 0)
                        {
                            MessageBox.Show("此單號查無相關資訊!");
                            tbGmo.SelectAll();
                            tbGmo.Focus();
                            return;
                        }
                        string queryBWART = string.Empty;
                        string popupMsg = string.Empty;

                        //檢查是否已出貨(自送調出 或 物流收貨)
                        sqlCmd = SqlStatement.UC04.CheckProductShip(GMOVEH.Rows[0]["EBELN"].ToString(), "641");
                        DataTable dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                        if (dtProductShipCount.Rows.Count <= 0)
                        {
                            MessageBox.Show("該品項無建單無法驗收");
                            tbGmo.SelectAll();
                            tbGmo.Focus();
                            return;
                        }

                        //若為物流派車，則增加檢查物流到貨(AB)
                        if (dtSTO2STO.Rows[0]["WMFLG"].ToString() == "X")
                        {
                            sqlCmd =
                                SqlStatement.UC04.CheckProductShip(GMOVEH.Rows[0]["EBELN"].ToString(), "AB");
                            dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                            if (dtProductShipCount.Rows.Count <= 0)
                            {
                                MessageBox.Show("物流到貨作業未操作");
                                tbGmo.SelectAll();
                                tbGmo.Focus();
                                return;
                            }
                        }
                    }

                    #endregion 3PL物流/自送 建單檢查

                    //若不等於收貨門市，需彈跳視窗提示
                    if (allData.Rows[0]["BSART"].ToString() == "ZRE3" && allData.Rows[0]["發貨門市"].ToString() != storeCode)
                    {
                        MessageBox.Show("本門市非單據發貨門市(" + allData.Rows[0]["發貨門市"].ToString() + ")，請確認!");
                        tbGmo.SelectAll();
                        tbGmo.Focus();
                    }
                    else if (allData.Rows[0]["BSART"].ToString() != "ZRE3" && allData.Rows[0]["收貨門市"].ToString() != storeCode)
                    {
                        MessageBox.Show("本門市非單據收貨門市(" + allData.Rows[0]["收貨門市"].ToString() + ")，請確認!");
                        tbGmo.SelectAll();
                        tbGmo.Focus();
                    }
                    //ZUB4 物流收貨判斷
                    else if (allData.Rows[0]["BSART"].ToString() == "ZUB4")
                    {
                        sqlCmd =
                            SqlStatement.UC04.CheckProductShip(
                                GMOVEH.Rows[0]["EBELN"].ToString(), "AB"
                            );
                        DataTable dtProductShipCount = dbConn.GetDataTable(sqlCmd);

                        if (dtProductShipCount.Rows.Count <= 0)
                        {
                            MessageBox.Show("物流到貨作業未操作");
                            tbSN.SelectAll();
                            tbSN.Focus();
                            return;
                        }
                        else
                        {
                            EnableControl(true, 3);
                            tbSN.Focus();
                        }
                    }
                    else
                    {
                        EnableControl(true, 3);
                        tbSN.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 產品Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSN_TextChanged(object sender, EventArgs e)
        {
            AfterInput = ((TextBox)sender).Text.Length;
            aftStr = ((TextBox)sender).Text;

            if ((Math.Abs(AfterInput - BeforeInput) > 1 && AfterInput == 10) || //Greater or less
                AfterInput == tbSN.MaxLength || //MaxLength
                (AfterInput - BeforeInput == 0 && befStr != aftStr && befStr != null)) //Equal
            {
                SearchProduct();
                tbSN.SelectAll();
            }
        }
        private void tbSN_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchProduct();
                tbSN.SelectAll();
            }
            else
            {
                BeforeInput = ((TextBox)sender).Text.Length;
                befStr = ((TextBox)sender).Text;
            }
        }

        /// <summary>
        /// 查詢產品
        /// </summary>
        private void SearchProduct()
        {
            try
            {
                if (string.IsNullOrEmpty(tbSN.Text))
                    return;

                string tmpEan = tbSN.Text.ToUpper();

                if (string.IsNullOrEmpty(tmpEan))
                    return;

                bool loop = true;
                while (loop)
                {
                    if (tmpEan.StartsWith("0") && tmpEan.Length > 0)
                        tmpEan = tmpEan.Substring(1);
                    else
                    {
                        loop = false;
                        if (tmpEan.Length == 0) return;
                        else tbSN.Text = tmpEan;
                    }
                }

                if (allData == null || allData.Rows.Count == 0)
                {
                    MessageBox.Show("依據單號取得所有產品發生錯誤，請聯繫系統管理員!");
                    return;
                }

                //20170314 判斷退貨當下是否該商品為退貨品
                string sqlCmd = sql.GetCommand("GetProduct", new string[] { storeCode, tbSN.Text });
                DataTable tmpProdDT = dbConn.GetDataTable(sqlCmd);
                if (tmpProdDT.Rows.Count <= 0)
                {
                    laProductName.Text = "此商品無鋪貨";
                    laProductName.ForeColor = Color.Red;
                    laNeedQty.Text = string.Empty;
                    laRealQty.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    tbSN.SelectAll();
                    tbSN.Focus();
                    gmo = null;
                    return;
                }

                List<Gmovet> query = (from row in allData.AsEnumerable()
                                      where (row["商品條碼(EAN)"].ToString().Trim() == tbSN.Text.Trim() ||
                                             row["商品條碼(EAN12)"].ToString().Trim() == tbSN.Text.Trim()) //20160919需求變更:抓EAN11 or EAN12
                                      select new Gmovet()
                                      {
                                          BSART = row["BSART"].ToString(),
                                          EBELN = row["EBELN"].ToString(),
                                          EBELP = row["EBELP"].ToString(),
                                          BEDNR = row["BEDNR"].ToString(),
                                          MATNR = row["物料號碼"].ToString(),
                                          EAN11 = row["商品條碼(EAN)"].ToString(),
                                          EAN12 = row["商品條碼(EAN12)"].ToString(),
                                          MAKTX = row["品名"].ToString(),
                                          WERKS = row["收貨門市"].ToString(),
                                          RESWK = row["發貨門市"].ToString(),
                                          MENGE_R = string.IsNullOrEmpty(row["應到數量"].ToString()) ? "0" : decimal.Parse(row["應到數量"].ToString()).ToString("0.###"),
                                          //MENGE_M_TODAY = string.IsNullOrEmpty(row["本次收發量"].ToString()) ? "0" : decimal.Parse(row["本次收發量"].ToString()).ToString("0.###"),
                                          MENGE_P = row["採購單數量"].ToString(),
                                          MENGE_M = string.IsNullOrEmpty(row["實際量"].ToString()) ? "0" : decimal.Parse(row["實際量"].ToString()).ToString("0.###"),
                                          MEINS = row["採購單計量單位"].ToString(),
                                          MEMO = row["備註"].ToString(),
                                          BADAT = row["需求日期"].ToString(),
                                          VBELN = row["VBELN"].ToString(),
                                          CANEDIT = row["CANEDIT"].ToString() == "1",
                                          CANDO = row["CANDO"].ToString() == "1"
                                      }).ToList();

                if (query == null || query.Count == 0)
                {
                    laProductName.Text = "該單號下查無此商品代碼!";
                    laProductName.ForeColor = Color.Red;
                    laNeedQty.Text = string.Empty;
                    laRealQty.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    tbSN.SelectAll();
                    tbSN.Focus();
                    gmo = null;
                    return;
                }
                else if (query.Count > 1)
                {
                    laProductName.Text = "請使用分單後之單號進行作業!";
                    laProductName.ForeColor = Color.Red;
                    laNeedQty.Text = string.Empty;
                    laRealQty.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    tbSN.SelectAll();
                    tbSN.Focus();
                    gmo = null;
                    return;
                }
                else gmo = query.First();

                if ((gmo.BSART == "ZRP2" || gmo.BSART == "ZRP4" || gmo.BSART == "ZFG1") && !cbSin.Checked)
                {
                    //第二特收表
                    sqlCmd = sql.GetCommand("CheckSpecialIn2", new string[] { gmo.EBELN });
                    if (!(dbConn.GetDataTableRecData(sqlCmd) != "" &&
                        int.Parse(dbConn.GetDataTableRecData(sqlCmd)) > 0))
                    {
                        DateTime finalDate = DateTime.Parse(gmo.BADAT).AddDays(5);
                        if (DateTime.Today > finalDate)
                        {
                            laProductName.Text = "產品已過收貨期限，不可作業";
                            laProductName.ForeColor = Color.Red;
                            laNeedQty.Text = decimal.Parse(gmo.MENGE_R).ToString("0.###");
                            laRealQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                            //tbQty.Text = (decimal.Parse(gmo.MENGE_M_TODAY) + decimal.Parse(gmo.MENGE_M)).ToString("0.###");
                            tbQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                            tbQty.Enabled = false;
                            btnSave.Enabled = false;
                            tbSN.SelectAll();
                            tbSN.Focus();
                            return;
                        }
                    }
                }

                if (!gmo.CANEDIT)
                {
                    laProductName.Text = "產品已完成收貨，不可異動";
                    laProductName.ForeColor = Color.Red;
                    laNeedQty.Text = decimal.Parse(gmo.MENGE_R).ToString("0.###");
                    laRealQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                    //tbQty.Text = (decimal.Parse(gmo.MENGE_M_TODAY) + decimal.Parse(gmo.MENGE_M)).ToString("0.###");
                    tbQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    tbSN.SelectAll();
                    tbSN.Focus();
                    return;
                }

                if (!gmo.CANDO)
                {
                    laProductName.Text = "查無總倉對應單據，不可異動";
                    laProductName.ForeColor = Color.Red;
                    laNeedQty.Text = decimal.Parse(gmo.MENGE_R).ToString("0.###");
                    laRealQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                    //tbQty.Text = (decimal.Parse(gmo.MENGE_M_TODAY) + decimal.Parse(gmo.MENGE_M)).ToString("0.###");
                    tbQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    tbSN.SelectAll();
                    tbSN.Focus();
                    return;
                }

                //if (decimal.Parse(gmo.MENGE_M) == decimal.Parse(gmo.MENGE_R))
                //{
                //    laProductName.Text = "此產品收(發)數量已完成，請重新輸入!";
                //    laProductName.ForeColor = Color.Red;
                //    laNeedQty.Text = decimal.Parse(gmo.MENGE_R).ToString("0.###");
                //    laRealQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                //    //tbQty.Text = (decimal.Parse(gmo.MENGE_M_TODAY) + decimal.Parse(gmo.MENGE_M)).ToString("0.###");
                //    tbQty.Enabled = false;
                //    btnSave.Enabled = false;
                //    tbSN.SelectAll();
                //    tbSN.Focus();
                //    return;
                //}

                laEan11.Text = gmo.EAN11; //20160919需求變更:抓EAN11 or EAN12
                if (string.IsNullOrEmpty(laEan11.Text))
                {
                    laProductName.Text = "此代碼查無對應之EAN11值";
                    laProductName.ForeColor = Color.Red;
                    laNeedQty.Text = string.Empty;
                    laRealQty.Text = string.Empty;
                    tbQty.Text = string.Empty;
                    tbQty.Enabled = false;
                    btnSave.Enabled = false;
                    tbSN.SelectAll();
                    tbSN.Focus();
                    gmo = null;
                    return;
                }

                laProductName.Text = gmo.MAKTX;
                laProductName.ForeColor = Color.Black;
                laNeedQty.Text = decimal.Parse(gmo.MENGE_R).ToString("0.###");
                laRealQty.Text = decimal.Parse(gmo.MENGE_M).ToString("0.###");
                //tbQty.Text = string.Empty;
                tbQty.Text = string.Empty; //decimal.Parse(gmo.MENGE_M).ToString("0.###");
                tbQty.Enabled = true;
                btnSave.Enabled = true;
                tbQty.Focus();
                //eanReal = tbSN.Text;
                eanReal = laEan11.Text; //20160919需求變更:抓EAN11 or EAN12
            }
            catch (Exception ex)
            {
                MessageBox.Show("查詢產品發生錯誤: " + ex.ToString());
            }
        }

        /// <summary>
        /// 輸入數量後按Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !string.IsNullOrEmpty(tbQty.Text))
            {
                if (btnSave.Enabled)
                    btnSave_Click(null, null);
            }
        }

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (eanReal != tbSN.Text)
                if (eanReal != laEan11.Text) //20160919需求變更:抓EAN11 or EAN12
                {
                    MessageBox.Show("[商品條碼]已異動，請重新進行資料查詢!");
                    btnSave.Enabled = false;
                    return;
                }

                if (gmo == null)
                {
                    MessageBox.Show("取得[GMO]類別取得發生錯誤，請聯繫系統管理員!");
                    return;
                }

                //調撥數量僅可介於 0 <= qty <= 應到(發)量
                //decimal maxQty = decimal.Parse(gmo.MENGE_R) - decimal.Parse(gmo.MENGE_M);
                decimal maxQty = decimal.Parse(gmo.MENGE_R);
                int tmpInt = 0;
                try
                {
                    tmpInt = Int32.Parse(tbQty.Text);
                    if (tmpInt < 0 || tmpInt > maxQty)
                    {
                        MessageBox.Show("[收發量]需大於等於0且小於等於應到(發)量!\n 0 <= 收發量 <= " + maxQty.ToString());
                        tbQty.SelectAll();
                        tbQty.Focus();
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("[收發量]僅可輸入整數數值，請重新輸入!");
                    tbQty.SelectAll();
                    tbQty.Focus();
                    return;
                }

                DialogResult dialogResult = MessageBox.Show("您確定進行資料異動?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (dialogResult == DialogResult.Yes)
                {
                    string bendr = gmo.BEDNR;
                    string bsart = gmo.BSART;
                    string ebeln = gmo.EBELN;
                    string ebelp = gmo.EBELP;
                    string matnr = gmo.MATNR;
                    string ean11 = gmo.EAN11;
                    string werks = gmo.WERKS;
                    string reswk = gmo.RESWK;
                    string empCode = empCodeReal; //tbEmpCode.Text;
                    string menge_p = gmo.MENGE_P;
                    string meins_p = gmo.MEINS;
                    string vbeln = gmo.VBELN;

                    string sqlCmd = sql.GetCommand("GetInsertCommand", new string[] { bsart, ebeln, ebelp, matnr, ean11, werks, reswk, empCode, menge_p, meins_p, tbQty.Text, vbeln });
                    if (dbConn.ExecSql(sqlCmd) == 0)
                    {
                        MessageBox.Show("未更新任何項目，請聯繫管理人員");
                    }
                    else
                    {
                        laEan11.Text = string.Empty; //20160919需求變更:抓EAN11 or EAN12
                        tbSN.Text = string.Empty;
                        laProductName.Text = string.Empty;
                        laNeedQty.Text = string.Empty;
                        laRealQty.Text = string.Empty;
                        tbQty.Text = string.Empty;
                        tbQty.Enabled = false;
                        btnSave.Enabled = false;
                        tbSN.SelectAll();
                        tbSN.Focus();

                        DataTable tmpDT;
                        //先設定第二層
                        if (source.DataSource != null)
                        {
                            tmpDT = (source.DataSource as DataTable).Copy();
                            source2.DataSource = tmpDT;
                            dgList2.DataSource = source2;
                            source2.ResetBindings(false);
                            BindDataGridStyle2(tmpDT);
                            dgList2.SelectionBackColor = dgList.SelectionBackColor;
                            dgList2.SelectionForeColor = Color.Black;
                            dgList2.Select(0);
                        }

                        //在設定第一層
                        string sn = tbGmo.Text;
                        sqlCmd = sql.GetCommand("GetSNInfoByEan", new string[] { bsart, ebeln, bendr, ean11, vbeln });
                        tmpDT = dbConn.GetDataTable(sqlCmd);
                        string rank = tmpDT.Rows[0]["Rank"].ToString();
                        Color color = rank == "1" ? Color.White :
                                      rank == "2" ? Color.LightPink :
                                      rank == "3" ? Color.LightGreen : Color.Red;
                        source.DataSource = tmpDT;
                        dgList.DataSource = source;
                        source.ResetBindings(false);
                        BindDataGridStyle(tmpDT);
                        dgList.SelectionBackColor = color;
                        dgList.SelectionForeColor = Color.Black;
                        dgList.Select(0);

                        MessageBox.Show("更新成功!");
                        gmo = null;
                        GmoSearch(false);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("儲存命令發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 完整顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(key) || key.Split(';').Length != 4)
                {
                    MessageBox.Show("取得Key失敗，請重新查詢單號!");
                    return;
                }

                bool needAdd = true;
                List<Control> rmc = new List<Control>();

                UC04_1 uc = null;
                if (listKey.Count > 0) uc = new UC04_1(listKey, ref dbConn);
                else uc = new UC04_1(key.Split(';')[0], key.Split(';')[1], key.Split(';')[2], key.Split(';')[3], ref dbConn);

                foreach (Control c in this.Parent.Controls)
                {
                    if (c.GetType() == typeof(PictureBox))
                        c.Visible = false;
                    else if (c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = false;
                    else if (c.GetType() == typeof(UC04))
                        c.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        this.Parent.Controls.Remove(c);

                if (needAdd)
                    this.Parent.Controls.Add(uc);

                this.Parent.Parent.Text = formName + " -- " + "驗收/發貨(資料查詢)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "應到數量":
                    case "本次收發量":
                    case "實際量":
                        tb.Format = "0.###";
                        break;
                }

                if (col.ColumnName.ToUpper() == "RANK" || col.ColumnName.ToUpper() == "CANEDIT" || col.ColumnName.ToUpper() == "CANDO") tb.Width = 0;
                else tb.Width = 150;

                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }

        private void BindDataGridStyle2(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "應到數量":
                    case "本次收發量":
                    case "實際量":
                        tb.Format = "0.###";
                        break;
                }

                if (col.ColumnName.ToUpper() == "RANK" || col.ColumnName.ToUpper() == "CANEDIT" || col.ColumnName.ToUpper() == "CANDO") tb.Width = 0;
                else tb.Width = 150;

                dts.GridColumnStyles.Add(tb);
            }

            dgList2.TableStyles.Clear();
            dgList2.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
