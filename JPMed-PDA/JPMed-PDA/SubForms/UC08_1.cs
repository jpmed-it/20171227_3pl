﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;

namespace JPMed_PDA.SubForms
{
    public partial class UC08_1 : UserControl
    {
        string formName = "倉管作業系統 -- 直退廠商作業";
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        SQLCreator sql = new SQLCreator("UC08_1");
        BindingSource source = new BindingSource();

        public UC08_1(string _bender , ref DBConnection dbConn)
        {
            InitializeComponent();

            try
            {
                string sqlCmd = sql.GetCommand("GetSelectCommand", new string[] { _bender });
                DataTable tmpDT = dbConn.GetDataTable(sqlCmd);
                source.DataSource = tmpDT;
                dgList.DataSource = source;
                source.ResetBindings(false);
                BindDataGridStyle(tmpDT);

                laCount.Text = tmpDT.Rows.Count.ToString();
                laQty.Text = tmpDT.AsEnumerable().Sum(r => Decimal.Parse(r["退貨量"].ToString())).ToString("0.###");
            }
            catch(Exception ex)
            {
                MessageBox.Show("查詢資料發生錯誤: " + ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("您確定關閉:[直退場商(資料查詢)]作業?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == typeof(UC08).Name)
                    {
                        control.Visible = true;

                        foreach (Control controlitem in control.Controls)
                        {
                            if (controlitem.Name == "tbSN")
                            {
                                controlitem.Focus();
                                ((TextBox)controlitem).SelectAll();
                                break;
                            }
                        }
                        break;
                    }
                }

                foreach (Control control in this.Parent.Controls)
                {
                    if (control.Name == this.Name)
                    {
                        this.Parent.Controls.Remove(control);
                        break;
                    }
                }
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                //DataGridColumnStyle colStyle = new DataGridTextBoxColumn();
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                switch (col.ColumnName.ToUpper())
                {
                    case "退貨量":
                    case "庫存量":
                        tb.Format = "0.###";
                        break;
                }

                if (col.ColumnName == "退貨確認") tb.Width = 80;
                else tb.Width = 150;
                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
