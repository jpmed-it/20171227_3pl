﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using JPMed_PDA.SqlStatement;
using JPMed_PDA.Utility;
using JPMed_PDA.Entity;

namespace JPMed_PDA.SubForms
{
    /// <summary>
    /// 自送調出確認 UC11
    /// </summary>
    public partial class UC11 : UserControl
    {
        string formName = "倉管作業系統";
        string empCodeReal = string.Empty;
        string eanReal = string.Empty;
        static string errMsg = string.Empty;
        static string storeCode = new ConfigSettings().Werks;
        DBConnection dbConn;
        BindingSource source = new BindingSource();
        DataTable defaultDT = null;
        string snNoForStore = string.Empty; //目前序號歸屬門市, 發貨門市
        string prevBENDR = string.Empty; //前次輸入的單號
        //最後一次按下Enter的時間
        DateTime enterClickTime = new DateTime();

        public UC11()
        {
            InitializeComponent();
            IntitalEvent();
            DefaultControl();
            IntitalControl();
        }

        /// <summary>
        /// 事件初始化
        /// </summary>
        private void IntitalEvent()
        {
            tbEmpCode.TextChanged += new EventHandler(tbEmpCode_TextChanged);
            tbEmpCode.KeyUp += new KeyEventHandler(tbEmpCode_KeyUp);
            btnExit.Click += new EventHandler(btnExit_Click);
            btnShowAll.Click += new EventHandler(btnShowAll_Click);
            btnSave.Click += new EventHandler(btnSave_Click);
        }

        /// <summary>
        /// 清空控制項
        /// </summary>
        private void DefaultControl()
        {
            laSCode.Text = storeCode;
            laSName.Text = string.Empty;

            tbEmpCode.Text = string.Empty;
            laEmpName.Text = string.Empty;

            btnShowAll.Enabled = false;
            btnSave.Enabled = false;
            tbBENDR.Enabled = false;

            dgList.DataSource = defaultDT;
        }

        /// <summary>
        /// 初始化資料
        /// </summary>
        private void IntitalControl()
        {
            try
            {
                dbConn = new DBConnection();

                if (!dbConn.TestConnect(out errMsg))
                {
                    while (true)
                    {
                        MessageBox.Show("連結資料庫發生錯誤: " + errMsg);

                        DialogResult dialogResult = MessageBox.Show("請問是否嘗試重新連線?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbConn = new DBConnection();
                            dbConn.ReloadSettings();
                            if (dbConn.TestConnect(out errMsg))
                                break;
                        }
                        else throw new Exception("無法連接資料庫");
                    }
                }

                //取得店家資訊
                string sqlCmd = SqlStatement.UC09.GetStoreNameByCode(storeCode);
                laSName.Text = dbConn.GetDataTableRecData(sqlCmd);
                if (string.IsNullOrEmpty(laSName.Text) || laSName.Text == DBNull.Value.ToString())
                {
                    EnableControl(false, 0);
                    MessageBox.Show("查無分店資訊!");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("系統執行發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 鎖定操作功能
        /// </summary>
        private void EnableControl(bool _enbleControl, int step)
        {
            switch (step)
            {
                case 0: //員工編號、員工姓名
                    tbEmpCode.Text = string.Empty;
                    tbEmpCode.Enabled = false;
                    break;

                case 1: //EmpCode Error Use
                    btnShowAll.Enabled = _enbleControl;
                    tbBENDR.Enabled = _enbleControl;
                    btnSave.Enabled = false;
                    Common.SelectAndFocus(tbBENDR);
                    break;
            }
        }

        /// <summary>
        /// 離開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult =
                MessageBox.Show("您確定關閉功能:[自送調出作業]?", "警告",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                this.Parent.Parent.Text = formName;
                Control rmc = null;
                foreach (Control c in this.Parent.Controls)
                {
                    if (c.Name == this.Name)
                        rmc = c;
                    else if (c.Name == "pb1" || c.Name == "laTimeStr" || c.Name == "laVersion")
                        c.Visible = true;
                }

                if (rmc != null)
                    this.Parent.Controls.Remove(rmc);
            }
        }

        #region 使用者名稱相關
        /// <summary>
        /// 取得使用者名稱
        /// </summary>
        /// <param name="_empCode"></param>
        private void GetEmpName(string _empCode)
        {
            try
            {
                tbEmpCode.Text = tbEmpCode.Text.ToUpper();
                string empName = string.Empty;
                string sqlCmd = SqlStatement.UC11.GetEmployeeNameByCode(tbEmpCode.Text);
                empName = dbConn.GetDataTableRecData(sqlCmd);

                if (string.IsNullOrEmpty(empName) || empName == DBNull.Value.ToString())
                {
                    EnableControl(false, 1);
                    tbEmpCode.Focus();
                    tbEmpCode.SelectAll();
                    laEmpName.Text = "查無此編號";
                    laEmpName.ForeColor = Color.Red;
                    dgList.DataSource = defaultDT;
                }
                else
                {
                    laEmpName.Text = empName;
                    empCodeReal = tbEmpCode.Text;
                    //tbEmpCode.Enabled = false;
                    laEmpName.ForeColor = Color.Black;
                    EnableControl(true, 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("取得使用者名稱發生錯誤: " + ex.Message);
            }
        }

        /// <summary>
        /// 使用者代碼change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmpCode_TextChanged(object sender, EventArgs e)
        {
            if (tbEmpCode.Text.Length == 6)
            {
                tbEmpCode.SelectAll();
                GetEmpName(tbEmpCode.Text);
            }
        }

        /// <summary>
        /// 使用者代碼按下Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tbEmpCode_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    tbEmpCode.SelectAll();
            //    GetEmpName(tbEmpCode.Text);
            //}
        }
        #endregion 使用者名稱相關

        /// <summary>
        /// 存檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //檢查STO2STO單號
                if (tbBENDR.Text.Length == 0)
                { MessageBox.Show("請輸入單號"); return; }
                string sqlCmd = SqlStatement.UC11.GetGmovehInfo(tbBENDR.Text);
                DataTable gmovehDT = dbConn.GetDataTable(sqlCmd);

                string strBSART = string.Empty;

                if (gmovehDT.Rows.Count > 0)
                {
                    strBSART = gmovehDT.Rows[0]["BSART"].ToString();

                    if (strBSART == "ZUB1")
                    {
                        sqlCmd = SqlStatement.UC11.GetSTO2STOByBednrOrEbeln(storeCode, tbBENDR.Text);
                    }
                    else if (strBSART == "ZUB2")
                    {
                        sqlCmd = SqlStatement.UC11.GetSTO2TDC1ByBednrOrEbeln(storeCode, tbBENDR.Text);
                    }
                    else if (strBSART == "ZUR1")
                    {
                        sqlCmd = SqlStatement.UC11.GetRE2TDC1ByBednrOrEbeln(storeCode, tbBENDR.Text);
                    }
                    else if (strBSART == "ZRE3" || strBSART == "ZUB3")
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號不允許");
                        return;
                    }
                    else
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("查無單號");
                        return;
                    }
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("查無單號");
                    return;
                }

                DataTable dt = dbConn.GetDataTable(sqlCmd);

                if (dt.Rows.Count > 0)
                {
                    if (strBSART == "ZUB3" && dt.Rows[0]["WMFLG"].ToString() == "X")
                    {
                        Common.SelectAndFocus(tbBENDR);
                        MessageBox.Show("單號不允許");
                        return;
                    }

                    if (dt.Rows[0]["EBELN"].ToString() == "" || dt.Rows[0]["BSART"].ToString() == "ZRE3")
                    {
                        Common.SelectAndFocus(tbBENDR);
                        MessageBox.Show("單號不允許");
                        return;
                    }

                    //檢查單號是否重複
                    sqlCmd = SqlStatement.UC11.GetGMOVET(dt.Rows[0]["EBELN"].ToString());

                    DataTable gmovetDT = dbConn.GetDataTable(sqlCmd);

                    if (gmovetDT.Rows.Count > 0)
                    {
                        Common.SelectAndFocus(tbBENDR);
                        MessageBox.Show("單號重複");
                        return;
                    }
                }
                else
                {
                    Common.SelectAndFocus(tbBENDR);
                    MessageBox.Show("查無單號");
                    return;
                }

                StringBuilder sb = new StringBuilder();

                string currBSART = dt.Rows[0]["BSART"].ToString();
                string currBEDNR = dt.Rows[0]["BEDNR"].ToString();
                string currEBELN = dt.Rows[0]["EBELN"].ToString();
                string currEAN11 = dt.Rows[0]["EAN11"].ToString();
                string currVBELN = dt.Rows[0]["EBELN"].ToString();


                sqlCmd = SqlStatement.UC11.GetSNInfo(currBSART, currEBELN, currBEDNR, currEAN11, currVBELN);
                DataTable prodTable = dbConn.GetDataTable(sqlCmd);

                List<Gmovet> GmovetList = new List<Gmovet>();

                for (int i = 0; i < prodTable.Rows.Count; i++)
                {
                    Gmovet objGmovet = new Gmovet();

                    objGmovet.BSART = prodTable.Rows[i]["BSART"].ToString();
                    objGmovet.EBELN = prodTable.Rows[i]["EBELN"].ToString();
                    objGmovet.EBELP = prodTable.Rows[i]["EBELP"].ToString();
                    objGmovet.MATNR = prodTable.Rows[i]["MATNR"].ToString();
                    objGmovet.EAN11 = prodTable.Rows[i]["EAN11"].ToString();
                    objGmovet.WERKS = prodTable.Rows[i]["WERKS"].ToString();
                    objGmovet.MENGE_P = prodTable.Rows[i]["MENGE_P"].ToString();
                    objGmovet.MEINS = prodTable.Rows[i]["MEINS_P"].ToString();
                    objGmovet.MENGE_M = prodTable.Rows[i]["MENGE_M"].ToString();
                    objGmovet.PROCD = string.Empty;
                    objGmovet.BEDNR = currBEDNR;
                    if (currBSART == "ZUB1" || currBSART == "ZUB2" || currBSART == "ZUR1")
                    {
                        objGmovet.VBELN = currEBELN;
                    }
                    GmovetList.Add(objGmovet);
                }

                string CRDAT = DateTime.Now.ToString("yyyyMMdd");
                string CHTME = DateTime.Now.ToString("HHmmss");
                sqlCmd = SqlStatement.UC11.InsertGMOVET(GmovetList, empCodeReal, CRDAT, CHTME);
                dbConn.ExecSql(sqlCmd);

                //更新下方DataGrid
                DataTable BENDRDataTable = new DataTable();
                BENDRDataTable.Columns.Add(new DataColumn("單號"));
                BENDRDataTable.Rows.Add(currBEDNR);

                dgList.DataSource = BENDRDataTable;
                BindDataGridStyle(BENDRDataTable);

                BENDRList.Add(currBEDNR);

                lbBoxCount.Text = Convert.ToString(Convert.ToInt32(lbBoxCount.Text) + 1);
                Common.SelectAndFocus(tbBENDR);

            }
            catch (Exception ex)
            {
                MessageBox.Show("新增自送調出資料錯誤: " + ex.Message);
            }
        }

        List<string> BENDRList = new List<string>();


        /// <summary>
        /// 完整顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            bool needAdd = true;
            List<Control> rmc = new List<Control>();

            UC11_1 uc = new UC11_1(BENDRList, ref dbConn);

            foreach (Control c in this.Parent.Controls)
            {
                if (c.GetType() == typeof(PictureBox))
                    c.Visible = false;
                else if (c.Name == "laTimeStr" || c.Name == "laVersion")
                    c.Visible = false;
                else if (c.GetType() == typeof(UC11))
                    c.Visible = false;
                else if (c.Name == uc.Name)
                {
                    c.Visible = true;
                    needAdd = false;
                }
                else
                    rmc.Add(c);
            }

            if (rmc.Count > 0)
                foreach (Control c in rmc)
                    this.Parent.Controls.Remove(c);

            if (needAdd)
                this.Parent.Controls.Add(uc);

            this.Parent.Parent.Text = formName + " -- " + "自送調出作業(資料查詢)";
        }

        private void tbBENDR_KeyUp(object sender, KeyEventArgs e)
        {
            if (prevBENDR != tbBENDR.Text)
            {
                btnSave.Enabled = false;
            }

            prevBENDR = tbBENDR.Text;

            if (e.KeyCode == Keys.Enter)
            {
                //按下Enter後間隔一秒後再按下Enter才有反應，避免重複按下
                if (enterClickTime > DateTime.Now)
                    return;
                enterClickTime = DateTime.Now.Add(new TimeSpan(0, 0, 1));

                if (tbBENDR.Text.Length == 0)
                { MessageBox.Show("請輸入單號"); return; }

                string sqlCmd = SqlStatement.UC11.GetGmovehInfo(tbBENDR.Text);
                DataTable gmovehDT = dbConn.GetDataTable(sqlCmd);

                string strBSART = string.Empty;

                if (gmovehDT.Rows.Count > 0)
                {
                    strBSART = gmovehDT.Rows[0]["BSART"].ToString();

                    if (strBSART == "ZUB1")
                    {
                        sqlCmd = SqlStatement.UC11.GetSTO2STOByBednrOrEbeln(storeCode, tbBENDR.Text);
                    }
                    else if (strBSART == "ZUB2")
                    {
                        sqlCmd = SqlStatement.UC11.GetSTO2TDC1ByBednrOrEbeln(storeCode, tbBENDR.Text);
                    }
                    else if (strBSART == "ZUR1")
                    {
                        sqlCmd = SqlStatement.UC11.GetRE2TDC1ByBednrOrEbeln(storeCode, tbBENDR.Text);
                    }
                    else if (strBSART == "ZRE3" || strBSART == "ZUB3")
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號不允許");
                        return;
                    }
                    else
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("查無單號");
                        return;
                    }
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("查無單號");
                    return;
                }

                DataTable dt = dbConn.GetDataTable(sqlCmd);
                if (dt.Rows.Count > 0)
                {
                    if (strBSART == "ZUB3" && dt.Rows[0]["WMFLG"].ToString() == "X")
                    {
                        Common.SelectAndFocus(tbBENDR);
                        MessageBox.Show("單號不允許");
                        return;
                    }

                    if (dt.Rows[0]["EBELN"].ToString() == "" || dt.Rows[0]["BSART"].ToString() == "ZRE3")
                    {
                        Common.SelectAndFocus(tbBENDR);
                        MessageBox.Show("單號不允許");
                        return;
                    }

                    sqlCmd = SqlStatement.UC11.GetGMOVET(dt.Rows[0]["EBELN"].ToString());

                    DataTable gmovetDT = dbConn.GetDataTable(sqlCmd);

                    if (gmovetDT.Rows.Count > 0)
                    {
                        tbBENDR.SelectAll();
                        MessageBox.Show("單號重複");
                        return;
                    }
                    btnSave.Enabled = true;
                }
                else
                {
                    tbBENDR.SelectAll();
                    MessageBox.Show("查無單號");
                    return;
                }
            }
        }

        private void BindDataGridStyle(DataTable dt)
        {
            DataGridTableStyle dts = new DataGridTableStyle();
            dts.MappingName = dt.TableName;

            foreach (DataColumn col in dt.Columns)
            {
                DataGridTextBoxColumn tb = new DataGridTextBoxColumn();
                tb.MappingName = col.ColumnName;
                tb.HeaderText = col.ColumnName;

                tb.Width = 300;

                dts.GridColumnStyles.Add(tb);
            }

            dgList.TableStyles.Clear();
            dgList.TableStyles.Add(dts);
            dts = null;
            GC.Collect();
        }
    }
}
