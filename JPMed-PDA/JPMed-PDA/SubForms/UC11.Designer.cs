﻿namespace JPMed_PDA.SubForms
{
    partial class UC11
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.laEmpName = new System.Windows.Forms.Label();
            this.tbEmpCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.laSName = new System.Windows.Forms.Label();
            this.laSCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.laKey = new System.Windows.Forms.Label();
            this.tbBENDR = new System.Windows.Forms.TextBox();
            this.lbBoxCount = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgList = new System.Windows.Forms.DataGrid();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular);
            this.btnSave.Location = new System.Drawing.Point(243, 156);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(72, 27);
            this.btnSave.TabIndex = 140;
            this.btnSave.Text = "確認";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(10, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.Text = "總箱數:";
            // 
            // laEmpName
            // 
            this.laEmpName.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Underline);
            this.laEmpName.Location = new System.Drawing.Point(179, 60);
            this.laEmpName.Name = "laEmpName";
            this.laEmpName.Size = new System.Drawing.Size(123, 16);
            this.laEmpName.Text = "王曉明";
            // 
            // tbEmpCode
            // 
            this.tbEmpCode.Font = new System.Drawing.Font("PMingLiU", 12F, System.Drawing.FontStyle.Regular);
            this.tbEmpCode.Location = new System.Drawing.Point(65, 57);
            this.tbEmpCode.MaxLength = 6;
            this.tbEmpCode.Name = "tbEmpCode";
            this.tbEmpCode.Size = new System.Drawing.Size(108, 26);
            this.tbEmpCode.TabIndex = 136;
            this.tbEmpCode.Text = "A1234567890";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(10, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.Text = "員編：";
            // 
            // btnShowAll
            // 
            this.btnShowAll.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnShowAll.Location = new System.Drawing.Point(243, 89);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(72, 26);
            this.btnShowAll.TabIndex = 134;
            this.btnShowAll.Text = "完整";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(10, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 24);
            this.label2.Text = "單號:";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.btnExit.Location = new System.Drawing.Point(243, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 24);
            this.btnExit.TabIndex = 133;
            this.btnExit.Text = "離開";
            // 
            // laSName
            // 
            this.laSName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSName.Location = new System.Drawing.Point(65, 32);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(149, 20);
            this.laSName.Text = "士林門市";
            // 
            // laSCode
            // 
            this.laSCode.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laSCode.Location = new System.Drawing.Point(10, 32);
            this.laSCode.Name = "laSCode";
            this.laSCode.Size = new System.Drawing.Size(49, 20);
            this.laSCode.Text = "T001";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 20);
            this.label1.Text = "所在門市：";
            // 
            // laKey
            // 
            this.laKey.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Underline);
            this.laKey.ForeColor = System.Drawing.Color.Black;
            this.laKey.Location = new System.Drawing.Point(18, 388);
            this.laKey.Name = "laKey";
            this.laKey.Size = new System.Drawing.Size(129, 20);
            this.laKey.Visible = false;
            // 
            // tbBENDR
            // 
            this.tbBENDR.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.tbBENDR.Location = new System.Drawing.Point(65, 91);
            this.tbBENDR.MaxLength = 22;
            this.tbBENDR.Name = "tbBENDR";
            this.tbBENDR.Size = new System.Drawing.Size(172, 26);
            this.tbBENDR.TabIndex = 153;
            this.tbBENDR.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbBENDR_KeyUp);
            // 
            // lbBoxCount
            // 
            this.lbBoxCount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lbBoxCount.Location = new System.Drawing.Point(82, 138);
            this.lbBoxCount.Name = "lbBoxCount";
            this.lbBoxCount.Size = new System.Drawing.Size(65, 20);
            this.lbBoxCount.Text = "0";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(10, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 20);
            this.label3.Text = "收貨明細:";
            // 
            // dgList
            // 
            this.dgList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgList.Location = new System.Drawing.Point(8, 213);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(300, 170);
            this.dgList.TabIndex = 139;
            // 
            // UC11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Khaki;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbBoxCount);
            this.Controls.Add(this.tbBENDR);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.laEmpName);
            this.Controls.Add(this.tbEmpCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnShowAll);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laSCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laKey);
            this.Name = "UC11";
            this.Size = new System.Drawing.Size(318, 425);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laEmpName;
        private System.Windows.Forms.TextBox tbEmpCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laSCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laKey;
        private System.Windows.Forms.TextBox tbBENDR;
        private System.Windows.Forms.Label lbBoxCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGrid dgList;
    }
}
