﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using JPMed_PDA.Utility;
using System.Runtime.InteropServices;
using System.Net;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;

namespace JPMed_PDA
{
    public partial class MainForm : Form
    {
        bool quit = false;
        string formName = "倉管作業系統";
        static string storeCode = new ConfigSettings().Werks;
        private System.Windows.Forms.Timer _tmrTicker = null;

        SyncTime syncTime;
        Thread task;

        public MainForm(bool needSyncTime)
        {
            InitializeComponent();
            //this.ControlBox = false;

            if (storeCode == "TDC1")
            {
                menuItem3.Enabled = false;
                //menuItem4.Enabled = false;
                menuItem5.Enabled = true;
                menuItem6.Enabled = false;
                menuItem7.Enabled = false;
                menuItem8.Enabled = false;
                menuItem9.Enabled = false;
            }
            else if (storeCode == "T034")
            {
                menuItem3.Enabled = true;
                //menuItem4.Enabled = true;
                menuItem5.Enabled = true;
                menuItem6.Enabled = false;
                menuItem7.Enabled = false;
                menuItem8.Enabled = false;
                menuItem9.Enabled = true;
            }

            string osversion = Environment.OSVersion.ToString();
            if (needSyncTime && osversion.Contains("Windows CE"))
            {
                syncTime = new SyncTime();
                task = new Thread(syncTime.DoWork);
                task.Start();

                laTimeStr.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + " 星期" + GetWeekName(DateTime.Now.DayOfWeek);

                _tmrTicker = new System.Windows.Forms.Timer();
                _tmrTicker.Interval = 1000;
                _tmrTicker.Enabled = true;
                _tmrTicker.Tick += new EventHandler(_tmrTicker_Tick);
            }

            laVersion.Text = "ver: " + System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();
        }

        void _tmrTicker_Tick(object sender, EventArgs e)
        {
            laTimeStr.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + " 星期" + GetWeekName(DateTime.Now.DayOfWeek);
        }

        private static string GetWeekName(DayOfWeek _dayOfWeek)
        {
            switch (_dayOfWeek)
            {
                case DayOfWeek.Monday: return "一";
                case DayOfWeek.Tuesday: return "二";
                case DayOfWeek.Wednesday: return "三";
                case DayOfWeek.Thursday: return "四";
                case DayOfWeek.Friday: return "五";
                case DayOfWeek.Saturday: return "六";
                case DayOfWeek.Sunday: return "日";
                default: return string.Empty;
            }
        }

        #region Menu

        #region 單據建立
        #region 1.調撥門市作業 UC05
        private void menuItem6_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.UC05 uc = new JPMed_PDA.SubForms.UC05();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbEmpCode") { c.Focus(); break; }

                this.Text = formName + " -- " + "調撥門市作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 2.總部通知調撥回倉品 UC06
        private void menuItem7_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.UC06 uc = new JPMed_PDA.SubForms.UC06();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbEmpCode") { c.Focus(); break; }

                this.Text = formName + " -- " + "總部通知調撥回倉品";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 3.門市退貨(有問題貨品)入退貨倉 UC07
        private void menuItem8_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.UC07 uc = new JPMed_PDA.SubForms.UC07();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbEmpCode") { c.Focus(); break; }

                this.Text = formName + " -- " + "門市退貨(有問題貨品)入退貨倉";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 4.直退廠商作業 UC08
        private void menuItem9_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.UC08 uc = new JPMed_PDA.SubForms.UC08();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbEmpCode") { c.Focus(); break; }

                this.Text = formName + " -- " + "直退廠商作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #endregion 單據建立

        #region 調撥作業
        List<Control> controlList = new List<Control>();

        private void AddSubPanel(Control uc, bool needAdd, string focusName)
        {
            controlList = new List<Control>();
            foreach (Control c in panel0.Controls)
            {
                if (c.Name == pb1.Name)
                    pb1.Visible = false;
                else if (c.Name == laTimeStr.Name)
                    laTimeStr.Visible = false;
                else if (c.Name == laVersion.Name)
                    laVersion.Visible = false;
                else if (c.Name == uc.Name)
                {
                    c.Visible = true;
                    needAdd = false;
                }
                else
                    controlList.Add(c);
            }

            if (controlList.Count > 0)
                foreach (Control c in controlList)
                    panel0.Controls.Remove(c);

            if (needAdd)
                panel0.Controls.Add(uc);

            foreach (Control c in panel0.Controls[3].Controls)
                if (c.Name == focusName) { c.Focus(); break; }
        }

        #region 1.物流收貨作業 UC09
        private void menuItem4_Click(object sender, EventArgs e)
        {
            try
            {
                AddSubPanel(new JPMed_PDA.SubForms.UC09(), true, "tbEmpCode");

                this.Text = formName + " -- " + "物流收貨作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 2.物流到貨作業 UC10
        private void menuItem13_Click(object sender, EventArgs e)
        {
            try
            {
                AddSubPanel(new JPMed_PDA.SubForms.UC10(), true, "tbEmpCode");

                this.Text = formName + " -- " + "物流到貨作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 3.自送調出確認 UC11
        private void menuItem15_Click(object sender, EventArgs e)
        {
            try
            {
                AddSubPanel(new JPMed_PDA.SubForms.UC11(), true, "tbEmpCode");

                this.Text = formName + " -- " + "自送調出作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region 4.驗收/退貨作業 UC04
        private void menuItem5_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.UC04 uc = new JPMed_PDA.SubForms.UC04();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbEmpCode") { c.Focus(); break; }

                this.Text = formName + " -- " + "驗收/退貨作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #endregion

        #region 其他

        #region 1.行動補標作業 LABELS
        private void menuItem10_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.LABELS uc = new JPMed_PDA.SubForms.LABELS();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbEmpCode") { c.Focus(); break; }

                this.Text = formName + " -- " + "行動補標作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 2.貨價卡列印(限桌機使用) 外部程式 PrintLabel
        //20170316 調整開啟程式名稱
        private void menuItem11_Click(object sender, EventArgs e)
        {
            try
            {
                string programPath = System.IO.Directory.GetCurrentDirectory();
                string FileName = "PrintLabel.exe";
                ProcessStartInfo processInfo = new ProcessStartInfo();

                Process.Start(FileName, programPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("呼叫貨價卡列印發生錯誤: " + ex.Message);
            }
        }
        #endregion

        #region 3.查價作業 UC02
        private void menuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                bool needAdd = true;
                List<Control> rmc = new List<Control>();
                JPMed_PDA.SubForms.UC02 uc = new JPMed_PDA.SubForms.UC02();
                foreach (Control c in panel0.Controls)
                {
                    if (c.Name == pb1.Name)
                        pb1.Visible = false;
                    else if (c.Name == laTimeStr.Name)
                        laTimeStr.Visible = false;
                    else if (c.Name == laVersion.Name)
                        laVersion.Visible = false;
                    else if (c.Name == uc.Name)
                    {
                        c.Visible = true;
                        needAdd = false;
                    }
                    else
                        rmc.Add(c);
                }

                if (rmc.Count > 0)
                    foreach (Control c in rmc)
                        panel0.Controls.Remove(c);

                if (needAdd)
                    panel0.Controls.Add(uc);

                foreach (Control c in panel0.Controls[1].Controls)
                    if (c.Name == "tbSN") { c.Focus(); break; }

                this.Text = formName + " -- " + "查價作業";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #endregion

        #region 離開
        private void menuItem2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("您確定離開系統", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes)
            {
                quit = true;
                this.Close();
            }
        }
        #endregion 離開

        #endregion Menu

        private void MainForm_Closing(object sender, CancelEventArgs e)
        {
            if (!quit)
            {
                DialogResult result = MessageBox.Show("您確定離開系統", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (result != DialogResult.Yes)
                    e.Cancel = true;
                else if (task != null)
                {
                    task.Abort();
                }
            }
        }

        private void MainForm_Closed(object sender, EventArgs e)
        {
            if (task != null)
            {
                task.Abort();
            }
        }
    }

    public class SyncTime
    {
        private volatile bool _shouldStop;
        SyncDatabaseTime syncTimer = new SyncDatabaseTime();

        public void DoWork()
        {
            while (!_shouldStop)
            {
                //同步DB時間
                string dbTime = string.Empty;
                if (!syncTimer.GetDatabaseTime(ref dbTime)) MessageBox.Show("同步時間發生錯誤: " + dbTime + "\n請確認目前系統時間是否正確!");
                else SyncDatabaseTime.SetSystemDateTime(DateTime.Parse(dbTime));

                //等待10秒
                Thread.Sleep(10000);
            }
        }

        public void RequestStop()
        {
            _shouldStop = true;
        }
    }
}